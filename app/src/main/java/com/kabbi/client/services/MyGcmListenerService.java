package com.kabbi.client.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.kabbi.client.R;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.PreOrderActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.UpdateStateEvent;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE;


public class MyGcmListenerService extends GcmListenerService {

    private boolean isSendingAllowed = true;
    private Intent sendData;
    private int requestCode;


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d("Push", "Message: " + data.toString());
        if (from != null && from.equals(AppParams.PUSH_SENDER_ID)) {
            JSONObject contentData = null;
            String commandData = null;
            try {
                commandData = data.getString("command");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            try {
                contentData = new JSONObject(data.getString("command_params"));
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            if (commandData != null) {
                sendData = new Intent(this, MainActivity.class);
                requestCode = 0;
                switch (commandData) {
                    case "new_balance":
                        sendData.putExtra("command", "new_balance");
                        getProfileNotificationInfo(contentData, data);
                        break;
                    case "bulk_notify":
                        sendData.putExtra("command", "bulk_notify");
                        getBalkNotification(data);
                        break;
                    case "update_order":
                        try {
                            if (contentData != null && contentData.getString("status") != null
                                    && contentData.getString("status").equals("pre_order")) {
                                sendData = new Intent(this, PreOrderActivity.class);
                                requestCode = 1;
                            }
                            sendData.putExtra("command", "update_order");
                            getOrderNotificationInfo(contentData, data);
                            break;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    }

    /**
     * Notification for update profile
     *
     * @param contentData contains profile information
     * @param data all data push
     */
    private void getProfileNotificationInfo(JSONObject contentData, Bundle data) {
        try {
            Profile profile = Profile.getProfile();
            PaymentType.addPersonalPayment(profile.getPhone(), contentData.getString("balance"),
                    contentData.getString("currency"));
            profile.setBalanceValue(contentData.getString("balance"));
            profile.setBonusValue(contentData.getString("bonus_balance"));
            profile.setBalanceCurrency(contentData.getString("bonus_balance"));
            profile.save();
        } catch (Exception e) {
            isSendingAllowed = false;
        }
        if (isSendingAllowed) {
            String contentText = data.getString("message");
            sendNotification(getString(R.string.app_name),
                    contentText, sendData.getStringExtra("status"));
        }
    }

    /**
     * Notification for show custom information
     * @param data all data push
     */
    private void getBalkNotification(Bundle data) {
            String contentText = data.getString("message");
            String contentTitle = data.getString("tickerText");
            sendNotification(contentTitle,
                    contentText, "bulk_message");
    }

    /**
     * Notification for update order
     * @param contentData contains order information
     * @param data all push data
     */
    private void getOrderNotificationInfo(JSONObject contentData, Bundle data) {
        try {
            sendData.putExtra("order_id", contentData.getString("order_id"));
            sendData.putExtra("status_label", contentData.getString("status_label"));
            sendData.putExtra("status", contentData.getString("status"));
        } catch (Exception e) {
            isSendingAllowed = false;
        }
        if (isSendingAllowed) {
            String message = data.getString("message", "");
            String contentText = !message.isEmpty() ?
                    message : sendData.getStringExtra("status_label");
            sendNotification(getString(R.string.app_name),
                    contentText, sendData.getStringExtra("status"));
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param title GCM title received.
     * @param message GCM message received.
     * @param status notification status. Use in order pushes for update order state
     */
    private void sendNotification(String title, String message, String status) {
        sendData.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        sendData.putExtra("type", "push");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, sendData,
                PendingIntent.FLAG_CANCEL_CURRENT);

        if (status != null && status.equals("car_assigned"))
            EventBus.getDefault().post(new UpdateStateEvent(sendData));

        String channelId = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        notificationBuilder.setSmallIcon(R.drawable.push)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle(notificationBuilder).bigText(message))
                .setContentText(message)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setAutoCancel(true)
                .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                .setLights(Color.RED, 1, 1)
                .setContentIntent(pendingIntent);

        if (status != null && (status.equals("car_assigned") || status.equals("car_at_place")))
            notificationBuilder.setSound(Uri.parse("android.resource://" +
                    getApplicationContext().getPackageName() + "/" + R.raw.sound));
        else
            notificationBuilder.setSound(Uri.parse("android.resource://" +
                    getApplicationContext().getPackageName() + "/" + R.raw.marimba_chord));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(AppParams.NOTIFICATION_ID, notificationBuilder.build());
        PowerManager powerManager = (PowerManager)
                getApplication().getSystemService(Context.POWER_SERVICE);

        boolean isInteractive = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            isInteractive = powerManager.isInteractive();
         else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            isInteractive = powerManager.isScreenOn();


        if (!isInteractive) {
            PowerManager.WakeLock wl = powerManager.newWakeLock(
                    PowerManager.FULL_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP
                            | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(5000);
            PowerManager.WakeLock wl_cpu = powerManager
                    .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            wl_cpu.acquire(5000);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = "my_channel_01";
        String channelName = "dsadsa";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }
}
