package com.kabbi.client.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kabbi.client.events.api.geo.LocationErrorEvent;
import com.kabbi.client.events.ui.map.CurrentLocationEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.utils.LocManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LocationService extends Service {

    private LocManager manager;
    private Location location;
    public static boolean isStarted;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        manager = new LocManager(getApplicationContext());
        manager.startUpdatesRequesting();
        location = manager.getLastLocation();
    }

    @Override
    public void onDestroy() {
        manager.stopUpdatesRequesting();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessage(GetCurrentLocationEvent getCurrentLocationEvent) {
        Log.d("Logos", "GetCurrentLocationEvent");
        location = manager.getLocation();
        if (location != null) {
            EventBus.getDefault().post(new CurrentLocationEvent(
                    location.getLatitude(), location.getLongitude(), true));
        } else EventBus.getDefault().post(new LocationErrorEvent());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
