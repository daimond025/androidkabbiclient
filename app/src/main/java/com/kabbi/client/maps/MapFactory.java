package com.kabbi.client.maps;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.kabbi.client.maps.gis.FragmentGisMap;
import com.kabbi.client.maps.google.FragmentGMap;
import com.kabbi.client.maps.yandex.FragmentYMap;
import com.kabbi.client.utils.AppPreferences;

public class MapFactory {

    private static final int MAP_TYPE_GOOGLE = 1;
    private static final int MAP_TYPE_GOOGLE_HYBRID = 2;
     private static final int MAP_TYPE_YANDEX = 3;
     private static final int MAP_TYPE_GIS = 4;

    public static FragmentAbstractMap getFragmentMap(Context context, int mapType) {
        FragmentAbstractMap fragmentMap;

        switch (mapType) {
            case MAP_TYPE_YANDEX:
                fragmentMap = FragmentYMap.newInstance();
                break;
            case MAP_TYPE_GIS:
                fragmentMap = FragmentGisMap.newInstance();
                break;
            case MAP_TYPE_GOOGLE_HYBRID:
            case MAP_TYPE_GOOGLE:
            default:
                fragmentMap = FragmentGMap.newInstance();
                break;
        }

        return fragmentMap;
    }

}
