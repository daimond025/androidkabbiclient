package com.kabbi.client.maps.yandex;

import android.animation.FloatEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.kabbi.client.R;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.MapActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.geo.CarFreeEvent;
import com.kabbi.client.events.api.geo.LocationErrorEvent;
import com.kabbi.client.events.ui.OrderRepeatEvent;
import com.kabbi.client.events.ui.map.CarBusyEvent;
import com.kabbi.client.events.ui.map.CoordsForReverseEvent;
import com.kabbi.client.events.ui.map.CurrentLocationEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.events.ui.map.MotionMapEvent;
import com.kabbi.client.events.ui.map.PointsMapEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.ui.map.PolygonEvent;
import com.kabbi.client.maps.FragmentAbstractMap;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Car;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.geo_listeners.ReverseListener;
import com.kabbi.client.network.requests.GetReverseRequest;
import com.kabbi.client.utils.DetectCity;
import com.kabbi.client.utils.LocaleHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.NoSubscriberEvent;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonOverlay;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonRender;
import ru.yandex.yandexmapkit.utils.GeoPoint;

import static com.kabbi.client.app.AppParams.WITH_SHOW_CAR;


public class FragmentYMap extends FragmentAbstractMap implements OnMapListener {

    public static FragmentYMap newInstance() {
        return new FragmentYMap();
    }

    private MapController mMapController;
    private OverlayManager mOverlayManager;

    private Overlay overlayCars;
    private OverlayItem car;
    private Overlay overlayOrder;

    private DetectCity detectCity;
    private Profile profile;
    private City city;
    private List<GeoPoint> points;

    private double lat, lon;
    private int[] icons = {R.drawable.pin_a,
            R.drawable.pin_b, R.drawable.pin_c,
            R.drawable.pin_d,R.drawable.pin_e};
    private ValueAnimator valueAnimator;

    private static final int DURATION = 5000;
    private long lastTouched = 0;
    private static final long SCROLL_TIME = 200L;
    private MapView mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<City> cityList = City.getCities();
        detectCity = new DetectCity(cityList);
        profile = Profile.getProfile();
        lat = 0;
        lon = 0;
        points = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mapView = new MapView(getActivity().getApplicationContext(),
                "xzG7oyGw0VyfgwwxTbONRdPza-5Is8aLqZJ5gTR36ABba4jxxpdw0OrgwgdyKqU5ObIQamCsGWjCEII5JvPamj~nObtKQuS6ez9J1MsuG5o=");
        onMapReady();

        return mapView;
    }


    @Override
    public void onDestroyView() {
        Log.d("Logos", "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        Log.d("Logos", "onStart Yandex");
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onMapReady() {
        Log.d("Logos", "onMapReady");
        mapView.setTag("static");
        mapView.showFindMeButton(false);
        mapView.showScaleView(false);
        mapView.showZoomButtons(false);
        mapView.showJamsButton(false);

        mMapController = mapView.getMapController();
        mMapController.addMapListener(this);

        mOverlayManager = mMapController.getOverlayManager();
        overlayOrder = new Overlay(mMapController);
        overlayCars = new Overlay(mMapController);

        mOverlayManager.addOverlay(overlayOrder);
        mOverlayManager.addOverlay(overlayCars);

        mOverlayManager.getMyLocation().setVisible(false);


        if (lat == 0) {
            City city;
            city = City.getFirstCity();
            lat = city.getLat();
            lon = city.getLon();
        }

        setCameraAndTitle();

        try {
            if (getTag().equals("main") && ((MainActivity) getActivity()).getAddressList().size() > 0
                    && ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0) {
                Log.d("Logos", "Bitch");
                City city = detectCity.getCity(
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLat()),
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLon()));
                if (city != null) {
                    profile.setCityId(city.getCityId());
                    profile.save();
                }
                EventBus.getDefault().post(new PolygonDetectEvent(city));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (getTag().equals("main")) {
            if (!((MainActivity) getActivity()).getOrder().getStatus().equals("empty")) {
                clearMap();
                List<Address> addressList = RoutePoint.getRoute(((MainActivity) getActivity()).getOrder());
                points.clear();
                int count = 0;
                for (Address address : addressList) {
                    try {
                        overlayOrder.addOverlayItem(new OverlayItem(new GeoPoint(Double.valueOf(address.getLat()),
                                Double.valueOf(address.getLon())), ContextCompat.getDrawable(getContext(), icons[count])));
                        count++;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ((MainActivity) getActivity()).updatePoints(false);
            }
        }
    }

    @Subscribe
    public void onMessage(PolygonEvent polygonEvent) {
        Log.d("Logos", "PolygonEvent");
        if (getAddressNumber() > 1) {
            city = detectCity.getCity(
                    Double.parseDouble(polygonEvent.getLat()),
                    Double.parseDouble(polygonEvent.getLon()));
            if (city != null) {
                profile.setCityId(city.getCityId());
                profile.save();
            }
            EventBus.getDefault().post(new PolygonDetectEvent(city));
        }
    }

    private void setCameraAndTitle() {
       /* OrderRepeatEvent orderRepeat = EventBus.getDefault().removeStickyEvent(OrderRepeatEvent.class);
        if (orderRepeat == null && !getTag().equals("main")) {
            setCameraAndTitleToCurrentLocation();
        }*/
    }


   /* private void setCameraAndTitleToCurrentLocation() {
        Log.d("Logos", "setCameraAndTitleToCurrentLocation " + getTag());
        mMapController.setPositionAnimationTo(new GeoPoint(lat,lon));
        mMapController.setZoomCurrent(17);
        EventBus.getDefault().postSticky(
                new CoordsForReverseEvent(String.valueOf(lat), String.valueOf(lon)));

        getSpiceManager().execute(
                new GetReverseRequest(
                        profile.getCityId(),
                        String.valueOf(lat),
                        String.valueOf(lon),
                        "0.1", "1",
                        profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                new ReverseListener());
        EventBus.getDefault().post(new GetCurrentLocationEvent());
    }*/

    private int getAddressNumber() {
        int addrNum = 1;
        if (getTag().equals("main")) {
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) addrNum = activity.getAddressList().size();
        } else {
            MapActivity activity = (MapActivity) getActivity();
            if (activity != null) addrNum = activity.getPointNum() + 1;
        }
        return addrNum;
    }


    public void onUpdateMapAfterUserInteraction() {
        try {
            GeoPoint cameraPosition = mMapController.getMapCenter();
            EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                    String.valueOf(cameraPosition.getLat()),
                    String.valueOf(cameraPosition.getLon())));
            if (getAddressNumber() > 1 || getAddressNumber() == 1 && city != null )
                getSpiceManager().execute(
                        new GetReverseRequest(
                                profile.getApiKey(),
                                profile.getCityId(),
                                String.valueOf(cameraPosition.getLat()),
                                String.valueOf(cameraPosition.getLon()),
                                AppParams.REVERSE_RADIUS, "1",
                                profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                        new ReverseListener());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapActionEvent(MapEvent mapEvent) {
        switch (mapEvent.getMsg()) {
            case MapEvent.MSG_SCROLL_END:
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.CHANGE));
                if (getAddressNumber() == 1) {
                    city = detectCity.getCity(
                            mMapController.getMapCenter().getLat(),
                            mMapController.getMapCenter().getLon());
                    if (city != null) {
                        profile.setCityId(city.getCityId());
                        profile.save();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new PolygonDetectEvent(city));
                        }
                    });
                }
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.UP));
                final long now = SystemClock.uptimeMillis();
                if (now - lastTouched > SCROLL_TIME) {
                    // Update the map
                    onUpdateMapAfterUserInteraction();
                }
                break;
            case MapEvent.MSG_SCROLL_BEGIN:
                lastTouched = SystemClock.uptimeMillis();
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.DOWN));
                break;
            case MapEvent.MSG_SCALE_END:
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.UP));
                break;
            case MapEvent.MSG_SCALE_BEGIN:
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.DOWN));
                break;
        }
    }


    @Subscribe
    public void onMessage(CarFreeEvent carFreeEvent) {
        if (!WITH_SHOW_CAR) return;

        clearMap();
        if (getTag().equals("main") && !((MainActivity) getActivity())
                .getOrder().getStatus().equals("car_assigned")) {
            if (carFreeEvent.getCarList().size() > 0) {
                for (Car car : carFreeEvent.getCarList()) {
                    overlayCars.addOverlayItem(new OverlayItem(new GeoPoint(car.getLat(), car.getLon()),
                            ContextCompat.getDrawable(getContext(), R.drawable.car)));
                }
            }
        }

        int count = 0;
        for (GeoPoint marker : points) {
            Log.d("Logos", points.size() + " points size");
            overlayOrder.addOverlayItem(new OverlayItem(marker, ContextCompat.getDrawable(getContext(), icons[count])));
            count++;
        }
    }

    public void clearMap() {
        overlayOrder.clearOverlayItems();
        overlayCars.clearOverlayItems();
    }

    public Bitmap drawTextToBitmap(Context gContext,
                                   int gResId,
                                   String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, gResId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(getResources().getColor(R.color.textColorWhite));
        paint.setTextSize((int) (18 * scale));
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        float x = (bitmap.getWidth() - bounds.width())/(float)2.2;
        float y = (bitmap.getHeight() + bounds.height())/(float)2.9;

        canvas.drawText(gText, x, y, paint);

        Paint paintTime = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTime.setColor(getResources().getColor(R.color.textColorWhite));
        paintTime.setTextSize((int) (8 * scale));
        Rect bounds2 = new Rect();
        paintTime.getTextBounds(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), 0, getString(R.string.fragments_FragmentAbstractMap_time_minute_long).length(), bounds2);
        float x2 = (bitmap.getWidth() - bounds2.width())/(float)2.2;
        float y2 = (bitmap.getHeight() + bounds2.height())/(float)2.0;

        canvas.drawText(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), x2, y2, paintTime);

        return bitmap;
    }


    private void showRipples(GeoPoint geo) {

    }


    @Subscribe
    public void onMessage(CarBusyEvent carBusyEvent) {
        if (car == null) {
            car = new OverlayItem(new GeoPoint(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()),
                    ContextCompat.getDrawable(getContext(), R.drawable.car));
            overlayOrder.addOverlayItem(car);
        } else
            car.setGeoPoint(new GeoPoint(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()));

        mMapController.setPositionAnimationTo(new GeoPoint(carBusyEvent.getCar().getLat(),
                carBusyEvent.getCar().getLon()));
        mMapController.setZoomCurrent(15);
    }

    @Subscribe
    public void onMessage(NoSubscriberEvent noSubscriberEvent) {

    }

    @Subscribe
    public void onMessage(CurrentLocationEvent currentLocationEvent) {
        Log.d("Logos", "CurrentLocationEvent " + currentLocationEvent.getLat() + " " + currentLocationEvent.getLon());
        mMapController.setZoomCurrent(17);
        mMapController.setPositionAnimationTo(new GeoPoint(currentLocationEvent.getLat(),
                currentLocationEvent.getLon()));
        if (currentLocationEvent.withReverse()) {
            EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                    String.valueOf(currentLocationEvent.getLat()),
                    String.valueOf(currentLocationEvent.getLon())));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getApiKey(),
                            profile.getCityId(),
                            String.valueOf(currentLocationEvent.getLat()),
                            String.valueOf(currentLocationEvent.getLon()),
                            AppParams.REVERSE_RADIUS, "1",
                            profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                    new ReverseListener());
        }
    }

    @Subscribe
    private void onMessage(LocationErrorEvent errorEvent) {
        Log.d("Logos", "setCameraAndTitleToFirstPoint " + getTag());
        Address firstAddress = null;
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) firstAddress = activity.getAddressList().get(0);
        if (firstAddress != null) {
            EventBus.getDefault().postSticky(
                    new CoordsForReverseEvent(String.valueOf(firstAddress.getLat()),
                            String.valueOf(firstAddress.getLon())));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getApiKey(),
                            profile.getCityId(),
                            String.valueOf(firstAddress.getLat()),
                            String.valueOf(firstAddress.getLon()),
                            AppParams.REVERSE_RADIUS, "1",
                            profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                    new ReverseListener());

            mMapController.setPositionAnimationTo(new GeoPoint(Double.valueOf(firstAddress.getLat()),
                    Double.valueOf(firstAddress.getLon())));
            mMapController.setZoomCurrent(17);
        }
    }

    @Subscribe
    public void onMessage(PointsMapEvent pointsMapEvent) {
        clearMap();
        Log.d("Logos", "PointsMapEvent " + getTag());
        points.clear();
        Log.d("Logos", "PointsMapEvent " + pointsMapEvent.isActiveOrder() + " " + pointsMapEvent.getAddressList().size());
        if (pointsMapEvent.isActiveOrder() || (pointsMapEvent.getAddressList().size() > 1 && !pointsMapEvent.isActiveOrder())) {
            int count = 0;
            Log.d("Logos", pointsMapEvent.getAddressList() + " Size");
            Log.d("Logos", "pointsMapEvent.isActiveOrder()");
            for (final Address address : pointsMapEvent.getAddressList()) {
                try {
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("new")) {
                        mMapController.setPositionAnimationTo(new GeoPoint(Double.valueOf(address.getLat()),
                                Double.valueOf(address.getLon())));
                        mMapController.setZoomCurrent(17);
                    }
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("car_assigned")) {
                        Bitmap icon = drawTextToBitmap(getActivity(),
                                R.drawable.pin_empty, pointsMapEvent.getTime());
                        Drawable marker = new BitmapDrawable(getResources(), icon);

                        overlayOrder.addOverlayItem(new OverlayItem(new GeoPoint(
                                Double.valueOf(address.getLat()),
                                Double.valueOf(address.getLon())),
                                marker));

                    } else {
                        GeoPoint mGeoPoint = new GeoPoint(
                                Double.valueOf(address.getLat()),
                                Double.valueOf(address.getLon()));
                        overlayOrder.addOverlayItem(new OverlayItem(mGeoPoint , ContextCompat.getDrawable(getContext(), icons[count])));
                        points.add(mGeoPoint);
                    }

                    if (valueAnimator != null && count == 0) {
                        valueAnimator.cancel();
                    }

                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("new")) {
                        final GeoPoint latLng = new GeoPoint(Double.valueOf(address.getLat()), Double.valueOf(address.getLon()));
                        showRipples(latLng);
                    }

                    count++;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        if (pointsMapEvent.getAddressList().size() == 1 && !pointsMapEvent.isActiveOrder()) {

            Address firstAddress = pointsMapEvent.getAddressList().get(0);
            mMapController.setPositionAnimationTo(new GeoPoint(
                    Double.valueOf(firstAddress.getLat()),
                    Double.valueOf(firstAddress.getLon())));
            mMapController.setZoomCurrent(17);
        }
        if (car != null) {
            overlayOrder.addOverlayItem(car);
        }
        mMapController.setZoomCurrent(mMapController.getZoomCurrent() - 0.01f);
        mMapController.setZoomCurrent(mMapController.getZoomCurrent() + 0.01f);
    }

    public String[] getMarkerCoords() {
        GeoPoint cameraPosition = mMapController.getMapCenter();
        String[] coords = new String[2];
        coords[0] = String.valueOf(cameraPosition.getLat());
        coords[1] = String.valueOf(cameraPosition.getLon());
        return coords;
    }

}

