package com.kabbi.client.maps.gis;

import android.animation.FloatEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.android.gms.maps.model.UrlTileProvider;
import com.kabbi.client.R;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.MapActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.geo.CarFreeEvent;
import com.kabbi.client.events.ui.OrderRepeatEvent;
import com.kabbi.client.events.ui.map.CarBusyEvent;
import com.kabbi.client.events.ui.map.CoordsForReverseEvent;
import com.kabbi.client.events.ui.map.CurrentLocationEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.events.ui.map.MapChangedEvent;
import com.kabbi.client.events.ui.map.PointsMapEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.ui.map.PolygonEvent;
import com.kabbi.client.maps.FragmentAbstractMap;
import com.kabbi.client.maps.google.TouchableWrapper;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Car;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.geo_listeners.ReverseListener;
import com.kabbi.client.network.requests.GetReverseRequest;
import com.kabbi.client.utils.DetectCity;
import com.kabbi.client.utils.LocaleHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.kabbi.client.app.AppParams.WITH_SHOW_CAR;

/**
 * Google map fragment
 */
public class FragmentGisMap extends FragmentAbstractMap implements
        OnMapReadyCallback {

    public static FragmentGisMap newInstance() {
        return new FragmentGisMap();
    }

    private GoogleMap googleMap;
    private View mOriginalContentView;
    private DetectCity detectCity;
    private Profile profile;
    private City city;
    private Marker carBusy;
    private Marker timeMarker;
    private GroundOverlay circle;
    private List<Marker> points;
    private List<Marker> cars;
    private Marker myLocationMarker;
    private double lat, lon;
    private int[] icons = {R.drawable.pin_a,
            R.drawable.pin_b, R.drawable.pin_c,
            R.drawable.pin_d, R.drawable.pin_e};
    private ValueAnimator valueAnimator;
    private static final int DURATION = 5000;
    private final String TILE_URL = "http://tile0.maps.2gis.com/tiles?x=%d&y=%d&z=%d&v=1";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<City> cityList = City.getCities();
        detectCity = new DetectCity(cityList);
        profile = Profile.getProfile();
        lat = 0;
        lon = 0;
        points = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        getMapAsync(this);

        mOriginalContentView = super.onCreateView(inflater, container, savedInstanceState);
        TouchableWrapper mTouchView = new TouchableWrapper(getActivity().getApplicationContext());
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onMapReady(GoogleMap map) {
        TileProvider tileProvider = new UrlTileProvider(256, 256) {
            @Override
            public URL getTileUrl(int x, int y, int zoom) {
                String s = String.format(Locale.ENGLISH, TILE_URL, x, y, zoom);

                try {
                    return new URL(s);
                } catch (Exception e) {
                    throw new AssertionError(e);
                }
            }
        };

        map.setMapType(GoogleMap.MAP_TYPE_NONE);
        map.addTileOverlay(new TileOverlayOptions().tileProvider(tileProvider));

        this.googleMap = map;

        if (lat == 0) {
            City city;
            city = City.getFirstCity();
            lat = city.getLat();
            lon = city.getLon();
        }

        setCameraAndTitle();

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                EventBus.getDefault().post(new MapChangedEvent(MapChangedEvent.TYPE_LOADING));
                if (getAddressNumber() == 1) {
                    city = detectCity.getCity(
                            googleMap.getCameraPosition().target.latitude,
                            googleMap.getCameraPosition().target.longitude);
                    if (city != null) {
                        profile.setCityId(city.getCityId());
                        profile.save();
                    }
                    EventBus.getDefault().post(new PolygonDetectEvent(city));
                }

                try {
                    CameraPosition cameraPosition = googleMap.getCameraPosition();
                    EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                            String.valueOf(cameraPosition.target.latitude),
                            String.valueOf(cameraPosition.target.longitude)));
                    if (getAddressNumber() > 1 || getAddressNumber() == 1 && city != null) {
                        getSpiceManager().execute(
                                new GetReverseRequest(
                                        profile.getApiKey(),
                                        profile.getCityId(),
                                        String.valueOf(cameraPosition.target.latitude),
                                        String.valueOf(cameraPosition.target.longitude),
                                        AppParams.REVERSE_RADIUS, "1",
                                        profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                                new ReverseListener());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });


        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                EventBus.getDefault().post(new MapChangedEvent(MapChangedEvent.TYPE_START));
            }
        });



        try {
            if (getTag().equals("main") && ((MainActivity) getActivity()).getAddressList().size() > 0
                    && ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0) {
                City city = detectCity.getCity(
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLat()),
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLon()));
                if (city != null) {
                    profile.setCityId(city.getCityId());
                    profile.save();
                }
                EventBus.getDefault().post(new PolygonDetectEvent(city));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getTag().equals("main")) {
            if (!((MainActivity) getActivity()).getOrder().getStatus().equals("empty")) {
                clearPointsMarkers();
                List<Address> addressList = RoutePoint.getRoute(((MainActivity) getActivity()).getOrder());
                points.clear();
                int count = 0;
                for (Address address : addressList) {
                    try {
                        points.add(googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .icon(BitmapDescriptorFactory.fromResource(icons[count]))));
                        count++;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ((MainActivity) getActivity()).updatePoints(false);
            }
        }
    }

    @Subscribe
    public void onMessage(PolygonEvent polygonEvent) {
        if (getAddressNumber() == 1) {
            city = detectCity.getCity(
                    Double.parseDouble(polygonEvent.getLat()),
                    Double.parseDouble(polygonEvent.getLon()));
            if (city != null) {
                profile.setCityId(city.getCityId());
                profile.save();
            }
            EventBus.getDefault().post(new PolygonDetectEvent(city));
        }
    }

    private void setCameraAndTitle() {
        OrderRepeatEvent orderRepeat = EventBus.getDefault()
                .removeStickyEvent(OrderRepeatEvent.class);
        if (orderRepeat != null && getTag().equals("main")) {
            setCameraAndTitleToFirstPoint();
        } else {
            setCameraAndTitleToCurrentLocation();
        }
    }

    private final static String TAG = FragmentGisMap.class.getSimpleName();


    private void setCameraAndTitleToFirstPoint() {
        Address firstAddress = null;
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            firstAddress = activity.getAddressList().get(0);
        }
        if (firstAddress != null) {
            googleMap.animateCamera(CameraUpdateFactory
                    .newLatLngZoom(new LatLng(
                            Double.valueOf(firstAddress.getLat()),
                            Double.valueOf(firstAddress.getLon())), 17));
        }
    }

    private void setCameraAndTitleToCurrentLocation() {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 17));
        EventBus.getDefault().postSticky(
                new CoordsForReverseEvent(String.valueOf(lat), String.valueOf(lon)));
        getSpiceManager().execute(
                new GetReverseRequest(
                        profile.getApiKey(),
                        profile.getCityId(),
                        String.valueOf(lat),
                        String.valueOf(lon),
                        AppParams.REVERSE_RADIUS, "1",
                        profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                new ReverseListener());
        EventBus.getDefault().post(new GetCurrentLocationEvent());
    }

    private int getAddressNumber() {
        int addrNum = 1;
        if (getTag().equals("main")) {
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) addrNum = activity.getAddressList().size();
        } else {
            MapActivity activity = (MapActivity) getActivity();
            if (activity != null) addrNum = activity.getPointNum() + 1;
        }
        return addrNum;
    }



    @Subscribe
    public void onMessage(CarFreeEvent carFreeEvent) {
        if (!WITH_SHOW_CAR) return;

        if (getTag().equals("main") && !((MainActivity) getActivity())
                .getOrder().getStatus().equals("car_assigned")) {
            clearFreeCarMarkers();
            if (carFreeEvent.getCarList().size() > 0) {
                for (Car car : carFreeEvent.getCarList()) {
                    Marker marker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(car.getLat(), car.getLon()))
                            .anchor(0.5f, 0.5f)
                            .rotation(car.getBearing())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    cars.add(marker);
                }
            }
        }
    }

    public Bitmap drawTextToBitmap(Context gContext,
                                   int gResId,
                                   String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, gResId);

        Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if (bitmapConfig == null) {
            bitmapConfig = Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(getResources().getColor(R.color.textColorWhite));
        paint.setTextSize((int) (18 * scale));
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        float x = (bitmap.getWidth() - bounds.width()) / (float) 2.2;
        float y = (bitmap.getHeight() + bounds.height()) / (float) 2.9;

        canvas.drawText(gText, x, y, paint);

        Paint paintTime = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTime.setColor(getResources().getColor(R.color.textColorWhite));
        paintTime.setTextSize((int) (8 * scale));
        Rect bounds2 = new Rect();
        paintTime.getTextBounds(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), 0, getString(R.string.fragments_FragmentAbstractMap_time_minute_long).length(), bounds2);
        float x2 = (bitmap.getWidth() - bounds2.width()) / (float) 2.2;
        float y2 = (bitmap.getHeight() + bounds2.height()) / (float) 2.0;

        canvas.drawText(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), x2, y2, paintTime);

        return bitmap;
    }


    @Subscribe
    public void onMessage(CarBusyEvent carBusyEvent) {
        if (carBusy == null) {
            clearBusyCarMarker();
            carBusy = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()))
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        } else {
            carBusy.setPosition(new LatLng(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()));
        }
        carBusy.setRotation(Float.valueOf(carBusyEvent.getCar().getBearing()));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(carBusyEvent.getCar().getLat(),
                carBusyEvent.getCar().getLon()), 15));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CurrentLocationEvent currentLocationEvent) {
        if (myLocationMarker == null) {
            //myLocationMarker = googleMap.addMarker(new MarkerOptions()
                   // .position(new LatLng(currentLocationEvent.getLat(), currentLocationEvent.getLon())));
        } else {
            //myLocationMarker.setPosition(new LatLng(currentLocationEvent.getLat(), currentLocationEvent.getLon()));
        }


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(currentLocationEvent.getLat(), currentLocationEvent.getLon()), 17));
        if (currentLocationEvent.withReverse()) {
            Log.d("Logos", "onMessage: CurrentLocationEvent");
            EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                    String.valueOf(currentLocationEvent.getLat()),
                    String.valueOf(currentLocationEvent.getLon())));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getApiKey(),
                            profile.getCityId(),
                            String.valueOf(currentLocationEvent.getLat()),
                            String.valueOf(currentLocationEvent.getLon()),
                            AppParams.REVERSE_RADIUS, "1",
                            profile.getTenantId(), LocaleHelper.getLanguage(getActivity().getBaseContext())),
                    new ReverseListener());
        }
    }


    private void showRipples(LatLng latLng) {
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.accent_bg_tariff, typedValue, true);

        GradientDrawable d = new GradientDrawable();
        d.setShape(GradientDrawable.OVAL);
        d.setSize(500, 500);
        d.setColor(typedValue.data);
        d.setStroke(0, Color.TRANSPARENT);

        Bitmap bitmap = Bitmap.createBitmap(d.getIntrinsicWidth()
                , d.getIntrinsicHeight()
                , Bitmap.Config.ARGB_8888);

        // Convert the drawable to bitmap
        Canvas canvas = new Canvas(bitmap);
        d.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        d.draw(canvas);

        // Radius of the circle
        final int radius = 300;

        // Add the circle to the map
        circle = googleMap.addGroundOverlay(new GroundOverlayOptions()
                .position(latLng, 2 * radius).image(BitmapDescriptorFactory.fromBitmap(bitmap)));
        // Prep the animator
        PropertyValuesHolder radiusHolder = PropertyValuesHolder.ofFloat("radius", 0, radius);
        PropertyValuesHolder transparencyHolder = PropertyValuesHolder.ofFloat("transparency", 0, 1);

        valueAnimator = new ValueAnimator();
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
        valueAnimator.setValues(radiusHolder, transparencyHolder);
        valueAnimator.setDuration(DURATION);
        valueAnimator.setEvaluator(new FloatEvaluator());
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedRadius = (float) valueAnimator.getAnimatedValue("radius");
                float animatedAlpha = (float) valueAnimator.getAnimatedValue("transparency");
                circle.setDimensions(animatedRadius * 2);
                circle.setTransparency(animatedAlpha);
            }
        });

        // start the animation
        valueAnimator.start();
    }


    public void animateCameraByMarkers(List<Marker> markers) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();

        int padding = 0; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200, 200, padding);
        googleMap.animateCamera(cu);
    }

    @Subscribe
    public void onMessage(PointsMapEvent pointsMapEvent) {
        Log.d(TAG, "Logos onMessage: " + pointsMapEvent.getAddressList());
        if (googleMap != null) {
            clearTimeMarker();
            clearPointsMarkers();
            stopRippleAnimation();
        }
        if (pointsMapEvent.isActiveOrder() || (pointsMapEvent.getAddressList().size() > 1 && !pointsMapEvent.isActiveOrder())) {
            points.clear();
            int count = 0;
            for (final Address address : pointsMapEvent.getAddressList()) {
                try {
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("new")) {
                        googleMap.animateCamera(CameraUpdateFactory
                                .newLatLngZoom(new LatLng(
                                        Double.valueOf(address.getLat()),
                                        Double.valueOf(address.getLon())), 18));
                    }
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("car_assigned")) {
                        timeMarker = googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(getActivity(),
                                        R.drawable.pin_empty, pointsMapEvent.getTime()))));
                    } else {
                        points.add(googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .icon(BitmapDescriptorFactory.fromResource(icons[count]))));
                    }

                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("new")) {
                        final LatLng latLng = new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon()));
                        startRippleAnimation(latLng);
                    }

                    count++;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } else {
            points.clear();
        }

        if (pointsMapEvent.getAddressList().size() > 1 && !pointsMapEvent.isActiveOrder()) {
            animateCameraByMarkers(points);
        } else if (pointsMapEvent.getAddressList().size() > 0 && pointsMapEvent.isActiveOrder()) {
            if (!pointsMapEvent.getStatusOrder().equals("new")) {
                List<Marker> markersActive = new ArrayList<>();
                // Add car marker
                if (carBusy != null) markersActive.add(carBusy);

                if (pointsMapEvent.getStatusOrder().equals("car_assigned"))
                    markersActive.add(timeMarker);
                else markersActive.add(points.get(0));

                animateCameraByMarkers(markersActive);
            }
        }

        if (carBusy != null) {
            clearBusyCarMarker();
            carBusy = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(carBusy.getPosition().latitude, carBusy.getPosition().longitude))
                    .anchor(0.5f, 0.5f)
                    .rotation(carBusy.getRotation())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        }
    }

    @Override
    public String[] getMarkerCoords() {
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        String[] coords = new String[2];
        coords[0] = String.valueOf(cameraPosition.target.latitude);
        coords[1] = String.valueOf(cameraPosition.target.longitude);
        return coords;
    }


    private void clearPointsMarkers() {
        for (Marker marker : points) {
            marker.remove();
        }
    }


    private void clearFreeCarMarkers() {
        for (Marker marker : cars) {
            marker.remove();
        }
    }


    private void clearBusyCarMarker() {
        if (carBusy != null) {
            carBusy.remove();
        }
    }


    private void clearTimeMarker() {
        if (timeMarker != null) {
            timeMarker.remove();
        }
    }


    private void startRippleAnimation(LatLng latLng) {
        showRipples(latLng);
    }


    private void stopRippleAnimation() {
        if (circle != null) circle.remove();
        if (valueAnimator != null) valueAnimator.cancel();
    }
}
