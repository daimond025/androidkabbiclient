package com.kabbi.client.maps.google;


import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.kabbi.client.events.ui.map.MotionMapEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Class for getting touch events on the map
 */
public class TouchableWrapper extends FrameLayout {


    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.DOWN));
                break;
            case MotionEvent.ACTION_UP:
                EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.UP));
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

}
