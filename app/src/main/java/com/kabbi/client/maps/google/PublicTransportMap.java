package com.kabbi.client.maps.google;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kabbi.client.R;
import com.kabbi.client.activities.PublicTransportActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.ui.map.CoordsForReverseEvent;
import com.kabbi.client.events.ui.map.CurrentLocationEvent;
import com.kabbi.client.models.Car;
import com.kabbi.client.network.listeners.geo_listeners.ReverseListener;
import com.kabbi.client.network.requests.GetReverseRequest;
import com.kabbi.client.utils.LocaleHelper;
import com.kabbi.client.views.adapters.map.MapInfoWindowAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PublicTransportMap extends SupportMapFragment
        implements OnMapReadyCallback{

    public static final String ARGUMENT_LAT_LON = "ARGUMENT_LAT_LON";

    private GoogleMap mGoogleMap;
    private LatLng mLatLng;
    private List<Marker> mMarkers = new ArrayList<>();
    private Map<String, Boolean> classVisibility = new HashMap<>();

    public static PublicTransportMap newInstance(LatLng latLng) {
        PublicTransportMap transportMap = new PublicTransportMap();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARGUMENT_LAT_LON, latLng);
        transportMap.setArguments(bundle);
        return transportMap;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        EventBus.getDefault().register(this);
        mLatLng = getArguments().getParcelable(ARGUMENT_LAT_LON);
        classVisibility.put(PublicTransportActivity.BUS_CLASS, true);
        classVisibility.put(PublicTransportActivity.TRAM_CLASS, true);
        classVisibility.put(PublicTransportActivity.TROLLEY_CLASS, true);
        classVisibility.put(PublicTransportActivity.FOLLOW_BUS_CLASS, true);
        getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 14));
    }


    public void showCars(List<Car> cars) {
        mGoogleMap.clear();
        mMarkers.clear();

        float markerSize = getContext().getResources().getDimension(R.dimen.public_transport_marker_size);

        for (Car car: cars) {
            LatLng latLng = new LatLng(car.getLat(), car.getLon());

            BitmapDescriptor icon = null;

            switch (car.getCarClass()) {
                case PublicTransportActivity.FOLLOW_BUS_CLASS:
                    int color = ContextCompat.getColor(getContext(), R.color.follow_bus_color);
                    icon = BitmapDescriptorFactory.fromBitmap(generateCircleBitmap(color, markerSize, car.getNumber()));
                    break;
                case PublicTransportActivity.BUS_CLASS:
                    color = ContextCompat.getColor(getContext(), R.color.bus_color);
                    icon = BitmapDescriptorFactory.fromBitmap(generateCircleBitmap(color, markerSize, car.getNumber()));
                    break;
                case PublicTransportActivity.TROLLEY_CLASS:
                    color = ContextCompat.getColor(getContext(), R.color.trolley_color);
                    icon = BitmapDescriptorFactory.fromBitmap(generateCircleBitmap(color, markerSize, car.getNumber()));
                    break;
                case PublicTransportActivity.TRAM_CLASS:
                    color = ContextCompat.getColor(getContext(), R.color.tram_color);
                    icon = BitmapDescriptorFactory.fromBitmap(generateCircleBitmap(color, markerSize,car.getNumber()));
                    break;
                default:
                    continue;
            }

            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .icon(icon);
            Marker marker = mGoogleMap.addMarker(markerOptions);
            marker.setTag(car);

            Boolean isVisible = classVisibility.get(car.getCarClass());
            if (isVisible != null) {
                marker.setVisible(isVisible);
            } else {
                marker.setVisible(false);
            }

            mMarkers.add(marker);
        }
    }

    public Bitmap generateCircleBitmap(int circleColor, float diameterDP, String text) {
        final int textColor = 0xffffffff;

        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float diameterPixels = diameterDP * (metrics.densityDpi / 160f);
        float radiusPixels = diameterPixels / 2;

        // Create the bitmap
        Bitmap output = Bitmap.createBitmap((int) diameterPixels, (int) diameterPixels,
                Bitmap.Config.ARGB_8888);

        // Create the canvas to draw on
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(0, 0, 0, 0);

        // Draw the circle
        final Paint paintC = new Paint();
        paintC.setAntiAlias(true);
        paintC.setColor(circleColor);
        canvas.drawCircle(radiusPixels, radiusPixels, radiusPixels, paintC);

        // Draw the text
        if (text != null && text.length() > 0) {
            final Paint paintT = new Paint();
            paintT.setColor(textColor);
            paintT.setAntiAlias(true);
            paintT.setTextSize(radiusPixels * 0.8f);
            final Rect textBounds = new Rect();
            paintT.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, radiusPixels - textBounds.exactCenterX(), radiusPixels - textBounds.exactCenterY(), paintT);
        }

        return output;
    }
    public void showCarsByClass(String carClass) {
        classVisibility.put(carClass, true);
        for (Marker marker: mMarkers) {
            Car car = (Car) marker.getTag();
            if (car.getCarClass().equals(carClass)) {
                marker.setVisible(true);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CurrentLocationEvent currentLocationEvent) {
        Log.d("Logos", "PublicTransportMap | onMessage | : " + currentLocationEvent.getLat() + " " + currentLocationEvent.getLon());
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(currentLocationEvent.getLat(), currentLocationEvent.getLon()), 15));
    }


    public void hideCarsByClass(String carClass) {
        classVisibility.put(carClass, false);
        for (Marker marker: mMarkers) {
            Car car = (Car) marker.getTag();
            if (car.getCarClass().equals(carClass)) {
                marker.setVisible(false);
            }
        }
    }

}
