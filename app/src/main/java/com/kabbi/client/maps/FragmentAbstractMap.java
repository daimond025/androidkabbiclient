package com.kabbi.client.maps;

import com.google.android.gms.maps.SupportMapFragment;
import com.octo.android.robospice.SpiceManager;
import com.kabbi.client.network.GeoSpiceService;

public abstract class FragmentAbstractMap extends SupportMapFragment {

    public FragmentAbstractMap() {
        // Required empty public constructor
    }

    private SpiceManager spiceManager = new SpiceManager(GeoSpiceService.class);

    @Override
    public void onStart() {
        spiceManager.start(getContext());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

    public abstract String[] getMarkerCoords();

}
