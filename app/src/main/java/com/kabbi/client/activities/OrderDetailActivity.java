package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.OrderInfoEvent;
import com.kabbi.client.events.api.client.RouteEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.OrderInfoListener;
import com.kabbi.client.network.listeners.OrderRouteListener;
import com.kabbi.client.network.requests.GetOrderInfoRequest;
import com.kabbi.client.network.requests.GetOrderRouteRequest;
import com.kabbi.client.utils.Validator;
import com.kabbi.client.utils.polygon.Line;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.kabbi.client.app.AppParams.USE_REVIEW_DETAIL;
import static com.kabbi.client.app.AppParams.USE_REVIEW_FOR_REJECTED;

public class OrderDetailActivity extends BaseSpiceActivity {

    private Order order;
    private String currency;
    private SimpleDraweeView mSimpleDraweeView;
    private List<Address> mAddressList;
    private ProgressDialog progressDialog;
    private Profile mProfile;
    private boolean isRtl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);

        mProfile = Profile.getProfile();
        setTheme(mProfile.getThemeId());
        order = Order.getOrder(getIntent().getStringExtra("order_id"));
        City city = City.getCity(mProfile.getCityId());
        if (city.getCurrency().equals("RUB"))
            currency = getString(R.string.currency);
        else
            currency = city.getCurrency();

        if (getIntent().getStringExtra("status").equals("completed")) {
            setContentView(R.layout.activity_detail_order_completed);
        } else {
            setContentView(R.layout.activity_detail_order);
        }

        Log.d("Logos", "order.getStatusDescription() " + order.getStatusDescription());
        if (order.getStatus().equals("rejected")) {
            if (!USE_REVIEW_FOR_REJECTED) {
                findViewById(R.id.ll_review).setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(order.getStatusDescription())) {
                TextView description = (TextView) findViewById(R.id.tv_detailorder_description);
                description.setText(order.getStatusDescription());
                description.setVisibility(View.VISIBLE);
            }
            findViewById(R.id.fl_order_info).setVisibility(View.GONE);
        }


        final Configuration config = getResources().getConfiguration();
        isRtl = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;

        if (order.getRoute() == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.activities_OptionsActivity_progress_title));
            progressDialog.show();
            getSpiceManager().execute(new GetOrderRouteRequest(mProfile.getAppId(), mProfile.getApiKey(),
                            order.getOrderId(), Profile.getProfile().getTenantId(), AppParams.TYPE_CLIENT),
                    new OrderRouteListener(order.getOrderId()));
        }

        if (getIntent().getStringExtra("type").equals("push")) {
            getSpiceManager().execute(new GetOrderInfoRequest(mProfile.getAppId(), mProfile.getApiKey(), this,
                            String.valueOf((new Date()).getTime()),
                            order,
                            mProfile.getTenantId()),
                    new OrderInfoListener());
        } else {
            loadOrderInfo();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void loadOrderInfo() {
        setTitle(getString(R.string.activities_OrderDetailActivity_order_number)
                + order.getOrderNumber());

        initToolbar();

        mSimpleDraweeView = (SimpleDraweeView) findViewById(R.id.sdv_map);
        SimpleDraweeView simpleDraweeDriver = (SimpleDraweeView) findViewById(R.id.sdv_detailorder_face);
        if (!AppParams.USE_PHOTO) simpleDraweeDriver.setVisibility(View.INVISIBLE);


        mAddressList = RoutePoint.getRoute(order);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_addresslist);


        int count = 0;
        int[] icons = {R.drawable.a,
                R.drawable.b,
                R.drawable.c,
                R.drawable.d,
                R.drawable.e};

        for (Address address : mAddressList) {
            try {
                View view = LayoutInflater.from(this).inflate(R.layout.item_address, null);
                ((ImageView) view.findViewById(R.id.iv_item_icon)).setImageResource(icons[count]);
                ((TextView) view.findViewById(R.id.tv_item_street)).setText(address.getLabel());
                ((TextView) view.findViewById(R.id.tv_item_city)).setText(address.getCity());
                linearLayout.addView(view);
                count++;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        } else {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(getString(R.string.activities_OrderDetailActivity_empty_driver));
        }

        if (order.getReview() != null && order.getReview().length() > 0) {
            (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.VISIBLE);
            (findViewById(R.id.btn_review)).setVisibility(View.GONE);
            setRating(Integer.valueOf(order.getReview()));
        } else {
            (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.GONE);
            (findViewById(R.id.btn_review)).setVisibility(View.VISIBLE);
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }

        parseDetailCost(order.getCost());
        ((TextView) findViewById(R.id.tv_detailorder_status)).setText(label(order));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int size = mSimpleDraweeView.getMeasuredWidth();
        mSimpleDraweeView.getLayoutParams().height = size;
        mSimpleDraweeView.setImageURI(getMapImage(size));
    }

    @Subscribe
    public void onMessage(RouteEvent routeEvent) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int size = displaymetrics.widthPixels;

        ViewGroup.LayoutParams params = mSimpleDraweeView.getLayoutParams();
        params.height = size;
        params.width = size;
        mSimpleDraweeView.setLayoutParams(params);
        mSimpleDraweeView.setImageURI(getMapImage(size));
        progressDialog.dismiss();
    }

    public Uri getMapImage(int size) {
        String[] labels = {"A", "B", "C", "D", "E"};
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < mAddressList.size(); i++) {
            builder.append(String.format("&markers=color:black|label:%s|" + mAddressList.get(i).getLat()
                    + "," + mAddressList.get(i).getLon(), labels[i]));
        }

        return Uri.parse("https://maps.googleapis.com/maps/api/staticmap?" + builder +
                "&size=" + size + "x" + size + "&language=" + Locale.getDefault().getLanguage()
                + "&maptype=roadmap" + "&path=" + "color:0x4f4a9f%7Cweight:5%7Cenc:" + order.getRoute());
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {
        switch (orderInfoEvent.getStatus()) {
            case "completed":
                order.setDetailCost(orderInfoEvent.getCost());
                order.setPaymentType(orderInfoEvent.getPaymentType());
                if (orderInfoEvent.getPaymentType().equals("CARD"))
                    order.setPan(orderInfoEvent.getPan());
                break;
            case "rejected":
                break;
        }
        order.setStatusLabel(orderInfoEvent.getStatusLabel());
        order.setStatus(orderInfoEvent.getStatus());
        order.setCar(orderInfoEvent.getCarDesc());
        order.setDriver(orderInfoEvent.getDriverName());
        order.setPhoto(orderInfoEvent.getPhoto());
        order.setCost(orderInfoEvent.getCost());
        order.setStatusId(orderInfoEvent.getStatusId());
        order.save();

        loadOrderInfo();
    }

    private void parseDetailCost(String sDetailCost) {
        try {
            JSONObject jsonCost = new JSONObject(sDetailCost);

             Double summaryCostValue = Validator.validateDouble(jsonCost, "summary_cost");


            if (summaryCostValue < 0) summaryCostValue = 0.0;
            TextView summaryCostTv = (TextView) findViewById(R.id.tv_detailorder_cost);
            TextView summaryCostCurrency = (TextView) findViewById(R.id.tv_detailorder_currency);
            if (summaryCostTv != null) summaryCostTv.setText(String.valueOf(summaryCostValue));
            if (summaryCostCurrency != null) summaryCostCurrency.setText(currency);

            if (order.getStatus().equals("completed")) {
                LinearLayout llDetailCost = (LinearLayout) findViewById(R.id.ll_detailorder_cost);
                LinearLayout llDetailCostMain = (LinearLayout) findViewById(R.id.ll_detailorder_cost_main);

                boolean isFix = jsonCost.getString("is_fix").equals("1");

                ((TextView) findViewById(R.id.tv_detailorder_order_time))
                        .setText(formatDate(order.getCreateTime()));

                if (isFix) llDetailCostMain.setVisibility(View.GONE);
                else praseMainOrderDatailsData(jsonCost);

                if (isFix) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    String beforeTimeWait = getString(R.string.activities_OrderDetailActivity_fix_cost);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(beforeTimeWait);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_currency))
                            .setText(currency);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result))
                            .setText(order.getFixCost());
                    if (llDetailCost != null) llDetailCost.addView(viewTimeCost);
                }

                if (!jsonCost.isNull("additional_cost")) {
                    View viewAdditionalCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    ((TextView) viewAdditionalCost.findViewById(R.id.tv_item_label)).setText(R.string.activities_OrderDetailActivity_additional_cost);
                    Double addCost = Validator.validateDouble(jsonCost, "additional_cost");
                    if (addCost < 0.0) addCost = 0.0;
                    ((TextView) viewAdditionalCost.findViewById(R.id.tv_item_currency))
                            .setText(currency);
                    ((TextView) viewAdditionalCost.findViewById(R.id.tv_item_result))
                            .setText(Double.toString(addCost));
                    if (llDetailCost != null) llDetailCost.addView(viewAdditionalCost);
                }

                if (!jsonCost.isNull("before_time_wait_cost")) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    String beforeTimeWait = getString(R.string.activities_OrderDetailActivity_before_time_wait_cost) + "  " +
                            formatTime(Double.valueOf(jsonCost.getString("before_time_wait")));
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(beforeTimeWait);
                    Double beforeTimeWaitVal = Validator.validateDouble(jsonCost, "before_time_wait_cost");
                    if (beforeTimeWaitVal < 0.0) beforeTimeWaitVal = 0.0;
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_currency))
                            .setText(currency);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result))
                            .setText(Double.toString(beforeTimeWaitVal));
                    if (llDetailCost != null) llDetailCost.addView(viewTimeCost);
                }

                if (!jsonCost.isNull("city_wait_cost") && !jsonCost.isNull("out_wait_cost")) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    String cityTimeWait = getString(R.string.activities_OrderDetailActivity_wait_cost) + "  " +
                            formatTime((Double.valueOf(jsonCost.getString("city_time_wait")) / 60) +
                                    (Double.valueOf(jsonCost.getString("out_time_wait")) / 60));
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(cityTimeWait);
                    Double cityWaitVal = Validator.validateDouble(jsonCost, "city_wait_cost");
                    Double outWaitVal = Validator.validateDouble(jsonCost, "out_wait_cost");
                    Double totalVal = cityWaitVal + outWaitVal;
                    if (totalVal < 0.0) totalVal = 0.0;
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_currency))
                            .setText(currency);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result))
                            .setText(Double.toString(totalVal));
                    if (llDetailCost != null) llDetailCost.addView(viewTimeCost);
                }

                if (!jsonCost.isNull("planting_include") && !isFix) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    Double plantingVal = Validator.validateDouble(jsonCost, "planting_include");
                    if (plantingVal < 0.0) plantingVal = 0.0;
                    String plantingCost = getString(R.string.activities_OrderDetailActivity_planting_include) + "  " +
                            plantingVal + " " +
                            getString(R.string.activities_OrderDetailActivity_distance);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(plantingCost);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_currency))
                            .setText(currency);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result))
                            .setText("0");
                    if (llDetailCost != null) llDetailCost.addView(viewTimeCost);
                }

                TextView summaryTv = (TextView) findViewById(R.id.tv_detailorder_complete_summary);
                TextView summaryCurrency = (TextView) findViewById(R.id.tv_detailorder_complete_summary_currency);
                Double summaryCostVal = Validator.validateDouble(jsonCost, "summary_cost");
                if (summaryCostVal < 0.0) summaryCostVal = 0.0;

                if (summaryTv != null) summaryTv.setText(Double.toString(summaryCostVal));
                if (summaryCurrency != null) summaryCurrency.setText(currency);

                if (AppParams.TAX > 0 && summaryCostVal != null) {
                    RelativeLayout flTax = (RelativeLayout) findViewById(R.id.rl_detail_tax);
                    TextView tvTax = (TextView) findViewById(R.id.tv_detailorder_complete_tax);
                    TextView tvTaxCost = (TextView) findViewById(R.id.tv_detailorder_complete_tax_cost);
                    flTax.setVisibility(View.VISIBLE);
                    String taxCost = getTaxFormCost(summaryCostVal, AppParams.TAX) + " " + currency;
                    String taxTitle = getString(R.string.activities_OrderDetailActivity_tax) + " "
                            + AppParams.TAX + "" + "%";
                    tvTax.setText(taxTitle);
                    tvTaxCost.setText(taxCost);
                }

                TextView tvPaid = (TextView) findViewById(R.id.tv_ordercompleted_paid);

                tvPaid.setText((order.getStatusId().equals(AppParams.STATUS_ID_COMPLETE_PAID) ?
                        getString(R.string.activities_OrderDetailActivity_paid).toUpperCase() :
                        getString(R.string.activities_OrderDetailActivity_no_paid).toUpperCase()));

                TextView paymentType = (TextView) findViewById(R.id.payment_type);
                TextView paymentCost = (TextView) findViewById(R.id.payment_summary_cost);
                TextView paymentCurrency = (TextView) findViewById(R.id.payment_summary_summary);
                paymentCost.setText(Double.toString(summaryCostVal));
                paymentCurrency.setText(currency);
                switch (order.getPaymentType()) {
                    case "CASH":
                        if (!isRtl)
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.wallet, 0, 0, 0);
                        else
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.wallet, 0);
                        paymentType.setText(R.string.activities_OrderDetailActivity_type_cash);
                        break;
                    case "PERSONAL_ACCOUNT":
                        if (!isRtl)
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parsonal, 0, 0, 0);
                        else
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.parsonal, 0);
                        paymentType.setText(R.string.activities_OrderDetailActivity_type_account);
                        break;
                    case "CORP_BALANCE":
                        if (!isRtl)
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.man, 0, 0, 0);
                        else
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.man, 0);
                        paymentType.setText(R.string.activities_OrderDetailActivity_type_corp);
                        break;
                    case "CARD":
                        if (!isRtl)
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.card, 0, 0, 0);
                        else
                            paymentType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.card, 0);
                        paymentType.setText(order.getPan());
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void praseMainOrderDatailsData(JSONObject jsonCost) {
        double costCity = 0.0;
        double costOutCity = 0.0;

        try {
            if (!jsonCost.isNull("city_cost_time"))
                costCity += Double.valueOf(jsonCost.getString("city_cost_time"));
            if (!jsonCost.isNull("city_cost"))
                costCity += Double.valueOf(jsonCost.getString("city_cost"));
            if (!jsonCost.isNull("out_city_cost_time"))
                costOutCity += Double.valueOf(jsonCost.getString("out_city_cost_time"));
            if (!jsonCost.isNull("out_city_cost"))
                costOutCity += Double.valueOf(jsonCost.getString("out_city_cost"));

            TextView cityLabelTv = (TextView)
                    findViewById(R.id.tv_detailorder_complete_city_label);
            String cityLabel = jsonCost.isNull("city_distance") ?
                    getString(R.string.activities_OrderDetailActivity_city_cost) + "  0 " +
                            getString(R.string.activities_OrderDetailActivity_distance) :
                    getString(R.string.activities_OrderDetailActivity_city_cost) + "  " +
                            jsonCost.getString("city_distance") + " " +
                            getString(R.string.activities_OrderDetailActivity_distance)
                            + "  " + formatTime(Double.valueOf(jsonCost.getString("city_time")));
            if (cityLabelTv != null) cityLabelTv.setText(cityLabel);

            TextView outCityLabelTv = (TextView)
                    findViewById(R.id.tv_detailorder_complete_outcity_label);
            String outCityLabel = jsonCost.isNull("out_city_distance")
                    ? getString(R.string.activities_OrderDetailActivity_out_city_cost) + "  0 " + getString(R.string.activities_OrderDetailActivity_distance)
                    : getString(R.string.activities_OrderDetailActivity_out_city_cost) + "  " + jsonCost.getString("out_city_distance") + " " +
                    getString(R.string.activities_OrderDetailActivity_distance)
                    + "  " + formatTime(Double.valueOf(jsonCost.getString("out_city_time")));
            if (outCityLabelTv != null) outCityLabelTv.setText(outCityLabel);

            TextView placeTv = (TextView) findViewById(R.id.tv_detailorder_complete_place);
            TextView placeCurrency = (TextView) findViewById(R.id.tv_detailorder_complete_place_currency);
            Double plantingValue = Validator.validateDouble(jsonCost, "planting_price");
            if (plantingValue < 0.0) plantingValue = 0.0;
            if (placeTv != null) placeTv.setText(Double.toString(plantingValue));
            if (placeCurrency != null) placeCurrency.setText(currency);

            TextView cityTv = (TextView) findViewById(R.id.tv_detailorder_complete_city);
            TextView cityCurrency = (TextView) findViewById(R.id.tv_detailorder_complete_city_currency);
            if (costCity < 0.0) costCity = 0.0;
            if (cityTv != null) cityTv.setText(String.valueOf(costCity));
            if (cityCurrency != null) cityCurrency.setText(currency);

            TextView outCityTv = (TextView) findViewById(R.id.tv_detailorder_complete_outcity);
            TextView outCityCurrency = (TextView) findViewById(R.id.tv_detailorder_complete_outcity_currency);
            if (costOutCity < 0.0) costOutCity = 0.0;
            if (outCityTv != null) outCityTv.setText(String.valueOf(costOutCity));
            if (outCityCurrency != null) outCityCurrency.setText(currency);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    public void sendReview(View v) {
        startActivityForResult(new Intent(this, ReviewActivity.class).putExtra("order_id", order.getOrderId()), ReviewActivity.REQUEST_CODE_ORDER_INFO);
    }

    private String label(Order order) {
        if (order.getStatus().equals("rejected")) {
            return getString(R.string.rejected);
        } else {
            return getString(R.string.completed) + " " + (order.getStatusId().equals(AppParams.STATUS_ID_COMPLETE_PAID) ? getString(R.string.activities_OrderDetailActivity_paid1) : getString(R.string.activities_OrderDetailActivity_no_paid1));
        }
    }

    private String getTaxFormCost(double summaryCost, double taxCost) {
        double result = summaryCost - (summaryCost / (1 + taxCost / 100));
        return new DecimalFormat("#0.00").format(result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ReviewActivity.REQUEST_CODE_ORDER_INFO:

                    (findViewById(R.id.btn_review)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.VISIBLE);

                    setRating(data.getIntExtra("rating", 0));

                    break;
            }
        }
    }

    private void setRating(int stars) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_stars);
        ImageView star1 = (ImageView) linearLayout.getChildAt(0);
        ImageView star2 = (ImageView) linearLayout.getChildAt(1);
        ImageView star3 = (ImageView) linearLayout.getChildAt(2);
        ImageView star4 = (ImageView) linearLayout.getChildAt(3);
        ImageView star5 = (ImageView) linearLayout.getChildAt(4);

        int[] drawablesArray = new int[5];
        for (int i = 0; i < 5; i++) {
            if (i <= (stars - 1)) {
                drawablesArray[i] = R.drawable.star_mini;
            } else {
                drawablesArray[i] = R.drawable.star_mini_grey;
            }
        }
        star1.setImageResource(drawablesArray[0]);
        star2.setImageResource(drawablesArray[1]);
        star3.setImageResource(drawablesArray[2]);
        star4.setImageResource(drawablesArray[3]);
        star5.setImageResource(drawablesArray[4]);
    }

    public void onClickShare(View v) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String sendText = getString(R.string.activities_OrderCompletedActivity_share, getString(R.string.app_name), mProfile.getAndroidUrl(), mProfile.getIosUrl());
        sendIntent.putExtra(Intent.EXTRA_TEXT, sendText);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.activities_OrderDetailActivity_share_title)));
    }

    public void onClickRepeat(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_OLD_ORDER);
        intent.putExtra("order_id", order.getOrderId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    public void onClickReverse(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_OLD_ORDER);
        intent.putExtra("order_id", order.getOrderId());
        intent.putExtra("reverse", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LastOrdersActivity.class));
        finish();
    }


    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(double time) {

        long mSec = Math.round(time * 60 * 1000);

        if (mSec > 0) {
            return String.format("%d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long) + ", %d " + getString(R.string.fragments_FragmentAbstractMap_time_second_long), TimeUnit.MILLISECONDS.toMinutes(mSec), TimeUnit.MILLISECONDS.toSeconds(mSec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mSec)));
        } else {
            return String.format("%d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long) + ", %d " + getString(R.string.fragments_FragmentAbstractMap_time_second_long), TimeUnit.MILLISECONDS.toMinutes(0), TimeUnit.MILLISECONDS.toSeconds(0) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(0)));
        }
    }

}
