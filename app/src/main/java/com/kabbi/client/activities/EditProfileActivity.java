package com.kabbi.client.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.ProfileEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.UpdateProfileRequestListener;
import com.kabbi.client.network.requests.UpdateProfileRequest;
import com.kabbi.client.utils.ImageEditor;
import com.kabbi.client.utils.InputMask;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import retrofit.mime.TypedFile;

import static android.provider.MediaStore.Images.Media.getBitmap;

public class EditProfileActivity
        extends BaseSpiceActivity implements TextView.OnEditorActionListener {

    public static final int REQUEST_CODE_PERMISSION_STORAGE = 5702;
    private static final int PHOTO_REQ_CODE = 533;
    public static final String EXTRA_REQUIRED = "EXTRA_REQUIRED";

    private SimpleDraweeView imgView;
    private SimpleDraweeView mEditImage;
    private Uri newImgUri;
    private Uri mUri;
    private boolean photoIsChanged;

    private Profile currentProfile;
    private String clientIdFromDB;
    private String imgUrlFromDB;
    private String nameFromDB;
    private String surnameFromDB;
    private String phoneMaskFromDB;
    private String phoneFromDB;
    private String emailFromDB;
    private File changeImage;

    private InputMask inputMask;
    private boolean mFormatting;

    private TextInputEditText editName;
    private TextInputEditText editSurname;
    private TextInputEditText editPhone;
    private TextInputEditText editEmail;

    private TextInputLayout editEmailLayout;
    private TextInputLayout editSurnameLayout;
    private TextInputLayout editNameLayout;

    private Button saveBtn;
    private boolean isRequiredData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_edit_profile);
        setTitle(R.string.activities_EditProfileActivity_title);

        if (getIntent().hasExtra(EXTRA_REQUIRED)) {
            isRequiredData = getIntent().getBooleanExtra(EXTRA_REQUIRED, false);
        }

        initToolbar();
        initVars();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_edit_profile);
        if (isRequiredData) {
            toolbar.setVisibility(View.GONE);
        } else {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) getSupportActionBar()
                    .setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initVars() {
        currentProfile = Profile.getProfile();
        clientIdFromDB = currentProfile.getClientId();
        imgUrlFromDB = currentProfile.getPhoto();
        nameFromDB = currentProfile.getName();
        surnameFromDB = currentProfile.getSurname();
        phoneMaskFromDB = currentProfile.getPhoneMask();
        phoneFromDB = currentProfile.getPhone();
        emailFromDB = currentProfile.getEmail();
    }

    private void initViews() {
        FloatingActionButton mMFabLeft = (FloatingActionButton) findViewById(R.id.fab_left);
        FloatingActionButton mMFabRight = (FloatingActionButton) findViewById(R.id.fab_right);

        imgView = (SimpleDraweeView) findViewById(R.id.edit_profile_img);
        editName = (TextInputEditText) findViewById(R.id.edit_profile_name);
        editSurname = (TextInputEditText) findViewById(R.id.edit_profile_surname);
        editPhone = (TextInputEditText) findViewById(R.id.edit_profile_phone);
        editEmail = (TextInputEditText) findViewById(R.id.edit_profile_email);

        editEmailLayout = (TextInputLayout) findViewById(R.id.edit_profile_email_layout);
        editSurnameLayout = (TextInputLayout) findViewById(R.id.edit_profile_surname_layout);
        editNameLayout = (TextInputLayout) findViewById(R.id.edit_profile_name_layout);

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, PHOTO_REQ_CODE);
            }
        });

        mMFabLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgView.setRotation(imgView.getRotation() - 90);
            }
        });

        mMFabRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgView.setRotation(imgView.getRotation() + 90);
            }
        });

        if (!imgUrlFromDB.isEmpty()) {
            mUri = Uri.parse(imgUrlFromDB);
            imgView.setImageURI(mUri);
        }
        editName.setText(nameFromDB);
        editSurname.setText(surnameFromDB);
        editEmail.setText(emailFromDB);
        editEmail.setOnEditorActionListener(this);
        setEditPhoneValues();
        setEditPhoneListeners();
        initWatchers();
        saveBtn = (Button) findViewById(R.id.edit_profile_save_btn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppParams.USE_PROFILE_DATA) {
                    boolean isValid = validateFields();
                    if (!isValid) return;
                }

                if (photoIsChanged) {
                    requestStorageWrapper();
                } else {
                    sendRequest();
                }
            }
        });

    }

    private void initWatchers() {
        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editEmailLayout.setErrorEnabled(false);
                editEmailLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });


        editSurname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editSurnameLayout.setErrorEnabled(false);
                editSurnameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editNameLayout.setErrorEnabled(false);
                editNameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean validateFields() {
        boolean isValid = true;
        if (TextUtils.isEmpty(editEmail.getText().toString().trim())) {
            editEmailLayout.setError(getString(R.string.activities_EditOrderActivity_invalid_email));
            isValid = false;
        } else {
            editEmailLayout.setErrorEnabled(false);
            editEmailLayout.setError(null);
        }

        if (TextUtils.isEmpty(editName.getText().toString().trim())) {
            editNameLayout.setError(getString(R.string.activities_EditOrderActivity_empty_field));
            isValid = false;
        } else {
            editNameLayout.setErrorEnabled(false);
            editNameLayout.setError(null);
        }

        if (TextUtils.isEmpty(editSurname.getText().toString().trim())) {
            editSurnameLayout.setError(getString(R.string.activities_EditOrderActivity_empty_field));
            isValid = false;
        } else {
            editSurnameLayout.setErrorEnabled(false);
            editSurnameLayout.setError(null);
        }
        return isValid;
    }

    private void setEditPhoneValues() {
        inputMask = new InputMask(phoneMaskFromDB, phoneFromDB);
        mFormatting = false;
        String newText = inputMask.getNewText();
        editPhone.setText(newText);
        editPhone.setSelection(inputMask.getSelection(newText));
    }

    private void setEditPhoneListeners() {
        editPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhone.setSelection(inputMask
                        .getSelection(editPhone.getText().toString().trim()));
                mFormatting = false;
            }
        });
        editPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            String phoneStr;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //empty
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                phoneStr = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    String result = inputMask.afterTextChangedSetText(phoneStr, s);
                    editPhone.setText(result);
                    editPhone.setSelection(inputMask.afterTextChangedSetSelection(result));
                } else {
                    mFormatting = false;
                }
            }
        });
    }

    @Subscribe
    public void onMessage(ProfileEvent event) {
        saveBtn.setEnabled(true);
        if (event.getSuccess() == 1) {
            event.getProfile().save();
            NavUtils.navigateUpFromSameTask(this);
        } else if (event.getSuccess() == 0) {
            new ToastWrapper(this, R.string.activities_EditProfileActivity_req_error).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_REQ_CODE && resultCode == RESULT_OK) {
            photoIsChanged = true;
            newImgUri = data.getData();
            imgView.setImageURI(newImgUri);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isRequiredData) return;
        super.onBackPressed();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            if (photoIsChanged) {
                requestStorageWrapper();
            } else {
                sendRequest();
            }
            return true;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestStorageWrapper() {
        int hasReadStoragePermission = ContextCompat
                .checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel(getString(R.string.activities_EditProfileActivity_permission_request),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(EditProfileActivity.this,
                                        new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                                        REQUEST_CODE_PERMISSION_STORAGE);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(EditProfileActivity.this,
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION_STORAGE);
            return;
        }
        sendRequest();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EditProfileActivity.this, R.style.MyCustomAlertDialogTheme)
                .setMessage(message)
                .setNegativeButton(
                        getString(R.string.activities_EditProfileActivity_permission_dialog_ok), okListener)
                .setPositiveButton(
                        getString(R.string.activities_EditProfileActivity_permission_dialog_cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendRequest();
                } else {
                    new ToastWrapper(this,
                            R.string.activities_EditProfileActivity_permission_rejected).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void sendRequest() {
        Log.d("Logos", "EditProfileActivity | sendRequest | : ");
        saveBtn.setEnabled(false);
        int countNum = phoneMaskFromDB
                .replaceAll("_","0")
                .replaceAll("\\D", "")
                .length();
        if (editPhone.getText().toString().trim()
                .replaceAll("\\D+", "").length() != countNum) {
            Log.d("Logos", "EditProfileActivity | sendRequest | : 1");
            editPhone.setError(getString(R.string.activities_AuthActivity_edit_phone_error));
            saveBtn.setEnabled(true);
        } else {
            boolean isRotate = imgView.getRotation() % 360 != 0;
            final String newPhone = editPhone.getText().toString().trim().replaceAll("\\D+", "");
            final String surname = editSurname.getText().toString().trim();
            final String name = editName.getText().toString().trim();
            final String email = editEmail.getText().toString().trim();
            final String time = String.valueOf((new Date()).getTime());
            TypedFile photo = null;
            Log.d("Logos", "EditProfileActivity | sendRequest | : 2");
            if (isRotate) {
                if (photoIsChanged) {
                    try {
                        int angle = (int) (imgView.getRotation() % 360);
                        changeImage = ImageEditor.rotate(this,
                                getBitmap(getContentResolver(), newImgUri),
                                angle);
                        photo = new TypedFile("multipart/form-data", changeImage);
                        saveData(newPhone, surname, name, email, time, photo);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    changeServerPhoto(new ImageObserver() {
                        @Override
                        public void onImageReady(TypedFile photo) {
                            saveData(newPhone, surname, name, email, time, photo);
                        }
                    });
                }
            } else {
                if (photoIsChanged) {
                    photo = new TypedFile("multipart/form-data",
                            new File(getRealPathFromURI(newImgUri)));
                    saveData(newPhone, surname, name, email, time, photo);
                } else {
                    saveData(newPhone, surname, name, email, time, null);
                }
            }
        }
    }

    private void changeServerPhoto(final ImageObserver imageObserver) {
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(mUri)
                .setProgressiveRenderingEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, this);

        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable Bitmap bitmap) {
                changeImage = ImageEditor.rotate(getBaseContext(), bitmap,
                        (int) (imgView.getRotation() % 360));
                TypedFile mTypedFile = new TypedFile("multipart/form-data", changeImage);
                imageObserver.onImageReady(mTypedFile);
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                // No cleanup required here.
            }

        }, CallerThreadExecutor.getInstance());

    }

    private void saveData(String newPhone, String surname, String name, String email, String time,
                          TypedFile photo) {
        currentProfile.setPhone(newPhone);
        currentProfile.save();
        getSpiceManager().execute(new UpdateProfileRequest(currentProfile.getAppId(),
                        currentProfile.getApiKey(),
                        getApplicationContext(),
                        clientIdFromDB, phoneFromDB, newPhone,
                        surname, name, email, time, photo, currentProfile.getTenantId()),
                new UpdateProfileRequestListener());
    }

    private String getRealPathFromURI(Uri contentURI)  {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private interface ImageObserver {
        void onImageReady(TypedFile typedFile);
    }

}
