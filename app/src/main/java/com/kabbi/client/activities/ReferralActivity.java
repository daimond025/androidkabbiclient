package com.kabbi.client.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.ActivateReferralEvent;
import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Referral;
import com.kabbi.client.network.listeners.ReferralSystemListListener;
import com.kabbi.client.network.listeners.ReferralSystemListener;
import com.kabbi.client.network.requests.GetReferralSystemListRequest;
import com.kabbi.client.network.requests.GetReferralSystemRequest;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ReferralActivity extends BaseSpiceActivity {

    public static final String BUFFER_REFERRAL = "referral";

    private ClipboardManager clipboard;
    private Profile mProfile;
    private Referral mReferral;
    private TextView mtTvCode;
    private Button mBtnShare;
    private TextView mTvReferralContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_referral);
        setTitle(R.string.activities_ReferralActivity_title);

        initVars();
        initView();
        initToolbar();
    }

    private void initVars() {
        clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mProfile = Profile.getProfile();
    }

    private void initView() {
        FrameLayout llReferralButton = (FrameLayout) findViewById(R.id.ll_referral_code);
        TextView tvAuthInfo = (TextView) findViewById(R.id.tv_referral_info);
        TextView tvCopy = (TextView) findViewById(R.id.tv_referral_copy);
        mtTvCode = (TextView) findViewById(R.id.tv_referral_code);
        mTvReferralContent = (TextView) findViewById(R.id.tv_referral_content);
        mBtnShare = (Button) findViewById(R.id.btn_send_referral);

        mBtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReferralShare();
            }
        });

        final TextView tvYourCode = (TextView) findViewById(R.id.tv_referral_your_code);


        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipData clip = ClipData.newPlainText(BUFFER_REFERRAL, mtTvCode.getText().toString());
                clipboard.setPrimaryClip(clip);
                new ToastWrapper(getBaseContext(), R.string.activities_ReferralActivity_referral_code_copied).show();
            }
        });

        if (mProfile.isAuthorized()) {
            tvAuthInfo.setVisibility(View.GONE);
            mReferral = Referral.getReferralByCityId(mProfile.getCityId());
            if (mReferral != null) {
                mBtnShare.setVisibility(View.VISIBLE);
                if (mReferral.getContent() != null && !mReferral.getContent().isEmpty())
                    mTvReferralContent.setText(mReferral.getContent());

                if (mReferral.getIsActive() && mReferral.getCode() != null) {
                    mtTvCode.setText(mReferral.getCode() );
                } else {
                    activateReferralSystem(mProfile.getPhone(), mProfile.getTenantId(), mReferral.getReferralId());
                }
            } else {
                new ToastWrapper(this, R.string.activities_ReferralActivity_referral_not_found).show();
                finish();
            }
        } else {
            tvYourCode.setVisibility(View.GONE);
            llReferralButton.setVisibility(View.GONE);
        }
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    public void onReferralShare() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String sendText = mReferral.getText();
        sendIntent.putExtra(Intent.EXTRA_TEXT, sendText);
        if (mReferral.getTitle() != null) sendIntent.putExtra(Intent.EXTRA_SUBJECT, mReferral.getTitle());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.activities_OrderDetailActivity_share_title)));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Subscribe
    public void onMessage(ActivateReferralEvent event) {
        if (event.getResult() == 1) {
            String code = event.getCode();
            mtTvCode.setText(code);
            mReferral.setCode(code);
            mReferral.setIsActive(true);
            mReferral.save();

            getSpiceManager().execute(new GetReferralSystemListRequest(
                    mProfile.getAppId(),
                    mProfile.getApiKey(),
                    getBaseContext(),
                    mProfile.getTenantId(),
                    mProfile.getPhone()), new ReferralSystemListListener(getBaseContext()));

        } else {
            new ToastWrapper(this, R.string.activities_ReferralActivity_referral_was_activated).show();
            if (mtTvCode.getText().toString().trim().isEmpty()) finish();
        }
    }

    @Subscribe
    public void onMessage(RequestCompletedEvent requestCompletedEvent) {
        int code = requestCompletedEvent.getRequestCode();
        if (code == RequestCompletedEvent.REFERRAL) {
            mReferral = Referral.getReferralByCityId(mProfile.getCityId());
            if (mReferral != null) {
                mBtnShare.setVisibility(View.VISIBLE);
                if (mReferral.getContent() != null && !mReferral.getContent().isEmpty())
                    mTvReferralContent.setText(mReferral.getContent());

                if (mReferral.getIsActive() && mReferral.getCode() != null) {
                    mtTvCode.setText(mReferral.getCode() );
                } else {
                    activateReferralSystem(mProfile.getPhone(), mProfile.getTenantId(), mReferral.getReferralId());
                }
            } else {
                new ToastWrapper(this, R.string.activities_ReferralActivity_referral_not_found).show();
                finish();
            }
        }
    }

    private void activateReferralSystem(String phone, String tenantId, String referral) {
        getSpiceManager().execute(new GetReferralSystemRequest(mProfile.getAppId(),
                mProfile.getApiKey(), this, phone, tenantId, referral),
                new ReferralSystemListener(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
