package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.Constants;
import com.kabbi.client.events.api.client.ActivateBonusSystemEvent;
import com.kabbi.client.events.api.client.CheckCardEvent;
import com.kabbi.client.events.api.client.ClientBalancesEvent;
import com.kabbi.client.events.api.client.ClientCardsEvent;
import com.kabbi.client.events.api.client.CreateCardEvent;
import com.kabbi.client.events.api.client.DeleteCardEvent;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Referral;
import com.kabbi.client.network.listeners.ActivateBonusSystemListener;
import com.kabbi.client.network.listeners.CheckCardListener;
import com.kabbi.client.network.listeners.ClientBalancesListener;
import com.kabbi.client.network.listeners.ClientCardsListener;
import com.kabbi.client.network.listeners.CreateCardListener;
import com.kabbi.client.network.requests.GetClientBalances;
import com.kabbi.client.network.requests.GetClientCards;
import com.kabbi.client.network.requests.PostActivateBonusSystem;
import com.kabbi.client.network.requests.PostCheckClientCard;
import com.kabbi.client.network.requests.PostCreateClientCard;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.views.ToastWrapper;
import com.kabbi.client.views.adapters.PaymentAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.kabbi.client.app.AppParams.USE_REFERRAL;


public class PaymentActivity extends BaseSpiceActivity {

    public static final String URL_CREATE_CARD = "webUrl";
    public static final String SUPPORT_CITY = "SUPPORT_CITY";
    private static final String TAG = PaymentActivity.class.getSimpleName();

    private SwitchCompat scPaymentBonus;
    private Profile profile;
    private boolean isAuthorized;
    private String clientId;
    private String phone;
    private String cardOrderId;

    private List<PaymentType> payments;
    private PaymentAdapter mPaymentAdapter;
    private ListView mPaymentList;
    private TextInputEditText promo;
    private AppPreferences mAppPreferences;
    private TextView tvBonusCost;

    private ProgressDialog mCreateCardProgressDialog;
    private ProgressDialog mTieChardDialog;
    private ProgressBar mPbLoadPayments;

    private Handler mCheckHandler;
    private Runnable mCheckRunnable;

    private boolean isEditOrder = false;
    private boolean isSupportedCity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_payment);
        setTitle(R.string.activities_PaymentActivity_title);
        isSupportedCity = getIntent().getBooleanExtra(SUPPORT_CITY, false);
        Log.d("Logos", "isSupportedCity " + isSupportedCity);

        initToolbar();
        initVars();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cardOrderId != null) {
            getSpiceManager().execute(new PostCheckClientCard(profile.getAppId(),
                            profile.getApiKey(),
                            clientId,
                            String.valueOf((new Date()).getTime()),
                            cardOrderId, phone, profile.getTenantId(), AppParams.TYPE_CLIENT),
                    new CheckCardListener());
            cardOrderId = null;
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initVars() {
        EventBus.getDefault().register(this);
        profile = Profile.getProfile();
        mAppPreferences = new AppPreferences(this);
        isAuthorized = profile.isAuthorized();
        clientId = profile.getClientId();
        phone = profile.getPhone();
        payments = PaymentType.getPayments();

        mCreateCardProgressDialog = new ProgressDialog(this);
        mCreateCardProgressDialog.setMessage(getString(R.string.activities_CardActivity_text_load));

        mTieChardDialog = new ProgressDialog(this);
        mTieChardDialog.setMessage(getString(R.string.activities_PaymentActivity_request_tie_card));

        isEditOrder = getIntent().getBooleanExtra(EditOrderActivity.EXTRA_EDIT_PAYMENT, false);
        Log.d(TAG, "Logos initVars isEditOrder: " + isEditOrder);
        if (isEditOrder) {
            mPaymentAdapter = new PaymentAdapter(payments, Order.getActiveOrder(), getSpiceManager(), this);
        } else {
            mPaymentAdapter = new PaymentAdapter(payments, profile, getSpiceManager(), this);
        }

        mPaymentAdapter.setPaymentDataListener(new PaymentAdapter.PaymentDataDidChanged() {
            @Override
            public void onChangeData(String currentPaymentType, String currentCompany, String currentPan) {
                if (!isEditOrder) {
                    saveProfilePayment(profile, currentPaymentType, currentCompany, currentPan);
                }
            }
        });
    }

    private void initViews() {
        mPbLoadPayments = (ProgressBar) findViewById(R.id.pb_load_payments);
        TextView paymentAddCard = (TextView) findViewById(R.id.payment_add_card);
        TextView inputCode = (TextView) findViewById(R.id.tv_payment_input_referral);

        RelativeLayout rlAdditionalContainer = (RelativeLayout) findViewById(R.id.payment_card_layout);
        if (isEditOrder) rlAdditionalContainer.setVisibility(View.GONE);


        FrameLayout flPaymentBonus = (FrameLayout) findViewById(R.id.fl_payment_bonus);
        if (!AppParams.PAYMENT_WITH_BONUS) flPaymentBonus.setVisibility(View.GONE);


        mPaymentList = (ListView) findViewById(R.id.rv_payment_types);
        mPaymentList.setAdapter(mPaymentAdapter);

        scPaymentBonus = (SwitchCompat) findViewById(R.id.sc_payment_bonus);
        tvBonusCost = (TextView) findViewById(R.id.tv_bonus_cost);
        tvBonusCost.setText(profile.getBonusValue());


        flPaymentBonus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!scPaymentBonus.isChecked())
                    if (profile.getBonusId() == Profile.BONUS_TYPE_UDS_GAME &&
                            profile.getBonusUdsState() == Profile.BONUS_UDS_STATUS_NONACTIVATE) {
                        showActivateUdsDialog();
                    } else scPaymentBonus.setChecked(true);
                else scPaymentBonus.setChecked(false);
            }
        });

        if (!USE_REFERRAL) inputCode.setVisibility(View.GONE);

        inputCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAuthorized) {
                    new ToastWrapper(PaymentActivity.this,
                            R.string.activities_PaymentActivity_add_card_auth_error).show();
                    return;
                }

                startActivity(new Intent(getBaseContext(), ActivateCodeActivity.class));
            }
        });

        if (AppParams.PAYMENT_WITH_CARDS) {
            if (paymentAddCard != null) paymentAddCard
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (isAuthorized) {
                                mCreateCardProgressDialog.show();
                                getSpiceManager().execute(new PostCreateClientCard(profile.getAppId(),
                                                profile.getApiKey(),
                                                clientId,
                                                String.valueOf((new Date()).getTime()),
                                                phone, profile.getTenantId(), AppParams.TYPE_CLIENT),
                                        new CreateCardListener());
                            }
                            else {
                                new ToastWrapper(PaymentActivity.this,
                                        R.string.activities_PaymentActivity_add_card_auth_error).show();
                            }
                        }
                    });
        }
        else if (paymentAddCard != null) paymentAddCard.setVisibility(View.GONE);

        if (scPaymentBonus != null)
            if (profile.getPaymentBonus() == Profile.BONUS_ACTIVE)
                scPaymentBonus.setChecked(true);
    }

    /**
     Set payment list height before view appeared
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        setListRealHeight();
    }

    public void startUpdatePayments() {
        if (isAuthorized) {
            mPbLoadPayments.setVisibility(View.VISIBLE);
            getSpiceManager().execute(new GetClientBalances(profile.getAppId(), profile.getApiKey(),
                            this,
                            profile.getTenantId(),
                            profile.getCityId(), Profile.getProfile().getPhone()),
                    new ClientBalancesListener(this));
        } else mPaymentAdapter.updateCurrentPayment(profile.getPaymentType());
    }

    /**
     Dynamic change the height
     */
    public void setListRealHeight() {
        int totalHeight = 0;
        for (int i = 0; i < mPaymentAdapter.getCount(); i++) {
            View mView = mPaymentAdapter.getView(i, null, mPaymentList);
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            totalHeight += mView.getMeasuredHeight() + mPaymentList.getDividerHeight();
            Log.d("Logos", "setListRealHeight: " +  totalHeight);
        }
        mPaymentList.getLayoutParams().height = totalHeight;
        mPaymentList.requestLayout();
    }


    private void saveProfilePayment(Profile profile, String currentPaymentType, String currentCompany, String currentPan) {
        profile.setPan(currentPan);
        profile.setCompany(currentCompany);
        profile.setPaymentType(currentPaymentType);
        if (currentPaymentType.equals("CORP_BALANCE")) profile.setClientType("company");
        else profile.setClientType("base");
        profile.save();
    }


    public void showActivateUdsDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_promo_code, null);
        promo = (TextInputEditText) view.findViewById(R.id.et_detail_bonus);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.MyCustomAlertDialogTheme)
                .setTitle(getString(R.string.activities_PaymentActivity_activate_uds_system))
                .setPositiveButton(getString(R.string.activities_PaymentActivity_dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    if (promo != null) {
                                        getSpiceManager().execute(new PostActivateBonusSystem(
                                                profile.getAppId(),
                                                profile.getApiKey(),
                                                profile.getTenantId(),
                                                profile.getPhone(), promo.getText().toString()), new ActivateBonusSystemListener());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getString(R.string.activities_PaymentActivity_dialog_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        })
                .setView(view);
        AlertDialog mActivateBonusSystemDialog = alertDialog.create();
        mActivateBonusSystemDialog.show();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (mCheckHandler != null)
            mCheckHandler.removeCallbacks(mCheckRunnable);
        super.onDestroy();
    }

    /**
     Update payment list after request
     */
    @Subscribe
    public void onMessage(ClientBalancesEvent clientBalancesEvent) {
        mAppPreferences.saveText(Constants.AP_SAVE_PAYMENTS, String.valueOf(new Date().getTime()));
        mPbLoadPayments.setVisibility(View.GONE);
        payments = PaymentType.getPayments();
        mPaymentAdapter.updateCurrentPayment(profile.getPaymentType());
        mPaymentAdapter.updateData(payments);
        tvBonusCost.setText(profile.getBonusValue());
        setListRealHeight();
    }

    @Subscribe
    public void onMessage(ClientCardsEvent event) {
        if (mTieChardDialog.isShowing()) {
            mTieChardDialog.dismiss();
            new ToastWrapper(this, R.string.activities_PaymentActivity_request_tie_card_success).show();
        }

        PaymentType.deleteCardPayments();
        saveProfilePayment(profile,
                mPaymentAdapter.getCurrentPaymentType(),
                mPaymentAdapter.getCurrentCompany(),
                mPaymentAdapter.getCurrentPan());
        ArrayList<String> cards = event.getClientCards();

        if (cards != null && !cards.isEmpty()) {
            for (int i = 0; i < cards.size(); i++) {
                String pan = cards.get(i);
                PaymentType.addCardPayment(pan);
            }
            profile.setPan(cards.get(cards.size() - 1));
            profile.save();
        }
        payments = PaymentType.getPayments();
        mPaymentAdapter.updateData(payments);
        setListRealHeight();
    }

    @Subscribe
    public void onMessage(DeleteCardEvent event) {
        if (event.getMessage().equals("true")) {
            PaymentType.deletePaymentByID(event.getPaymentID());
            new ToastWrapper(this, R.string.activities_PaymentActivity_success_remove_card).show();
            payments = PaymentType.getPayments();
            mPaymentAdapter.updateData(payments);
            setListRealHeight();
        }
        else new ToastWrapper(this, R.string.activities_PaymentActivity_error_remove_card).show();
    }

    @Subscribe
    public void onMessage(ActivateBonusSystemEvent activateBonusSystemEvent) {
        switch (activateBonusSystemEvent.getActivateId()) {
            case Profile.BONUS_UDS_STATUS_NONACTIVATE:
                new ToastWrapper(this, R.string.activities_PaymentActivity_activate_bonus_error).show();
                break;
            case Profile.BONUS_UDS_STATUS_ACTIVATE:
                scPaymentBonus.setChecked(true);
                profile.setBonusUdsState(Profile.BONUS_UDS_STATUS_ACTIVATE);
                new ToastWrapper(this, R.string.activities_PaymentActivity_activate_bonus_success).show();
                break;
        }
    }

    @Subscribe
    public void onMessage(CreateCardEvent pageEvent) {
        mCreateCardProgressDialog.dismiss();
        if (!pageEvent.hasError()) {
            cardOrderId = pageEvent.getOrderId();
            String webPageString = pageEvent.getWebPageString();
            if (!webPageString.isEmpty()) {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(Color.WHITE);
                builder.setShowTitle(true);
                CustomTabsIntent customTabsIntent = builder.build();
                try {
                    customTabsIntent.launchUrl(this, Uri.parse("googlechrome://navigate?url=" + webPageString));
                } catch (Exception e) {
                    customTabsIntent.launchUrl(this, Uri.parse(webPageString));
                }
            }
        } else {
            new ToastWrapper(this,
                    R.string.activities_PaymentActivity_add_card_error).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }


    @Subscribe
    public void onMessage(CheckCardEvent event) {
        int resultCode = event.getResultCode();
        switch (resultCode) {
            case 0:
                getSpiceManager().execute(new GetClientCards(profile.getAppId(),
                                profile.getApiKey(), clientId,
                                String.valueOf((new Date()).getTime()),
                                phone, profile.getTenantId(), AppParams.TYPE_CLIENT),
                        new ClientCardsListener());
                new ToastWrapper(this,
                        R.string.activities_PaymentActivity_add_card_success).show();
                break;
            case 1:
                new ToastWrapper(this,
                        R.string.activities_PaymentActivity_add_card_error).show();
                break;
            case 20:
                startTieRequest();
                break;
        }
    }


    private void startTieRequest() {
        mTieChardDialog.show();
        if (mCheckHandler != null && mCheckRunnable != null) {
            mCheckHandler.removeCallbacks(mCheckRunnable);
        }
        mCheckHandler = new Handler();
        mCheckRunnable = new Runnable() {
            @Override
            public void run() {
                getSpiceManager().execute(new GetClientCards(profile.getAppId(),
                                profile.getApiKey(), clientId,
                                String.valueOf((new Date()).getTime()),
                                phone, profile.getTenantId(), AppParams.TYPE_CLIENT),
                        new ClientCardsListener());

            }
        };
        mCheckHandler.postDelayed(mCheckRunnable, 5000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_item_refresh:
                startUpdatePayments();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (scPaymentBonus.isChecked()) {
            profile.setPaymentBonus(Profile.BONUS_ACTIVE);
        } else {
            profile.setPaymentBonus(Profile.BONUS_INACTIVE);
        }

        profile.save();

        if (isEditOrder) {
            Intent intent = new Intent();
            intent.putExtra(EditOrderActivity.EXTRA_PAYMENT_TYPE, mPaymentAdapter.getCurrentPaymentType());
            intent.putExtra(EditOrderActivity.EXTRA_COMPANY_ID, mPaymentAdapter.getCurrentCompany());
            intent.putExtra(EditOrderActivity.EXTRA_PAN, mPaymentAdapter.getCurrentPan());
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Intent returnIntent = new Intent(this, MainActivity.class);
            returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_PAYMENT);
            NavUtils.navigateUpTo(this, returnIntent);
        }
    }


}

