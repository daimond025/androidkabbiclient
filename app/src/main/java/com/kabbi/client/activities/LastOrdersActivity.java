package com.kabbi.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;

import com.kabbi.client.R;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.views.adapters.LastOrdersRecyclerAdapter;

import java.util.List;

public class LastOrdersActivity extends AppCompatActivity {

    private List<Order> orderList;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_last_orders);
        setTitle(R.string.activities_LastOrdersActivity_title);
        initToolbar();

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_lastorders);
        assert mRecyclerView != null;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orderList = Order.getOrders();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int size = mRecyclerView.getMeasuredWidth();
        LastOrdersRecyclerAdapter adapter = new LastOrdersRecyclerAdapter(this, orderList, "now", size);
        if (orderList.size() > 0) {
            mRecyclerView.setAdapter(adapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            View view = findViewById(R.id.rl_last_orders_empty);
            if (view != null) view.setVisibility(View.VISIBLE);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        startActivity(returnIntent);
    }

}
