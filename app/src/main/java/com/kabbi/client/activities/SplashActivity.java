package com.kabbi.client.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.DeepParams;
import com.kabbi.client.events.api.client.AppUpdateEvent;
import com.kabbi.client.events.api.client.ClientCardsEvent;
import com.kabbi.client.events.api.client.SaveCitiesEvent;
import com.kabbi.client.models.DeepOrder;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.CitiesRequestListener;
import com.kabbi.client.network.listeners.ClientProfileListener;
import com.kabbi.client.network.listeners.DownloadLinksListener;
import com.kabbi.client.network.listeners.PingListener;
import com.kabbi.client.network.listeners.ReferralSystemListListener;
import com.kabbi.client.network.requests.GetClientProfileRequest;
import com.kabbi.client.network.requests.GetDownloadLinksRequest;
import com.kabbi.client.network.requests.GetPingRequest;
import com.kabbi.client.network.requests.GetReferralSystemListRequest;
import com.kabbi.client.network.requests.GetTenantCityListRequest;
import com.kabbi.client.services.RegistrationIntentService;
import com.kabbi.client.services.RegistrationJobIntentService;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.utils.LocaleHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import static com.kabbi.client.app.AppParams.DEFAULT_CALENDAR;
import static com.kabbi.client.app.AppParams.DEFAULT_LANG;
import static com.kabbi.client.app.AppParams.IS_DEMO;
import static com.kabbi.client.app.AppParams.USE_REFERRAL;
import static com.kabbi.client.app.AppParams.FILL_SPLASH;


public class SplashActivity extends BaseSpiceActivity {

    private static final int SPLASH_DELAY = 15000;
    private Handler splashHandler;

    private Profile profile;
    private String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isServicesAvailable, bDeviceToken, bCityRequest;
    private AlertDialog mErrorDialog, mUpdateDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                finish();
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
            );
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean("sentTokenToServer", false);
                if (sentToken) {
                    try {
                        regId = intent.getExtras().getString("token", "empty");
                        new AppPreferences(getApplicationContext()).saveText("device_token", regId);
                        bDeviceToken = true;
                        if (bCityRequest) {
                            nextActivity();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        setupSplash();
        initDB();
        updatePaymentTypes();
        startRequests();
        startPlayServices();
        initHandler();
    }

    private void initDB() {
        profile = Profile.getProfile();
        if (profile == null) {
            LocaleHelper.onCreate(this, DEFAULT_LANG);
            profile = new Profile();
            profile.setCityId("0");
            profile.setMap(AppParams.DEFAULT_MAP);
            profile.setThemeId(AppParams.DEFAULT_THEME);
            profile.setCalendar(DEFAULT_CALENDAR);
            profile.setTenantId(AppParams.TENANT_ID);
            profile.setApiKey(AppParams.API_KEY);
            profile.setAppId(AppParams.APP_ID);
            if (AppParams.PAYMENT_WITH_CASH) profile.setPaymentType("CASH");
            else profile.setPaymentType("CARD");
        }

        if (TextUtils.isEmpty(profile.getApiKey())
                || TextUtils.isEmpty(profile.getAppId())
                || TextUtils.isEmpty(profile.getTenantId())) {
            profile.setApiKey(AppParams.API_KEY);
            profile.setAppId(AppParams.APP_ID);
            profile.setTenantId(AppParams.TENANT_ID);
        }

        if (!IS_DEMO) profile.setThemeId(AppParams.DEFAULT_THEME);
        profile.save();

        if (PaymentType.getPayments().isEmpty()
                && AppParams.PAYMENT_WITH_CASH) {
                if (AppParams.PAYMENT_WITH_CASH)
                    PaymentType.addCashPayment("CASH", "CASH");
            }
        }


    private void startRequests() {
        getSpiceManager().execute(new GetTenantCityListRequest(profile.getAppId(), profile.getApiKey(),
                        getApplicationContext(),
                        String.valueOf((new Date()).getTime()), profile.getTenantId()),
                new CitiesRequestListener());

        getSpiceManager().execute(new GetPingRequest(profile.getAppId(), profile.getApiKey(),  this, profile.getTenantId()),
                new PingListener());

        getSpiceManager().execute(new GetDownloadLinksRequest(this, profile.getAppId(), profile.getApiKey(), profile.getTenantId()),
                new DownloadLinksListener());

        if (USE_REFERRAL) {
            getSpiceManager().execute(new GetReferralSystemListRequest(profile.getAppId(), profile.getApiKey(),
                    this, profile.getTenantId(),
                    profile.getPhone()), new ReferralSystemListListener(getBaseContext()));
        }
    }

    private void startPlayServices() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int resultCode = api.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            final int requestCode = 1;
            Dialog errorDialog = api.getErrorDialog(this, resultCode, requestCode);
            errorDialog.setCancelable(false);
            errorDialog.show();
        } else {
            isServicesAvailable = true;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            } else {
                Intent intent = new Intent(this, RegistrationJobIntentService.class);
                RegistrationJobIntentService.enqueueWork(this, intent);
            }
        }
    }

    private void initHandler() {
        if (splashHandler != null) splashHandler.removeCallbacksAndMessages(null);
        splashHandler = new Handler();
        splashHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isServicesAvailable && (!bCityRequest || !bDeviceToken)) {
                    showErrorDialog(getString(R.string.activities_SplashActivity_dialog_title),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing here because we override this button later to change the close behaviour.
                                    // However, we still need this because on older versions of Android unless we
                                    // pass a handler the button doesn't get instantiated
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // As well as the last
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!bCityRequest) startRequests();
                                    if (!bDeviceToken) startPlayServices();
                                    initHandler();
                                    updatePaymentTypes();
                                }
                            });
                }
            }
        }, SPLASH_DELAY);
    }

    private void showErrorDialog(String message,
                                 DialogInterface.OnClickListener settingsListener,
                                 DialogInterface.OnClickListener quitListener,
                                 DialogInterface.OnClickListener repeatListener) {
        if (mUpdateDialog != null && mUpdateDialog.isShowing()) return;
        mErrorDialog = new AlertDialog.Builder(SplashActivity.this, R.style.MyCustomAlertDialogTheme)
                .setMessage(message)
                .setNegativeButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_settings), settingsListener)
                .setPositiveButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_quit), quitListener)
                .setNeutralButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_repeat), repeatListener)
                .setCancelable(false)
                .create();
        mErrorDialog.show();
        // Overriding standard behaviour
        mErrorDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_SETTINGS));
            }
        });
        mErrorDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Subscribe
    public void onMessage(AppUpdateEvent event) {
        if (splashHandler != null) splashHandler.removeCallbacks(null);
        mUpdateDialog = new AlertDialog.Builder(SplashActivity.this, R.style.MyCustomAlertDialogTheme)
                .setMessage(R.string.activities_SplashActivity_dialog_update_title)
                .setNegativeButton(R.string.activities_SplashActivity_dialog_btn_quit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SplashActivity.this.finish();
                    }
                })
                .setPositiveButton(R.string.activities_SplashActivity_dialog_btn_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openPlayStorePage(SplashActivity.this);
                    }
                })
                .setCancelable(false)
                .create();
        mUpdateDialog.show();
    }

    private void nextActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void updatePaymentTypes() {
        if (profile != null && profile.isAuthorized()) {
            getSpiceManager().execute(new GetClientProfileRequest(
                    profile.getAppId(), profile.getApiKey(),
                    profile.getPhone(), profile.getTenantId()),
                    new ClientProfileListener(this));
        }
    }

    @Subscribe
    public void onMessage(SaveCitiesEvent saveCitiesEvent) {
        if (saveCitiesEvent.isSaveCities()) {
            bCityRequest = true;
            nextActivity();
            if (bDeviceToken) {
            }
        }
    }

    @Subscribe
    public void onMessage(ClientCardsEvent event) {
        PaymentType.deleteCardPayments();
        ArrayList<String> cards = event.getClientCards();
        if (cards != null && !cards.isEmpty()) {
            for (int i = 0; i < cards.size(); i++) {
                String pan = cards.get(i);
                PaymentType.addCardPayment(pan);
            }
            profile.setPan(cards.get(cards.size() - 1));
            profile.save();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mRegistrationBroadcastReceiver,
                        new IntentFilter("registrationComplete"));
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mErrorDialog != null) mErrorDialog.dismiss();
        if (splashHandler != null) splashHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (splashHandler != null) splashHandler.removeCallbacks(null);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void setupSplash() {
        ImageView ivSplash = findViewById(R.id.iv_splash);
        ImageView ivSplashFull = findViewById(R.id.iv_full_splash);
        if (FILL_SPLASH) {
            ivSplash.setVisibility(View.GONE);
            ivSplashFull.setVisibility(View.VISIBLE);
        } else {
            ivSplash.setVisibility(View.VISIBLE);
            ivSplashFull.setVisibility(View.GONE);
        }
    }

    public static void openPlayStorePage(Context context) {
        String packageName = context.getPackageName();
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + packageName));
        boolean marketFound = false;

        final List<ResolveInfo> otherApps = context.getPackageManager()
                .queryIntentActivities(intent, 0);
        for (ResolveInfo otherApp: otherApps) {
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.vending")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                intent.setComponent(componentName);
                context.startActivity(intent);
                marketFound = true;
                break;
            }
        }

        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
            context.startActivity(webIntent);
        }
    }

}
