package com.kabbi.client.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.OrdersInfoEvent;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.OrdersInfoListener;
import com.kabbi.client.network.requests.GetOrdersInfoRequest;
import com.kabbi.client.views.adapters.LastOrdersRecyclerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.List;

public class PreOrdersActivity extends BaseSpiceActivity {

    private LastOrdersRecyclerAdapter mAdapter;
    private Handler getUpdateTimeHandler;
    private Runnable getUpdateTimeRunnable;

    private List<Order> orderList;
    private RecyclerView mRecyclerView;
    private StringBuilder sbOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_pre_orders);
        setTitle(R.string.activities_PreOrdersActivity_title);
        EventBus.getDefault().register(this);
        initToolbar();

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_lastorders);
        assert mRecyclerView != null;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orderList = Order.getPreOrders();

        sbOrders = new StringBuilder();
        for (int i = 0; i < orderList.size(); i++) {
            if (i == orderList.size() - 1) {
                sbOrders.append(orderList.get(i).getOrderId());
            } else {
                sbOrders.append(orderList.get(i).getOrderId());
                sbOrders.append(",");
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int size = mRecyclerView.getMeasuredWidth();
        mAdapter = new LastOrdersRecyclerAdapter(this, orderList, "pre", size);
        if (orderList.size() > 0) {
            mRecyclerView.setAdapter(mAdapter);
            updateTimeToOrder();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            View view = findViewById(R.id.rl_last_orders_empty);
            if (view != null) view.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onMessage(OrdersInfoEvent event) {
        mAdapter.setOrderList(event.getList());
    }

    private void updateTimeToOrder() {
        if (getUpdateTimeHandler != null && getUpdateTimeRunnable != null) {
            getUpdateTimeHandler.removeCallbacks(getUpdateTimeRunnable);
        }
        getUpdateTimeHandler = new Handler();
        getUpdateTimeRunnable = new Runnable() {
            @Override
            public void run() {
               // mAdapter.notifyDataSetChanged();
                Profile profile = Profile.getProfile();
                getSpiceManager().execute(new GetOrdersInfoRequest(profile.getAppId(), profile.getApiKey(),
                        getBaseContext(), sbOrders.toString(),
                        Profile.getProfile().getTenantId()), new OrdersInfoListener());
                getUpdateTimeHandler.postDelayed(getUpdateTimeRunnable, 30000);
            }
        };
        getUpdateTimeHandler.post(getUpdateTimeRunnable);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER);
        startActivity(returnIntent);
    }

    @Override
    protected void onStop() {
        if (getUpdateTimeHandler != null)
            getUpdateTimeHandler.removeCallbacks(getUpdateTimeRunnable);
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
