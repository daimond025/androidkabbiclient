package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kabbi.client.R;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.ConfidentialListener;
import com.kabbi.client.network.listeners.CreateCardListener;
import com.kabbi.client.network.listeners.OfferPageEvent;
import com.kabbi.client.network.requests.GetConfidentialRequest;
import com.kabbi.client.network.requests.PostCreateClientCard;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;


public class OfferActivity extends BaseSpiceActivity {

    private Profile profile;
    private WebView mWebView;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_offer);
        EventBus.getDefault().register(this);
        initToolbar();
        initView();
        initVars();
    }


    private void initView() {
        mWebView = (WebView) findViewById(R.id.wv_offer_page);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

            }

        });

    }

    private void initVars() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.activities_CardActivity_text_load));

        profile = Profile.getProfile();
        getSpiceManager().execute(new GetConfidentialRequest(profile.getAppId(), profile.getApiKey(),  this, Profile.getProfile().getTenantId()),
                new ConfidentialListener());
    }


    @Override
    protected void onResume() {
        super.onResume();
        progressDialog.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
        progressDialog.cancel();
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.activities_OfferActivity_title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Subscribe
    public void onMessage(OfferPageEvent offerPageEvent) {
        progressDialog.dismiss();
        String url = offerPageEvent.url;
        if (url != null && !url.isEmpty()) {
            mWebView.loadDataWithBaseURL(null, url, "text/html", "UTF-8", null);
        } else new ToastWrapper(this, offerPageEvent.code);
    }
}
