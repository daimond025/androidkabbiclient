package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kabbi.client.R;
import com.kabbi.client.models.Profile;

import static com.kabbi.client.app.AppParams.WEB_APPLICATION_TITLE;
import static com.kabbi.client.app.AppParams.WEB_APPLICATION_URL;

public class WebAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_web_app);
        setTitle(WEB_APPLICATION_TITLE);

        prepareToolbar();
        initViews();
    }

    private void initViews() {
        WebView webView = (WebView) findViewById(R.id.web_application);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(WEB_APPLICATION_URL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void prepareToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }


}
