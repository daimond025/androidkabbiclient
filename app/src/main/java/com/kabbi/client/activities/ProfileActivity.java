package com.kabbi.client.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.InputMask;

import static com.kabbi.client.app.AppParams.SHOWN_CLIENT_ID;

public class ProfileActivity extends AppCompatActivity {

    private String imgUrlFromDB;
    private String fullNameFromDB;
    private String emailFromDB;
    private String phoneFromDB = "";
    private Profile mCurrentProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_profile);
        setTitle("");

        initToolbar();
        initVars();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initVars() {
        mCurrentProfile = Profile.getProfile();
        imgUrlFromDB = mCurrentProfile.getPhoto();
        fullNameFromDB = mCurrentProfile.getName() + " " + mCurrentProfile.getSurname();
        emailFromDB = mCurrentProfile.getEmail();
        String netPhone = mCurrentProfile.getPhone();

        if (!netPhone.isEmpty()) phoneFromDB = InputMask
                .getProfilePhone(mCurrentProfile.getPhoneMask(), netPhone);
    }

    private void initViews() {
        SimpleDraweeView imgView = (SimpleDraweeView) findViewById(R.id.profile_photo);
        TextView profileName = (TextView) findViewById(R.id.profile_name);
        TextView profileEmail = (TextView) findViewById(R.id.profile_email);
        TextView profilePhone = (TextView) findViewById(R.id.profile_phone);
        TextView tvProfileId = (TextView) findViewById(R.id.tv_profile_client_id);
        LinearLayout llProfileId = (LinearLayout) findViewById(R.id.ll_profile_layout_client_id);

        if (!imgUrlFromDB.isEmpty()) {
            Uri uri = Uri.parse(imgUrlFromDB);
            if (imgView != null) imgView.setImageURI(uri);
        }
        if (!fullNameFromDB.trim().isEmpty() && profileName != null) profileName.setText(fullNameFromDB);
        if (!emailFromDB.isEmpty() && profileEmail != null) {
            profileEmail.setTextColor(ContextCompat.getColor(this, R.color.textColorBlack));
            profileEmail.setText(emailFromDB);
        }
        if (!phoneFromDB.isEmpty() && profilePhone != null) {
            profilePhone.setTextColor(ContextCompat.getColor(this, R.color.textColorBlack));
            profilePhone.setText(phoneFromDB);
        }

        if (SHOWN_CLIENT_ID) llProfileId.setVisibility(View.VISIBLE); else llProfileId.setVisibility(View.GONE);
        tvProfileId.setText(mCurrentProfile.getClientId());

        TextView offer = (TextView) findViewById(R.id.tv_profile_offer);
        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), OfferActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_profile:
                Intent editIntent = new Intent(this, EditProfileActivity.class);
                startActivity(editIntent);
                return true;
            case R.id.action_logout_profile:
                PaymentType.deleteAllPayments();
                Profile profile = Profile.getProfile();
                profile.clearProfileFields();
                profile.save();
                Intent authIntent = new Intent(this, AuthActivity.class);
                startActivity(authIntent);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent backIntent = new Intent(this, MainActivity.class);
        backIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_PROFILE);
        NavUtils.navigateUpTo(this, backIntent);
    }

}
