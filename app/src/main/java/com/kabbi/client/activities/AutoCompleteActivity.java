package com.kabbi.client.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.geo.AutoCompleteEvent;
import com.kabbi.client.events.api.geo.FullReverseEvent;
import com.kabbi.client.events.api.geo.SearchEvent;
import com.kabbi.client.events.ui.AddressSelectedEvent;
import com.kabbi.client.fragments.AddressesFragment;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.geo_listeners.AutoCompleteListener;
import com.kabbi.client.network.listeners.geo_listeners.FullReverseListener;
import com.kabbi.client.network.listeners.geo_listeners.SearchListener;
import com.kabbi.client.network.requests.GetAutoCompleteRequest;
import com.kabbi.client.network.requests.GetReverseRequest;
import com.kabbi.client.network.requests.GetSearchRequest;
import com.kabbi.client.utils.AddressList;
import com.kabbi.client.utils.LocaleHelper;
import com.kabbi.client.views.adapters.AutoCompleteAdapter;
import com.kabbi.client.views.pager.AddressesPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AutoCompleteActivity extends GeoSpiceActivity implements
        SearchView.OnQueryTextListener, AdapterView.OnItemClickListener {

    public static final String USER_COORDS_KEY = "auto_complete_activity.user_coords_key";
    public static final String USER_ADDRESS_KEY = "auto_complete_activity.user_address_key";

    public static final String EXTRA_MAP_ADDRESS = "auto_complete_activity.EXTRA_MAP_ADDRESS";
    public static final int REQUEST_MAP_ADDRESS = 123;

    private MenuItem menuItem;
    private ListView completeListView;
    private TextView completeTextView;

    private Handler handler;
    private String textSearch;
    private Profile profile;
    private String[] userCoords;
    private int pointNum;
    private List<Address> addresses;
    private TypedValue typedValueText;

    private ViewPager mVpAddress;
    private TabLayout mTlAddress;
    private TextView mTvMapChoice;
    private LinearLayout mLlMapChoice;
    private AddressesPager mAddressesPager;

    private boolean isEditOrder = false;

    private int[] icons = {R.drawable.pin_a,
            R.drawable.pin_b, R.drawable.pin_c,
            R.drawable.pin_d,R.drawable.pin_e};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_auto_complete);
        setTitle(R.string.activities_AutoCompleteActivity_title);

        handler = new Handler();
        typedValueText = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_icon_bg, typedValue, true);

        initToolbar();
        initViews();
        initVars();
    }

    private void initViews() {
        completeListView = (ListView) findViewById(R.id.auto_complete_list);
        assert completeListView != null;
        completeListView.setOnItemClickListener(this);
        completeTextView = (TextView) findViewById(R.id.auto_complete_back);

        mVpAddress = (ViewPager) findViewById(R.id.vp_addresses);
        mTlAddress = (TabLayout) findViewById(R.id.tl_addresses);
        mTvMapChoice = (TextView) findViewById(R.id.tv_map_choice);
        mLlMapChoice = (LinearLayout) findViewById(R.id.ll_map_choice);

        mLlMapChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(AutoCompleteActivity.this, MapActivity.class);
                mapIntent.putExtra(EditOrderActivity.EXTRA_EDIT_ADDRESS, isEditOrder);
                mapIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
                startActivityForResult(mapIntent, REQUEST_MAP_ADDRESS);
            }
        });
    }

    private void initVars() {
        isEditOrder = getIntent().getBooleanExtra(EditOrderActivity.EXTRA_EDIT_ADDRESS, false);
        profile = Profile.getProfile();
        userCoords = getIntent().getStringArrayExtra(USER_COORDS_KEY);
        pointNum = getIntent().getIntExtra(MainActivity.POINT_NUM_KEY, 0);
        if (pointNum == 0) {
            mLlMapChoice.setVisibility(View.GONE);
        } else {
            mLlMapChoice.setVisibility(View.VISIBLE);
        }

        getSpiceManager().execute(new GetReverseRequest(
                        profile.getApiKey(),
                        profile.getCityId(), userCoords[0], userCoords[1],
                        AppParams.REVERSE_RADIUS, "30", profile.getTenantId(), LocaleHelper.getLanguage(getBaseContext())),
                new FullReverseListener());



        mAddressesPager = new AddressesPager(getSupportFragmentManager(), getBaseContext(),
                new ArrayList<>(Address.getAddressHistory()), AddressList.EMPTY, AddressList.EMPTY);

        mVpAddress.setAdapter(mAddressesPager);
        mTlAddress.setupWithViewPager(mVpAddress);

        mTvMapChoice.setCompoundDrawablesWithIntrinsicBounds(icons[pointNum], 0, 0, 0);
        mVpAddress.setOffscreenPageLimit(2);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        textSearch = newText;

        if (newText.length() > 0) {
            hideAddressView();
        } else {
            showAddressView();
        }

        if (newText.length() >= 3) {
            if (completeListView.getCount() == 0) {
                completeTextView.setText(R.string.activities_AutoCompleteActivity_text_process);
            }
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (AppParams.USE_SEARCH && pointNum != 0) {
                        getSpiceManager().execute(
                                new GetSearchRequest(
                                        profile.getApiKey(),
                                        profile.getCityId(), textSearch,
                                        userCoords[0], userCoords[1], profile.getTenantId()),
                                new SearchListener());
                    } else {
                        getSpiceManager().execute(
                                new GetAutoCompleteRequest(
                                        profile.getApiKey(),
                                        profile.getCityId(), textSearch,
                                        userCoords[0], userCoords[1], profile.getTenantId(),
                                        LocaleHelper.getLanguage(getBaseContext())),
                                new AutoCompleteListener());
                    }
                }
            }, 500);
        }
        return true;
    }


    private void hideAddressView() {
        mVpAddress.animate().alpha(0).setDuration(500).start();
        mTlAddress.animate().alpha(0).setDuration(500).start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mVpAddress.setVisibility(View.GONE);
                mTlAddress.setVisibility(View.GONE);
            }
        }, 500);
    }

    private void showAddressView() {
        completeListView.setVisibility(View.GONE);
        completeTextView.setVisibility(View.GONE);
        mVpAddress.animate().alpha(1).setDuration(500).start();
        mTlAddress.animate().alpha(1).setDuration(500).start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mVpAddress.setVisibility(View.VISIBLE);
                mTlAddress.setVisibility(View.VISIBLE);
            }
        }, 500);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Address newAddress = addresses.get(position);
        if (!isEditOrder) {
            Intent returnIntent = new Intent(this, MainActivity.class);
            returnIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
            newAddress.setUseType(Address.TYPE_AUTOCOMPLETE);
            returnIntent.putExtra(MainActivity.ADDRESS_KEY, newAddress);
            returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_COMPLETE);
            NavUtils.navigateUpTo(this, returnIntent);
        } else {
            Intent intent = new Intent();
            intent.putExtra(EditOrderActivity.EXTRA_ADDRESS, newAddress);
            intent.putExtra(EditOrderActivity.EXTRA_POSITION, pointNum);
            setResult(RESULT_OK, intent);
            finish();
        }
    }


    @Subscribe
    public void onMessage(SearchEvent event) {
        executeGeoEvent(event.getAddresses());
    }

    @Subscribe
    public void onMessage(AutoCompleteEvent event) {
        executeGeoEvent(event.getAddresses());
    }

    @Subscribe
    public void onMessage(FullReverseEvent event) {
        AddressesFragment addressesFragment = mAddressesPager.fragments.get(AddressesPager.ADDRESSES_NEAR);
        if (addressesFragment != null) {
            addressesFragment.updateView(event.getAddresses());
        }
    }

    @Subscribe
    public void onMessage(AddressSelectedEvent addressSelectedEvent) {
        Address newAddress = addressSelectedEvent.getAddress();
        if (!isEditOrder) {
            Intent returnIntent = new Intent(this, MainActivity.class);
            returnIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
            newAddress.setUseType(Address.TYPE_AUTOCOMPLETE);
            returnIntent.putExtra(MainActivity.ADDRESS_KEY, newAddress);
            returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_COMPLETE);
            NavUtils.navigateUpTo(this, returnIntent);
        } else {
            Intent intent = new Intent();
            intent.putExtra(EditOrderActivity.EXTRA_ADDRESS, newAddress);
            intent.putExtra(EditOrderActivity.EXTRA_POSITION, pointNum);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_MAP_ADDRESS:
                    Address newAddress = data.getParcelableExtra(EXTRA_MAP_ADDRESS);
                    if (newAddress != null) {
                        Intent intent = new Intent();
                        intent.putExtra(EditOrderActivity.EXTRA_ADDRESS, newAddress);
                        intent.putExtra(EditOrderActivity.EXTRA_POSITION, pointNum);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
            }
        }
    }

    private void executeGeoEvent(List<Address> addresses) {
        this.addresses = addresses;
        if (addresses != null && addresses.size() > 0) {
            completeTextView.setText(R.string.activities_AutoCompleteActivity_text_greeting);
            AutoCompleteAdapter completeAdapter = new AutoCompleteAdapter(
                    getApplicationContext(), addresses);
            completeListView.setAdapter(completeAdapter);
            completeListView.setVisibility(View.VISIBLE);
            completeTextView.setVisibility(View.GONE);
        } else {
            completeTextView.setText(R.string.activities_AutoCompleteActivity_text_error);
            completeListView.setAdapter(null);
            completeListView.setVisibility(View.GONE);
            completeTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_complete, menu);
        menuItem = menu.findItem(R.id.action_complete_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.activities_AutoCompleteActivity_search_hint));
        searchView.setOnQueryTextListener(this);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        AutoCompleteTextView searchTextView = (AutoCompleteTextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchTextView.setHintTextColor(typedValueText.data);

        TypedArray a = getTheme().obtainStyledAttributes(R.style.AppTheme, new int[] {R.attr.custom_cursor});
        int attributeResourceId = a.getResourceId(0, 0);
        a.recycle();

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, attributeResourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MenuItemCompat.expandActionView(menuItem);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_complete_search:
                MenuItemCompat.expandActionView(menuItem);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
