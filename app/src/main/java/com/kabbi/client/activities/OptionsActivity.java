package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.SaveCitiesEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.CitiesRequestListener;
import com.kabbi.client.network.listeners.TariffsRequestListener;
import com.kabbi.client.network.requests.GetTariffsListRequest;
import com.kabbi.client.network.requests.GetTenantCityListRequest;
import com.kabbi.client.utils.LocaleHelper;
import com.kabbi.client.utils.Validator;
import com.kabbi.client.views.ToastWrapper;
import com.kabbi.client.views.adapters.LanguageAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.kabbi.client.app.AppParams.MAPS;

public class OptionsActivity extends BaseSpiceActivity {

    private Profile profile;
    private String newTenant, newApiKey, newAppId;

    private TextInputEditText tenantEdit, apiKeyEdit, appIdEdit;
    private RadioGroup radioGroupMaps, radioGroupColors, calendarGroup;
    private ProgressDialog tenantRequestsDialog;
    private RecyclerView mLanguagesList;
    private LanguageAdapter mLanguageAdapter;


    public static final String RESULT_KEY_OPTION = "RESULT_KEY_OPTION";
    private boolean isDataSavedSuccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_options);
        setTitle(R.string.activities_OptionsActivity_title);

        initVars();
        initToolbar();
        initViews();
        initColors();
        initMaps();
        initCalendar();
        initLanguages();
    }

    private void initVars() {
        profile = Profile.getProfile();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        tenantEdit = (TextInputEditText) findViewById(R.id.options_tenant_edit);
        tenantEdit.setText(profile.getTenantId());
        apiKeyEdit = (TextInputEditText) findViewById(R.id.options_api_key_edit);
        apiKeyEdit.setText(profile.getApiKey());
        appIdEdit = (TextInputEditText) findViewById(R.id.options_app_id_edit);
        appIdEdit.setText(profile.getAppId());

        radioGroupMaps = (RadioGroup) findViewById(R.id.options_maps);
        radioGroupColors = (RadioGroup) findViewById(R.id.options_colors);
        calendarGroup = (RadioGroup) findViewById(R.id.options_calendar);
        mLanguagesList = (RecyclerView) findViewById(R.id.rv_languages);
        TextView calendarTitle = (TextView) findViewById(R.id.options_calendar_title);

        if (!AppParams.USE_CALENDAR_EXTENSION) {
            calendarGroup.setVisibility(View.GONE);
            calendarTitle.setVisibility(View.GONE);
        }

        if (!AppParams.IS_DEMO) {
            View tenantTitle = findViewById(R.id.options_tenant_title);
            if (tenantTitle != null) tenantTitle.setVisibility(View.GONE);
            View tenantLayout = findViewById(R.id.options_tenant_layout);
            if (tenantLayout != null) tenantLayout.setVisibility(View.GONE);
            View colorsTitle = findViewById(R.id.options_colors_title);
            if (colorsTitle != null) colorsTitle.setVisibility(View.GONE);
            radioGroupColors.setVisibility(View.GONE);
            apiKeyEdit.setVisibility(View.GONE);
            appIdEdit.setVisibility(View.GONE);
        }
        Button saveBtn = (Button) findViewById(R.id.options_confirm);
        if (saveBtn != null) saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkData();
            }
        });
    }

    private void initLanguages() {
        Set<String> validLang = Validator.validateLanguages(AppParams.SUPPORT_LANG, AppParams.LANGUAGES);
        ArrayList langList = new ArrayList<>(validLang);

        String lang = LocaleHelper.getLanguage(getBaseContext());
        if (!validLang.contains(lang)) lang = AppParams.DEFAULT_LANG;

        mLanguageAdapter = new LanguageAdapter(this, langList, lang);

        mLanguagesList.setAdapter(mLanguageAdapter);
        mLanguagesList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initColors() {
        int count = 0;
        for (int themeId : AppParams.THEMES) {
            radioGroupColors.getChildAt(count).setTag(themeId);
            if (themeId == profile.getThemeId())
                ((RadioButton) radioGroupColors.getChildAt(count)).setChecked(true);
            count++;
        }
    }

    private void initMaps() {
        int count = 0;
        for (int iMap : AppParams.ALL_MAPS) {
            View mapView = radioGroupMaps.getChildAt(count);
            mapView.setTag(iMap);
            List<Integer> mapList = new ArrayList<Integer>();
            for (int i : MAPS) {
                mapList.add(i);
            }
            if (!mapList.contains(iMap))
                mapView.setVisibility(View.GONE);
            if (iMap == profile.getMap())
                ((RadioButton) radioGroupMaps.getChildAt(count)).setChecked(true);
            count++;
        }
    }

    private void initCalendar() {
        int count = 0;
        for (int calendar : AppParams.CALENDARS) {
            calendarGroup.getChildAt(count).setTag(calendar);
            if (calendar == profile.getCalendar())
                ((RadioButton) calendarGroup.getChildAt(count)).setChecked(true);
            count++;
        }
    }

    private void checkData() {
        tenantRequestsDialog = new ProgressDialog(this);
        tenantRequestsDialog.setTitle(getString(R.string.activities_OptionsActivity_progress_title));
        tenantRequestsDialog.setMessage(getString(R.string.activities_OptionsActivity_progress_message));
        tenantRequestsDialog.setCancelable(false);
        tenantRequestsDialog.show();
        newTenant = tenantEdit.getText().toString().trim();
        newApiKey = apiKeyEdit.getText().toString().trim();
        newAppId = appIdEdit.getText().toString().trim();
        getSpiceManager().execute(new GetTenantCityListRequest(newAppId, newApiKey, getApplicationContext(),
                        String.valueOf((new Date()).getTime()), newTenant),
                new CitiesRequestListener());
    }

    @Subscribe
    public void onMessage(SaveCitiesEvent saveCitiesEvent) {
        int infoText = saveCitiesEvent.getInfoText();
        if (saveCitiesEvent.isSaveCities()) {
            saveData(infoText);
            isDataSavedSuccess = true;
            getSpiceManager().execute(new GetTariffsListRequest(profile.getAppId(), profile.getApiKey(),
                            this, String.valueOf((new Date()).getTime()),
                            profile.getCityId(), profile.getPhone(), profile.getTenantId()),
                    new TariffsRequestListener(this));
        } else {
            showError(infoText);
        }
    }

    private void saveData(int infoText) {
        tenantRequestsDialog.dismiss();
        profile.setTenantId(newTenant);
        profile.setApiKey(newApiKey);
        profile.setAppId(newAppId);

        if (AppParams.IS_DEMO) {
            profile.setThemeId((int) radioGroupColors
                    .findViewById(radioGroupColors.getCheckedRadioButtonId()).getTag());
        }
        profile.setMap((int) radioGroupMaps
                .findViewById(radioGroupMaps.getCheckedRadioButtonId()).getTag());
        profile.setCalendar((int) calendarGroup
                .findViewById(calendarGroup.getCheckedRadioButtonId()).getTag());
        profile.save();
        String lang = mLanguageAdapter.getCurrentLang();
        LocaleHelper.setLocale(this, lang);
        new ToastWrapper(this, infoText).show();
    }

    private void showError(int errorText) {
        tenantRequestsDialog.dismiss();
        new ToastWrapper(this, errorText).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        if (isDataSavedSuccess)
            returnIntent.putExtra(MainActivity.BACK_KEY, OptionsActivity.RESULT_KEY_OPTION);
        startActivity(returnIntent);
    }


}
