package com.kabbi.client.activities;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kabbi.client.R;
import com.kabbi.client.fragments.PageItemFragment;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;

import java.util.ArrayList;
import java.util.List;

import static ru.yandex.core.CoreApplication.getView;

public class TariffActivity extends AppCompatActivity {

    private List<Tariff> tariffList;
    private LinearLayout llNavi;
    private int countTariffs;
    private boolean isRtl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_tariff);
        setTitle(R.string.activities_TariffActivity_title);
        initToolbar();

        tariffList = new ArrayList<>();
        Profile profile = Profile.getProfile();
        String tariffId = getIntent().getStringExtra("tariff_id");
        Log.d("Logos", "Logos onCreate: " + tariffId);

        for (Tariff tariff : Tariff.getTariffs(City.getCity(profile.getCityId()))) {
            if (tariff.getClientTypes().contains(profile.getClientType())) {
                if (profile.getClientType().equals("base")
                        || profile.getClientType().equals("company") && tariff.getCompanies().contains(profile.getCompany())) {
                    tariffList.add(tariff);
                }
            }
        }


        final Configuration config = getResources().getConfiguration();
        isRtl = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;

        countTariffs = tariffList.size();
        llNavi = (LinearLayout) findViewById(R.id.ll_navi);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);


        for (int i = 0; i < countTariffs; i++) {
            ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.item_image_tariff, null);
            Log.d("Logos", "Logos onCreate: " + tariffList.get(i));
            if (tariffList.get(i).getTariffId().equals(tariffId)) {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_blue));

                if (isRtl) {
                    pager.setCurrentItem(countTariffs - 1 - i);
                } else {
                    pager.setCurrentItem(i);
                }
            } else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_gray));
            }

            llNavi.addView(imageView);
        }

        ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                int directionPosition = position;
                if (isRtl) {
                    directionPosition = countTariffs - 1 - position;
                }


                llNavi.removeAllViews();
                for (int i = 0; i < countTariffs; i++) {
                    ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.item_image_tariff, null);
                    if (i == directionPosition)
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_blue));
                    else
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_gray));

                    llNavi.addView(imageView);
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
        pager.addOnPageChangeListener(pageChangeListener);
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (isRtl) {
                return PageItemFragment.newInstance(position, tariffList.get(tariffList.size() - position - 1).getTariffId());
            } else {
                return PageItemFragment.newInstance(position, tariffList.get(position).getTariffId());
            }

        }

        @Override
        public int getCount() {
            return tariffList.size();
        }
    }
}
