package com.kabbi.client.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.DeepParams;
import com.kabbi.client.events.api.client.ClientCardsEvent;
import com.kabbi.client.events.api.client.UpdateStateEvent;
import com.kabbi.client.events.ui.ChangeTariff;
import com.kabbi.client.events.ui.OrderRepeatEvent;
import com.kabbi.client.events.ui.OrderTimeEvent;
import com.kabbi.client.events.ui.UpdatePointsEvent;
import com.kabbi.client.events.ui.map.CurrentLocationEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.events.ui.map.PointsMapEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.ui.map.PolygonEvent;
import com.kabbi.client.fragments.MainFragment;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.DeepOrder;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Referral;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.PingListener;
import com.kabbi.client.network.listeners.TariffsTypeRequestListener;
import com.kabbi.client.network.requests.GetPingRequest;
import com.kabbi.client.network.requests.GetTariffsTypeRequest;
import com.kabbi.client.services.LocationService;
import com.kabbi.client.utils.AddressList;
import com.kabbi.client.utils.HashMD5;
import com.kabbi.client.utils.LocManager;
import com.kabbi.client.utils.espresso.EspressoIdlingResource;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.kabbi.client.activities.PaymentActivity.SUPPORT_CITY;
import static com.kabbi.client.app.AppParams.USE_PROFILE_DATA;
import static com.kabbi.client.app.AppParams.USE_PUBLIC_TRANSPORT;
import static com.kabbi.client.app.AppParams.USE_REFERRAL;
import static com.kabbi.client.app.AppParams.USE_WEB_APPLICATION;
import static com.kabbi.client.app.AppParams.WEB_APPLICATION_TITLE;

public class MainActivity extends BaseSpiceActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    public static final String BACK_KEY = "main_activity.back_key";
    public static final String BACK_VALUE_PROFILE = "main_activity.value_profile";
    public static final String BACK_VALUE_AUTH = "main_activity.value_auth";
    public static final String BACK_VALUE_COMPLETE = "main_activity.value_complete";
    public static final String BACK_VALUE_MAP = "main_activity.value_map";
    public static final String BACK_VALUE_NEW_ORDER = "main_activity.new_order";
    public static final String BACK_VALUE_OLD_ORDER = "main_activity.old_order";
    public static final String BACK_VALUE_PAYMENT = "main_activity.payment";
    public static final String BACK_VALUE_SING_IN = "main_activity.auth.sing_in";

    public static final String POINT_NUM_KEY = "main_activity.point_num_key";
    public static final String ADDRESS_KEY = "complete_activity.address_key";

    private final static String TAG = MainActivity.class.getSimpleName();

    public static final int REQUEST_CODE_DETAIL = 2;
    public static final int REQUEST_CODE_WISHES = 3;
    public static final int REQUEST_CODE_TARIFF = 4;
    public static final int REQUEST_LOCATION = 5;

    private GoogleApiClient mGoogleApiClient;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;
    private Profile profile;
    private Order oldOrder;
    private Order order;
    private List<Address> addressList;
    private MainFragment mainFragment;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    public City currentCity;
    private boolean isNextWithLocation = false;
    private boolean isWasLocationRequest = false;

    private TextView phoneView;

    public boolean activeOrder;
    private BroadcastReceiver dateReceiver;
    private DeepOrder deepOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_main);
        setTitle(R.string.activities_MainActivity_title);
        EventBus.getDefault().register(this);

        EspressoIdlingResource.increment();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        initVars();
        initToolbarAndDrawer();
        setMainFragment();

        if (AppParams.IS_DEMO && profile.isFirstSign()) {
            showHelper();
        }

        startDateReceiver();
        checkProfileData();
    }

    private void startDateReceiver() {
        dateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_CHANGED)) {
                    getSpiceManager().execute(new GetPingRequest(profile.getAppId(), profile.getApiKey(),  getApplicationContext(), profile.getTenantId()),
                            new PingListener());
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(dateReceiver, intentFilter);
    }

    private void initVars() {
        activeOrder = false;
        profile = Profile.getProfile();
        oldOrder = Order.getActiveOrder();
        if (oldOrder != null) {
            order = oldOrder;
        } else {
            order = new Order();
            order.setStatus("empty");
        }

        Intent appLinkIntent = getIntent();
        if (DeepParams.DEEP_SCHEME.equals(appLinkIntent.getScheme())) {
            addressList = getDeepList(getIntent());
        } else {
            addressList = new AddressList();
            addressList.add(new Address());
            Address addressB = new Address();
            addressB.setEmpty(true);
            addressList.add(addressB);
        }


        backNavigation();
    }

    private void backNavigation() {
        returnFromPush(getIntent());
        String backKey = getIntent().getStringExtra(BACK_KEY);
        if (backKey != null && backKey.equals(BACK_VALUE_OLD_ORDER)) {
            returnToOldOrder(getIntent());
        } else if (backKey != null && backKey.equals(BACK_VALUE_NEW_ORDER)) {
            returnToNewOrder(oldOrder);
        }
    }

    private List<Address> getDeepList(Intent appLinkIntent) {
        List<Address> addresses = new AddressList();
        try {
            Uri appLinkData = appLinkIntent.getData();
            deepOrder = DeepOrder.parseFromUri(appLinkData);

            Address addressFrom = new Address();
            addressFrom.setCity(deepOrder.getFromCity());
            addressFrom.setHouse(deepOrder.getFromHouse());
            addressFrom.setStreet(deepOrder.getFromStreet());
            addressFrom.setLabel(deepOrder.getFromStreet() + " " + deepOrder.getFromHouse());
            addressFrom.setUseType(Address.TYPE_ROUTE);
            addressFrom.setType("house");
            addressFrom.setLat(String.valueOf(deepOrder.getFromLat()));
            addressFrom.setLon(String.valueOf(deepOrder.getFromLon()));
            String hash = HashMD5.getHash(addressFrom.getCity() + addressFrom.getStreet() +
                    addressFrom.getHouse() + addressFrom.getLabel());
            addressFrom.setHash(hash);
            addresses.add(0, addressFrom);

            Address addressTo = new Address();
            addressTo.setCity(deepOrder.getToCity());
            addressTo.setHouse(deepOrder.getToHouse());
            addressTo.setStreet(deepOrder.getToStreet());
            addressTo.setLabel(deepOrder.getToStreet() + " " + deepOrder.getToHouse());
            addressTo.setUseType(Address.TYPE_ROUTE);
            addressTo.setType("house");
            String hashTo = HashMD5.getHash(addressTo.getCity() + addressTo.getStreet() +
                    addressTo.getHouse() + addressTo.getLabel());
            addressTo.setHash(hashTo);
            addresses.add(1, addressTo);
            addressTo.setLat(String.valueOf(deepOrder.getToLat()));
            addressTo.setLon(String.valueOf(deepOrder.getToLon()));

            EventBus.getDefault().postSticky(new OrderRepeatEvent());
            EventBus.getDefault().post(new PolygonEvent(addresses.get(0).getLat(), addresses.get(0).getLon()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addresses;
    }

    public DeepOrder getDeepOrder() {
        return deepOrder;
    }

    private void checkProfileData() {

        boolean isProfileWithoutData = TextUtils.isEmpty(profile.getName())
                || TextUtils.isEmpty(profile.getEmail())
                || TextUtils.isEmpty(profile.getSurname());

        if (USE_PROFILE_DATA && isProfileWithoutData && profile.isAuthorized()) {
            Intent profileIntent = new Intent(this, EditProfileActivity.class);
            profileIntent.putExtra(EditProfileActivity.EXTRA_REQUIRED, true);
            startActivity(profileIntent);
        }
    }

    private void initToolbarAndDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.menu_drawer_open, R.string.menu_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //mainFragment.showContent();
            }
        } ;
        drawer.addDrawerListener(drawerToggle);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        int headers = nvDrawer.getHeaderCount();
        if (headers > 0) {
            for (int i = 0; i < headers; i++) nvDrawer.removeHeaderView(nvDrawer.getHeaderView(i));
        }
        phoneView = (TextView) nvDrawer.findViewById(R.id.left_phone);
        if (!profile.isAuthorized()) setupDrawerHeader(nvDrawer);
        else setupDrawerHeaderAuth(nvDrawer);
        setupDrawerContent(nvDrawer);
    }

    private void showHelper() {
        profile.setFirstSign(false);
        profile.save();
        String info = "Вы скачали демо-приложение Гутакс.\n" +
                "\n" +
                "Чтобы протестировать приложение вам необходимо:\n" +
                "\n" +
                "1. Подключить ваш аккаунт Гутакса в настройках. Для того, чтобы узнать ваш ID," +
                " свяжитесь с специалистами Гутакса по телефону +73412310932 или sales@gootax.pro;\n" +
                "\n" +
                "2. Подключите СМС-аккаунт или создайте нового клиента в Гутаксе и используйте его код, указанный в системе.\n" +
                "\n" +
                "3. Можно тестировать.";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.MyCustomAlertDialogTheme)
                .setTitle("Добро пожаловать!")
                .setMessage(info)
                .setPositiveButton(getString(R.string.activities_PaymentActivity_dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog mActivateBonusSystemDialog = alertDialog.create();
        mActivateBonusSystemDialog.show();
    }

    private void setupDrawerHeaderAuth(NavigationView navigationView) {
        navigationView.inflateHeaderView(R.layout.nav_header);
        View headerLayout = navigationView.getHeaderView(0);
        LinearLayout rootLayout = (LinearLayout) headerLayout
                .findViewById(R.id.nav_header_layout);
        SimpleDraweeView photoView = (SimpleDraweeView)
                headerLayout.findViewById(R.id.nav_header_photo);
        TextView headerName = (TextView) headerLayout.findViewById(R.id.nav_header_name);
        String photoUrl = profile.getPhoto();
        if (!photoUrl.isEmpty()) {
            Uri uri = Uri.parse(photoUrl);
            photoView.setImageURI(uri);
        } else {
            photoView.getHierarchy().reset();
        }

        String fullName = profile.getName() + " " + profile.getSurname();
        if (!fullName.trim().isEmpty()) {
            headerName.setText(fullName);
        } else {
            headerName.setText(R.string.menu_header_name);
        }
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(profileIntent);
                drawer.closeDrawers();
            }
        });
        rootLayout.setBackgroundResource(R.color.colorPrimary);

    }

    private void setupDrawerHeader(NavigationView navigationView) {
        navigationView.inflateHeaderView(R.layout.nav_head_info);
        View headerLayout = navigationView.getHeaderView(0);
        RelativeLayout rootLayout = (RelativeLayout) headerLayout
                .findViewById(R.id.ll_haeder_info);
        rootLayout.setBackgroundResource(R.color.drawer_back);

        Button button = (Button) headerLayout.findViewById(R.id.btn_left_auth);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startActivityIntent = new Intent(MainActivity.this, AuthActivity.class);
                startActivityIntent
                        .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                startActivity(startActivityIntent);
                drawer.closeDrawers();
            }
        });
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setItemIconTintList(null);
        Menu menu = nvDrawer.getMenu();
        if (menu != null) {
            MenuItem menuItem = menu.findItem(R.id.nav_web_application);
            if (USE_WEB_APPLICATION) {
                menuItem.setTitle(WEB_APPLICATION_TITLE);
            } else {
                menuItem.setVisible(false);
            }
        }
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Intent startActivityIntent;
        switch(menuItem.getItemId()) {
            case R.id.nav_payment:
                startActivityIntent = new Intent(this, PaymentActivity.class);
                startActivityIntent.putExtra(SUPPORT_CITY, currentCity != null);
                break;
            case R.id.nav_history:
                startActivityIntent = new Intent(this, LastOrdersActivity.class);
                break;
            case R.id.nav_preorders:
                startActivityIntent = new Intent(this, PreOrdersActivity.class);
                break;
            case R.id.nav_options:
                startActivityIntent = new Intent(this, OptionsActivity.class);
                //finish();
                break;
            case R.id.nav_help:
                startActivityIntent = new Intent(this, HelpActivity.class);
                break;
            case R.id.nav_referral:
                startActivityIntent = new Intent(this, ReferralActivity.class);
                break;
            case R.id.nav_public_transport:
                startActivityIntent = new Intent(this, PublicTransportActivity.class);
                isNextWithLocation = true;
                City city = mainFragment.city;
                String[] coords = mainFragment.callMapFragmentForCoords();
                startActivityIntent.putExtra(PublicTransportActivity.EXTRA_CURRENT_POSITION,
                        new LatLng(Double.valueOf(coords[0]), Double.valueOf(coords[1])));
                startActivityIntent.putExtra(PublicTransportActivity.EXTRA_TRANSPORT_CITY_ID,
                        city.getCityId());
                startActivityIntent.putExtra(PublicTransportActivity.EXTRA_CURRENT_TARIFF_ID,
                        order.getTariff());
                break;
            case R.id.nav_web_application:
                startActivityIntent = new Intent(this, WebAppActivity.class);
                break;
            default:
                startActivityIntent = new Intent(this, LastOrdersActivity.class);
        }
        startActivity(startActivityIntent);
        drawer.closeDrawers();
        drawer.closeDrawer(Gravity.START);
    }

    private void setMainFragment() {
        mainFragment = MainFragment.getInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, mainFragment);
        transaction.commit();
    }

    private void reloadMainFragment() {
        setTitle(R.string.activities_MainActivity_title);
        mainFragment = MainFragment.getInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, mainFragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_DETAIL:
                    Address address = addressList.get(0);
                    String street = data.getStringExtra("street");
                    if (street != null && !street.equals("") && address.isGps()) {
                        address.setStreet(street);
                        address.setLabel(street);
                    }
                    address.setPorch(data.getStringExtra("porch"));
                    address.setApt(data.getStringExtra("apt"));
                    addressList.set(0, address);
                    order.setComment(data.getStringExtra("comment"));
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdateAddressList();
                    break;
                case REQUEST_CODE_WISHES:
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).startCostRequest();
                    break;
                case REQUEST_CODE_TARIFF:
                    EventBus.getDefault().post(new ChangeTariff(data.getStringExtra("tariff_id")));
                    break;
                case REQUEST_LOCATION:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new GetCurrentLocationEvent());
                        }
                    }, 2000);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if (DeepParams.DEEP_SCHEME.equals(intent.getScheme())) {
            addressList = getDeepList(intent);
            reloadMainFragment();
            return;
        }

        returnFromPush(intent);
        Order oldOrder = Order.getActiveOrder();
        String backKey = intent.getStringExtra(BACK_KEY);
        if (backKey != null) {
            switch (backKey) {
                case BACK_VALUE_PROFILE:
                case BACK_VALUE_AUTH:
                    initToolbarAndDrawer();
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdatePayment();
                    break;
                case BACK_VALUE_COMPLETE:
                case BACK_VALUE_MAP:
                    int pointNum = intent.getIntExtra(POINT_NUM_KEY, 0);
                    Address address = intent.getParcelableExtra(ADDRESS_KEY);
                    if (pointNum >= ((AddressList) addressList).realSize()) {
                        addressList.add(address);
                    } else {
                        addressList.set(pointNum, address);
                    }
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdateAddressList();
                    final String lat = addressList.get(pointNum).getLat();
                    final String lon = addressList.get(pointNum).getLon();
                    if (lat != null && !lat.isEmpty() && lon != null && !lon.isEmpty() && profile.getMap() != 3) {
                        EventBus.getDefault().post(new CurrentLocationEvent(
                                Double.valueOf(lat),
                                Double.valueOf(lon), false));
                    }
                    break;
                case BACK_VALUE_NEW_ORDER:
                    returnToNewOrder(oldOrder);
                    break;
                case BACK_VALUE_OLD_ORDER:
                    returnToOldOrder(intent);
                    break;
                case BACK_VALUE_PAYMENT:
                    if (oldOrder != null) {
                        order = oldOrder;
                        activeOrder = true;
                    } else {
                        //order = new Order();
                        //order.setStatus("empty");
                        activeOrder = false;
                    }
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdatePayment();
                    break;
                case OptionsActivity.RESULT_KEY_OPTION:
                    finish();
                    startActivity(getIntent());
                    break;
            }
        }
        super.onNewIntent(intent);
    }

    private void returnFromPush(Intent intent) {
        String type = intent.getStringExtra("type");
        String orderId = intent.getStringExtra("order_id");


        if (type != null && type.equals("push") && orderId != null) {
            Order tempOrder = Order.getOrder(orderId);
            if (tempOrder != null) {
                order = tempOrder;
                reloadMainFragment();
            }
        }
    }

    private void returnToOldOrder(Intent intent) {
        activeOrder = false;
        order = new Order();
        String orderId = intent.getStringExtra("order_id");
        if (orderId != null) {
            Order orderOld = Order.getOrder(orderId);
            order.setStatus("empty");
            order.setTariff(orderOld.getTariff());
            addressList = RoutePoint.getRouteWithFakeAddress(orderOld);
            boolean isReverse = intent.getBooleanExtra("reverse", false);
            if (isReverse) Collections.reverse(addressList);
            EventBus.getDefault().postSticky(new OrderRepeatEvent());
            if (addressList.size() > 0)
                EventBus.getDefault().post(new PolygonEvent(addressList.get(0).getLat(), addressList.get(0).getLon()));
        }
        reloadMainFragment();
    }

    private void returnToNewOrder(Order oldOrder) {
        reloadMainFragment();
        if (oldOrder != null) {
            order = oldOrder;
            activeOrder = true;
        } else {
            order = new Order();
            order.setStatus("empty");
            activeOrder = false;
        }
        addressList.clear();
        addressList.add(new Address());
        Address addressB = new Address();
        addressB.setEmpty(true);
        addressList.add(addressB);
    }

    @Subscribe
    public void onMessage(UpdatePointsEvent updatePointsEvent) {
        updatePoints(activeOrder);
    }

    @Subscribe
    public void onMessage(UpdateStateEvent updateStateEvent) {
        startActivity(updateStateEvent.getIntent());
    }

    @Subscribe
    public void onMessage(OrderTimeEvent orderTimeEvent) {
        order.setOrderTime(orderTimeEvent.getOrderTime());
        getSpiceManager().execute(new GetTariffsTypeRequest(profile.getAppId(), profile.getApiKey(),  profile.getCityId(),
                        orderTimeEvent.getOrderTime()),
                new TariffsTypeRequestListener(this, order.getTariff()));
        ((MainFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flContent)).callUpdateOrderTime();
    }

    @Subscribe
    public void onMessage(ClientCardsEvent event) {
        PaymentType.deleteCardPayments();
        ArrayList<String> cards = event.getClientCards();
        if (cards != null && !cards.isEmpty()) {
            for (int i = 0; i < cards.size(); i++) {
                String pan = cards.get(i);
                PaymentType.addCardPayment(pan);
            }
            profile.setPan(cards.get(cards.size() - 1));
            profile.save();
        }
    }

    @Subscribe
    public void onMessage(final PolygonDetectEvent event) {
        if (event.getCity() != null) {
            if (!phoneView.getText().toString().equals(event.getCity().getPhone())) {
                updatePhoneView(event.getCity().getPhone());
                getOrder().setTariff("");
            }
        }

        if (USE_REFERRAL)
            updateReferralByCity(event.getCity());

        if (USE_PUBLIC_TRANSPORT)
            updatePublicTransportByCity(event.getCity());
    }

    private void hideDrawerItem(Menu menu, @IdRes int res) {
        if (menu != null) {
            MenuItem item = menu.findItem(res);
            if (item != null) item.setVisible(false);
        }
    }

    private void showDrawerItem(Menu menu, @IdRes int res) {
        if (menu != null) {
            MenuItem item = menu.findItem(res);
            if (item != null) item.setVisible(true);
        }
    }

    private void updateReferralByCity(City city) {
        Menu menu = nvDrawer.getMenu();
        currentCity = city;
        if (USE_REFERRAL && city != null) {
            Referral referral = Referral.getReferralByCityId(city.getCityId());
            if (referral == null) hideDrawerItem(menu, R.id.nav_referral);
            else showDrawerItem(menu, R.id.nav_referral);
        }
        else {
            hideDrawerItem(menu, R.id.nav_referral);
        }
    }


    private void updatePublicTransportByCity(City city) {
        Menu menu = nvDrawer.getMenu();
        if (USE_PUBLIC_TRANSPORT && city != null) {
            showDrawerItem(menu, R.id.nav_public_transport);
        }
        else {
            hideDrawerItem(menu, R.id.nav_public_transport);
        }
    }

    private void updatePhoneView(final String phone) {
        if (phone != null && !phone.isEmpty()) {
            phoneView.setText(phone);
            phoneView.setVisibility(View.VISIBLE);
            phoneView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        doCall(phone);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            phoneView.setVisibility(View.GONE);
        }
    }

    private void doCall(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        String phoneTemp = "tel:" + phone;
        callIntent.setData(Uri.parse(phoneTemp));
        startActivity(callIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePoints(activeOrder);
    }


    public void updatePoints(boolean activeOrder) {
        try {
            View imgView = findViewById(R.id.iv_center);
            assert imgView != null;
            if (addressList.size() > 1) {
                imgView.setVisibility(View.INVISIBLE);
            } else {
                if (!activeOrder)
                    imgView.setVisibility(View.VISIBLE);
            }
            if (!activeOrder) {
                ((MainFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.flContent)).startCostRequest();
            }
            if (addressList.size() > 0) {
                EventBus.getDefault().post(new PointsMapEvent(
                        addressList, activeOrder, order.getStatus(), ""));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTitleText(String title) {
        toolbar.setTitle(title);
    }

    public Order getOrder() {
        return order;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return drawerToggle.onOptionsItemSelected(item)
                || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        drawer.removeDrawerListener(drawerToggle);
        EventBus.getDefault().unregister(this);
        unregisterReceiver(dateReceiver);
        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        if (!LocationService.isStarted && !isWasLocationRequest) {
            isWasLocationRequest = true;
            requestLocationWrapper();
        }
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (LocationService.isStarted && !isNextWithLocation) {
            LocationService.isStarted = false;
            stopService(new Intent(this, LocationService.class));
        }
        isNextWithLocation = false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.MyCustomAlertDialogTheme);
        alertDialog.setTitle(getString(R.string.activities_MainActivity_exit_title));
        alertDialog.setMessage(getString(R.string.activities_MainActivity_exit_question));
        alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_exit_answer_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_exit_answer_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void requestLocationWrapper() {
        int hasWriteLocPermission = ContextCompat
                .checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteLocPermission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                return;
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                        LocManager.REQUEST_CODE_PERMISSION_LOC);
                return;
            }
        }
        startLocService();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this, R.style.MyCustomAlertDialogTheme)
                .setMessage(message)
                .setNegativeButton(
                        getString(R.string.activities_MainActivity_permission_dialog_ok), okListener)
                .setPositiveButton(
                        getString(R.string.activities_MainActivity_permission_dialog_cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LocManager.REQUEST_CODE_PERMISSION_LOC:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocService();
                } else {
                    new ToastWrapper(this,
                            R.string.activities_MainActivity_permission_rejected).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startLocService() {
        int hasWriteLocPermission = ContextCompat
                .checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteLocPermission == PackageManager.PERMISSION_GRANTED) {
            LocationService.isStarted = true;
            startService(new Intent(this, LocationService.class));
        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mMLocationRequest = LocationRequest.create();
        mMLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mMLocationRequest.setInterval(30 * 1000);
        mMLocationRequest.setFastestInterval(5 * 1000);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mMLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    MainActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException ignored) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

}

