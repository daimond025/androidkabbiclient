package com.kabbi.client.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.ActivateReferralCodeEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.ReferralSystemCodeListener;
import com.kabbi.client.network.listeners.ReferralSystemListener;
import com.kabbi.client.network.requests.GetReferralSystemCodeRequest;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ActivateCodeActivity extends BaseSpiceActivity {

    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_activate_code);
        setTitle(R.string.activities_ActivateCodeActivity_title);

        initToolbar();
        initVars();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }


    private void initVars() {
        profile = Profile.getProfile();
    }


    private void initView() {
        Button confirm = (Button) findViewById(R.id.btn_code_confirm);
        final EditText code = (EditText) findViewById(R.id.tv_referral_code);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendReferralCode(profile.getPhone(), profile.getTenantId(), code.getText().toString());
            }
        });
    }


    @Subscribe
    public void onMessage(ActivateReferralCodeEvent event) {
        if (event.isActivated()) {
            new ToastWrapper(this, R.string.activities_ReferralActivity_referral_code_success).show();
            finish();
        } else new ToastWrapper(this, R.string.activities_ActivateCodeActivity_code_activated_error).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void sendReferralCode(String phone, String tenantId, String code) {
        getSpiceManager().execute(new GetReferralSystemCodeRequest(profile.getAppId(), profile.getApiKey(),  this, phone, tenantId, code),
                new ReferralSystemCodeListener(this));
    }

}
