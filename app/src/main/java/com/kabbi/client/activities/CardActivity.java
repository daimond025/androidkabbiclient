package com.kabbi.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.CheckCardEvent;
import com.kabbi.client.events.api.client.CreateCardEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.CheckCardListener;
import com.kabbi.client.network.listeners.CreateCardListener;
import com.kabbi.client.network.requests.PostCheckClientCard;
import com.kabbi.client.network.requests.PostCreateClientCard;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;


public class CardActivity extends BaseSpiceActivity {

    public static final String RESULT_SUCCESS_KEY = "card_activity.result_success_type";
    public static final String RESULT_SUCCESS_CARD = "card_activity.result_success_card";
    public static final String RESULT_SUCCESS_NULL = "card_activity.result_success_null";

    private ProgressDialog progressDialog;
    private boolean isRedirected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_card);
        setTitle(R.string.activities_CardActivity_title);

        initToolbar();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        WebChromeClient webChromeClient = new WebChromeClient();
        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog != null) progressDialog.cancel();

            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                isRedirected = true;
            }
        };
        WebView mWebView = (WebView) findViewById(R.id.webview_card);
        if (mWebView != null) {
            mWebView.setWebViewClient(client);
            mWebView.setWebChromeClient(webChromeClient);
            mWebView.getSettings().setJavaScriptEnabled(true);
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.activities_CardActivity_text_load));
        progressDialog.show();
        String webPageString = getIntent().getStringExtra(PaymentActivity.URL_CREATE_CARD);
        if (!webPageString.isEmpty()) {
            assert mWebView != null;
            mWebView.loadUrl(webPageString);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isRedirected) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_CARD);
            setResult(RESULT_OK, returnIntent);
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_NULL);
            setResult(RESULT_OK, returnIntent);
        }
        finish();
    }

}
