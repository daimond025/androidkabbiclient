package com.kabbi.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.views.adapters.WishesAdapter;

import java.util.List;


public class WishesActivity extends AppCompatActivity {

    private List<Option> options;
    private RecyclerView lvWishes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_wishes);
        setTitle(R.string.activities_WishesActivity_title);
        initToolbar();

        lvWishes = (RecyclerView) findViewById(R.id.rv_wishes);
        try {
            City city = City.getCity(Profile.getProfile().getCityId());

            String currency;
            if (city.getCurrency().equals("RUB")) currency = getString(R.string.currency);
            else currency = city.getCurrency();

            Tariff tariff = Tariff.getTariffById(getIntent().getStringExtra("tariff_id"));
            if (tariff != null) {

                options = Option.getOptionsFromType(tariff);

                if (options != null && options.size() > 0) {
                    WishesAdapter wishesAdapter = new WishesAdapter(options, currency);
                    lvWishes.setLayoutManager(new LinearLayoutManager(this));
                    lvWishes.setAdapter(wishesAdapter);
                } else {
                    emptyOptions();
                }

            }
        } catch (Exception e) {
            emptyOptions();
            e.printStackTrace();
        }
    }

    private void emptyOptions() {
        lvWishes.setVisibility(View.GONE);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent());
        finish();
    }
}
