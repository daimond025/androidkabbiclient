package com.kabbi.client.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.CallCostEvent;
import com.kabbi.client.events.api.client.EditOrderEvent;
import com.kabbi.client.events.api.client.EditOrderResultEvent;
import com.kabbi.client.fragments.OrderMenuFragment;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.CostListener;
import com.kabbi.client.network.listeners.EditOrderListener;
import com.kabbi.client.network.listeners.EditOrderResultListener;
import com.kabbi.client.network.requests.EditOrderRequest;
import com.kabbi.client.network.requests.EditOrderResultRequest;
import com.kabbi.client.network.requests.GetCostRequest;
import com.kabbi.client.utils.AddressList;
import com.kabbi.client.views.ToastWrapper;
import com.kabbi.client.views.adapters.EditAddressAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.List;

public class EditOrderActivity extends BaseSpiceActivity {

    public static final String TAG = EditOrderActivity.class.getSimpleName();

    public static final int REQUEST_EDIT_PAYMENT = 345;
    public static final int REQUEST_EDIT_ADDRESS = 346;

    public static final String EXTRA_EDIT_PAYMENT = "EXTRA_EDIT_PAYMENT";
    public static final String EXTRA_COMPANY_ID = "EXTRA_COMPANY_ID";
    public static final String EXTRA_PAYMENT_TYPE = "EXTRA_PAYMENT_TYPE";
    public static final String EXTRA_PAN = "EXTRA_PAN";

    public static final String EXTRA_EDIT_ADDRESS = "EXTRA_EDIT_ADDRESS";
    public static final String EXTRA_ADDRESS = "EXTRA_ADDRESS";
    public static final String EXTRA_POSITION = "EXTRA_POSITION";

    public static final int CODE_ORDER_UPDATE_SUCCESS = 1;
    public static final int CODE_ORDER_UPDATE_ERROR = 0;
    public static final int CODE_ORDER_UPDATE_CONTINUE = 300;

    private ListView mLvAddressList;
    private TextView mTvCallCost;
    private FloatingActionButton mFabAddAddress;
    private Button mButtonChangePayment;


    private Order mOrder;
    private List<Address> mAddressList;
    private Profile mProfile;

    private EditAddressAdapter mAddressAdapter;
    private ProgressDialog mProgressDialog;

    private String mUpdateId;
    private Handler mHandlerUpdateOrder;
    private Runnable mRunnableUpdateOrder;

    private boolean isWasSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);

        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_edit_order);
        setTitle(R.string.activities_EditOrderActivity_title);

        initToolbar();
        initVars();
        initViews();
        updateAddAddressState(mAddressList.size());
        updatePaymentState(mOrder.getPaymentType());
        callCost();
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }


    private void initVars() {
        String orderId = getIntent().getStringExtra(OrderMenuFragment.EXTRA_EDIT_ORDER_ID);
        mOrder = Order.getOrder(orderId);
        mAddressList = RoutePoint.getRoute(mOrder);
        mProfile = Profile.getProfile();
        mAddressAdapter = new EditAddressAdapter(this, mAddressList);
        mAddressAdapter.setActionDelegate(new EditAddressAdapter.ActionDelegate() {
            @Override
            public void onDeleteClicked(int position) {
                mAddressList.remove(position);
                updateList(mAddressList);
                updateAddAddressState(mAddressList.size());
                callCost();
            }
        });
    }


    private void initViews() {
        mLvAddressList = (ListView) findViewById(R.id.lv_edit_order_addresses);
        mTvCallCost = (TextView) findViewById(R.id.tv_edit_order_info);
        mFabAddAddress = (FloatingActionButton) findViewById(R.id.fab_edit_order_add_address);
        mButtonChangePayment = (Button) findViewById(R.id.button_edit_order_payment_type);
        Button buttonSaveOrder = (Button) findViewById(R.id.button_edit_order_save);
        TextView tvAddressesTitle = (TextView) findViewById(R.id.tv_edit_order_addresses_title);
        TextView tvPaymentTitle = (TextView) findViewById(R.id.tv_edit_order_payment_title);


        tvAddressesTitle.setText(getString(R.string.activities_EditOrderActivity_addresses).toUpperCase());
        tvPaymentTitle.setText(getString(R.string.activities_EditOrderActivity_payment).toUpperCase());

        mButtonChangePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), PaymentActivity.class);
                intent.putExtra(EXTRA_EDIT_PAYMENT, true);
                startActivityForResult(intent, REQUEST_EDIT_PAYMENT);
            }
        });

        mLvAddressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) return;

                Address address = mAddressList.get(position);
                String location[] = {address.getLat(), address.getLon()};

                Intent intent = new Intent(getBaseContext(), AutoCompleteActivity.class);
                intent.putExtra(MainActivity.POINT_NUM_KEY, position);
                intent.putExtra(EXTRA_EDIT_ADDRESS, true);
                intent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, location);
                startActivityForResult(intent, REQUEST_EDIT_ADDRESS);
            }
        });

        mFabAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mAddressList.size();
                Address address = mAddressList.get(0);
                String location[] = {address.getLat(), address.getLon()};

                Intent intent = new Intent(getBaseContext(), AutoCompleteActivity.class);
                intent.putExtra(MainActivity.POINT_NUM_KEY, position);
                intent.putExtra(EXTRA_EDIT_ADDRESS, true);
                intent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, location);
                startActivityForResult(intent, REQUEST_EDIT_ADDRESS);
            }
        });

        buttonSaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                getSpiceManager().execute(new EditOrderRequest(getBaseContext(),
                        mOrder,
                        mAddressList,
                        mProfile), new EditOrderListener());
            }
        });


        mLvAddressList.setAdapter(mAddressAdapter);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.activities_EditOrderActivity_updating_order));
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        updateList(mAddressList);
    }

    private void updateAddAddressState(int addressesSize) {
        if (addressesSize >= 5) {
            mFabAddAddress.setVisibility(View.GONE);
        } else {
            mFabAddAddress.setVisibility(View.VISIBLE);
        }
    }


    private void updatePaymentState(String paymentType) {
        switch (paymentType) {
            case "CASH":
                mButtonChangePayment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.wallet, 0, 0, 0);
                mButtonChangePayment.setText(R.string.fragments_MainFooterFragment_type_cash);
                break;
            case "PERSONAL_ACCOUNT":
                mButtonChangePayment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.parsonal, 0, 0, 0);
                mButtonChangePayment.setText(R.string.fragments_MainFooterFragment_type_account);
                break;
            case "CORP_BALANCE":
                mButtonChangePayment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.man, 0, 0, 0);
                mButtonChangePayment.setText(R.string.fragments_MainFooterFragment_type_corp);
                break;
            case "CARD":
                mButtonChangePayment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.card, 0, 0, 0);
                mButtonChangePayment.setText(R.string.fragments_MainFooterFragment_type_card);
                break;
        }
    }


    private void updateList(List<Address> addresses) {
        mAddressAdapter.updateList(addresses);
        setListViewHeightBasedOnChildren(mLvAddressList);
    }


    private void callCost() {
        getSpiceManager().execute(new GetCostRequest(mProfile.getAppId(), mProfile.getApiKey(), this,
                        String.valueOf(new Date().getTime()), mOrder, mProfile, mAddressList, null),
                new CostListener(this));
    }


    private void showDialog() {
        mProgressDialog.show();
    }


    private void hideDialog() {
        mProgressDialog.dismiss();
    }


    @SuppressLint("SetTextI18n")
    @Subscribe
    public void onMessage(CallCostEvent callCostEvent) {
        String currency;
        try {
            City city = City.getCity(Profile.getProfile().getCityId());

            if (city.getCurrency().equals("RUB"))
                currency = getString(R.string.currency);
            else
                currency = city.getCurrency();
        } catch (Exception e) {
            currency = getString(R.string.currency);
        }
        if (callCostEvent.getCost().length() > 0) {
            mTvCallCost.setText("~ " + callCostEvent.getCost() + " " + currency + ", "
                    + callCostEvent.getDis() + " " + getString(R.string.activities_OrderDetailActivity_distance) + ", "
                    + callCostEvent.getTime() + " " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long));
        } else {
            mTvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_error);
        }
    }


    @Subscribe
    public void onMessage(EditOrderEvent editOrderEvent) {
        String updateId = editOrderEvent.getUpdateId();
        if (updateId != null && !updateId.isEmpty()) {
            this.mUpdateId = updateId;
            startOrderUpdate();
        } else {
            hideDialog();
            showMessage(getString(R.string.activities_EditOrderActivity_update_error));
        }
    }


    private void startOrderUpdate() {
        if (mHandlerUpdateOrder != null || mRunnableUpdateOrder != null) {
            mHandlerUpdateOrder.removeCallbacks(mRunnableUpdateOrder);
        }

        mRunnableUpdateOrder = new Runnable() {
            @Override
            public void run() {
                getSpiceManager().execute(new EditOrderResultRequest(getBaseContext(), mUpdateId, mProfile),
                        new EditOrderResultListener());
                mHandlerUpdateOrder.postDelayed(mRunnableUpdateOrder, 1000);
            }
        };

        mHandlerUpdateOrder = new Handler();

        mHandlerUpdateOrder.post(mRunnableUpdateOrder);
    }


    private void stopOrderUpdate() {
        if (mHandlerUpdateOrder != null || mRunnableUpdateOrder != null) {
            mHandlerUpdateOrder.removeCallbacks(mRunnableUpdateOrder);
        }
    }


    @Subscribe
    public void onMessage(EditOrderResultEvent editOrderResultEvent) {
        int updateResult = editOrderResultEvent.getUpdateResult();
        Log.d(TAG, "Logos onMessage: " + updateResult);
        switch (updateResult) {
            case CODE_ORDER_UPDATE_CONTINUE:
                break;
            case CODE_ORDER_UPDATE_SUCCESS:
                mOrder.save();
                mProfile.save();
                RoutePoint.getDeletePoints(mOrder);
                RoutePoint.saveOrderAddressesAndRoute(mOrder, mAddressList);
                hideDialog();
                stopOrderUpdate();
                showMessage(getString(R.string.activities_EditOrderActivity_update_success));
                isWasSave = true;
                break;
            case CODE_ORDER_UPDATE_ERROR:
            default:
                hideDialog();
                stopOrderUpdate();
                showMessage(getString(R.string.activities_EditOrderActivity_update_error));
                break;
        }
    }


    private void showMessage(String message) {
        new ToastWrapper(this, message).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_EDIT_PAYMENT:
                    String paymentType = data.getStringExtra(EXTRA_PAYMENT_TYPE);
                    String companyId = data.getStringExtra(EXTRA_COMPANY_ID);
                    String pan = data.getStringExtra(EXTRA_PAN);

                    mOrder.setPaymentType(paymentType);
                    mOrder.setPan(pan);
                    mOrder.setCompanyId(companyId);
                    updatePaymentState(mOrder.getPaymentType());
                    Log.d(TAG, "Logos onActivityResult: " + mOrder);
                    break;
                case REQUEST_EDIT_ADDRESS:
                    Address address = data.getParcelableExtra(EXTRA_ADDRESS);
                    int position = data.getIntExtra(EXTRA_POSITION, -1);
                    if (position >= ((AddressList) mAddressList).realSize()) {
                        mAddressList.add(address);
                    } else {
                        mAddressList.set(position, address);
                    }
                    Log.d(TAG, "Logos onActivityResult: " + mAddressList);
                    updateList(mAddressList);
                    updateAddAddressState(mAddressList.size());
                    callCost();
                    break;
            }
        }
    }


    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (isWasSave) {
            super.onBackPressed();
        } else {
            Intent returnIntent = new Intent(this, MainActivity.class);
            returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_OLD_ORDER);
            returnIntent.putExtra("order_id", mOrder.getOrderId());
            startActivity(returnIntent);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        hideDialog();
        stopOrderUpdate();
    }
}
