package com.kabbi.client.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.ReviewEvent;
import com.kabbi.client.fragments.CompletedMenuFragment;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.AddReviewListener;
import com.kabbi.client.network.listeners.OrderRouteListener;
import com.kabbi.client.network.requests.AddReviewRequest;
import com.kabbi.client.network.requests.GetOrderRouteRequest;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kabbi.client.app.AppParams.USE_REVIEW_DETAIL;

public class OrderCompletedActivity extends BaseSpiceActivity {

    private static final String TAG = OrderCompletedActivity.class.getSimpleName();

    public Order order;
    private AppCompatRatingBar mRatingBar;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_complete);
        order = Order.getOrder(getIntent().getStringExtra("order_id"));
        setTitle(R.string.activities_OrderCompletedActivity_title);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_bg, typedValue, true);
        TypedValue typedValueText = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        City city = City.getCity(Profile.getProfile().getCityId());
        String currency;
        if (city.getCurrency().equals("RUB"))
            currency = getString(R.string.currency);
        else
            currency = city.getCurrency();

        switch (order.getStatusId()) {
            case AppParams.STATUS_ID_COMPLETE_PAID:
                setTitle(R.string.activities_OrderCompletedActivity_paid);
                break;
            case AppParams.STATUS_ID_COMPLETE_NOPAID:
                setTitle(R.string.activities_OrderCompletedActivity_no_paid);
                break;
            default:
                setTitle(R.string.activities_OrderCompletedActivity_title);
        }

        
        initToolbar();

        ((TextView) findViewById(R.id.tv_ordercompleted_date))
                .setText(formatDate(order.getCreateTime()));

        if (order.getCost().length() > 0) {
            String cost = "0";
            try {
                JSONObject jsonCost = new JSONObject(order.getCost());
                cost = jsonCost.getString("summary_cost");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ((TextView) findViewById(R.id.tv_ordercompleted_cost)).setText(cost + " " + currency);
        }

        CompletedMenuFragment completedMenuFragment = new CompletedMenuFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.ll_menu, completedMenuFragment);
        transaction.commit();

        initView();
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void initView() {
        mRatingBar = (AppCompatRatingBar) findViewById(R.id.rb_review);
        LinearLayout ratingLayout = (LinearLayout) findViewById(R.id.ll_stars);

        if (USE_REVIEW_DETAIL) {
            mRatingBar.setVisibility(View.GONE);
            ratingLayout.setVisibility(View.VISIBLE);
        } else {
            mRatingBar.setVisibility(View.VISIBLE);
            ratingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (USE_REVIEW_DETAIL) {
            startActivity(new Intent(this, MainActivity.class)
                    .putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER));
            finish();
        } else {
            mDialog = new ProgressDialog(this);
            mDialog.setMessage(getString(R.string.activities_OrderCompletedActivity_send_review_waiting));
            sendReview();
        }

    }

    void sendReview() {
        float rating = mRatingBar.getRating();
        Log.d("Logos", String.valueOf(rating));
        if (rating > 0) {
            Profile profile = Profile.getProfile();
            getSpiceManager().execute(
                    new AddReviewRequest(profile.getAppId(), profile.getApiKey(), this, order.getOrderId(),
                            String.valueOf(Math.round(rating)),
                            "", profile.getTenantId()),
                    new AddReviewListener());
            mDialog.show();
        } else {
            startActivity(new Intent(this, MainActivity.class)
                    .putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER));
            finish();
        }
    }


    @Subscribe
    public void onMessage(ReviewEvent reviewEvent) {
        mDialog.dismiss();
        if (reviewEvent.getStatus().equals("1")) {

            order.setReview(Math.round(mRatingBar.getRating()) + "");
            order.save();

            new ToastWrapper(this, R.string.activities_ReviewActivity_toast).show();
            startActivity(new Intent(this, MainActivity.class)
                    .putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER));
            finish();
        } else {
            new ToastWrapper(this, R.string.activities_ReviewActivity_request_fail).show();
            startActivity(new Intent(this, MainActivity.class)
                    .putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER));
            finish();
        }
    }


    public void reviewOrder(View v) {
        if (order.getReview() == null || order.getReview().trim().isEmpty()) {
            startActivityForResult(new Intent(this, ReviewActivity.class).putExtra("order_id", order.getOrderId()), ReviewActivity.REQUEST_CODE_ORDER_COMPLETE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ReviewActivity.REQUEST_CODE_ORDER_COMPLETE:
                    order = Order.getOrder(getIntent().getStringExtra("order_id"));
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_stars);
                    ImageView star1 = (ImageView) linearLayout.getChildAt(0);
                    ImageView star2 = (ImageView) linearLayout.getChildAt(1);
                    ImageView star3 = (ImageView) linearLayout.getChildAt(2);
                    ImageView star4 = (ImageView) linearLayout.getChildAt(3);
                    ImageView star5 = (ImageView) linearLayout.getChildAt(4);

                    int[] drawablesArray = new int[5];
                    for (int i = 0; i < 5; i++) {
                        if (i <= (Integer.valueOf(order.getReview()) - 1)) {
                            drawablesArray[i] = R.drawable.star;
                        } else {
                            drawablesArray[i] = R.drawable.star_grey;
                        }
                    }
                    star1.setImageResource(drawablesArray[0]);
                    star2.setImageResource(drawablesArray[1]);
                    star3.setImageResource(drawablesArray[2]);
                    star4.setImageResource(drawablesArray[3]);
                    star5.setImageResource(drawablesArray[4]);

                    break;
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

}
