package com.kabbi.client.activities;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kabbi.client.R;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.LocaleHelper;
import com.kabbi.client.utils.OpenFile;
import com.kabbi.client.views.adapters.CountryActivityAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CountryActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener, AdapterView.OnItemClickListener {

    private ArrayList<SparseArray<String>> data;
    private CountryActivityAdapter countryAdapter;
    private ListView listView;
    private MenuItem menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_country);
        setTitle(R.string.activities_CountryActivity_title);

        initToolbar();
        initCountryList();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initCountryList() {
        data = new ArrayList<>();
        listView = (ListView) findViewById(R.id.list_country);
        JSONArray jsonArray;
        String language = LocaleHelper.getLanguage(this);

        try {
            jsonArray = new JSONArray((new OpenFile(this, "phonecodes.json")).getJson());
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject objectMask = new JSONObject(jsonArray.getString(i));
                SparseArray<String>mask = new SparseArray<>();
                if (language.equals("ru")) mask.append(0, objectMask.getString("name_ru"));
                else mask.append(0, objectMask.getString("name_en"));
                mask.append(1, objectMask.getString("mask"));
                data.add(mask);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        countryAdapter = new CountryActivityAdapter(this, data);
        listView.setAdapter(countryAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Profile profile = Profile.getProfile();
        profile.setCountry((view.findViewById(R.id.item_country))
                .getTag(R.id.tag_item_name).toString());
        profile.setPhoneMask((view.findViewById(R.id.item_country))
                .getTag(R.id.tag_item_type).toString());
        profile.save();
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String temp) {
        if (temp.length() >= 1){
            ArrayList<SparseArray<String>>dataNew = new ArrayList<>();
            for (SparseArray<String> itemArr : data) {
                if (itemArr.get(0).toLowerCase().indexOf(temp.toLowerCase()) >= 0){
                    dataNew.add(itemArr);
                }
            }
            countryAdapter = new CountryActivityAdapter(this, dataNew);
            listView.setAdapter(countryAdapter);
        } else {
            countryAdapter = new CountryActivityAdapter(this, data);
            listView.setAdapter(countryAdapter);
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_country, menu);
        menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.activities_CountryActivity_search_hint));
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            try {
                MenuItemCompat.expandActionView(menuItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}