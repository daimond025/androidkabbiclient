package com.kabbi.client.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.geo.CarFreeEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.maps.google.PublicTransportMap;
import com.kabbi.client.models.Car;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.network.listeners.CarsListener;
import com.kabbi.client.network.requests.GetCarsRequest;
import com.kabbi.client.services.LocationService;
import com.kabbi.client.views.adapters.TariffAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class PublicTransportActivity extends BaseSpiceActivity {

    public static final String EXTRA_TRANSPORT_CITY_ID = "EXTRA_TRANSPORT_CITY_ID";
    public static final String EXTRA_CURRENT_POSITION = "EXTRA_CURRENT_POSITION";
    public static final String EXTRA_CURRENT_TARIFF_ID = "EXTRA_CURRENT_TARIFF_ID";
    public static final int TARIFF_UPDATE_INTERVAL = 15_000;

    public static final String BUS_CLASS = "110";
    public static final String TROLLEY_CLASS = "111";
    public static final String TRAM_CLASS = "112";
    public static final String FOLLOW_BUS_CLASS = "113";

    private Profile mProfile;
    private City mCity;
    private PublicTransportMap mPublicTransportMap;
    private LatLng mCurrentPosition;
    private Handler mUpdateTariffHandler;
    private Runnable mUpdateTariffRunnable;
    private ImageButton ibCurrentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setTheme(Profile.getProfile().getThemeId()); // TODO
        setContentView(R.layout.activity_public_transport);
        setTitle(R.string.activities_PublicTransportActivity_title);

        prepare();

        int hasWriteLocPermission = ContextCompat
                .checkSelfPermission(PublicTransportActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteLocPermission == PackageManager.PERMISSION_GRANTED) {
            LocationService.isStarted = true;
            startService(new Intent(this, LocationService.class));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void prepare() {
        prepareToolbar();
        prepareVars();
        prepareView();
    }


    private void prepareToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_transport);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }


    private void prepareVars() {
        mProfile = Profile.getProfile();

        String cityId = getIntent().getStringExtra(EXTRA_TRANSPORT_CITY_ID); // TODO
        mCurrentPosition = getIntent().getParcelableExtra(EXTRA_CURRENT_POSITION); // TODO

        mCity = City.getCity(cityId);
    }


    private void prepareView() {

        ibCurrentPosition = (ImageButton) findViewById(R.id.ib_current_position);
        ibCurrentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new GetCurrentLocationEvent());
            }
        });

        final TextView tvBus = (TextView) findViewById(R.id.tv_bus);
        tvBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvBus.setSelected(!tvBus.isSelected());
                changeCarVisibility(!tvBus.isSelected(), BUS_CLASS);
                if (tvBus.isSelected()) {
                    tvBus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.bus_disabled),
                            null,
                            null);
                } else {
                    tvBus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.bus),
                            null,
                            null);
                }
            }
        });


        final TextView tvFollowBus = (TextView) findViewById(R.id.tv_follow_bus);
        tvFollowBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvFollowBus.setSelected(!tvFollowBus.isSelected());
                changeCarVisibility(!tvFollowBus.isSelected(), FOLLOW_BUS_CLASS);

                if (tvFollowBus.isSelected()) {
                    tvFollowBus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.follow_bus_disabled),
                            null,
                            null);
                } else {
                    tvFollowBus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.follow_bus),
                            null,
                            null);
                }
            }
        });


        final TextView tvTrolleybus = (TextView) findViewById(R.id.tv_trolleybus);
        tvTrolleybus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTrolleybus.setSelected(!tvTrolleybus.isSelected());
                changeCarVisibility(!tvTrolleybus.isSelected(), TROLLEY_CLASS);

                if (tvTrolleybus.isSelected()) {
                    tvTrolleybus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.trolley_disabled),
                            null,
                            null);
                } else {
                    tvTrolleybus.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.trolley),
                            null,
                            null);
                }
            }
        });



        final TextView tvTram = (TextView) findViewById(R.id.tv_tram);
        tvTram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTram.setSelected(!tvTram.isSelected());
                changeCarVisibility(!tvTram.isSelected(), TRAM_CLASS);

                if (tvTram.isSelected()) {
                    tvTram.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.tram_disabled),
                            null,
                            null);
                } else {
                    tvTram.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            ContextCompat.getDrawable(getBaseContext(), R.drawable.tram),
                            null,
                            null);
                }
            }
        });


        LatLng latLng = new LatLng(mCurrentPosition.latitude, mCurrentPosition.longitude);
        mPublicTransportMap = PublicTransportMap.newInstance(latLng);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_transport_map_container, mPublicTransportMap, "main_transport")
                .commit();
    }


    @Override
    protected void onResume() {
        super.onResume();
        startUpdateTariffs();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mUpdateTariffHandler != null && mUpdateTariffRunnable != null) {
            mUpdateTariffHandler.removeCallbacks(mUpdateTariffRunnable);
        }
    }


    private void changeCarVisibility(boolean isVisible, String carClass) {
        if (isVisible) {
            mPublicTransportMap.showCarsByClass(carClass);
        } else {
            mPublicTransportMap.hideCarsByClass(carClass);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startUpdateTariffs() {
        if (mUpdateTariffHandler != null && mUpdateTariffRunnable != null) {
            mUpdateTariffHandler.removeCallbacks(mUpdateTariffRunnable);
        }

        mUpdateTariffHandler = new Handler();
        mUpdateTariffRunnable = new Runnable() {
            @Override
            public void run() {
                findCarsByTariff();
                mUpdateTariffHandler.postDelayed(mUpdateTariffRunnable, TARIFF_UPDATE_INTERVAL);
            }
        };
        mUpdateTariffHandler.post(mUpdateTariffRunnable);
    }


    private void findCarsByTariff() {
        getSpiceManager().execute(new GetCarsRequest(
                mProfile.getAppId(),
                mProfile.getApiKey(),
                this,
                mCity.getCityId(),
                String.valueOf(mCurrentPosition.latitude),
                String.valueOf(mCurrentPosition.longitude),
                mProfile.getTenantId(),
                true,
                AppParams.RADIUS_PUBLIC_TANSPORT
        ), new CarsListener());
    }


    @Subscribe
    public void onMessage(CarFreeEvent carFreeEvent) {
        List<Car> cars = carFreeEvent.getCarList();
        mPublicTransportMap.showCars(cars);
    }

}
