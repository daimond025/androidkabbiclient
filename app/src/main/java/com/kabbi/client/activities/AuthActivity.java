package com.kabbi.client.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;

import com.kabbi.client.R;
import com.kabbi.client.fragments.AuthSignInFragment;
import com.kabbi.client.fragments.AuthSignUpFragment;
import com.kabbi.client.models.Profile;
import com.kabbi.client.views.adapters.AuthFragmentPagerAdapter;

public class AuthActivity extends AppCompatActivity {

    public static final String FRAGMENT_KEY = "auth_activity.fragment_key";
    public static final String FRAGMENT_SIGN_UP = "auth_activity.fragment_sign_up";
    public static final String FRAGMENT_SIGN_IN = "auth_activity.fragment_sign_in";

    private ViewPager viewPager;
    private AuthFragmentPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_auth);
        setTitle(R.string.activities_AuthActivity_title);

        initToolbar();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        TypedValue typedValueIndicaton = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_icon_bg, typedValueIndicaton, true);
        TypedValue typedValueText = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);
        viewPager = (ViewPager) findViewById(R.id.auth_pager);
        pagerAdapter = new AuthFragmentPagerAdapter(getSupportFragmentManager(),
                AuthActivity.this);
        pagerAdapter.addFragment(AuthSignUpFragment.newInstance());
        pagerAdapter.addFragment(AuthSignInFragment.newInstance());
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.auth_tabs);
        tabLayout.setSelectedTabIndicatorColor(typedValueIndicaton.data);
        tabLayout.setTabTextColors(typedValueText.data, typedValueText.data);
        assert tabLayout != null;
        tabLayout.setupWithViewPager(viewPager);
        String currentFragment = getIntent().getStringExtra(FRAGMENT_KEY);
        if (currentFragment != null) setFragment(currentFragment);
    }

    public void setFragment(String requiredFragment) {
        if (requiredFragment.equals(FRAGMENT_SIGN_UP)) {
            viewPager.setCurrentItem(0);
        } else if (requiredFragment.equals(FRAGMENT_SIGN_IN)) {
            viewPager.setCurrentItem(1);
        }
    }

    public void updateSignInFragViews() {
        AuthSignInFragment signInFragment = (AuthSignInFragment) pagerAdapter.getItem(1);
        if (signInFragment != null) signInFragment.updateViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent backIntent = new Intent(this, MainActivity.class);
        backIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_AUTH);
        NavUtils.navigateUpTo(this, backIntent);
    }

    @Override
    protected void onDestroy() {
        viewPager.clearOnPageChangeListeners();
        super.onDestroy();
    }

}
