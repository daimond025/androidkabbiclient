package com.kabbi.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Profile;


public class DetailAddressActivity extends AppCompatActivity {

    private TextInputEditText etStreet;
    private TextInputEditText etPorch;
    private TextInputEditText etApt;
    private TextInputEditText etComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_detail_address);
        setTitle(R.string.activities_DetailAddressActivity_title);
        initToolbar();

        initViews();
        setUpViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        etStreet = (TextInputEditText) findViewById(R.id.et_detail_street);
        if (etStreet != null && !AppParams.WITH_STREET) etStreet.setVisibility(View.GONE);

        etPorch = (TextInputEditText) findViewById(R.id.et_detail_porch);
        if (etPorch != null && !AppParams.WITH_PORCH) etPorch.setVisibility(View.GONE);

        etApt = (TextInputEditText) findViewById(R.id.et_detail_apt);
        if (etApt != null && !AppParams.WITH_FLAT) etApt.setVisibility(View.GONE);

        etComment = (TextInputEditText) findViewById(R.id.et_detail_comment);
        if (etComment != null && !AppParams.WITH_COMMENT) etComment.setVisibility(View.GONE);

    }

    private void setUpViews() {
        String street = getIntent().getStringExtra("street");
        if (street == null) {
            TextInputLayout layoutStreet = (TextInputLayout)
                    findViewById(R.id.layout_detail_street);
            if (layoutStreet != null) layoutStreet.setVisibility(View.GONE);
        } else {
            if (!street.equals(getString(R.string.fragments_MainHeaderFragment_gps_address)))
                etStreet.setText(street);
        }
        etPorch.setText(getIntent().getStringExtra("porch"));
        etApt.setText(getIntent().getStringExtra("apt"));
        etComment.setText(getIntent().getStringExtra("comment"));
        etComment.setOnEditorActionListener(new TextInputEditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onBackPressed();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (etStreet.getVisibility() == View.VISIBLE) {
            intent.putExtra("street", etStreet.getText().toString().trim());
        }
        intent.putExtra("porch", etPorch.getText().toString().trim());
        intent.putExtra("apt", etApt.getText().toString().trim());
        intent.putExtra("comment", etComment.getText().toString().trim());
        setResult(RESULT_OK, intent);
        finish();
    }

}
