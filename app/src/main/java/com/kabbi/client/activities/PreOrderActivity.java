package com.kabbi.client.activities;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.OrderInfoEvent;
import com.kabbi.client.events.api.client.RejectOrderEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.OrderInfoListener;
import com.kabbi.client.network.listeners.RejectOrderListener;
import com.kabbi.client.network.listeners.RejectResultOrderListener;
import com.kabbi.client.network.requests.GetOrderInfoRequest;
import com.kabbi.client.network.requests.RejectOrderRequest;
import com.kabbi.client.network.requests.RejectOrderResultRequest;
import com.kabbi.client.utils.TimeHelper;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.kabbi.client.utils.persian.PersianCalendarUtils.getPersianFormat;

public class PreOrderActivity extends BaseSpiceActivity {

    private Profile profile;
    private Order order;
    private SimpleDraweeView simpleDraweeDriver;
    private Handler getInfoHandler;
    private Runnable getInfoRunnable;
    private ProgressDialog cancelProgressDialog;
    private SimpleDraweeView mSimpleDraweeView;
    private List<Address> mAddressList;

    private Handler rejectHandler;
    private Runnable rejectRunnable;
    private int requestCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profile = Profile.getProfile();
        setTheme(profile.getThemeId());
        order = Order.getOrder(getIntent().getStringExtra("order_id"));

        setContentView(R.layout.activity_preorder);
        cancelProgressDialog = new ProgressDialog(this);

        EventBus.getDefault().register(this);

        loadOrderInfo();

        startOrderInfo();

    }

    @Override
    protected void onStart() {
        super.onStart();

        getSpiceManager().execute(new GetOrderInfoRequest(
                profile.getAppId(), profile.getApiKey(), getApplicationContext(),
                String.valueOf((new Date()).getTime()), order, profile.getTenantId()),
                new OrderInfoListener());
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (getInfoHandler != null)
            getInfoHandler.removeCallbacks(getInfoRunnable);
        if (rejectHandler != null)
            rejectHandler.removeCallbacks(rejectRunnable);
        super.onDestroy();
    }

    private void loadOrderInfo() {

        switch (profile.getCalendar()) {
            case Profile.GREGORIAN_CALENDAR:
                setTitle(formatDate(TimeHelper.getFakeTimeFromString(order.getOrderTime()).getTime()));
                break;
            case Profile.PERSIAN_CALENDAR:
                String time = order.getOrderTime();
                setTitle(getPersianFormat(time));
                break;
        }
        initToolbar();

        mSimpleDraweeView = (SimpleDraweeView) findViewById(R.id.sdv_map);
        simpleDraweeDriver = (SimpleDraweeView) findViewById(R.id.sdv_detailorder_face);
        if (!AppParams.USE_PHOTO) simpleDraweeDriver.setVisibility(View.INVISIBLE);

        mAddressList = RoutePoint.getRoute(order);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_addresslist);

        int count = 0;
        int[] icons = {R.drawable.a,
                R.drawable.b,
                R.drawable.c,
                R.drawable.d,
                R.drawable.e};

        for (Address address : mAddressList) {
            try {

                View view = LayoutInflater.from(this).inflate(R.layout.item_address, null);
                ((ImageView) view.findViewById(R.id.iv_item_icon)).setImageResource(icons[count]);
                ((TextView) view.findViewById(R.id.tv_item_street)).setText(address.getLabel());
                ((TextView) view.findViewById(R.id.tv_item_city)).setText(address.getCity());

                linearLayout.addView(view);
                count++;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        } else {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(getString(R.string.activities_PreOrderActivity_driver_empty));
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }

        ((TextView) findViewById(R.id.tv_lastorders_status)).setText(formatTime(new Date().getTime(), order.getOrderTime()));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        setListRealHeight();
    }

    public void setListRealHeight() {
        int size = mSimpleDraweeView.getMeasuredWidth();

        String[] labels = {"A","B","C","D","E"};
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < mAddressList.size(); i++ ) {
            builder.append(String.format("&markers=color:black|label:%s|" + mAddressList.get(i).getLat()
                    + "," + mAddressList.get(i).getLon(), labels[i]));
        }

        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?" + builder +
                "&size=" + size + "x" + size + "&language=" + Locale.getDefault().getLanguage()
                + "&maptype=roadmap" + "&path=" + "color:0x4f4a9f%7Cweight:5%7Cenc:" + order.getRoute());

        mSimpleDraweeView.getLayoutParams().height = size;
        mSimpleDraweeView.setImageURI(uri);
    }

    public void rejectOrder(View v) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.MyCustomAlertDialogTheme);
            alertDialog.setTitle(getString(R.string.fragments_MainFragment_delete_order1));
            alertDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order2));
            alertDialog.setPositiveButton(getString(R.string.fragments_MainFragment_yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //(findViewById(R.id.btn_cancel_order)).setEnabled(false);
                            cancelProgressDialog.setMessage(getResources()
                                    .getString(R.string.fragments_MainFragment_delete_order_execute));
                            cancelProgressDialog.setCancelable(false);
                            cancelProgressDialog.show();
                            getSpiceManager().execute(new RejectOrderRequest(profile.getAppId(), profile.getApiKey(),
                                            getApplicationContext(),
                                    String.valueOf((new Date()).getTime()),
                                    order, profile.getTenantId()),
                                    new RejectOrderListener());
                        }
                    });
            alertDialog.setNegativeButton(getString(R.string.fragments_MainFragment_no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {

        order.setStatusLabel(orderInfoEvent.getStatusLabel());
        order.setStatus(orderInfoEvent.getStatus());
        order.setCar(orderInfoEvent.getCarDesc());
        order.setDriver(orderInfoEvent.getDriverName());
        order.setPhoto(orderInfoEvent.getPhoto());
        order.setCost(orderInfoEvent.getCost());
        order.setStatusId(orderInfoEvent.getStatusId());
        order.save();

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }

        switch (orderInfoEvent.getStatus()) {
            case "completed":
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);

                startActivity(new Intent(getApplicationContext(), OrderCompletedActivity.class).putExtra("order_id", order.getOrderId()));
                break;
            case "rejected":
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);
                if (rejectHandler != null)
                    rejectHandler.removeCallbacks(rejectRunnable);
                order.setStatusLabel(getString(R.string.rejected));
                order.save();

                startActivity(new Intent(getApplicationContext(), OrderDetailActivity.class).putExtra("order_id", order.getOrderId()).putExtra("type", "main").putExtra("status", "rejected"));
                break;
            case "pre_order":

                break;
            default:
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER);
                intent.putExtra("order_id", order.getOrderId());
                startActivity(intent);
                finish();

        }
    }

    @Subscribe
    public void onMessage(RejectOrderEvent event) {
        switch (event.getType()) {
            case "begin":
                startRejectOrder();
                break;
            case "process":
                if (requestCount > 4)
                    EventBus.getDefault().post(new OrderInfoEvent("rejected", "rejected", "", "", "", "",
                            0, 0, (float) 0, 0, "", "0", "0", "", "", "", null, null));
                else requestCount++;
                break;
            case "complete":
                break;
            case "error":
                new ToastWrapper(this, R.string.error_reject_order).show();
                if (rejectHandler != null)
                    rejectHandler.removeCallbacks(rejectRunnable);
                cancelProgressDialog.cancel();
                break;
            default:
                break;
        }
    }

    private void startRejectOrder() {
        if (rejectHandler != null && rejectRunnable != null) {
            getInfoHandler.removeCallbacks(getInfoRunnable);
        }
        rejectHandler = new Handler();
        rejectRunnable = new Runnable() {
            @Override
            public void run() {
                getSpiceManager().execute(
                        new RejectOrderResultRequest(
                                profile.getAppId(), profile.getApiKey(),
                                order , Profile.getProfile().getTenantId()),
                        new RejectResultOrderListener());
                rejectHandler.postDelayed(rejectRunnable, 1000);
            }
        };
        rejectHandler.post(rejectRunnable);
    }

    private void startOrderInfo() {
        if (getInfoHandler != null && getInfoRunnable != null) {
            getInfoHandler.removeCallbacks(getInfoRunnable);
        }
        getInfoHandler = new Handler();
        getInfoRunnable = new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.tv_lastorders_status)).setText(formatTime(new Date().getTime(), order.getOrderTime()));

                getSpiceManager().execute(new GetOrderInfoRequest(profile.getAppId(), profile.getApiKey(),  getApplicationContext(),
                        String.valueOf((new Date()).getTime()), order, profile.getTenantId()),
                        new OrderInfoListener());
                getInfoHandler.postDelayed(getInfoRunnable, 5000);

            }
        };
        ((TextView) findViewById(R.id.tv_lastorders_status)).setText(formatTime(new Date().getTime(), order.getOrderTime()));
        getInfoHandler.post(getInfoRunnable);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(long longDateCreate, String strDatePlant) {
        Date datePlant = TimeHelper.getFakeTimeFromString(strDatePlant);

        String str = "%d " + getString(R.string.activities_PreOrderActivity_date_day_short) + ". %d " + getString(R.string.activities_PreOrderActivity_date_hour_short) + ". %d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_short) + ".";

        if (datePlant != null) {
            long mSec = datePlant.getTime() - longDateCreate;
            if (mSec > 0) {
                long days = TimeUnit.MILLISECONDS.toDays(mSec);
                long hours = TimeUnit.MILLISECONDS.toHours(mSec) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mSec));
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mSec) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mSec));
                return String.format(str, days, hours, minutes);
            } else {
                return String.format(str, 0, 0, 0);
            }
        } else {
            return String.format(str, 0, 0, 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, PreOrdersActivity.class));
        finish();
    }

}
