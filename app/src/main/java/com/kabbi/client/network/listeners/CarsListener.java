package com.kabbi.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.geo.CarFreeEvent;
import com.kabbi.client.models.Car;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CarsListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {

        try {
            Log.d("CARS", new String(((TypedByteArray) response.getBody()).getBytes()));

            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
            if (jsonObject.getString("info").equals("OK")) {
                JSONArray jsonArrayCar = jsonObject.getJSONObject("result").getJSONArray("cars");
                List<Car> carList = new ArrayList<>();
                for (int i = 0; i < jsonArrayCar.length(); i++) {
                    Car car = new Car(Double.valueOf(jsonArrayCar.getJSONObject(i).getString("car_lat")),
                            Double.valueOf(jsonArrayCar.getJSONObject(i).getString("car_lon")),
                            jsonArrayCar.getJSONObject(i).getString("car_number"),
                            jsonArrayCar.getJSONObject(i).optString("car_class", ""),
                            Float.valueOf(jsonArrayCar.getJSONObject(i).getString("degree"))
                            );
                    carList.add(car);
                }
                EventBus.getDefault().post(new CarFreeEvent(carList));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
