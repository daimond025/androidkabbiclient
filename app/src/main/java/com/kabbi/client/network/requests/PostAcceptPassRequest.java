package com.kabbi.client.network.requests;


import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.App;
import com.kabbi.client.utils.AppPreferences;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class PostAcceptPassRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String phone;
    private String password;
    private int city_id;
    private String time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;
    private String referall;
    private String deviceToken;

    public PostAcceptPassRequest(String appId, String apiKey,  Context context, String phone,
                                 String password, int city_id, String time, String tenantid, String referral) {
        super(Response.class, IGootaxApi.class);


        this.deviceToken = new AppPreferences(context).getText("device_token");

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", String.valueOf(city_id));
        requestParams.put("current_time", time);
        requestParams.put("device_token", deviceToken);
        requestParams.put("password", password);
        requestParams.put("phone", phone);

        if (referral != null) {
            requestParams.put("referral_code", referral);
            this.referall = referral;
        }

        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
        this.phone = phone;
        this.password = password;
        this.city_id = city_id;
        this.time = time;
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().acceptPassword(signature,
                typeclient, tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, city_id, time, deviceToken, password, phone, referall);
    }

}
