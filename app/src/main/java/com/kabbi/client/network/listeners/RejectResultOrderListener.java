package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.RejectOrderEvent;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 17.10.16.
 */

public class RejectResultOrderListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("Logos", new String(((TypedByteArray) response.getBody()).getBytes()));
            String code = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getString("code");
            switch (code) {
                case "0":
                    JSONObject result = new JSONObject(new String(((TypedByteArray)
                            response.getBody()).getBytes())).getJSONObject("result");
                    String rejectCode = result.getString("reject_result");
                    if (rejectCode.equals("1"))
                        EventBus.getDefault().post(new RejectOrderEvent("complete"));
                    else
                        EventBus.getDefault().post(new RejectOrderEvent("error"));
                case "300":
                    EventBus.getDefault().post(new RejectOrderEvent("process"));
                    break;
                default:
                    EventBus.getDefault().post(new RejectOrderEvent("error"));
                    break;
            }
        } catch (JSONException e) {
        }
    }
}


