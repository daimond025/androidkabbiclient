package com.kabbi.client.network.listeners;


import com.kabbi.client.events.api.client.RejectOrderEvent;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class RejectOrderListener implements RequestListener<Response> {


    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            String code = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getString("code");
            if (code.equals("0")) {
                EventBus.getDefault().post(new RejectOrderEvent("begin"));
            }
        } catch (JSONException e) {

        }
    }
}
