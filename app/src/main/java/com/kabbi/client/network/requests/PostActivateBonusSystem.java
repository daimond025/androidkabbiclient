package com.kabbi.client.network.requests;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 08.09.16.
 */
public class PostActivateBonusSystem extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String tenantid;
    private String typeclient;
    private String phone;
    private String promoCode;
    private String currentTime;
    private String versionclient;
    private String appId;

    public PostActivateBonusSystem(String appId, String apiKey, String tenantid, String phone, String promoCode) {
        super(Response.class, IGootaxApi.class);

        this.phone = phone;
        this.tenantid = tenantid;
        this.promoCode = promoCode;
        this.typeclient = "android";
        this.currentTime = String.valueOf(new Date().getTime());
        this.lang = Locale.getDefault().getLanguage();
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.currentTime);
        requestParams.put("client_phone", this.phone);
        requestParams.put("promo_code", this.promoCode);
        this.signature = HashMD5.getSignature(requestParams, apiKey);
    }


    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().postActivateBonusSystem(signature, tenantid, lang, typeclient, versionclient, appId, BuildConfig.VERSION_NAME, currentTime, phone, promoCode);
    }
}
