package com.kabbi.client.network.requests;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 24.08.16.
 */
public class GetClientProfileRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String tenantid;
    private String phone;
    private String currentTime;
    private String typeclient;
    private String versionclient;
    private String appId;

    public GetClientProfileRequest(String appId, String apiKey,  String phone, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.phone = phone;
        this.tenantid = tenantid;
        this.currentTime = String.valueOf(new Date().getTime());
        this.typeclient = "android";
        this.versionclient = AppParams.CLIENT_VERSION;
        this.lang = Locale.getDefault().getLanguage();
        this.appId = appId;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.currentTime);
        requestParams.put("phone", this.phone);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getClientProfileResult(signature, tenantid, lang, typeclient, versionclient, appId, BuildConfig.VERSION_NAME, currentTime, phone);
    }
}
