package com.kabbi.client.network.requests;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.models.City;
import com.kabbi.client.utils.DetectCity;
import com.kabbi.client.utils.TimeHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.client.Response;

public class ConstingOrderRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;

    private String additional_options = "";
    private String address = "";
    private String city_id = "";
    private String client_phone = "";
    private String order_time = "";
    private String tariff_id = "";
    private String time;
    private Map<String, String> tariffsMap = new LinkedHashMap<>();


    public ConstingOrderRequest(String appId, String apiKey, Context context, String time, Order order, Profile profile,
                              List<Address> addressList) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        JSONObject jsonObjectSource = new JSONObject();
        JSONArray jsonArrayAddress = new JSONArray();
        for (Address address : addressList) {
            if (!address.isEmpty()) {
                try {
                    JSONObject jsonObjectAddressFrom = new JSONObject();
                    jsonObjectAddressFrom.put("city_id", profile.getCityId());
                    jsonObjectAddressFrom.put("city", address.getCity());
                    if (address.getType().equals("house"))
                        jsonObjectAddressFrom.put("street", address.getStreet());
                    else
                        jsonObjectAddressFrom.put("street", address.getLabel());
                    jsonObjectAddressFrom.put("house", address.getHouse());
                    jsonObjectAddressFrom.put("housing", address.getHousing());
                    jsonObjectAddressFrom.put("porch", address.getPorch());
                    jsonObjectAddressFrom.put("apt", address.getApt());
                    jsonObjectAddressFrom.put("lat", address.getLat());
                    jsonObjectAddressFrom.put("lon", address.getLon());
                    jsonArrayAddress.put(jsonObjectAddressFrom);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            jsonObjectSource.put("address", jsonArrayAddress);
            this.address = URLEncoder.encode(jsonObjectSource.toString(), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

        List<Tariff> tariffList = Tariff.getTariffs(City.getCity(profile.getCityId()));

        for (int i = 0; i < tariffList.size(); i++) {
            Tariff tariff = tariffList.get(i);
            tariffsMap.put("tariff_id[" + i + "]", tariff.getTariffId());
        }

        this.client_phone = profile.getPhone();
        this.tariff_id = order.getTariff(); // TODO
        this.time = time;
        this.versionclient = AppParams.CLIENT_VERSION;

        this.city_id = profile.getCityId();


        if (!order.getOrderTime().isEmpty()) this.order_time = order.getOrderTime();


        try {
            StringBuilder sbOptions = new StringBuilder();
            List<Option> options = Option.getOptionsChecked(order.getTariff());
            for (Option option : options) {
                sbOptions.append(option.getOptionIdSecondary());
                sbOptions.append(",");
            }

            if (options.size() > 0) {
                try {
                    this.additional_options = URLEncoder.encode(sbOptions.toString()
                            .substring(0, sbOptions.toString().length() - 1), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestParams.put("additional_options", this.additional_options);
        requestParams.put("address", this.address);
        requestParams.put("city_id", this.city_id);
        requestParams.put("current_time", this.time);
        try {
            requestParams.put("order_time", URLEncoder.encode(this.order_time, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        requestParams.put("phone", this.client_phone);
        requestParams.putAll(tariffsMap);
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = profile.getTenantId();
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().postCostingOrder(deviceid, signature, typeclient, tenantid, lang,
                versionclient, appId, BuildConfig.VERSION_NAME, additional_options, address,
                city_id, time, order_time, client_phone, tariffsMap);
    }

}
