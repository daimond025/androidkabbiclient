package com.kabbi.client.network.listeners.geo_listeners;

import com.kabbi.client.events.api.geo.AutoCompleteEvent;

public class AutoCompleteListener extends GeoListener {

    public AutoCompleteListener() {
        super(new AutoCompleteEvent());
    }

}
