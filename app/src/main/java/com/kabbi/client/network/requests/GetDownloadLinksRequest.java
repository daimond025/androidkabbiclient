package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by andrew on 19/12/2017.
 */

public class GetDownloadLinksRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String time;
    private String signature;
    private String typeClient;
    private String tenantId;
    private String lang;
    private String deviceId;
    private String versionClient;
    private String appId;

    public GetDownloadLinksRequest(Context context, String appId, String apiKey, String tenantId) {
        super(Response.class, IGootaxApi.class);

        this.time = String.valueOf(System.currentTimeMillis());
        this.appId = appId;
        this.typeClient = "android";
        this.deviceId = GenUDID.getUDID(context);
        this.lang = Locale.getDefault().getLanguage();
        this.tenantId = tenantId;
        this.versionClient = AppParams.CLIENT_VERSION;;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.time);
        this.signature = HashMD5.getSignature(requestParams, apiKey);

    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getDownloadLinks(deviceId, signature, typeClient, tenantId, lang, versionClient, appId, BuildConfig.VERSION_NAME, time);
    }
}
