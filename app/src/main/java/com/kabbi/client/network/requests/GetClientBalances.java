package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.App;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;
import retrofit.http.Header;
import retrofit.http.Query;

/**
 * Created by gootax on 02.12.16.
 */

public class GetClientBalances extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String deviceid;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String versionclient;
    private String appId;

    private String current_time;
    private String city_id;
    private String phone;

    public GetClientBalances(String appId, String apiKey, Context context, String tenantid, String city_id, String phone) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());
        this.city_id = city_id;
        this.phone = phone;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.current_time);
        requestParams.put("city_id", city_id);
        requestParams.put("phone", this.phone);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getClientBalances(deviceid, signature, typeclient, tenantid,
                lang, versionclient, appId, BuildConfig.VERSION_NAME, current_time, city_id, phone);
    }
}
