package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class AddReviewRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String current_time;
    private String order_id;
    private String grade;
    private String versionclient;
    private String text = "";
    private String appId;

    public AddReviewRequest(String appId, String apiKey,  Context context, String order_id,
                            String grade, String text, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());
        this.order_id = order_id;
        this.grade = grade;
        try {
            this.text = URLEncoder.encode(text, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", current_time);
        requestParams.put("grade", this.grade);
        requestParams.put("order_id",this.order_id);
        requestParams.put("text", this.text);
        this.signature = HashMD5.getSignature(requestParams, apiKey);

        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().addReview(signature, typeclient,
                tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, current_time, grade, order_id, text);
    }
}
