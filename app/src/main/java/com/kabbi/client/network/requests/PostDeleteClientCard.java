package com.kabbi.client.network.requests;

import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;


public class PostDeleteClientCard extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String versionclient;
    private String tenantid;
    private String clientId;
    private String currentTime;
    private String phone;
    private String pan;
    private String appId;
    private String typeClient;

    public PostDeleteClientCard(String appId, String apiKey,
                                String clientId, String phone, String tenantid, String pan, String typeClient) {
        super(Response.class, IGootaxApi.class);

        this.tenantid = tenantid;
        this.clientId = clientId;
        this.currentTime = String.valueOf(new Date().getTime());
        this.lang = Locale.getDefault().getLanguage();
        this.phone = phone;
        this.pan = pan;
        this.typeClient = typeClient;

        Log.d("Logos", "Pan " + pan);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("client_id", clientId);
        requestParams.put("current_time", currentTime);
        requestParams.put("phone", phone);
        requestParams.put("pan", pan);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().postDeleteClientCard(signature, tenantid, lang, typeClient, versionclient, appId, BuildConfig.VERSION_NAME, clientId, currentTime, phone, pan);
    }

}
