package com.kabbi.client.network.interfaces;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

public interface IGeoGootaxApi {

    @GET("/reverse")
    Response getReverseResult(
            @Query("city_id") String cityId,
            @Query("format") String format,
            @Query("hash") String hash,
            @Query("lang") String lang,
            @Query("point.lat") String lat,
            @Query("point.lon") String lon,
            @Query("radius") String rad,
            @Query("size") String size,
            @Query("tenant_id") String tenantId,
            @Query("type_app") String typeApp
    );

    @GET("/autocomplete")
    Response getAutoCompleteResult(
            @Query("city_id") String cityId,
            @Query("focus.point.lat") String lat,
            @Query("focus.point.lon") String lon,
            @Query("format") String format,
            @Query("hash") String hash,
            @Query("lang") String lang,
            @Query("tenant_id") String tenantId,
            @Query("text") String text,
            @Query("type_app") String typeApp,
            @Query("radius") String radius
    );

    @GET("/search")
    Response getSearchResult(
            @Query("city_id") String cityId,
            @Query("focus.point.lat") String lat,
            @Query("focus.point.lon") String lon,
            @Query("format") String format,
            @Query("hash") String hash,
            @Query("lang") String lang,
            @Query("tenant_id") String tenantId,
            @Query("text") String text,
            @Query("type_app") String typeApp
    );

}
