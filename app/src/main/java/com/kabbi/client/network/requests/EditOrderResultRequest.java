package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by andrew on 24/10/2017.
 */

public class EditOrderResultRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {


    private String currentTime;
    private String signature;
    private String typeClient;
    private String tenantId;
    private String lang;
    private String deviceId;
    private String versionClient;
    private String appId;

    private String mUpdateId;
    private String mPhone;


    public EditOrderResultRequest(Context context, String updateId, Profile profile) {
        super(Response.class, IGootaxApi.class);

        currentTime = String.valueOf(new Date().getTime());
        mUpdateId = updateId;
        mPhone = profile.getPhone();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", currentTime);
        requestParams.put("phone", mPhone);
        requestParams.put("update_id", updateId);

        this.signature = HashMD5.getSignature(requestParams, profile.getApiKey());
        this.typeClient =  "android";
        this.tenantId = profile.getTenantId();
        this.lang = Locale.getDefault().getLanguage();
        this.deviceId = GenUDID.getUDID(context);
        this.versionClient = AppParams.CLIENT_VERSION;
        this.appId = profile.getAppId();
    }


    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getUpdateOrderResult(deviceId, signature, typeClient, tenantId, lang, versionClient,
                appId, BuildConfig.VERSION_NAME, currentTime, mPhone, mUpdateId);
    }
}

