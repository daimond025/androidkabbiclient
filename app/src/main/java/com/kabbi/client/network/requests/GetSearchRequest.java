package com.kabbi.client.network.requests;

import com.kabbi.client.network.interfaces.IGeoGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetSearchRequest extends RetrofitSpiceRequest<Response, IGeoGootaxApi> {

    private String cityId;
    private String text;
    private String lat;
    private String lon;
    private String typeApp;
    private String tenantId;
    private String format;
    private String lang;
    private String hash;

    public GetSearchRequest(String apiKey,  String cityId, String text,
                            String lat, String lon, String tenantId) {
        super(Response.class, IGeoGootaxApi.class);
        this.cityId = cityId;
        this.text = text;
        this.lat = lat;
        this.lon = lon;
        this.typeApp = "client";
        this.tenantId = tenantId;
        this.format = "gootax";
        this.lang = Locale.getDefault().getLanguage();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", cityId);
        requestParams.put("focus.point.lat", lat);
        requestParams.put("focus.point.lon", lon);
        requestParams.put("format", format);
        requestParams.put("lang", lang);
        requestParams.put("tenant_id", tenantId);
        try {
            requestParams.put("text", URLEncoder.encode(text, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        requestParams.put("type_app", typeApp);
        this.hash = HashMD5.getSignature(requestParams, apiKey);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getSearchResult(cityId, lat, lon, format,
                hash, lang, tenantId, text, typeApp);
    }

}

