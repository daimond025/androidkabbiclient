package com.kabbi.client.network.requests;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 12.04.17.
 */

public class GetClientHistoryAddressRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final static String TAG = GetClientHistoryAddressRequest.class.getSimpleName();

    private String phone;
    private String time;
    private String signature;
    private String typeClient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;
    private String cityId;

    public GetClientHistoryAddressRequest(String appId, String apiKey, Context context, String tenantid,
                                          String typeClient, String phone, String cityId) {
        super(Response.class, IGootaxApi.class);

        this.time = String.valueOf(System.currentTimeMillis());
        Log.d("Referral", "GetReferralSystemListRequest: " + time);


        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();


        this.versionclient = AppParams.CLIENT_VERSION;
        this.tenantid = tenantid;
        this.typeClient = typeClient;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.appId = appId;
        this.cityId = cityId;

        requestParams.put("city_id", cityId);
        requestParams.put("current_time", time);
        if (phone != null && cityId != null) {
            this.phone = phone;
            requestParams.put("phone", phone);
        }

        this.signature = HashMD5.getSignature(requestParams, apiKey);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        Response response = getService().getClientHistoryAddress(deviceid, signature,
                typeClient, tenantid, lang, versionclient, appId, BuildConfig.VERSION_NAME, cityId, time, phone);
        Log.d(TAG, "Logos loadDataFromNetwork: " + response.getUrl());
        Log.d(TAG, "Logos loadDataFromNetwork: " + response.getBody());
        return response;
    }

}
