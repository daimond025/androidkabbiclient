package com.kabbi.client.network.listeners.geo_listeners;

import android.util.Log;

import com.kabbi.client.events.api.geo.ReverseEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.utils.HashMD5;
import com.kabbi.client.utils.Validator;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ReverseListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        boolean isGpsAddress = true;
        ArrayList<Address> listData = new ArrayList<>();
        JSONArray jsonResults = null;
        try {
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            jsonResults = jsonResult.getJSONArray("results");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        if (jsonResults != null && jsonResults.length() != 0) {
            for (int i = 0; i < jsonResults.length(); i++) {
                try {
                    JSONObject addressJson = new JSONObject(jsonResults.getString(i));
                    JSONObject addressJsonDetail = new JSONObject(Validator.validateStr(addressJson, "address"));
                    Address address = new Address();
                    address.setType(Validator.validateStr(addressJson, "type"));
                    address.setCity(Validator.validateStr(addressJsonDetail, "city"));
                    address.setStreet(Validator.validateStr(addressJsonDetail, "street"));
                    address.setHouse(Validator.validateStr(addressJsonDetail, "house"));
                    address.setLabel(Validator.validateStr(addressJsonDetail, "label"));
                    address.setLat(Validator.validateStr(addressJsonDetail, "lat"));
                    address.setLon(Validator.validateStr(addressJsonDetail, "lon"));
                    String hash = HashMD5.getHash(address.getCity() + address.getStreet() +
                            address.getHouse() + address.getLabel());
                    address.setHash(hash);
                    listData.add(address);
                    isGpsAddress = false;
                } catch (JSONException | NullPointerException ignored) {}
            }
        }
        if (listData.isEmpty()) {
            Address gpsAddress = new Address();
            listData.add(gpsAddress);
            isGpsAddress = true;
        }

        ReverseEvent event = new ReverseEvent(listData.get(0), isGpsAddress);
        EventBus.getDefault().post(event);
    }

}
