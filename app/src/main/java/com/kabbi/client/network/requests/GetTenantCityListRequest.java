package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetTenantCityListRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {
    private String time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;

    public GetTenantCityListRequest(String appId, String apiKey,  Context context, String time, String tenantid) {
        super(Response.class, IGootaxApi.class);
        this.time = time;
        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", time);
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() {
        return getService().getCities(signature, typeclient, tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, time);
    }

}
