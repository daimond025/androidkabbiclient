package com.kabbi.client.network.requests;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.Constants;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;


public class GetTariffsListRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String time;
    private String city_id;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;
    private String phone;


    public GetTariffsListRequest(String appId, String apiKey,  Context context, String time, String city_id, String phone, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", city_id);
        requestParams.put("current_time", time);

        if (phone != null && !phone.isEmpty()) {
            requestParams.put("phone", phone);
            this.phone = phone;
        }

        this.time = time;
        this.city_id = city_id;
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.versionclient = AppParams.CLIENT_VERSION;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        Log.d("Logos", deviceid);
        this.appId = appId;
        AppPreferences appPreferences = new AppPreferences(context);
        appPreferences.saveText(Constants.AP_SAVE_TARIFFS, String.valueOf(time));
    }


    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getTariffs(signature, typeclient, tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, city_id, time, phone);
    }

}
