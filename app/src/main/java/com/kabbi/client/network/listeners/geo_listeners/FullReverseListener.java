package com.kabbi.client.network.listeners.geo_listeners;

import com.kabbi.client.events.api.geo.FullReverseEvent;

public class FullReverseListener extends GeoListener {

    public FullReverseListener() {
        super(new FullReverseEvent());
    }

}