package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.models.Profile;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 06.12.16.
 */

public class PingListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("Logos", new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            long serverTime = jsonResult.getLong("time") * 1000;
            long inaccuracy = new Date().getTime() - serverTime;
            Profile profile = Profile.getProfile();
            profile.setInaccuracy(inaccuracy);
            profile.save();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
