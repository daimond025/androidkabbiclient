package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.core.BaseRequestListener;
import com.kabbi.client.utils.Validator;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 30.08.16.
 */
public class ClientProfileListener extends BaseRequestListener {

    public ClientProfileListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(Response response) {
        Log.d("Logos", new String(((TypedByteArray) response.getBody()).getBytes()));
        try {
            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
            if (jsonObject.getString("info").equals("OK")) {
                JSONObject jsonProfile = jsonObject.getJSONObject("result").getJSONObject("client_profile");

                Profile profile = Profile.getProfile();
                profile.setClientId(Validator.validateStr(jsonProfile, "client_id"));
                profile.setSurname(Validator.validateStr(jsonProfile, "surname"));
                profile.setName(Validator.validateStr(jsonProfile, "name"));
                profile.setPatronymic(Validator.validateStr(jsonProfile, "patronymic"));
                profile.setEmail(Validator.validateStr(jsonProfile, "email"));
                profile.setBirth(Validator.validateStr(jsonProfile, "birth"));
                profile.setPhoto(Validator.validateStr(jsonProfile, "photo"));
                profile.save();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
