package com.kabbi.client.network.listeners;


import android.content.Context;
import android.util.Log;

import com.kabbi.client.network.core.BaseRequestListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.CallCostEvent;

import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CostListener extends BaseRequestListener {


    public CostListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(Response response) {
        try {
            Log.d("CALLCOST", new String(((TypedByteArray)
                    response.getBody()).getBytes()));
            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            if (jsonObject.getString("info").equals("OK")) {
                JSONObject result = jsonObject.getJSONObject("result").getJSONObject("cost_result");
                EventBus.getDefault().post(new CallCostEvent(result.getString("summary_cost"),
                        result.getString("summary_distance"), result.getString("summary_time"),
                        result.getInt("is_fix") > 0));
            }

            if (jsonObject.getString("info").equals("INTERNAL_ERROR")){
                EventBus.getDefault().post(new CallCostEvent("","","", false));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
