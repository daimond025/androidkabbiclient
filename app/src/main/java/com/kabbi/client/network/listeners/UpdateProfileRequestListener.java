package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.models.PaymentType;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.ProfileEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class UpdateProfileRequestListener implements RequestListener<Response> {

    private Profile profile;
    private JSONObject jsonProfile;

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        JSONObject jsonResult = null;
        jsonProfile = null;
        profile = Profile.getProfile();
        Log.d("Logos", new String(((TypedByteArray) response.getBody())
                .getBytes()));
        int success = 0;
        try {
            jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            jsonProfile = jsonResult.getJSONObject("client_profile");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }

        fillСommonFields();

        if (jsonResult != null && jsonProfile != null) {
            success = Validator.validateInt(jsonResult, "update_result");
        }
        EventBus.getDefault().post(new ProfileEvent(profile, success));
    }

    private void fillСommonFields() {
        profile.setClientId(Validator.validateStr(jsonProfile, "client_id"));
        profile.setSurname(Validator.validateStr(jsonProfile, "surname"));
        profile.setName(Validator.validateStr(jsonProfile, "name"));
        profile.setPatronymic(Validator.validateStr(jsonProfile, "patronymic"));
        profile.setEmail(Validator.validateStr(jsonProfile, "email"));
        profile.setBirth(Validator.validateStr(jsonProfile, "birth"));
        profile.setPhoto(Validator.validateStr(jsonProfile, "photo"));
    }
}
