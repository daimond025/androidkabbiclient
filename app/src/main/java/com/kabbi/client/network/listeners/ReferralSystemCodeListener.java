package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.events.api.client.ActivateReferralCodeEvent;
import com.kabbi.client.events.api.client.ActivateReferralEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.core.BaseRequestListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 12.04.17.
 */

public class ReferralSystemCodeListener extends BaseRequestListener {

    public ReferralSystemCodeListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(Response response) {
        try {
            Log.d("Logos", new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));

            int result = jsonResult.optInt("result");

            EventBus.getDefault().post(new ActivateReferralCodeEvent(result));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
