package com.kabbi.client.network.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.CompanyPageEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CompanyRequestListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        String result = "";
        try {
            result = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result").getString("result");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        result = result.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        EventBus.getDefault().post(new CompanyPageEvent(result));
    }

}
