package com.kabbi.client.network.listeners;


import android.content.Context;

import com.activeandroid.query.Update;
import com.kabbi.client.R;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.views.ToastWrapper;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class TariffsTypeRequestListener implements RequestListener<Response> {

    private Context mContext;
    private String currentTariffId;

    public TariffsTypeRequestListener(Context context, String currentTariffId) {
        mContext = context;
        this.currentTariffId = currentTariffId;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            JSONArray jsonArrayTariffsType = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getJSONArray("result");
            if (jsonArrayTariffsType != null && jsonArrayTariffsType.length() > 0) {

                for (int i = 0; i < jsonArrayTariffsType.length(); i++) {
                    String tariffID = jsonArrayTariffsType.getJSONObject(i).getString("tariff_id");
                    String tariffType = jsonArrayTariffsType.getJSONObject(i).getString("type");
                    Tariff curTariff = Tariff.getTariffById(tariffID);
                    if (curTariff != null && !curTariff.getTariffType().equals(tariffType)) {
                        Option.updateAllOptionsChecked(tariffID);
                        new Update(Tariff.class).set("tariffType = ?", tariffType).where("tariffId = ?", tariffID).execute();
                        if (tariffID.equals(currentTariffId)) {
                            new ToastWrapper(mContext, R.string.activities_MainActivity_type_success).show();
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
