package com.kabbi.client.network.listeners;

import android.text.TextUtils;
import android.util.Log;

import com.kabbi.client.R;
import com.kabbi.client.events.api.client.AppUpdateEvent;
import com.kabbi.client.utils.DataBaseHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.SaveCitiesEvent;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Tariff;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class CitiesRequestListener implements RequestListener<Response> {

    public static final int OLD_CLIENT_VERSION = 105;

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        EventBus.getDefault().post(new SaveCitiesEvent(false,
                R.string.activities_OptionsActivity_progress_conn_error));
    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", "cities req: " + new String(((TypedByteArray)
                response.getBody()).getBytes()));
        boolean isSaved = false;
        try {
            JSONObject json = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            int responseCode = json.optInt("code");
            if (responseCode == OLD_CLIENT_VERSION) {
                EventBus.getDefault().post(new AppUpdateEvent());
                return;
            }

            JSONArray jsonResult = json.getJSONObject("result").getJSONArray("city_list");

            if (jsonResult.length() > 0) {
                Option.deleteAllOptions();
                Tariff.deleteAllTariffs();
                City.deleteAllCities();
            }

            List<City> cities = new ArrayList<>();

            for (int i = 0; i < jsonResult.length(); i++) {
                String lat = jsonResult.getJSONObject(i).getString("city_lat");
                String lon = jsonResult.getJSONObject(i).getString("city_lon");
                String cityResponseArea = jsonResult.getJSONObject(i).getString("city_reseption_area");
                if (TextUtils.isEmpty(lat) || TextUtils.isEmpty(lon)) {
                    continue;
                }

                City city = new City(jsonResult.getJSONObject(i).getString("city_id"),
                        jsonResult.getJSONObject(i).getString("city_name"),
                        Double.valueOf(lat),
                        Double.valueOf(lon),
                        jsonResult.getJSONObject(i).getString("currency"),
                        jsonResult.getJSONObject(i).getString("phone"),
                        cityResponseArea);
                         cities.add(city);
            }

            DataBaseHelper.executeTransactionFromList(cities);

            isSaved = true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int infoText = isSaved ?
                R.string.activities_OptionsActivity_progress_success :
                R.string.activities_OptionsActivity_progress_acc_error;
        EventBus.getDefault().post(new SaveCitiesEvent(isSaved, infoText));
    }

}
