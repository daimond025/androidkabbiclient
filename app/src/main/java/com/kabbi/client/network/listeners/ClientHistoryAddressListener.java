package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Referral;
import com.kabbi.client.utils.DataBaseHelper;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 06.12.16.
 */

public class ClientHistoryAddressListener implements RequestListener<Response> {

    private static final String TAG = ClientHistoryAddressListener.class.getSimpleName();

    @Override
    public void onRequestFailure(SpiceException e) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d(TAG, "Logos onRequestSuccess: " + new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONArray addressesArray = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONArray("result");
            
            if (addressesArray != null && addressesArray.length() > 0) {
                Address.deleteAllAddressHisory();
                ArrayList<Address> addresses = new ArrayList<>();
                for (int i = 0; i < addressesArray.length(); i++) {
                    JSONObject resultAddress = addressesArray.getJSONObject(i);
                    JSONObject parsedAddress = resultAddress.getJSONObject("address");

                    Address address = new Address();

                    address.setCity(parsedAddress.optString("city", ""));
                    address.setHouse(parsedAddress.optString("house", ""));
                    address.setLat(parsedAddress.optString("lat", ""));
                    address.setLon(parsedAddress.optString("lon", ""));
                    address.setType("house");
                    address.setPorch(parsedAddress.optString("porch", ""));
                    address.setStreet(parsedAddress.optString("label", ""));
                    address.setLabel(resultAddress.optString("label", ""));
                    address.setUseType(Address.TYPE_HISTORY);

                    String hash = HashMD5.getHash(address.getCity() + address.getStreet() +
                            address.getHouse() + address.getLabel());
                    address.setHash(hash);
                    addresses.add(address);
                }
                DataBaseHelper.executeTransactionFromList(addresses);
            }
            EventBus.getDefault().post(new RequestCompletedEvent(RequestCompletedEvent.CLIENT_HISTORY));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
