package com.kabbi.client.network.requests;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.Constants;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by andrew on 24/10/2017.
 */

public class EditOrderRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    public static final String TAG = EditOrderRequest.class.getSimpleName();

    private String currentTime;
    private String signature;
    private String typeClient;
    private String tenantId;
    private String lang;
    private String deviceId;
    private String versionClient;
    private String appId;

    private String orderId;
    private String phone;
    private String address;
    private String payment;
    private String companyId = "";
    private String pan = "";


    public EditOrderRequest(Context context, Order order, List<Address> addressList, Profile profile) {
        super(Response.class, IGootaxApi.class);

        JSONObject jsonObjectSource = new JSONObject();
        JSONArray jsonArrayAddress = new JSONArray();

        for (Address address : addressList) {
            if (!address.isEmpty()) {
                try {
                    JSONObject jsonObjectAddressFrom = new JSONObject();
                    jsonObjectAddressFrom.put("city_id", profile.getCityId());
                    jsonObjectAddressFrom.put("city", address.getCity());
                    if (address.getType().equals("house"))
                        jsonObjectAddressFrom.put("street", address.getStreet());
                    else
                        jsonObjectAddressFrom.put("street", address.getLabel());
                    jsonObjectAddressFrom.put("house", address.getHouse());
                    jsonObjectAddressFrom.put("housing", address.getHousing());
                    jsonObjectAddressFrom.put("porch", address.getPorch());
                    jsonObjectAddressFrom.put("apt", address.getApt());
                    jsonObjectAddressFrom.put("lat", address.getLat());
                    jsonObjectAddressFrom.put("lon", address.getLon());
                    jsonArrayAddress.put(jsonObjectAddressFrom);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            jsonObjectSource.put("address", jsonArrayAddress);
            Log.d(TAG, "Logos EditOrderRequest: " + jsonObjectSource);
            this.address = URLEncoder.encode(jsonObjectSource.toString(), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

        currentTime = String.valueOf(new Date().getTime());
        orderId = order.getOrderId();
        phone = profile.getPhone();
        payment = order.getPaymentType();

        switch (payment) {
            case Constants.PAYMENT_CORPORATION:
                companyId = order.getCompanyId();
                break;
            case Constants.PAYMENT_CARD:
                pan = order.getPan();
                break;
        }

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();

        requestParams.put("address", address);
        requestParams.put("company_id", companyId);
        requestParams.put("current_time", currentTime);
        requestParams.put("order_id", orderId);
        requestParams.put("pan", pan);
        requestParams.put("payment", payment);
        requestParams.put("phone", phone);

        this.signature = HashMD5.getSignature(requestParams, profile.getApiKey());
        this.typeClient =  "android";
        this.tenantId = profile.getTenantId();
        this.lang = Locale.getDefault().getLanguage();
        this.deviceId = GenUDID.getUDID(context);
        this.versionClient = AppParams.CLIENT_VERSION;
        this.appId = profile.getAppId();

    }


    @Override
    public Response loadDataFromNetwork() throws Exception {
        Response response = getService().postUpdateOrder(deviceId, signature, typeClient, tenantId, lang, versionClient,
                appId, BuildConfig.VERSION_NAME, address, companyId, currentTime, orderId, pan, payment, phone);
        Log.d("Logos", "Logos loadDataFromNetwork: " + response.getUrl());
        Log.d("Logos", "Logos loadDataFromNetwork: " + response.getHeaders());
        Log.d("Logos", "Logos loadDataFromNetwork: " + response.getBody());
        return response;
    }
}
