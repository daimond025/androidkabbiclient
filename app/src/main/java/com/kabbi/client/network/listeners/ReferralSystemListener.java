package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.events.api.client.ActivateReferralEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.core.BaseRequestListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 06.12.16.
 */

public class ReferralSystemListener extends BaseRequestListener {

    public ReferralSystemListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(Response response) {
        try {
            Log.d("Logos", "REF_SYSTEM: " + new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");

            String code = null;
            int result = jsonResult.getInt("is_activate");

            if (result == 1) {
                code = jsonResult.getString("code");
            }

            EventBus.getDefault().post(new ActivateReferralEvent(result, code));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
