package com.kabbi.client.network.interfaces;


import java.util.Map;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

public interface IGootaxApi {

    @GET("/get_tenant_city_list")
    Response getCities(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time);


    @GET("/get_tariffs_list")
    Response getTariffs(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("city_id") String city_id,
            @Query("current_time") String current_time,
            @Query("phone") String phone);

    @GET("/get_info_page")
    Response getCompanyPage(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time);

    @GET("/get_order_info")
    Response getOrderInfo(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time,
            @Query("need_car_photo") String need_car_photo,
            @Query("need_driver_photo") String need_driver_photo,
            @Query("order_id") String order_id);

    @GET("/get_cars")
    Response getCars(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("car_class_id[0]") String carClassIdZero,
            @Query("car_class_id[1]") String carClassIdFirst,
            @Query("car_class_id[2]") String carClassIdSecond,
            @Query("car_class_id[3]") String carClassIdThird,
            @Query("city_id") String city_id,
            @Query("current_time") String current_time,
            @Query("from_lat") String from_lat,
            @Query("from_lon") String from_lon,
            @Query("radius") String radius);

    @FormUrlEncoded
    @POST("/send_password")
    Response sendPassword(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("current_time") String current_time,
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/accept_password")
    Response acceptPassword(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("city_id") int city_id,
            @Field("current_time") String current_time,
            @Field("device_token") String device_token,
            @Field("password") String password,
            @Field("phone") String phone,
            @Field("referral_code") String referral);

    @Multipart
    @POST("/update_client_profile")
    Response updateProfile(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Part("client_id") String clientId,
            @Part("current_time") String current_time,
            @Part("email") String email,
            @Part("name") String name,
            @Part("new_phone") String newPhone,
            @Part("old_phone") String oldPhone,
            @Part("photo") TypedFile photo,
            @Part("surname") String surname);


    @FormUrlEncoded
    @POST("/create_order")
    Response createOrder(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("additional_options") String additional_options,
            @Field("address") String address,
            @Field("bonus_payment") int bonus_payment,
            @Field("city_id") String city_id,
            @Field("client_id") String client_id,
            @Field("client_phone") String client_phone,
            @Field("comment") String comment,
            @Field("company_id") String company_id,
            @Field("current_time") String time,
            @Field("device_token") String device_token,
            @Field("order_time") String order_time,
            @Field("pan") String pan,
            @Field("pay_type") String pay_type,
            @Field("tariff_id") String tariff_id,
            @Field("type_request") String type_request,
            @Field("promo_code") String promo_code
    );

    @FormUrlEncoded
    @POST("/reject_order")
    Response rejectOrder(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("Request-Id") String uuid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("current_time") String time,
            @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("/send_response")
    Response addReview(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("current_time") String current_time,
            @Field("grade") String grade,
            @Field("order_id") String order_id,
            @Field("text") String text);

    @FormUrlEncoded
    @POST("/create_client_card")
    Response postCreateClientCard(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("client_id") String clientId,
            @Field("current_time") String currentTime,
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/delete_client_card")
    Response postDeleteClientCard(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("client_id") String clientId,
            @Field("current_time") String currentTime,
            @Field("phone") String phone,
            @Field("pan") String pan);

    @FormUrlEncoded
    @POST("/check_client_card")
    Response postCheckClientCard(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("client_id") String clientId,
            @Field("current_time") String currentTime,
            @Field("order_id") String orderId,
            @Field("phone") String phone);

    @GET("/get_client_cards")
    Response getClientCards(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("client_id") String clientId,
            @Query("current_time") String currentTime,
            @Query("phone") String phone);

    @GET("/get_order_route")
    Response getOrderRouteResult(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime,
            @Query("order_id") String orderID
    );


    @GET("/get_client_profile")
    Response getClientProfileResult(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime,
            @Query("phone") String phone
    );

    @FormUrlEncoded
    @POST("/activate_bonus_system")
    Response postActivateBonusSystem(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("typeclient") String typeclient,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("current_time") String currentTime,
            @Field("client_phone") String phone,
            @Field("promo_code") String promoCode
    );

    @GET("/get_tariffs_type")
    Response getTariffType(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("city_id") String cityId,
            @Query("current_time") String currentTime,
            @Query("date") String date
    );

    @GET("/get_reject_order_result")
    Response getRejectOrderResult(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime,
            @Query("request_id") String uuid
    );

    @GET("/get_client_balance")
    Response getClientBalances(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime,
            @Query("city_id") String cityId,
            @Query("phone") String phone
    );

    @GET("/get_orders_info")
    Response getOrdersInfo(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time,
            @Query("need_car_photo") String need_car_photo,
            @Query("need_driver_photo") String need_driver_photo,
            @Query("orders_id") String order_id
    );

    @GET("/ping")
    Response getPing(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time
    );

    @GET("/get_confidential_page")
    Response getConfidentialPage(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time
    );


    @FormUrlEncoded
    @POST("/activate_referral_system")
    Response getReferralSystem(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("phone") String phone,
            @Field("referral_id") String referral_id,
            @Field("current_time") String current_time
    );

    @FormUrlEncoded
    @POST("/activate_referral_system_code")
    Response getReferralSystemCode(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("code") String code,
            @Field("current_time") String current_time,
            @Field("phone") String phone
    );

    @GET("/get_referral_system_list")
    Response getReferralSystemList(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String current_time,
            @Query("phone") String phone

    );

    @GET("/get_client_history_address")
    Response getClientHistoryAddress(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("city_id") String cityId,
            @Query("current_time") String current_time,
            @Query("phone") String phone

    );

    @FormUrlEncoded
    @POST("/update_order")
    Response postUpdateOrder(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("address") String address,
            @Field("company_id") String companyId,
            @Field("current_time") String currentTime,
            @Field("order_id") String orderId,
            @Field("pan") String pan,
            @Field("payment") String payment,
            @Field("phone") String phone
    );

    @GET("/update_order_result")
    Response getUpdateOrderResult(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime,
            @Query("phone") String phone,
            @Query("update_id") String updateId

    );


    @GET("/get_download_links")
    Response getDownloadLinks(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Query("current_time") String currentTime

    );


    @FormUrlEncoded
    @POST("/costing_order")
    Response postCostingOrder(
            @Header("deviceid") String deviceid,
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("versionclient") String versionclient,
            @Header("appid") String appid,
            @Header("appversion") String appversion,
            @Field("additional_options") String additionalOption,
            @Field("address") String address,
            @Field("city_id") String cityId,
            @Field("current_time") String currentTime,
            @Field("order_time") String orderTime,
            @Field("phone") String phone,
            @FieldMap Map<String, String> tariffId

    );
}
