package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Order;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 17.10.16.
 */

public class RejectOrderResultRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String versionclient;
    private String appId;

    private String uuid;
    private String time;

    public RejectOrderResultRequest(String appId, String apiKey,  Order order, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.time = String.valueOf((new Date()).getTime());
        this.lang = Locale.getDefault().getLanguage();
        this.uuid = order.getUuid();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.time);
        requestParams.put("request_id", this.uuid);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getRejectOrderResult(signature, typeclient, tenantid, lang, versionclient, appId, BuildConfig.VERSION_NAME, time, uuid);
    }
}
