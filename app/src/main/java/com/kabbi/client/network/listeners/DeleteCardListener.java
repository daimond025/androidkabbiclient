package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.DeleteCardEvent;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class DeleteCardListener implements RequestListener<Response> {

    private String paymentID;

    public DeleteCardListener(String paymentID) {
        this.paymentID = paymentID;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", new String(((TypedByteArray) response.getBody())
                .getBytes()));
        try {
            JSONObject json = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));

            if (json.getString("info").equals("OK") && json.getString("result").equals("true"))
                EventBus.getDefault().post(new DeleteCardEvent("true", paymentID));
            else EventBus.getDefault().post(new DeleteCardEvent("false"));

        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
    }

}

