package com.kabbi.client.network.listeners;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.kabbi.client.events.api.client.RouteEvent;
import com.kabbi.client.models.Order;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 24.08.16.
 */
public class OrderRouteListener implements RequestListener<Response> {

    private final static String TAG = OrderRouteListener.class.getSimpleName();
    private String orderID;

    public OrderRouteListener(String orderID) {
        this.orderID = orderID;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
    }

    @Override
    public void onRequestSuccess(Response response) {
        Order order = Order.getOrder(orderID);
        ArrayList<LatLng> arrayList = new ArrayList<>();
        Log.d(TAG, "Logos onRequestSuccess: " + new String(((TypedByteArray) response.getBody()).getBytes()));
        try {
            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
            double lat;
            double lon;

            if (jsonObject.getString("info").equals("OK")) {
                JSONArray jsonArrayCar = jsonObject.getJSONObject("result").getJSONArray("route_data");
                for (int i = 0; i < jsonArrayCar.length(); i++) {
                        lat = jsonArrayCar.getJSONObject(i).getDouble("lat");
                        lon = jsonArrayCar.getJSONObject(i).getDouble("lon");
                        arrayList.add(new LatLng(lat,lon));
                }
                String result = PolyUtil.encode(arrayList);

                if (order != null && !result.isEmpty()) {
                    order.setRoute(result);
                    order.save();
                }
            }
            EventBus.getDefault().post(new RouteEvent());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
