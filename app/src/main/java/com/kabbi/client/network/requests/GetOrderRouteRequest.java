package com.kabbi.client.network.requests;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 24.08.16.
 */
public class GetOrderRouteRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private final String lang;
    private String tenantid;
    private String versionclient;
    private String typeClient;
    private String orderID;
    private String currentTime;
    private String appId;

    public GetOrderRouteRequest(String appId, String apiKey,  String orderID, String tenantid, String typeClient) {
        super(Response.class, IGootaxApi.class);

        this.orderID = orderID;
        this.tenantid = tenantid;
        this.currentTime = String.valueOf(new Date().getTime());
        this.appId = appId;
        this.typeClient = typeClient;
        this.lang = Locale.getDefault().getLanguage();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.currentTime);
        requestParams.put("order_id", this.orderID);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.versionclient = AppParams.CLIENT_VERSION;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getOrderRouteResult(signature, tenantid, lang, typeClient, versionclient, appId, BuildConfig.VERSION_NAME, currentTime, orderID);
    }
}
