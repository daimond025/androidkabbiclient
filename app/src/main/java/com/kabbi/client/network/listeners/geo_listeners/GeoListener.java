package com.kabbi.client.network.listeners.geo_listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.geo.GeoEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.utils.HashMD5;
import com.kabbi.client.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public abstract class GeoListener implements RequestListener<Response> {

    private GeoEvent event;

    public GeoListener(GeoEvent event) {
        this.event = event;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        ArrayList<Address> listData = new ArrayList<>();
        JSONArray jsonResults = null;
        try {
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            jsonResults = jsonResult.getJSONArray("results");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        if (jsonResults != null && jsonResults.length() != 0) {
            listData = new ArrayList<>();
            for (int i = 0; i < jsonResults.length(); i++) {
                try {
                    JSONObject addressJson = new JSONObject(jsonResults.getString(i));
                    JSONObject addressJsonDetail = new JSONObject(Validator.validateStr(addressJson, "address"));
                    Address address = new Address();
                    address.setType(Validator.validateStr(addressJson, "type"));
                    address.setCity(Validator.validateStr(addressJsonDetail, "city"));
                    address.setStreet(Validator.validateStr(addressJsonDetail, "street"));
                    address.setHouse(Validator.validateStr(addressJsonDetail, "house"));
                    address.setLabel(Validator.validateStr(addressJsonDetail, "label"));
                    address.setLat(Validator.validateStr(addressJsonDetail, "lat"));
                    address.setLon(Validator.validateStr(addressJsonDetail, "lon"));
                    String hash = HashMD5.getHash(address.getCity() + address.getStreet() +
                            address.getHouse() + address.getLabel() + address.getLat() + address.getLon());
                    address.setHash(hash);
                    listData.add(address);
                } catch (JSONException | NullPointerException ignored) {}
            }
        }
        event.setAddresses(listData);
        EventBus.getDefault().post(event);
    }

}
