package com.kabbi.client.network.requests;


import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Order;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class RejectOrderRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String uuid;
    private String versionclient;
    private String appId;

    private String time;
    private String order_id;

    public RejectOrderRequest(String appId, String apiKey,  Context context, String time, Order order, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.time = time;
        this.order_id = order.getOrderId();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.time);
        requestParams.put("order_id", this.order_id);

        this.uuid = order.getUuid();
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().rejectOrder(signature, typeclient, tenantid, lang, deviceid, uuid, versionclient, appId, BuildConfig.VERSION_NAME, time, order_id);
    }
}
