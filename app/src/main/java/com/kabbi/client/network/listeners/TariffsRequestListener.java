package com.kabbi.client.network.listeners;


import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.kabbi.client.events.api.client.SaveTariffsEvent;
import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.network.core.BaseRequestListener;
import com.kabbi.client.utils.DataBaseHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Tariff;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class TariffsRequestListener extends BaseRequestListener {

    public TariffsRequestListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(final Response response) {
        Log.d("Logos", new String(((TypedByteArray)
                response.getBody()).getBytes()));
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                parseTariffs(response);
            }
        });
    }

    private void parseTariffs(Response response) {
        try {
            JSONArray jsonArrayTariffs = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getJSONArray("result");

            List<Tariff> tariffList = new ArrayList<>();
            List<Option> optionList = new ArrayList<>();
            Option.deleteAllOptions();
            Tariff.deleteAllTariffs();
            for (int i = 0; i < jsonArrayTariffs.length(); i++) {
                City city = City.getCity(jsonArrayTariffs.getJSONObject(i).getString("city_id"));
                if (city != null) {
                    Tariff tariff = new Tariff();
                    tariff.setCityID(city.getId());
                    tariff.setTariffId(jsonArrayTariffs.getJSONObject(i).getString("tariff_id"));
                    tariff.setTariffType(jsonArrayTariffs.getJSONObject(i).getString("type"));
                    tariff.setTariffName(jsonArrayTariffs.getJSONObject(i).getString("tariff_name"));
                    tariff.setTariffIcon(jsonArrayTariffs.getJSONObject(i).getString("logo"));
                    tariff.setTariffIconMini(jsonArrayTariffs.getJSONObject(i).getString("logo_thumb"));
                    tariff.setDescription(jsonArrayTariffs.getJSONObject(i).getString("description"));
                    tariff.setBonus(jsonArrayTariffs.getJSONObject(i).getString("is_bonus"));
                    tariff.setCarClassId(jsonArrayTariffs.getJSONObject(i).optString("car_class_id"));
                    tariffList.add(tariff);
                    try {
                        JSONArray jsonArrayOptions = new JSONArray(jsonArrayTariffs.getJSONObject(i).getString("additional_options"));
                        for (int k = 0; k < jsonArrayOptions.length(); k++) {
                            Option option = new Option();
                            option.setOptionIdPrymary(jsonArrayOptions.getJSONObject(k).getString("id"));
                            option.setOptionIdSecondary(jsonArrayOptions.getJSONObject(k).getString("additional_option_id"));
                            option.setPrice(jsonArrayOptions.getJSONObject(k).getString("price"));
                            option.setTariffType(jsonArrayOptions.getJSONObject(k).getString("tariff_type"));
                            option.setTariffID(Integer.parseInt(tariff.getTariffId()));
                            option.setOptionName(jsonArrayOptions.getJSONObject(k).getJSONObject("option").getString("name"));
                            option.setChecked(false);
                            optionList.add(option);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONArray clientTypes = jsonArrayTariffs.getJSONObject(i).optJSONArray("client_type");
                    if (clientTypes!= null && clientTypes.length() > 0) {
                        String str = "";
                        for(int a = 0; a < clientTypes.length(); a++) {
                            str = str + clientTypes.getString(a) + ",";
                        }
                        tariff.setClientTypes(str);
                    }

                    JSONArray companies = jsonArrayTariffs.getJSONObject(i).optJSONArray("companies");
                    if (companies != null && companies.length() > 0) {
                        String comp = "";
                        for(int a = 0; a < companies.length(); a++) {
                            comp = comp + companies.getString(a) + ",";
                        }
                        tariff.setCompanies(comp);
                    }
                }
            }

            DataBaseHelper.executeTransactionFromList(tariffList);
            DataBaseHelper.executeTransactionFromList(optionList);

            EventBus.getDefault().post(new SaveTariffsEvent(tariffList));
        } catch (Exception e) {
            EventBus.getDefault().post(new SaveTariffsEvent(null));
            e.printStackTrace();
        }

        EventBus.getDefault().post(new RequestCompletedEvent(RequestCompletedEvent.TARIFF));
    }

}
