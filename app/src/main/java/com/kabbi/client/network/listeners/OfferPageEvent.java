package com.kabbi.client.network.listeners;

/**
 * Created by gootax on 17.03.17.
 */

public class OfferPageEvent {

    public String url;
    public int code;

    public OfferPageEvent(String url) {
        this.url = url;
    }

}
