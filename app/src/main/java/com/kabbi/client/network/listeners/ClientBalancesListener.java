package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.events.api.client.ClientBalancesEvent;
import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Property;
import com.kabbi.client.network.core.BaseRequestListener;
import com.kabbi.client.utils.Validator;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 02.12.16.
 */

public class ClientBalancesListener extends BaseRequestListener {


    public ClientBalancesListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {
        EventBus.getDefault().post(new ClientBalancesEvent());
    }

    @Override
    public void onSuccess(Response response) {
        try {
            PaymentType.deleteAllPayments();
            Property.deleteAllProperty();
            Log.d("Logos", new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            String currency = jsonResult.getJSONObject("currency").getString("code");
            Profile profile = Profile.getProfile();

            try {
                JSONArray companyResult = new JSONArray(jsonResult.getString("company_balance"));
                for (int i = 0; i < companyResult.length(); i++) {
                    String companyId = companyResult.getJSONObject(i).getString("company_id");
                    String companyName = companyResult.getJSONObject(i).getString("company_name");
                    String companyValue = companyResult.getJSONObject(i).getString("value");

                    PaymentType.addCorpPayment(companyId, companyName, companyValue, currency);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                JSONArray companyResult = new JSONArray(jsonResult.getString("cards"));
                for (int i = 0; i < companyResult.length(); i++) {
                    String pan = companyResult.getString(i);
                    PaymentType.addCardPayment(pan);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                String balanceResult = jsonResult.getJSONObject("personal_balance").getString("value");
                String bonusResult = jsonResult.getJSONObject("personal_bonus").getString("value");
                JSONObject jsonBonusType = Validator.validateJSONObj(jsonResult, "bonus_system");
                PaymentType.addPersonalPayment(profile.getPhone(), balanceResult, currency);

                profile.setBalanceCurrency(currency);
                profile.setBalanceValue(balanceResult);
                profile.setBonusValue(bonusResult);
                profile.setBonusId(Validator.validateInt(jsonBonusType, "id"));
                profile.setBonusUdsState(Validator.validateInt(jsonBonusType, "active"));

                switch (profile.getPaymentType()) {
                    case "CARD":
                        if (PaymentType.getPayment(profile.getPan()) == null) profile.setPaymentType("CASH");
                        break;
                    case "CORP_BALANCE":
                        if (PaymentType.getPayment(profile.getCompany()) == null) profile.setPaymentType("CASH");
                        break;
                    case "PERSONAL_ACCOUNT":
                        if (PaymentType.getPayment(profile.getPhone()) == null) profile.setPaymentType("CASH");
                        break;
                    default:
                        if (PaymentType.getPayment(profile.getPaymentType()) == null) profile.setPaymentType("CASH");
                }

                profile.save();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new ClientBalancesEvent());
        EventBus.getDefault().post(new RequestCompletedEvent(RequestCompletedEvent.BALANCES));
    }


}
