package com.kabbi.client.network.core;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import com.kabbi.client.R;
import com.kabbi.client.views.ToastWrapper;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import static com.kabbi.client.activities.SplashActivity.openPlayStorePage;
import static com.kabbi.client.network.listeners.CitiesRequestListener.OLD_CLIENT_VERSION;

public abstract class BaseRequestListener implements RequestListener<Response> {

    public static final int CODE_NOT_FOUND = 42;

    private Context mContext;


    public BaseRequestListener(Context context) {
        mContext = context;
    }


    public abstract void onFailure(SpiceException spiceException);

    public abstract void onSuccess(Response response);


    @Override
    public void onRequestFailure(SpiceException spiceException) {
        onFailure(spiceException);
    }


    @Override
    public void onRequestSuccess(Response response) {
        try {
            JSONObject json = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            int responseCode = json.optInt("code");
            if (responseCode == OLD_CLIENT_VERSION) {
                showUpdateDialog();
            } else if (responseCode == CODE_NOT_FOUND) {
                showErrorDialog(R.string.activities_ReferralActivity_referral_code_not_found);
            } else {
                onSuccess(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showUpdateDialog() {
        AlertDialog updateDialog = new AlertDialog.Builder(mContext, R.style.MyCustomAlertDialogTheme)
                .setMessage(R.string.activities_SplashActivity_dialog_update_title)
                .setNegativeButton(R.string.activities_SplashActivity_dialog_btn_quit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                })
                .setPositiveButton(R.string.activities_SplashActivity_dialog_btn_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openPlayStorePage(mContext);
                    }
                })
                .setCancelable(false)
                .create();
        updateDialog.show();
    }


    private void showErrorDialog(@StringRes int resIdMessage) {
        new ToastWrapper(mContext, resIdMessage).show();
    }
}
