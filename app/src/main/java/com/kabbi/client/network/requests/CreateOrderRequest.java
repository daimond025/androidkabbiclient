package com.kabbi.client.network.requests;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.models.City;
import com.kabbi.client.utils.DetectCity;
import com.kabbi.client.utils.TimeHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit.client.Response;

public class CreateOrderRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;

    private String additional_options = "";
    private String address = "";
    private String city_id = "";
    private String client_id = "";
    private String client_phone = "";
    private String comment = "";
    private String company_id = "";
    private String device_token = "";
    private String order_time = "";
    private String pan = "";
    private String pay_type = "";
    private String tariff_id = "";
    private String time;
    private String type_request = "";
    private String promo_code;
    private int bonus_payment;


    public CreateOrderRequest(String appId, String apiKey,  Context context, String time, Order order, Profile profile,
                              List<Address> addressList, String promoCode) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        JSONObject jsonObjectSource = new JSONObject();
        JSONArray jsonArrayAddress = new JSONArray();

        for (Address address : addressList) {
            if (!address.isEmpty()) {
                try {
                    JSONObject jsonObjectAddressFrom = new JSONObject();
                    jsonObjectAddressFrom.put("city_id", profile.getCityId());
                    jsonObjectAddressFrom.put("city", address.getCity());
                    if (address.getType().equals("house"))
                        jsonObjectAddressFrom.put("street", address.getStreet());
                    else
                        jsonObjectAddressFrom.put("street", address.getLabel());
                    jsonObjectAddressFrom.put("house", address.getHouse());
                    jsonObjectAddressFrom.put("housing", address.getHousing());
                    jsonObjectAddressFrom.put("porch", address.getPorch());
                    jsonObjectAddressFrom.put("apt", address.getApt());
                    jsonObjectAddressFrom.put("lat", address.getLat());
                    jsonObjectAddressFrom.put("lon", address.getLon());
                    jsonArrayAddress.put(jsonObjectAddressFrom);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            jsonObjectSource.put("address", jsonArrayAddress);
            this.address = URLEncoder.encode(jsonObjectSource.toString(), "UTF-8").replace("+", "%20");
            this.comment = URLEncoder.encode(order.getComment(), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

        this.bonus_payment = profile.getPaymentBonus();

        this.client_phone = profile.getPhone();

        this.client_id = profile.getClientId();
        this.tariff_id = order.getTariff();
        this.time = time;
        this.type_request = "1";
        this.versionclient = AppParams.CLIENT_VERSION;
        this.promo_code = promoCode;
        if (promo_code == null) this.promo_code = "";

        this.city_id = profile.getCityId();
        String token = new AppPreferences(context).getText("device_token");
        if (TextUtils.isEmpty(token)) token = "emptyToken";
        this.device_token = token;

        if (!order.getOrderTime().isEmpty()) this.order_time = order.getOrderTime();

        this.pay_type = profile.getPaymentType();
        if (pay_type.equals("CARD")) pan = profile.getPan();
        if (pay_type.equals("CORP_BALANCE")) company_id = profile.getCompany();

        try {
            StringBuilder sbOptions = new StringBuilder();
            List<Option> options = Option.getOptionsChecked(order.getTariff());
            for (Option option : options) {
                sbOptions.append(option.getOptionIdSecondary());
                sbOptions.append(",");
            }

            if (options.size() > 0) {
                try {
                    this.additional_options = URLEncoder.encode(sbOptions.toString()
                            .substring(0, sbOptions.toString().length() - 1), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestParams.put("additional_options", this.additional_options);
        requestParams.put("address", this.address);
        requestParams.put("bonus_payment", String.valueOf(this.bonus_payment));
        requestParams.put("city_id", this.city_id);
        requestParams.put("client_id", this.client_id);
        requestParams.put("client_phone", this.client_phone);
        requestParams.put("comment", this.comment);
        requestParams.put("company_id", this.company_id);
        requestParams.put("current_time", this.time);
        requestParams.put("device_token", this.device_token);
        try {
            requestParams.put("order_time", URLEncoder.encode(this.order_time, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        requestParams.put("pan", this.pan);
        requestParams.put("pay_type", this.pay_type);
        requestParams.put("tariff_id", this.tariff_id);
        requestParams.put("type_request", this.type_request);
        requestParams.put("promo_code", this.promo_code);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = profile.getTenantId();
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().createOrder(signature, typeclient, tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME,
                additional_options, address, bonus_payment, city_id, client_id, client_phone, comment,
                company_id, time, device_token, order_time, pan, pay_type, tariff_id,
                type_request, promo_code);
    }

}
