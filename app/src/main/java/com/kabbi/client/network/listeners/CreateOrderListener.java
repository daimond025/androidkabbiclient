package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.app.Constants;
import com.kabbi.client.network.core.BaseRequestListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.CreateOrderEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CreateOrderListener extends BaseRequestListener {

    public CreateOrderListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {
        EventBus.getDefault().post(new CreateOrderEvent(Constants.REQUEST_FAILURE_CODE));

    }

    @Override
    public void onSuccess(Response response) {
        try {
            Log.d("CREATE_ORDER", new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            JSONObject jsonOrder = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            if (jsonOrder.getString("info").equals("OK"))
                EventBus.getDefault().post(new CreateOrderEvent(jsonOrder.getJSONObject("result").getString("order_id"),
                        jsonOrder.getJSONObject("result").getString("order_number"),
                        jsonOrder.getInt("code"), jsonOrder.getJSONObject("result").getString("type")));
            else
                EventBus.getDefault().post(new CreateOrderEvent(jsonOrder.getInt("code")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
