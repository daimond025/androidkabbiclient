package com.kabbi.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.ReviewEvent;

import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AddReviewListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("REVIEW", new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getJSONObject("result");

            EventBus.getDefault().post(new ReviewEvent(jsonObject.getString("response_result")));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
