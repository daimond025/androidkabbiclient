package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.EditOrderResultEvent;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


/**
 * Created by andrew on 24/10/2017.
 */

public class EditOrderResultListener implements RequestListener<Response> {


    @Override
    public void onRequestFailure(SpiceException spiceException) {}


    @Override
    public void onRequestSuccess(Response response) {
        int resultCode = 0;

        try {
            Log.d("Logos EditOrder ", new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            JSONObject requestResult = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            int code = requestResult.optInt("code", -1);

            switch (code) {
                case 0:
                    resultCode = requestResult.optInt("result", 0);
                    break;
                case 300:
                    resultCode = 300;
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(new EditOrderResultEvent(resultCode));
    }

}
