package com.kabbi.client.network.listeners;


import android.util.Log;

import com.kabbi.client.models.Address;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.OrderInfoEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import static com.kabbi.client.app.AppParams.CAR_PHOTO;
import static com.kabbi.client.app.AppParams.PHOTO_TYPE;
import static com.kabbi.client.app.AppParams.DRIVER_PHOTO;
import static com.kabbi.client.app.AppParams.USE_PHOTO;

public class OrderInfoListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
    Log.d("Logos", new String(((TypedByteArray)
            response.getBody()).getBytes()));

        //{"code":0,"info":"OK","result":{"order_info":{"order_number":"20873","status_id":"4","status_group":"new","status_name":"\u0418\u0434\u0435\u0442 \u043f\u043e\u0438\u0441\u043a \u0430\u0432\u0442\u043e...","car_data":[],"predv_price":"60.00","detail_cost_info":[]}}}

        //{"result":{"order_info":{"status_id":17,"order_number":"20874","status_group":"car_assigned","detail_cost_info":[],
        // "car_data":{"driver_phone":"79121232131","car_lat":"56.84414617646224",
        // "driver_photo":"https:\/\/3colors.uatgootax.ru\/file\/show-external-file?filename=thumb_56f394ba0488e.jpg&id=68",
        // "degree":"0.0","raiting":"2.3","speed":"0.0","car_lon":"53.24191486394258","car_photo":null,
        // "car_description":"Acura MDX Красный a888aa","driver_fio":"Евгений Отчествович","car_time":"3"},
        // "status_name":"Водитель выехал","predv_price":"60.00"}},"info":"OK","code":0}

        //{"result":{"order_info":{"order_number":"20917","status_name":"Выполнен","predv_price":"50.00","status_id":"37","status_group":"completed",
        // "detail_cost_info":{"out_wait_cost":"25.92","writeoff_bonus_id":null,"city_wait_driving":"10","out_city_cost_time":null,"city_wait_price":"3.33","city_cost_time":null,"additional_cost":null,"order_id":"26191","supply_price":"10","start_point_location":"in","city_time_wait":"0","out_next_cost_unit":"1_km","planting_price":"50","time":"1460636652","out_city_time":"0","city_wait_cost":"0","summary_cost":"76","out_wait_driving":"11","city_distance":"0.0","planting_include_time":"1","city_time":"0","distance_for_plant":"0","out_wait_price":"5","city_next_km_price":"15","summary_time":"5.2","accrual_out":"DISTANCE","out_next_km_price_time":"0","out_next_km_price":"20","out_time_wait":"311","detail_id":"2985","planting_include":"1","accrual_city":"DISTANCE","out_wait_time":"1","city_wait_time":"1","refill_bonus_id":"15","out_city_cost":"0","before_time_wait":"0.0","is_fix":"0","refill_bonus":"7.6","distance_for_plant_cost":"0","summary_distance":"0.0","city_next_km_price_time":"230","out_city_distance":"0.0","city_cost":"0","bonus":null,"city_next_cost_unit":"1_km","before_time_wait_cost":"0"},"car_data":{"driver_phone":"79121232131","car_lat":"56.84421327292157","driver_photo":"https:\/\/3colors.uatgootax.ru\/file\/show-external-file?filename=thumb_56f394ba0488e.jpg&id=68","degree":"0.0","raiting":"2.3","speed":"0.0","car_lon":"53.24168197133385","car_photo":null,"car_description":"Acura MDX Красный a888aa","driver_fio":"Евгений Отчествович","car_time":"3"},"currency":"RUB"}},"info":"OK","code":0}

        try {
            JSONObject jsonOrder = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            if (jsonOrder.getString("info").equals("OK")) {
                JSONObject jsonOrderInfo = jsonOrder.getJSONObject("result").getJSONObject("order_info");

                double raiting = 0;
                String driverPhone = "0";
                String carLabelTime = "0";
                try {
                    raiting = Double.valueOf(jsonOrderInfo.getJSONObject("car_data").getString("raiting"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    driverPhone = jsonOrderInfo.getJSONObject("car_data").getString("driver_phone");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    carLabelTime = jsonOrderInfo.getJSONObject("car_data").getString("car_time");
                   /* int carTime = Integer.parseInt(carLabelTime) * 60 * 1000;
                    long orderTime = jsonOrderInfo.getInt("order_time")*1000;
                    timeToCar = (int) (orderTime + carTime - TimeHelper.getRealCurrentTimeLong());*/
                } catch (Exception e) {
                    e.printStackTrace();
                }




                OrderInfoEvent orderInfoEvent = new OrderInfoEvent();
                orderInfoEvent.setStatus(jsonOrderInfo.getString("status_group"));
                orderInfoEvent.setStatusLabel(jsonOrderInfo.getString("status_name"));
                orderInfoEvent.setStatusId(jsonOrderInfo.getString("status_id"));
                orderInfoEvent.setRaiting(raiting);
                orderInfoEvent.setDriverPhone(driverPhone);
                orderInfoEvent.setTime(carLabelTime);
                if (jsonOrderInfo.getString("status_group").equals("rejected")) {
                    orderInfoEvent.setStatusDescription(jsonOrderInfo.optString("status_description", null));
                }

                if (jsonOrderInfo.getString("status_group").equals("new")
                        || jsonOrderInfo.getString("status_group").equals("completed")
                        || jsonOrderInfo.getString("status_group").equals("rejected")
                        || jsonOrderInfo.getString("status_group").equals("pre_order")) {
                    orderInfoEvent.setLat(0);
                    orderInfoEvent.setLon(0);
                    orderInfoEvent.setDegree(0f);
                } else {
                    orderInfoEvent.setLat(Double.valueOf(jsonOrderInfo.getJSONObject("car_data").getString("car_lat")));
                    orderInfoEvent.setLon(Double.valueOf(jsonOrderInfo.getJSONObject("car_data").getString("car_lon")));
                    orderInfoEvent.setDegree(Float.valueOf(jsonOrderInfo.getJSONObject("car_data").getString("degree")));
                }

                if (jsonOrderInfo.getString("status_group").equals("completed")) {
                    orderInfoEvent.setCost(jsonOrderInfo.getString("detail_cost_info"));
                    orderInfoEvent.setFixCost(jsonOrderInfo.getString("predv_price"));
                }

                try {
                    orderInfoEvent.setDriverName(jsonOrderInfo.getJSONObject("car_data").getString("driver_fio"));
                    orderInfoEvent.setCarDesc(jsonOrderInfo.getJSONObject("car_data").getString("car_description"));
                    if (USE_PHOTO) {
                        switch (PHOTO_TYPE) {
                            case DRIVER_PHOTO:
                                orderInfoEvent.setPhoto(jsonOrderInfo.getJSONObject("car_data").getString("driver_photo"));
                                break;
                            case CAR_PHOTO:
                                orderInfoEvent.setPhoto(jsonOrderInfo.getJSONObject("car_data").getString("car_photo"));
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    orderInfoEvent.setPaymentType(jsonOrderInfo.getJSONObject("payment").getString("type"));
                    if (jsonOrderInfo.getJSONObject("payment").getString("type").equals("CARD")) {
                        orderInfoEvent.setPan(jsonOrderInfo.getJSONObject("payment").getString("pan"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ArrayList<Address> addressList = new ArrayList<>();
                Address address;
                try {
                    JSONArray addressArray = jsonOrderInfo.getJSONArray("address");
                    if (addressArray.length() > 0) {
                        for (int i = 0; i < addressArray.length(); i++) {
                            address = new Address();
                            address.setCity(addressArray.getJSONObject(i).getString("city"));
                            address.setStreet(addressArray.getJSONObject(i).getString("street"));
                            address.setHouse(addressArray.getJSONObject(i).getString("house"));
                            address.setType("house");
                            address.setLabel(addressArray.getJSONObject(i).getString("street") + " "
                                    + addressArray.getJSONObject(i).getString("house"));
                            address.setLat(addressArray.getJSONObject(i).getString("lat"));
                            address.setLon(addressArray.getJSONObject(i).getString("lon"));
                            address.setGps(false);
                            String hash = HashMD5.getHash(address.getStreet() + address.getHouse()
                                    + address.getLabel());

                            address.setHash(hash);
                            address.save();
                            addressList.add(address);
                        }

                    }
                    orderInfoEvent.setAddresses(addressList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                EventBus.getDefault().post(orderInfoEvent);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
