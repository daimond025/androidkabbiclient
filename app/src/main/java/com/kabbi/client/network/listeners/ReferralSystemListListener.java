package com.kabbi.client.network.listeners;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.models.Referral;
import com.kabbi.client.network.core.BaseRequestListener;
import com.kabbi.client.utils.DataBaseHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 06.12.16.
 */

public class ReferralSystemListListener extends BaseRequestListener {

    public ReferralSystemListListener(Context context) {
        super(context);
    }

    @Override
    public void onFailure(SpiceException spiceException) {

    }

    @Override
    public void onSuccess(Response response) {
        try {
            Log.d("Referral", "ref sys list: " + new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONArray referralSystems = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result").getJSONArray("referral_system");
            ArrayList<Referral> referrals = new ArrayList<>();
            Referral.deleteAllReferrals();

            if (referralSystems.length() > 0) {
                for (int i = 0; i < referralSystems.length(); i++) {
                    Referral referral = new Referral();
                    referral.setReferralId(referralSystems.getJSONObject(i).getString("referral_id"));
                    referral.setContent(referralSystems.getJSONObject(i).getString("content"));
                    referral.setText(referralSystems.getJSONObject(i).getString("text"));
                    referral.setCityId(referralSystems.getJSONObject(i).getString("city_id"));
                    referral.setTitle(referralSystems.getJSONObject(i).getString("title"));

                    String code = "";
                    boolean isActive = false;

                    int active = referralSystems.getJSONObject(i).optInt("is_activate");
                    if (active > 0) isActive = true;
                    referral.setIsActive(isActive);

                    if (isActive) code = referralSystems.getJSONObject(i).getString("code");
                    referral.setCode(code);

                    referrals.add(referral);
                }

                DataBaseHelper.executeTransactionFromList(referrals);

                for(int i = 0; i<referrals.size(); i++) {
                    if (referrals.get(i).getCode() != null) {
                        Log.d("Referral", "ref system code: " + referrals.get(i).getCode());
                    }
                }
            }
        } catch (JSONException e) {
            Log.d("Referral", e.toString());
            e.printStackTrace();
        }

        EventBus.getDefault().post(new RequestCompletedEvent(RequestCompletedEvent.REFERRAL));
    }

}
