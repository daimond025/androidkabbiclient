package com.kabbi.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.CheckCardEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CheckCardListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", new String(((TypedByteArray) response.getBody())
                .getBytes()));
        int resultCode = 1;
        try {
            resultCode = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getInt("code");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new CheckCardEvent(resultCode));
    }

}
