package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.ConstingOrderEvent;
import com.kabbi.client.models.Cost;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ConstingOrderListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        String result = new String(((TypedByteArray) response.getBody()).getBytes());
        Log.d("Logos", "ConstingOrderListener | onRequestSuccess | : " + result);
        List<Cost> costs = new ArrayList<>();
        try {
            JSONObject responseJson = new JSONObject(result);
            JSONObject resultJson = responseJson.optJSONObject("result");
            JSONArray tariffs = resultJson.getJSONArray("tariffs");
            JSONObject generalJson = resultJson.optJSONObject("general");
            long time = generalJson.getLong("summary_time");
            long distance = generalJson.getLong("summary_distance");
            for (int i = 0; i < tariffs.length(); i++) {
                JSONObject costJson = tariffs.getJSONObject(i);
                String tariffId = costJson.getString("tariff_id");
                String summaryCost = costJson.getString("summary_cost");
                String summaryCostNoDiscount = costJson.getString("summary_cost_no_discount");
                boolean isFix = costJson.getBoolean("is_fix");
                Cost cost = new Cost(tariffId, summaryCost, summaryCostNoDiscount, isFix);
                costs.add(cost);
                EventBus.getDefault().post(new ConstingOrderEvent(costs, time, distance));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
