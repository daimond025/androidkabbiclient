package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.ActivateBonusSystemEvent;
import com.kabbi.client.models.Profile;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 08.09.16.
 */
public class ActivateBonusSystemListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
    }

    //{"code":0,"info":"OK","result":{"activate_bonus_system":1},"current_time":"1474021054459"}

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("Logos", new String(((TypedByteArray)
                    response.getBody()).getBytes()));
            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
            if (jsonObject.getString("code").equals("0")) {
                    EventBus.getDefault().post(new ActivateBonusSystemEvent(Profile.BONUS_UDS_STATUS_ACTIVATE));
            } else {
                EventBus.getDefault().post(new ActivateBonusSystemEvent(Profile.BONUS_UDS_STATUS_NONACTIVATE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
