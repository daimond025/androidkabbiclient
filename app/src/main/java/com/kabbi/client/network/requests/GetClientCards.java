package com.kabbi.client.network.requests;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetClientCards extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String tenantid;
    private String versionclient;
    private String clientId;
    private String currentTime;
    private String phone;
    private String appId;
    private String typeClient;

    public GetClientCards(String appId, String apiKey, String clientId, String currentTime, String phone, String tenantid, String typeClient) {
        super(Response.class, IGootaxApi.class);
        this.tenantid = tenantid;
        this.clientId = clientId;
        this.currentTime = currentTime;
        this.lang = Locale.getDefault().getLanguage();
        this.phone = phone;
        this.appId = appId;
        this.typeClient = typeClient;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("client_id", clientId);
        requestParams.put("current_time", currentTime);
        requestParams.put("phone", phone);
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.versionclient = AppParams.CLIENT_VERSION;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getClientCards(signature, tenantid, lang, typeClient, versionclient, appId, BuildConfig.VERSION_NAME, clientId, currentTime, phone);
    }

}
