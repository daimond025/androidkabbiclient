package com.kabbi.client.network.requests;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;
public class GetCarsRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String current_time;
    private String city_id;
    private String radius;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String from_lat, from_lon;
    private String appId;
    private String carClassIdZero;
    private String carClassIdFirst;
    private String carClassIdSecond;
    private String carClassIdThird;

    public GetCarsRequest(String appId, String apiKey,  Context context, String city_id,
                          String lat, String lon, String tenantid, boolean withClassId, String radius) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());
        this.from_lat = lat;
        this.from_lon = lon;
        this.radius = radius;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        if (withClassId) {
            carClassIdZero = "110";
            carClassIdFirst = "111";
            carClassIdSecond = "112";
            carClassIdThird = "113";

            requestParams.put("car_class_id[0]", "110");
            requestParams.put("car_class_id[1]", "111");
            requestParams.put("car_class_id[2]", "112");
            requestParams.put("car_class_id[3]", "113");
        }
        requestParams.put("city_id", city_id);
        requestParams.put("current_time", this.current_time);
        requestParams.put("from_lat", this.from_lat);
        requestParams.put("from_lon", this.from_lon);
        requestParams.put("radius", radius);

        this.city_id = city_id;
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getCars(signature, typeclient, tenantid,
                lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, carClassIdZero, carClassIdFirst, carClassIdSecond,
                carClassIdThird, city_id, current_time, from_lat, from_lon, radius);
    }
}
