package com.kabbi.client.network.requests;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Order;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

import static com.kabbi.client.app.AppParams.CAR_PHOTO;
import static com.kabbi.client.app.AppParams.PHOTO_TYPE;
import static com.kabbi.client.app.AppParams.DRIVER_PHOTO;
import static com.kabbi.client.app.AppParams.USE_PHOTO;

public class GetOrderInfoRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionClient;
    private String appId;

    private String time;
    private String order_id;
    private String need_car_photo;
    private String need_driver_photo;

    public GetOrderInfoRequest(String appId, String apiKey,  Context context, String time, Order order, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.time = time;
        if (USE_PHOTO) {
            switch (PHOTO_TYPE) {
                case DRIVER_PHOTO:
                    this.need_car_photo = "false";
                    this.need_driver_photo = "true";
                    break;
                case CAR_PHOTO:
                    this.need_car_photo = "true";
                    this.need_driver_photo = "false";
                    break;
            }
        } else {
            this.need_car_photo = "false";
            this.need_driver_photo = "false";
        }

        this.order_id = order.getOrderId();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", time);
        requestParams.put("need_car_photo", this.need_car_photo);
        requestParams.put("need_driver_photo", this.need_driver_photo);
        requestParams.put("order_id", this.order_id);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        Log.d("Logos", "saveProfileData: " +  lang);
        this.deviceid = GenUDID.getUDID(context);
        this.versionClient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getOrderInfo(signature, typeclient, tenantid, lang, deviceid, versionClient, appId, BuildConfig.VERSION_NAME,
                time, need_car_photo, need_driver_photo, order_id);
    }

}
