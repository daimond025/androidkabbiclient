package com.kabbi.client.network.requests;

import android.content.Context;
import android.util.Log;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 12.04.17.
 */

public class GetReferralSystemRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String phone;
    private String time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;
    private String referall;

    public GetReferralSystemRequest(String appId, String apiKey,  Context context, String phone, String tenantid, String referral) {
        super(Response.class, IGootaxApi.class);

        this.time = String.valueOf(System.currentTimeMillis());
        this.phone = phone;
        this.referall = referral;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("phone", phone);
        requestParams.put("referral_id", referral);
        requestParams.put("current_time", time);

        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getReferralSystem(deviceid, signature,
                typeclient, tenantid, lang, versionclient, appId, BuildConfig.VERSION_NAME, phone, referall, time);
    }

}
