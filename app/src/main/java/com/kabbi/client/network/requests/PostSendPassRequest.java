package com.kabbi.client.network.requests;


import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class PostSendPassRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String phone;
    private String current_time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String versionclient;
    private String appId;

    public PostSendPassRequest(String appId, String apiKey,  Context context, String phone,
                               String current_time, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", current_time);
        requestParams.put("phone", phone);

        this.phone = phone;
        this.current_time = current_time;
        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.versionclient = AppParams.CLIENT_VERSION;
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().sendPassword(signature,
                typeclient, tenantid, lang, deviceid, versionclient, appId, BuildConfig.VERSION_NAME, current_time, phone);
    }

}
