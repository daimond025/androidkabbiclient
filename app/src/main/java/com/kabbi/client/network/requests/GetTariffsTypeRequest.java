package com.kabbi.client.network.requests;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;


public class GetTariffsTypeRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private final String lang;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String versionclient;
    private String appId;

    private String city_id;
    private String date;
    private String current_time;

    public GetTariffsTypeRequest(String appId, String apiKey,  String city_id, String date) {
        super(Response.class, IGootaxApi.class);

        this.city_id = city_id;
        this.date = date;
        this.current_time = String.valueOf((new Date()).getTime());
        this.lang = Locale.getDefault().getLanguage();
        this.appId = appId;


        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", city_id);
        requestParams.put("current_time", current_time);
        requestParams.put("date", date);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = Profile.getProfile().getTenantId();
        this.versionclient = AppParams.CLIENT_VERSION;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getTariffType(signature, typeclient, tenantid, lang, versionclient, appId, BuildConfig.VERSION_NAME, city_id, date, current_time);
    }

}
