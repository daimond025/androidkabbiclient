package com.kabbi.client.network.listeners.geo_listeners;

import com.kabbi.client.events.api.geo.SearchEvent;

public class SearchListener extends GeoListener {

    public SearchListener() {
        super(new SearchEvent());
    }

}
