package com.kabbi.client.network.requests;

import android.content.Context;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.network.interfaces.IGootaxApi;
import com.kabbi.client.utils.GenUDID;
import com.kabbi.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

/**
 * Created by gootax on 06.12.16.
 */

public class GetConfidentialRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String deviceid;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String versionclient;
    private String appId;

    private String current_time;


    public GetConfidentialRequest(String appId, String apiKey,  Context context, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());

        LinkedHashMap<String, String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", this.current_time);

        this.signature = HashMD5.getSignature(requestParams, apiKey);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.versionclient = AppParams.CLIENT_VERSION;
        this.deviceid = GenUDID.getUDID(context);
        this.appId = appId;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getConfidentialPage(deviceid, signature, typeclient, tenantid,
                lang, versionclient, appId, BuildConfig.VERSION_NAME, current_time);
    }

}
