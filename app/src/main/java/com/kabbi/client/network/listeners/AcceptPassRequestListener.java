package com.kabbi.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.kabbi.client.events.api.client.ProfileEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AcceptPassRequestListener implements RequestListener<Response> {

    private Profile profile;
    private JSONObject jsonProfile;
    public static final int REFERRAL_ERROR = 18;

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", "accept pass: " + new String(((TypedByteArray) response.getBody())
                .getBytes()));

        JSONObject mJsonResult = null;
        jsonProfile = null;
        profile = Profile.getProfile();
        int success = 0;
        int referral_success = 0;


        try {
            JSONObject result =  new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            int code = result.getInt("code");
            if (code == REFERRAL_ERROR) {
                success = REFERRAL_ERROR;
            } else {
                mJsonResult = result.getJSONObject("result");
                Log.d("Logos", "" + mJsonResult);
                jsonProfile = mJsonResult.getJSONObject("client_profile");
                fillСommonFields();
                success = 1;
            }
            referral_success = mJsonResult.optInt("activate_referral_system");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }


        EventBus.getDefault().post(new ProfileEvent(profile, success, referral_success));
    }

    private void fillСommonFields() {
        profile.setClientId(Validator.validateStr(jsonProfile, "client_id"));
        profile.setSurname(Validator.validateStr(jsonProfile, "surname"));
        profile.setName(Validator.validateStr(jsonProfile, "name"));
        profile.setPatronymic(Validator.validateStr(jsonProfile, "patronymic"));
        profile.setEmail(Validator.validateStr(jsonProfile, "email"));
        profile.setBirth(Validator.validateStr(jsonProfile, "birth"));
        profile.setPhoto(Validator.validateStr(jsonProfile, "photo"));
        profile.save();
    }
}
