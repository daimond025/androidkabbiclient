package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.events.api.client.OrdersInfoEvent;
import com.kabbi.client.models.Order;
import com.kabbi.client.utils.DataBaseHelper;
import com.kabbi.client.utils.Validator;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import static com.kabbi.client.app.AppParams.CAR_PHOTO;
import static com.kabbi.client.app.AppParams.PHOTO_TYPE;
import static com.kabbi.client.app.AppParams.DRIVER_PHOTO;
import static com.kabbi.client.app.AppParams.USE_PHOTO;

/**
 * Created by gootax on 06.12.16.
 */

public class OrdersInfoListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", new String(((TypedByteArray) response.getBody())
                .getBytes()));
        try {
            JSONArray orderList = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result").getJSONArray("order_list");
            ArrayList<Order> orders= new ArrayList<>();
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

            for (int i = 0; i < orderList.length(); i++) {
                String orderId = orderList.getJSONObject(i).getString("order_id");
                JSONObject orderInfo = orderList.getJSONObject(i).getJSONObject("order_info");
                JSONObject carData = Validator.validateJSONObj(orderInfo, "car_data");
                Order tempOrder = Order.getOrder(orderId);

                if (tempOrder != null) {
                    Long realTime = orderInfo.getLong("order_time")*1000;
                    tempOrder.setOrderTime(formatter.format(new Date(realTime)));
                    tempOrder.setStatus(Validator.validateStr(orderInfo, "status_group"));
                    tempOrder.setStatusId(Validator.validateStr(orderInfo, "status_id"));
                    tempOrder.setStatusLabel(Validator.validateStr(orderInfo, "status_name"));

                    tempOrder.setDriver(Validator.validateStr(carData, "driver_fio"));
                    tempOrder.setCar(Validator.validateStr(carData, "car_description"));
                    if (USE_PHOTO) {
                        switch (PHOTO_TYPE) {
                            case DRIVER_PHOTO:
                                tempOrder.setPhoto(Validator.validateStr(carData, "driver_photo"));
                                break;
                            case CAR_PHOTO:
                                tempOrder.setPhoto(Validator.validateStr(carData, "car_photo"));
                                break;
                        }
                    }

                    orders.add(tempOrder);
                }
            }
           DataBaseHelper.executeTransactionFromList(orders);
            EventBus.getDefault().post(new OrdersInfoEvent(orders));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

