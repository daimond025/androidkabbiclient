package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.app.Constants;
import com.kabbi.client.models.Profile;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.util.ThrowableFailureEvent;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by gootax on 06.12.16.
 */

public class ConfidentialListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        OfferPageEvent mOfferPageEvent = new OfferPageEvent(null);
        mOfferPageEvent.code = Constants.REQUEST_FAILURE_CODE;
        EventBus.getDefault().post(mOfferPageEvent);
    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("Logos", new String(((TypedByteArray) response.getBody())
                    .getBytes()));
            JSONObject jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");

            String url = jsonResult.getString("result");

            EventBus.getDefault().post(new OfferPageEvent(url));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
