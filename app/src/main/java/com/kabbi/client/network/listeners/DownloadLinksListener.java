package com.kabbi.client.network.listeners;

import android.util.Log;

import com.kabbi.client.models.Profile;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by andrew on 19/12/2017.
 */

public class DownloadLinksListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        Log.d("Logos", new String(((TypedByteArray) response.getBody())
                .getBytes()));
        try {
            JSONObject json = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes()));

            JSONObject result = json.optJSONObject("result");

            String iosUrl = result.optString("ios","");
            String androidUrl = result.optString("android","");

            Profile profile = Profile.getProfile();
            profile.setAndroidUrl(androidUrl);
            profile.setIosUrl(iosUrl);
            profile.save();

        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
    }

}
