package com.kabbi.client.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.kabbi.client.app.DeepParams;

public class DeepOrder implements Parcelable {

    private double fromLat;
    private double fromLon;

    private double toLat;
    private double toLon;

    private String fromStreet;
    private String fromCity;
    private String fromHouse;

    private String toStreet;
    private String toCity;
    private String toHouse;

    private String tariffId;


    public DeepOrder(double fromLat, double fromLon, double toLat, double toLon, String fromStreet, String fromCity, String fromHouse, String toStreet, String toCity, String toHouse, String tariffId) {
        this.fromLat = fromLat;
        this.fromLon = fromLon;
        this.toLat = toLat;
        this.toLon = toLon;
        this.fromStreet = fromStreet;
        this.fromCity = fromCity;
        this.fromHouse = fromHouse;
        this.toStreet = toStreet;
        this.toCity = toCity;
        this.toHouse = toHouse;
        this.tariffId = tariffId;
    }

    protected DeepOrder(Parcel in) {
        fromLat = in.readDouble();
        fromLon = in.readDouble();
        toLat = in.readDouble();
        toLon = in.readDouble();
        fromStreet = in.readString();
        fromCity = in.readString();
        fromHouse = in.readString();
        toStreet = in.readString();
        toCity = in.readString();
        toHouse = in.readString();
        tariffId = in.readString();
    }

    public static final Creator<DeepOrder> CREATOR = new Creator<DeepOrder>() {
        @Override
        public DeepOrder createFromParcel(Parcel in) {
            return new DeepOrder(in);
        }

        @Override
        public DeepOrder[] newArray(int size) {
            return new DeepOrder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(fromLat);
        parcel.writeDouble(fromLon);
        parcel.writeDouble(toLat);
        parcel.writeDouble(toLon);
        parcel.writeString(fromStreet);
        parcel.writeString(fromCity);
        parcel.writeString(fromHouse);
        parcel.writeString(toStreet);
        parcel.writeString(toCity);
        parcel.writeString(toHouse);
        parcel.writeString(tariffId);
    }

    public double getFromLat() {
        return fromLat;
    }

    public void setFromLat(double fromLat) {
        this.fromLat = fromLat;
    }

    public double getFromLon() {
        return fromLon;
    }

    public void setFromLon(double fromLon) {
        this.fromLon = fromLon;
    }

    public double getToLat() {
        return toLat;
    }

    public void setToLat(double toLat) {
        this.toLat = toLat;
    }

    public double getToLon() {
        return toLon;
    }

    public void setToLon(double toLon) {
        this.toLon = toLon;
    }

    public String getFromStreet() {
        return fromStreet;
    }

    public void setFromStreet(String fromStreet) {
        this.fromStreet = fromStreet;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromHouse() {
        return fromHouse;
    }

    public void setFromHouse(String fromHouse) {
        this.fromHouse = fromHouse;
    }

    public String getToStreet() {
        return toStreet;
    }

    public void setToStreet(String toStreet) {
        this.toStreet = toStreet;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToHouse() {
        return toHouse;
    }

    public void setToHouse(String toHouse) {
        this.toHouse = toHouse;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    @Override
    public String toString() {
        return "DeepOrder{" +
                "fromLat=" + fromLat +
                ", fromLon=" + fromLon +
                ", toLat=" + toLat +
                ", toLon=" + toLon +
                ", fromStreet='" + fromStreet + '\'' +
                ", fromCity='" + fromCity + '\'' +
                ", fromHouse='" + fromHouse + '\'' +
                ", toStreet='" + toStreet + '\'' +
                ", toCity='" + toCity + '\'' +
                ", toHouse='" + toHouse + '\'' +
                ", tariffId='" + tariffId + '\'' +
                '}';
    }


    public static DeepOrder parseFromUri(Uri appLinkData) {
        String fromLat = appLinkData.getQueryParameter(DeepParams.DEEP_LAT_FROM);
        String toLat = appLinkData.getQueryParameter(DeepParams.DEEP_LAT_TO);
        String fromLon = appLinkData.getQueryParameter(DeepParams.DEEP_LON_FROM);
        String toLon = appLinkData.getQueryParameter(DeepParams.DEEP_LON_TO);

        String fromCity = appLinkData.getQueryParameter(DeepParams.DEEP_CITY_FROM);
        String fromStreet = appLinkData.getQueryParameter(DeepParams.DEEP_STREET_FROM);
        String fromHouse = appLinkData.getQueryParameter(DeepParams.DEEP_HOUSE_FROM);
        String toCity = appLinkData.getQueryParameter(DeepParams.DEEP_CITY_TO);
        String toStreet = appLinkData.getQueryParameter(DeepParams.DEEP_STREET_TO);
        String toHouse = appLinkData.getQueryParameter(DeepParams.DEEP_HOUSE_TO);
        String tariffId = appLinkData.getQueryParameter(DeepParams.DEEP_TARIFF_ID);
        return new DeepOrder(
                Double.valueOf(fromLat),
                Double.valueOf(fromLon),
                Double.valueOf(toLat),
                Double.valueOf(toLon),
                fromStreet,
                fromCity,
                fromHouse,
                toStreet,
                toCity,
                toHouse,
                tariffId
        );
    }
}
