package com.kabbi.client.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.kabbi.client.app.AppParams;

@Table(name = "profile")
public class Profile extends Model {

    public static final int BONUS_TYPE_GOOTAX = 1;
    public static final int BONUS_TYPE_UDS_GAME = 2;

    public static final int BONUS_UDS_STATUS_NONACTIVATE = 0;
    public static final int BONUS_UDS_STATUS_ACTIVATE = 1;

    public static final int BONUS_INACTIVE = 0;
    public static final int BONUS_ACTIVE = 1;

    public static final int GREGORIAN_CALENDAR = 0;
    public static final int PERSIAN_CALENDAR = 1;

    @Column(name = "client_id")
    private String clientId = "";
    @Column(name = "name")
    private String name = "";
    @Column(name = "surname")
    private String surname = "";
    @Column(name = "patronymic")
    private String patronymic = "";
    @Column(name = "birth")
    private String birth = "";
    @Column(name = "photo")
    private String photo = "";
    @Column(name = "email")
    private String email = "";
    @Column(name = "phone")
    private String phone = "";
    @Column(name = "phone_mask")
    private String phoneMask = "";
    @Column(name = "country")
    private String country = "";
    @Column(name = "city_id")
    private String cityId = "";
    @Column(name = "balance_value")
    private String balanceValue = "";
    @Column(name = "balance_currency")
    private String balanceCurrency = "";
    @Column(name = "bonus_value")
    private String bonusValue = "";
    @Column(name = "payment_type")
    private String paymentType = "";
    @Column(name = "pan")
    private String pan = "";
    @Column(name = "company")
    private String company = "";
    @Column(name = "authorized")
    private boolean authorized = false;
    @Column(name = "colorID")
    private long colorID = 0;
    @Column(name = "theme")
    private int themeId;
    @Column(name = "tenant_id")
    private String tenantId = "";
    @Column(name = "map")
    private int map = 1;
    @Column(name = "payment_bonus")
    private int paymentBonus = 0;
    @Column(name = "bonus_id")
    private int bonusId = 1;
    @Column(name = "bonus_state")
    private int bonusUdsState = 0;
    @Column(name = "first_sign")
    private boolean firstSign = true;
    @Column(name = "inaccuracy")
    private long inaccuracy = 0;
    @Column(name = "client_type")
    private String clientType = "";
    @Column(name = "calendar_type")
    private int calendar = 0;
    @Column(name = "api_key")
    private String apiKey = "";
    @Column(name = "app_id")
    private String appId = "";
    @Column(name = "android_url")
    private String androidUrl = "";
    @Column(name = "ios_url")
    private String iosUrl = "";

    public Profile() {
        super();
    }

    public static Profile getProfile() {
        return new Select().from(Profile.class).executeSingle();
    }

    public String getClientId() {
        return clientId.equals("null") ? "" : clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name.equals("null") ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname.equals("null") ? "" : surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic.equals("null") ? "" : patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirth() {
        return birth.equals("null") ? "" : birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getPhoto() {
        return photo.equals("null") ? "" : photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone.equals("null") ? "" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneMask() {
        return phoneMask.equals("null") ? "" : phoneMask;
    }

    public void setPhoneMask(String phoneMask) {
        this.phoneMask = phoneMask;
    }

    public String getEmail() {
        return email.equals("null") ? "" : email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country.equals("null") ? "" : country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCityId() {
        return cityId.equals("null") ? "" : cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getBalanceValue() {
        return balanceValue.equals("null") ? "" : balanceValue;
    }

    public void setBalanceValue(String balanceValue) {
        this.balanceValue = balanceValue;
    }

    public String getBalanceCurrency() {
        return balanceCurrency.equals("null") ? "" : balanceCurrency;
    }

    public void setBalanceCurrency(String balanceCurrency) {
        this.balanceCurrency = balanceCurrency;
    }

    public String getAndroidUrl() {
        return androidUrl;
    }

    public void setAndroidUrl(String androidUrl) {
        this.androidUrl = androidUrl;
    }

    public String getIosUrl() {
        return iosUrl;
    }

    public void setIosUrl(String iosUrl) {
        this.iosUrl = iosUrl;
    }

    public String getBonusValue() {
        return bonusValue.equals("null") ? "" : bonusValue;
    }

    public void setBonusValue(String bonusValue) {
        this.bonusValue = bonusValue + " B";
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String type) {
        this.paymentType = type;
    }

    public String getPan() {
        return pan.equals("null") ? "" : pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCompany() {
        return company.equals("null") ? "" : company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public long getColorID() {
        return colorID;
    }

    public void setColorID(long colorID) {
        this.colorID = colorID;
    }

    public int getThemeId() {
        return themeId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public int getMap() {
        return map;
    }

    public void setMap(int map) {
        this.map = map;
    }


    public int getPaymentBonus() {
        return paymentBonus;
    }

    public void setPaymentBonus(int paymentBonus) {
        this.paymentBonus = paymentBonus;
    }

    public int getBonusId() {
        return bonusId;
    }

    public void setBonusId(int bonusId) {
        this.bonusId = bonusId;
    }

    public int getBonusUdsState() {
        return bonusUdsState;
    }

    public void setBonusUdsState(int bonusUdsState) {
        this.bonusUdsState = bonusUdsState;
    }

    public boolean isFirstSign() {
        return firstSign;
    }

    public void setFirstSign(boolean firstSign) {
        this.firstSign = firstSign;
    }

    public long getInaccuracy() {
        return inaccuracy;
    }

    public void setInaccuracy(long inaccuracy) {
        this.inaccuracy = inaccuracy;
    }

    public String getClientType() {
        if (clientType != null && !clientType.isEmpty()) {
            return clientType;
        }
        else return "base";
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public int getCalendar() {
        return calendar;
    }

    public void setCalendar(int calendar) {
        this.calendar = calendar;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public void clearProfileFields() {
        clientId = "";
        name = "";
        surname = "";
        patronymic = "";
        birth = "";
        photo = "";
        email = "";
        phone = "";
        phoneMask = "";
        country = "";
        balanceValue = "";
        balanceCurrency = "";
        bonusValue = "";

        if (AppParams.PAYMENT_WITH_CASH) setPaymentType("CASH");
        else setPaymentType("CARD");

        pan = "";
        company = "";
        authorized = false;
        colorID = AppParams.DEFAULT_THEME;
        paymentBonus = 0;
        bonusUdsState = 0;
        calendar = 0;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "clientId='" + clientId + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birth='" + birth + '\'' +
                ", photo='" + photo + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", phoneMask='" + phoneMask + '\'' +
                ", country='" + country + '\'' +
                ", cityId='" + cityId + '\'' +
                ", balanceValue='" + balanceValue + '\'' +
                ", balanceCurrency='" + balanceCurrency + '\'' +
                ", bonusValue='" + bonusValue + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", pan='" + pan + '\'' +
                ", company='" + company + '\'' +
                ", authorized=" + authorized +
                ", colorID=" + colorID +
                ", themeId=" + themeId +
                ", tenantId='" + tenantId + '\'' +
                ", map=" + map +
                ", paymentBonus=" + paymentBonus +
                ", bonusId=" + bonusId +
                ", bonusUdsState=" + bonusUdsState +
                ", firstSign=" + firstSign +
                ", inaccuracy=" + inaccuracy +
                ", clientType='" + clientType + '\'' +
                ", calendar=" + calendar +
                ", apiKey='" + apiKey + '\'' +
                ", appId='" + appId + '\'' +
                ", androidUrl='" + androidUrl + '\'' +
                ", iosUrl='" + iosUrl + '\'' +
                '}';
    }
}
