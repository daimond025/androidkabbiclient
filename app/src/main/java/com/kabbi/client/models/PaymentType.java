package com.kabbi.client.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;

import java.util.List;

@Table(name = "payment")
public class PaymentType extends Model {

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name = "type")
    private String paymentType;

    @Column(name = "balance")
    private String balance;

    @Column(name = "description")
    private String paymentDescription;

    public PaymentType() {
        super();
    }

    public PaymentType(String paymentId, String paymentType) {
        super();
        this.paymentId = paymentId;
        this.paymentType = paymentType;

    }

    public PaymentType(String paymentId, String paymentType, String balance) {
        super();
        this.paymentId = paymentId;
        this.paymentType = paymentType;
        this.balance = balance;
    }

    public PaymentType(String paymentId, String paymentName, String paymentDescription, String balance) {
        super();
        this.paymentId = paymentId;
        this.paymentType = paymentName;
        this.paymentDescription = paymentDescription;
        this.balance = balance;
    }


    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public static List<PaymentType> getPayments() {
        return new Select().from(PaymentType.class).execute();
    }

    public static void deleteCardPayments() {
        new Delete().from(PaymentType.class).where("type = 'CARD'").execute();
    }

    public static void deleteAllPayments() {
        new Delete().from(PaymentType.class).where("type != 'CASH'").execute();
    }

    public static void deletePaymentByID(String paymentId) {
        new Delete().from(PaymentType.class).where("payment_id == ?", paymentId).execute();
    }


    public static PaymentType getPayment(String id) {
        return new Select()
                .from(PaymentType.class)
                .where("payment_id = ?", id)
                .executeSingle();
    }

    public static void addCashPayment(String id, String type) {
        PaymentType paymentType = PaymentType.getPayment(id);
        if (paymentType == null) {
            paymentType = new PaymentType("CASH", type);
            paymentType.save();
        }
    }

    public static void addCardPayment(String id) {
        PaymentType paymentType = PaymentType.getPayment(id);
        if (paymentType == null) {
            if (AppParams.PAYMENT_WITH_CASH)
                paymentType = new PaymentType(id, "CARD");
            paymentType.save();
        }
    }

    public static void addPersonalPayment(String id, String value, String currency) {
        PaymentType paymentType = PaymentType.getPayment(id);
        if (paymentType == null) {
            paymentType = new PaymentType(id, "PERSONAL_ACCOUNT", value + " " + currency);
            paymentType.save();
        }
        else {
            paymentType.setBalance(value + " " + currency);
            paymentType.save();
        }
    }

    public static void addCorpPayment(String id, String company, String value, String currency) {
        PaymentType paymentType = PaymentType.getPayment(id);
        if (paymentType == null) {
            paymentType = new PaymentType(id, "CORP_BALANCE", company, value + " " + currency);
            paymentType.save();
        }
        else {
            paymentType.setBalance(value + " " + currency);
            paymentType.setPaymentDescription(company);
            paymentType.save();
        }
    }
}
