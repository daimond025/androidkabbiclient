package com.kabbi.client.models;

import android.support.annotation.Nullable;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.utils.AddressList;

import java.util.List;

@Table(name = "routes")
public class RoutePoint extends Model {

    @Column(name = "orders", onDelete = Column.ForeignKeyAction.CASCADE)
    private Order order;
    @Column(name = "addresses")
    private Address address;
    @Column(name = "address_num")
    private int addressNum;

    public RoutePoint() {
        super();
    }


    RoutePoint(Order order, Address address, int addressNum) {
        super();
        this.order = order;
        this.address = address;
        this.addressNum = addressNum;
    }

    public static void saveOrderAddressesAndRoute(Order order,
                                                  List<Address> addresses) {
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < addresses.size(); i++) {
                Address address = addresses.get(i);
                if (!address.isEmpty()) {
                    Address sameAddress = Address.getAddressByHash(address.getHash());
                    if (sameAddress == null) {
                        address.setUseType(Address.TYPE_ROUTE);
                        address.save();
                        new RoutePoint(order, address, i).save();
                    } else {
                        sameAddress.setUseType(Address.TYPE_ROUTE);
                        sameAddress.save();
                        new RoutePoint(order, sameAddress, i).save();
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<Address> getRoute(Order order) {
        List<RoutePoint> points = new Select()
                .from(RoutePoint.class)
                .where("orders = ?", order.getId())
                .orderBy("address_num ASC")
                .execute();
        List<Address> addresses = new AddressList();
        for (RoutePoint point : points) {
            addresses.add(point.getAddress());
        }
        return addresses;
    }

    public static List<Address> getRouteWithFakeAddress(Order order) {
        List<Address> addresses = getRoute(order);
        if (((AddressList) addresses).realSize() < 2) {
            Address secondAddress = new Address();
            secondAddress.setEmpty(true);
            addresses.add(secondAddress);
        }
        return addresses;
    }

    public static List<RoutePoint> getRoutePoints() {
        return new Select().from(RoutePoint.class).execute();
    }

    public static void getDeletePoints(Order order) {
        new Delete().from(RoutePoint.class).where("orders = ?", order.getId()).execute();
    }

    @Nullable
    public static Address getFirstAddress(Order order) {
        RoutePoint routePoint = new Select()
                .from(RoutePoint.class)
                .where("orders = ?", order.getId())
                .orderBy("address_num ASC")
                .executeSingle();
        if (routePoint != null) {
            return routePoint.getAddress();
        } else {
            return null;
        }
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getAddressNum() {
        return addressNum;
    }

    public void setAddressNum(int addressNum) {
        this.addressNum = addressNum;
    }

}
