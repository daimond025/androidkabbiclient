package com.kabbi.client.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

/**
 * Created by gootax on 14.04.17.
 */

@Table(name = "referrals")
public class Referral extends Model {

    @Column(name = "city_id")
    private String cityId;

    @Column(name = "referral_id")
    private String referralId;

    @Column(name = "content")
    private String content;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    @Column(name = "code")
    private String code;

    @Column(name = "isActive")
    private boolean isActive;

    public static Referral getReferralByCityId(String cityId) {
        return new Select().from(Referral.class).where("city_id = ?", cityId).executeSingle();
    }

    public static void deleteAllReferrals() {
        new Delete().from(Referral.class).execute();
    }

    public String getReferralId() {
        return referralId;
    }

    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

}
