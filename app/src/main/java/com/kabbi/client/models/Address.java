package com.kabbi.client.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kabbi.client.utils.AddressList;

import java.util.ArrayList;
import java.util.List;

@Table(name = "addresses")
public class Address extends Model implements Parcelable {

    public static final String TYPE_HISTORY = "HISTORY";
    public static final String TYPE_ROUTE = "ROUTE";
    public static final String TYPE_FAKE = "FAKE";
    public static final String TYPE_AUTOCOMPLETE = "AUTOCOMPLETE";

    @Column(name = "city")
    private String city = "";
    @Column(name = "street")
    private String street = "";
    @Column(name = "house")
    private String house = "";
    @Column(name = "housing")
    private String housing = "";
    @Column(name = "porch")
    private String porch = "";
    @Column(name = "lat")
    private String lat = "";
    @Column(name = "lon")
    private String lon = "";
    @Column(name = "label")
    private String label = "";
    @Column(name = "favorite")
    private boolean favorite = false;
    @Column(name = "type")
    private String type = "";
    @Column(name = "hash")
    private String hash = "";
    @Column(name = "apt")
    private String apt = "";
    @Column(name = "gpsAddress")
    private boolean isGps = false;
    @Column(name = "emptyAddress")
    private boolean isEmpty = false;
    @Column(name = "use_type")
    private String useType = "";

    public Address() {
        super();
    }

    public Address(Parcel in) {
        this.city = in.readString();
        this.street = in.readString();
        this.house = in.readString();
        this.housing = in.readString();
        this.porch = in.readString();
        this.lat = in.readString();
        this.lon = in.readString();
        this.label = in.readString();
        this.favorite = (in.readInt() != 0);
        this.type = in.readString();
        this.hash = in.readString();
        this.apt = in.readString();
        this.isGps = (in.readInt() != 0);
        this.isEmpty = (in.readInt() != 0);
        this.useType = in.readString();
    }


    public static List<Address> getAddresses() {
        return new Select().from(Address.class).where("gpsAddress != ?", true).execute();
    }


    public static Address getAddressByHash(String hash) {
        return new Select()
                .from(Address.class)
                .where("hash = ?", hash)
                .executeSingle();
    }

    public static void deleteAllAddressHisory() {
        new Delete()
                .from(Address.class)
                .where("use_type = ?", TYPE_HISTORY)
                .execute();
    }

    public static List<Address> getAddressHistory() {
        List<Address> addresses = new Select()
                .from(Address.class)
                .where("use_type = ?", TYPE_HISTORY)
                .execute();
        return addresses == null ? AddressList.EMPTY : addresses;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getHouse() {
        return house;
    }
    public void setHouse(String house) {
        this.house = house;
    }
    public String getHousing() {
        return housing;
    }
    public void setHousing(String housing) {
        this.housing = housing;
    }
    public String getPorch() {
        return porch;
    }
    public void setPorch(String porch) {
        this.porch = porch;
    }
    public String getLat() {
        return lat;
    }
    public void setLat(String lat) {
        this.lat = lat;
    }
    public String getLon() {
        return lon;
    }
    public void setLon(String lon) {
        this.lon = lon;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        if (label != null && !label.isEmpty()) isEmpty = false;
        this.label = label;
    }
    public boolean isFavorite() {
        return favorite;
    }
    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getHash() {
        return hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    public String getApt() {
        return apt;
    }
    public void setApt(String apt) {
        this.apt = apt;
    }
    public boolean isGps() {
        return isGps;
    }
    public void setGps(boolean gps) {
        isGps = gps;
    }
    public boolean isEmpty() {
        return isEmpty;
    }
    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                ", housing='" + housing + '\'' +
                ", porch='" + porch + '\'' +
                ", lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                ", label='" + label + '\'' +
                ", favorite=" + favorite +
                ", type='" + type + '\'' +
                ", hash='" + hash + '\'' +
                ", apt='" + apt + '\'' +
                ", isGps=" + isGps +
                ", isEmpty=" + isEmpty +
                ", useType='" + useType + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeString(street);
        dest.writeString(house);
        dest.writeString(housing);
        dest.writeString(porch);
        dest.writeString(lat);
        dest.writeString(lon);
        dest.writeString(label);
        dest.writeInt(favorite ? 1 : 0);
        dest.writeString(type);
        dest.writeString(hash);
        dest.writeString(apt);
        dest.writeInt(isGps ? 1 : 0);
        dest.writeInt(isEmpty ? 1 : 0);
        dest.writeString(useType);
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {

        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

}
