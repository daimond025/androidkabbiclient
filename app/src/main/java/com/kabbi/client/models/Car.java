package com.kabbi.client.models;

public class Car {

    private double lat;
    private double lon;
    private String number;
    private String carClass;
    private float bearing;

    public Car(double lat, double lon, float bearing, String number) {
        this.lat = lat;
        this.lon = lon;
        this.bearing = bearing;
        this.number = number;
    }

    public Car(double lat, double lon, String number, String carClass, float bearing) {
        this.lat = lat;
        this.lon = lon;
        this.number = number;
        this.carClass = carClass;
        this.bearing = bearing;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }
}
