package com.kabbi.client.models;

public class Cost {

    private String tariffId;
    private String summaryCost;
    private String summaryCostNoDiscount;
    private boolean isFix;

    public Cost(String tariffId, String summaryCost, String summaryCostNoDiscount, boolean isFix) {
        this.tariffId = tariffId;
        this.summaryCost = summaryCost;
        this.summaryCostNoDiscount = summaryCostNoDiscount;
        this.isFix = isFix;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getSummaryCost() {
        return summaryCost;
    }

    public void setSummaryCost(String summaryCost) {
        this.summaryCost = summaryCost;
    }

    public String getSummaryCostNoDiscount() {
        return summaryCostNoDiscount;
    }

    public void setSummaryCostNoDiscount(String summaryCostNoDiscount) {
        this.summaryCostNoDiscount = summaryCostNoDiscount;
    }

    public boolean isFix() {
        return isFix;
    }

    public void setFix(boolean fix) {
        isFix = fix;
    }
}
