package com.kabbi.client.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kabbi.client.utils.CollectionHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gootax on 13.06.17.
 */

@Table(name = "property")
public class Property extends Model {

    public static final String TYPE_LIST = "list";
    public static final String TYPE_STING = "string";

    @Column(name = "property_id")
    private String propertyId;
    @Column(name = "company_id")
    private String companyId;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "required")
    private int required;
    @Column(name = "property_values")
    private String values;
    @Column(name = "property_value")
    private String value;

    public Property(String propertyId, String companyId, String name, String type, int required, Map<String, String> values) {
        this.propertyId = propertyId;
        this.companyId = companyId;
        this.name = name;
        this.type = type;
        this.required = required;
        setValues(values);
    }

    public Property() {
    }

    public static List<Property> getPropertyById(String id) {
        Log.d("Logos", "getPropertyById: " +  id);
        return new Select().from(Property.class).where("company_id = ?", id).execute();
    }

    public static void deleteAllProperty() {
        new Delete().from(Property.class).execute();
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public Map<String, String> getValues() {
        if (values == null) return new HashMap<>();
        return CollectionHelper.parseHashMap(values);
    }

    public void setValues(Map<String, String> values) {
        this.values = values.toString();
    }

    public String getValue() {
        return value == null ? "" : value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
