package com.kabbi.client.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Table(name = "tariffs")
public class Tariff extends Model {

/*    public static final String CURRENT_TYPE = "CURRENT";
    public static final String HOLIDAY_TYPE = "HOLIDAY";
    public static final String EXCEPTION_TYPE = "EXCEPTION";*/

    @Column(name = "tariffId")
    private String tariffId;
    @Column(name = "tariffName")
    private String tariffName;
    @Column(name = "tariffIcon")
    private String tariffIcon;
    @Column(name = "tariffIconMini")
    private String tariffIconMini;
    @Column(name = "tariffType")
    private String tariffType;
    @Column(name = "description")
    private String description;
    @Column(name = "cityID")
    private long cityID;
    @Column(name = "is_bonus")
    private String bonus;
    @Column(name = "client_types")
    private String clientTypes;
    @Column(name = "client_companies")
    private String clientCompanies;
    @Column(name = "car_class_id")
    private String carClassId;

    public Tariff() {
        super();
    }

    public Tariff(String tariffId, String tariffName, long cityID) {
        super();
        this.tariffId = tariffId;
        this.tariffName = tariffName;
        this.cityID = cityID;
    }

    public static void deleteAllTariffs() {
        new Delete().from(Tariff.class).execute();
    }

    public static Tariff getTariffById(String tariffId) {
        return new Select().from(Tariff.class).where("tariffId = ?", tariffId).executeSingle();
    }

    public static List<Tariff> getTariffs(City city) {
        return new Select().from(Tariff.class).where("cityID = ?", city.getId()).execute();
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getTariffIcon() {
        return tariffIcon;
    }

    public void setTariffIcon(String tariffIcon) {
        this.tariffIcon = tariffIcon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTariffIconMini() {
        return tariffIconMini;
    }

    public void setTariffIconMini(String tariffIconMini) {
        this.tariffIconMini = tariffIconMini;
    }

    public long getCityID() {
        return cityID;
    }

    public void setCityID(long cityID) {
        this.cityID = cityID;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getTariffType() {
        return tariffType != null ? tariffType : "";
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public List<String> getClientTypes() {
        return Arrays.asList(clientTypes.split(","));
    }

    public void setClientTypes(String clientTypes) {
        this.clientTypes = clientTypes;
    }

    public List<String> getCompanies() {
        if (clientCompanies == null) return Collections.emptyList();
        else return Arrays.asList(clientCompanies.split(","));
    }

    public void setCompanies(String clientCompanies) {
        this.clientCompanies = clientCompanies;
    }

    public String getCarClassId() {
        return carClassId;
    }

    public void setCarClassId(String carClassId) {
        this.carClassId = carClassId;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "tariffId='" + tariffId + '\'' +
                ", tariffName='" + tariffName + '\'' +
                ", tariffIcon='" + tariffIcon + '\'' +
                ", tariffIconMini='" + tariffIconMini + '\'' +
                ", tariffType='" + tariffType + '\'' +
                ", description='" + description + '\'' +
                ", cityID=" + cityID +
                ", bonus='" + bonus + '\'' +
                ", clientTypes='" + clientTypes + '\'' +
                ", clientCompanies='" + clientCompanies + '\'' +
                ", carClassId='" + carClassId + '\'' +
                '}';
    }
}
