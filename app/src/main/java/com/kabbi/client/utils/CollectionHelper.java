package com.kabbi.client.utils;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gootax on 13.06.17.
 */

public class CollectionHelper {

    public static Map<String, String> parseHashMap(String property) {
        String validMap = "";
        if (property.length() > 2) validMap = property.substring(1, property.length() - 1); // TODO regex
        HashMap<String, String> map = new HashMap<>();
        String[] pairs = validMap.split(",");
        for (String pair : pairs) {
            String[] keyValue = pair.split("=");
            map.put(keyValue[0], keyValue[1]);
        }
        return map;
    }

}
