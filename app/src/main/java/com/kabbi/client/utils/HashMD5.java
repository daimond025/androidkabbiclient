package com.kabbi.client.utils;


import android.util.Log;

import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Profile;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;


public class HashMD5 {

    public static String getHash(String str) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(str.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32)
                hashtext = "0" + hashtext;
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getSignature(LinkedHashMap<String,String> params, String apiKey) {
        String rawSignature = params.toString();
        String signature = rawSignature.substring(1, rawSignature.length() - 1)
                .replaceAll(", ", "&")
                .replaceAll("%5C%2F", "%2F%2F")
                .replaceAll("\\*", "%2A")
                .replaceAll(":", "%3A")
                .replaceAll("\\|", "%7C")
                .replaceAll("\\+", "%20")
                .replaceAll("\\[", "%5B")
                .replaceAll("]", "%5D")
                + apiKey;
        Log.d("Logos", "sign params: " +  signature);
        Log.d("Logos", "sign: " +  HashMD5.getHash(signature));
        return HashMD5.getHash(signature);
    }

}