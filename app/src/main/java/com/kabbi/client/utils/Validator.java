package com.kabbi.client.utils;


import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Validator {

    public static String validateStr(JSONObject object, String param) {
        String result = "";
        try {
            result = object.getString(param);
        } catch (NullPointerException | JSONException ignored) {}
        return result;
    }

    public static int validateInt(JSONObject object, String param) {
        int result = 0;
        try {
            result = object.getInt(param);
        } catch (NullPointerException | JSONException | ClassCastException ignored) {}
        return result;
    }

    public static double validateDouble(JSONObject object, String param) {
        double result = 0.0;
        try {
            result = object.getDouble(param);
        } catch (NullPointerException | JSONException | ClassCastException ignored) {}
        return result;
    }

    @Nullable
    public static JSONObject validateJSONObj(JSONObject object, String param) {
        JSONObject result = null;
        try {
            result = object.getJSONObject(param);
        } catch (NullPointerException | JSONException ignored) {}
        return result;
    }

    @Nullable
    public static JSONArray validateJSONArr(JSONObject object, String param) {
        JSONArray result = null;
        try {
            result = object.getJSONArray(param);
        } catch (NullPointerException | JSONException ignored) {}
        return result;
    }

    public static Set<String> validateLanguages(String[] langValid, String[] langList) {
        Set<String> set = new LinkedHashSet<>();
        if (langList.length > 0 && langValid.length > 0) {
            set.addAll(Arrays.asList(langValid));
            set.retainAll(Arrays.asList(langList));
        }
        return set;
    }

}
