package com.kabbi.client.utils;

import android.content.Context;
import android.text.Editable;
import android.util.SparseArray;
import android.widget.ListView;

import com.facebook.common.util.StreamUtil;
import com.facebook.stetho.common.StringUtil;
import com.kabbi.client.R;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class InputMask {

    private String mask, firstSymbol;
    private String[] maskArr;
    private String phone;

    public InputMask(String mask, String phone) {
        this.mask = mask;
        this.phone = phone;
        firstSymbol = mask.replaceAll("\\D+", "");
        maskArr = mask.split("");
    }

    public String getNewText() {
        String newText;
        if (phone.length() == 0) {
            newText = mask;
        } else {
            newText = reload(phone.replaceAll("\\D+", "").substring(firstSymbol.length()));
        }
        return newText;
    }

    public int getSelection(String currentText) {
        int firstIndexForInput = currentText.indexOf('_');
        //if '_' is missing
        int selection;
        if (firstIndexForInput == -1) {
            try {
                selection = reload(phone.replaceAll("\\D+", "").substring(firstSymbol.length())).length();
            } catch (StringIndexOutOfBoundsException e) {
                selection = currentText.length();
            }
        } else {
            selection = firstIndexForInput;
        }
        return selection;
    }

    public String afterTextChangedSetText(String phoneStr, Editable s) {
        String result;
        if (!(s.toString().replaceAll("\\D+", "").length() < firstSymbol.length())) {
            result = reload(s.toString().replaceAll("\\D+", "").substring(firstSymbol.length()));
        } else {
            result = reload(phoneStr.replaceAll("\\D+", "").substring(firstSymbol.length()));
        }
        return result;
    }

    public int afterTextChangedSetSelection(String result) {
        int countSelection = 0;
        int selection = 0;
        for(int j=0; j < result.length(); j++) {
            if (result.substring(j, j+1).matches("[0-9]*")) {
                selection = countSelection + 1;
            }
            countSelection++;
        }
        return selection;
    }

    private String reload(String temp) {
        Map<Integer, String> maskMap = new TreeMap<>();
        Map <Integer, String> maskMapKey = new TreeMap<>();
        int count_symbol = 0;
        for (String symbol : maskArr) {
            if (symbol.equals(""))
                continue;
            maskMap.put(count_symbol, symbol);
            if (symbol.equals("_")) {
                maskMapKey.put(count_symbol, symbol);
            }
            count_symbol++;
        }
        StringBuilder stringBuilder = new StringBuilder();
        String [] tempArr = temp.split("");
        int count = 1;
        if (temp.length() > 0) {
            for (Map.Entry<Integer, String> entry : maskMapKey.entrySet()) {
                maskMap.put(entry.getKey(), tempArr[count]);
                count++;
                if (count == tempArr.length)
                    break;
            }
        }
        for (Map.Entry<Integer, String> entry : maskMap.entrySet()) {
            stringBuilder.append(entry.getValue());
        }
        return stringBuilder.toString();
    }

    public static String getProfilePhone(String phoneMask, String phone) {
        char[] phoneMaskArr = phoneMask.toCharArray();
        char[] phoneArr = phone.toCharArray();
        int phoneArrIndex = phoneMask.trim().replaceAll("\\D+", "").length();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < phoneMaskArr.length; i++) {
            if (phoneMaskArr[i] == '_') {
                result.append(phoneArr[phoneArrIndex]);
                ++phoneArrIndex;
            } else {
                result.append(phoneMaskArr[i]);
            }
        }
        return result.toString();
    }


    public String getGermanMask(String newPhone) {
        if (newPhone.length() > 13) {
            return mask;
        } else {
            return getShortestMask(newPhone);
        }
    }


    private String getShortestMask(String phone) {
        List<String> maskList = Arrays.asList(
                "+49(___)__-__",
                "+49(___)__-___",
                "+49(___)__-____",
                "+49(___)___-____",
                "+49(___)____-____"
        );

        for (String mask: maskList) {
            if (phone.length() <= StringUtils.countMatches(mask, "_") + 2){
                return mask;
            }
        }
        return maskList.get(maskList.size() - 1);
    }

}