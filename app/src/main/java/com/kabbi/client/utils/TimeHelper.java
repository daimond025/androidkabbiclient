package com.kabbi.client.utils;

import android.util.Log;

import com.kabbi.client.models.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gootax on 07.12.16.
 */

public class TimeHelper {

    /**
     * @param time must have format "dd.MM.yyyy HH:mm:ss"
     */
    public static Date getFakeTimeFromString(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);
        Long trueTime = 0L;
        try {
            trueTime = formatter.parse(time).getTime() + Profile.getProfile().getInaccuracy();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(trueTime);
    }

    public static Long getRealCurrentTimeLong() {
        return new Date().getTime() - Profile.getProfile().getInaccuracy();
    }
}
