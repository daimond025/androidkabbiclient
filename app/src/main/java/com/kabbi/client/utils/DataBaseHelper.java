package com.kabbi.client.utils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;

import java.util.List;

/**
 * Created by gootax on 10.01.17.
 */

public class DataBaseHelper  {

    public static <T extends Model> void executeTransactionFromList(List<T> list) {
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).save();
            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
    }

}
