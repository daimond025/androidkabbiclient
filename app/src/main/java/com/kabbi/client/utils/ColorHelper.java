package com.kabbi.client.utils;

import android.graphics.Color;

/**
 * Created by gootax on 12.09.17.
 */

public class ColorHelper {

    public static boolean isColorDark(int color){
        double darkness = 1-(0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color))/255;
        return darkness >= 0.4;
    }

}
