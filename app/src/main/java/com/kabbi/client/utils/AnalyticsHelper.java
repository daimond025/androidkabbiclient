package com.kabbi.client.utils;

import android.app.Application;

import com.kabbi.client.app.AppParams;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

/**
 * Class for using analytics in app
 */
public class AnalyticsHelper {

    public static final int METRICA_SESSION_TIMEOUT = 60; //sec

    public static final String METRICA_EVENT_REG = "User registration";
    public static final String METRICA_EVENT_CREATE_ORDER = "Creating order";
    public static final String METRICA_EVENT_REJECT_ORDER = "Rejecting order";

    public static void init(Application application) {
        if (AppParams.WITH_METRICS) {
            YandexMetricaConfig.Builder configBuilder =
                    YandexMetricaConfig.newConfigBuilder(AppParams.METRICS_KEY);
            configBuilder.setSessionTimeout(METRICA_SESSION_TIMEOUT);
            YandexMetricaConfig extendedConfig = configBuilder.build();
            YandexMetrica.activate(application, extendedConfig);
            YandexMetrica.enableActivityAutoTracking(application);
        }
    }

    public static void sendEvent(String event) {
        if (AppParams.WITH_METRICS) YandexMetrica.reportEvent(event);
    }


}
