package com.kabbi.client.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.provider.MediaStore.Images.Media.getBitmap;

/**
 * Created by gootax on 23.03.17.
 */

public class ImageEditor {

    public static File rotate(Context context, Bitmap bitmap, int angle) {
        Bitmap image = null;
        Matrix matrix = new Matrix();
        matrix.setRotate(angle);
        image =  Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return savePhoto(context, image);
    }

    public static File savePhoto(Context context, Bitmap source) {
        File newImage = null;
        try {
            newImage = new File(context.getCacheDir() , "Rotate.jpg");
            FileOutputStream fos = new FileOutputStream(newImage);
            if (source != null){
                source.compress(Bitmap.CompressFormat.JPEG, 100 , fos);
                Log.d("Logos", "savePhoto");
                fos.flush();
                fos.close();
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newImage;
    }

}
