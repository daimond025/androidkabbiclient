package com.kabbi.client.views.pickers;

/**
 * Created by gootax on 04.05.17.
 */

public interface TimeSelectedListener {
    void onTimeChangedToAny(long newTime);
    void onTimeChangedToNow();
}
