package com.kabbi.client.views.adapters.map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.kabbi.client.R;
import com.kabbi.client.models.Car;


public class MapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context mContext;

    public MapInfoWindowAdapter(Context context) {
        mContext = context;
    }


    @Override
    @SuppressLint("InflateParams")
    public View getInfoWindow(Marker marker) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.popup_info_dialog, null);
        Car car = (Car) marker.getTag();
        SosInfoViewHolder sosInfoViewHolder = new SosInfoViewHolder(view);
        sosInfoViewHolder.initData(car);
        return view;
    }


    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }


    private class SosInfoViewHolder {

        private TextView mTvCarNumber;


        private SosInfoViewHolder(View view) {
            mTvCarNumber = (TextView) view.findViewById(R.id.tv_popup_dialog_number);
        }


        private void initData(Car car) {
            mTvCarNumber.setText(car.getNumber());
        }
    }

}
