package com.kabbi.client.views.adapters;

import android.content.Context;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kabbi.client.R;

import java.util.List;

public class CountryActivityAdapter extends BaseAdapter {

    private LayoutInflater lInflater;
    private List<SparseArray<String>> list;

    public CountryActivityAdapter(Context context, List<SparseArray<String>> list) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    static class CountryViewHolder {
        public TextView itemCountry;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_country, parent, false);
            CountryViewHolder countryViewHolder = new CountryViewHolder();
            countryViewHolder.itemCountry = (TextView) rowView.findViewById(R.id.item_country);
            rowView.setTag(countryViewHolder);
        }
        CountryViewHolder holder = (CountryViewHolder) rowView.getTag();
        holder.itemCountry.setText(Html.fromHtml(
                list.get(position).get(0) + "<br/><small><font color='#cccccc'>" +
                        list.get(position).get(1) + "</font></small>"));
        holder.itemCountry.setTag(R.id.tag_item_name, list.get(position).get(0));
        holder.itemCountry.setTag(R.id.tag_item_type, list.get(position).get(1));
        return rowView;
    }

}
