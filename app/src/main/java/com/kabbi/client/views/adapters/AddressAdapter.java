package com.kabbi.client.views.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.events.ui.ItemAddressActionEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.utils.AddressList;

import java.util.ArrayList;
import java.util.List;

import org.greenrobot.eventbus.EventBus;

import static com.kabbi.client.app.AppParams.WITH_MAX_ONE_ADDRESS;
import static com.kabbi.client.app.AppParams.WITH_PORCH;

public class AddressAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Address> addressList;
    private List<Integer> icons;
    private boolean isDefinedCity;

    public AddressAdapter(Context context, List<Address> list) {
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addressList = list;
        this.isDefinedCity = true;
        initIcons();
    }

    private void initIcons() {
        icons = new ArrayList<>();
        icons.add(R.drawable.a);
        icons.add(R.drawable.b);
        icons.add(R.drawable.c);
        icons.add(R.drawable.d);
        icons.add(R.drawable.e);
    }

    private static class AddressViewHolder {
        ImageView itemIcon;
        TextView itemStreet;
        TextView itemCity;
        TextView tvPointLabel;
        ImageView itemRemove;
        TextView tvHeaderLabel;
        RelativeLayout rlActionContent;
    }

    @Override
    public int getCount() {
        return (WITH_MAX_ONE_ADDRESS) ? 1 : ((AddressList) addressList).realSize();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_header, parent, false);
            AddressViewHolder viewHolder = new AddressViewHolder();
            viewHolder.itemIcon = (ImageView) rowView.findViewById(R.id.iv_item_icon);
            viewHolder.itemStreet = (TextView) rowView.findViewById(R.id.tv_item_street);
            viewHolder.itemCity = (TextView) rowView.findViewById(R.id.tv_item_city);
            viewHolder.itemRemove = (ImageView) rowView.findViewById(R.id.btn_item_remove);
            viewHolder.tvHeaderLabel = (TextView) rowView.findViewById(R.id.tv_address_header_label);
            viewHolder.rlActionContent = (RelativeLayout) rowView.findViewById(R.id.ll_address_detail_container);
            rowView.setTag(viewHolder);
        }
        // fill data
        AddressViewHolder holder = (AddressViewHolder) rowView.getTag();
        customizeViews(position, holder);
        return rowView;
    }

    private void customizeViews(final int position, AddressViewHolder holder) {
        holder.itemIcon.setImageResource(icons.get(position));
        Address currentAddress = addressList.get(position);
        setStreetText(currentAddress, holder.itemStreet);
        setStreetColor(currentAddress, holder.itemStreet, holder.itemCity);
        setCity(currentAddress, holder.itemCity, position);
        setActionButton(position, currentAddress, holder.itemRemove, holder.tvHeaderLabel, holder.rlActionContent);
    }

    private void setStreetText(Address currentAddress, TextView itemStreet) {
        if (!currentAddress.isEmpty()) {
            itemStreet.setVisibility(View.VISIBLE);
            itemStreet.setText(currentAddress.getLabel());
        } else {
            itemStreet.setVisibility(View.GONE);
        }
    }

    private void setStreetColor(Address currentAddress, TextView itemStreet, TextView itemCity) {
        if (isDefinedCity) {
            itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorBlack));
            itemCity.setTextColor(ContextCompat.getColor(context, R.color.textColorGrey));
        } else {
            itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorRed));
            itemCity.setTextColor(ContextCompat.getColor(context, R.color.textColorRed));
        }
    }

    private void setCity(Address currentAddress, TextView itemCity, int position) {
        boolean isFakeSecondPoint =
                (position == AddressList.SECOND_POINT) && currentAddress.isEmpty();
        String city = currentAddress.getCity();

        if (currentAddress.isEmpty() || city == null || city.isEmpty()) {
            itemCity.setVisibility(View.GONE);
        } else {
            itemCity.setText(city);
            itemCity.setVisibility(View.VISIBLE);
        }

        if (isFakeSecondPoint){
            itemCity.setVisibility(View.VISIBLE);
            itemCity.setText(R.string.fragments_MainHeaderFragment_empty_address);
        }
    }

    private void setActionButton(final int position, Address currentAddress, ImageView itemRemove,
                                 TextView label, RelativeLayout actionContent) {
        actionContent.setVisibility(View.VISIBLE);

        boolean isFakeSecondPoint =
                (position == AddressList.SECOND_POINT) && currentAddress.isEmpty();
        if (position == 0 || isFakeSecondPoint) {
            itemRemove.setVisibility(View.GONE);
        } else {
            itemRemove.setVisibility(View.VISIBLE);
        }

        String gpsAddress =
                context.getString(R.string.fragments_MainHeaderFragment_gps_address);
        if (position == 0 && currentAddress.isGps() && currentAddress.getStreet().equals(gpsAddress)) {
            label.setText(context.getString(R.string.fragments_MainHeaderFragment_refine_address));
        } else if (position == 0 && currentAddress.getPorch().isEmpty()) {
            label.setText(context.getString(R.string.activities_DetailAddressActivity_hint_porch));
        } else if (position == 0 && !currentAddress.getPorch().isEmpty()) {
            String porch = "№" + currentAddress.getPorch();
            label.setText(porch);
        }

        if (position == 0 && isDefinedCity && !addressList.get(position).getUseType().equals(Address.TYPE_FAKE) && WITH_PORCH)
            label.setVisibility(View.VISIBLE);
        else
            label.setVisibility(View.GONE);

        if (itemRemove.getVisibility() == View.VISIBLE) {
            actionContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new ItemAddressActionEvent(ItemAddressActionEvent.ACTION_DELETE, position));
                }
            });
        } else if (label.getVisibility() == View.VISIBLE) {
            actionContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new ItemAddressActionEvent(ItemAddressActionEvent.ACTION_INFO, position));
                }
            });
        } else {
            actionContent.setVisibility(View.GONE);
        }
    }


    public void updateList(List<Address> list, boolean type) {
        this.addressList = list;
        this.isDefinedCity = type;
        notifyDataSetChanged();
    }

}