package com.kabbi.client.views;

import android.util.Log;

import com.kabbi.client.events.ui.OrderTimeEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.views.pickers.TimeSelectedListener;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gootax on 04.05.17.
 * Listener for TimeDateDialogFragment
 */

public class TimeDateListener implements TimeSelectedListener {

    @Override
    public void onTimeChangedToAny(long newTime) {
        Long time = newTime - Profile.getProfile().getInaccuracy();
        Log.d("Logos", "onTimeChangedToAny: " +  time);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);
        String dateTime = dateFormat.format(new Date(time));
        EventBus.getDefault().post(new OrderTimeEvent(dateTime));
    }

    @Override
    public void onTimeChangedToNow() {
        EventBus.getDefault().post(new OrderTimeEvent(""));
    }

}
