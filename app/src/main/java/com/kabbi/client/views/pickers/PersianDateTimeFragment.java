package com.kabbi.client.views.pickers;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;


import com.kabbi.client.R;
import com.kabbi.client.utils.persian.PersianCalendar;
import com.kabbi.client.utils.persian.PersianCalendarConstants;
import com.kabbi.client.utils.persian.PersianCalendarUtils;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by gootax on 03.05.17.
 */

public class PersianDateTimeFragment extends Fragment{

    public static final int DAY_MIN_VALUE = 1;
    public static final int DAY_MAX_VALUE = 31;
    public static final int DAY_NOT_LEAP_VALUE = 29;
    public static final int DAY_LEAP_VALUE = 30;

    private static final int HOUR_MIN_VALUE = 0;
    private static final int HOUR_MAX_VALUE = 23;

    private static final int MINUTES_MIN_VALUE = 0;
    private static final int MINUTES_MAX_VALUE = 59;

    private LinearLayout mYearContainer;

    private NumberPicker mYearPicker;
    private NumberPicker mMonthPicker;
    private NumberPicker mDayPicker;
    private NumberPicker mHourPicker;
    private NumberPicker mMinutesPicker;

    private PersianCalendar initCalendar;
    private PersianCalendar maxCalendar;
    private PersianCalendar nowCalendar;

    private Date mInitDate;
    private Date mMaxDate;


    public static PersianDateTimeFragment getInstance(Date dateInit,
                                                      Date dateMax) {
        PersianDateTimeFragment mPersianDateTimeFragment = new PersianDateTimeFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(TimeDateDialogFragment.EXTRA_INIT_DATE, dateInit);
        bundle.putSerializable(TimeDateDialogFragment.EXTRA_MAX_DATE, dateMax);

        mPersianDateTimeFragment.setArguments(bundle);
        return mPersianDateTimeFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onParseExtraData(getArguments());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.time_fragment, container, false);

        mYearContainer = (LinearLayout) mView.findViewById(R.id.ll_year);

        mYearPicker = (NumberPicker) mView.findViewById(R.id.np_year);
        mMonthPicker = (NumberPicker) mView.findViewById(R.id.np_month);
        mDayPicker = (NumberPicker) mView.findViewById(R.id.np_day);
        mHourPicker = (NumberPicker) mView.findViewById(R.id.np_hour);
        mMinutesPicker = (NumberPicker) mView.findViewById(R.id.np_min);

        initializeCalendar();
        initializePickers();

        return mView;
    }


    private void initializeCalendar() {
        initCalendar = new PersianCalendar(mInitDate.getTime());
        maxCalendar = new PersianCalendar(mMaxDate.getTime());
        nowCalendar = new PersianCalendar(new Date().getTime());
    }


    private void initializePickers() {
        // Year
        initPicker(mYearPicker, initCalendar.getPersianYear(), maxCalendar.getPersianYear());

        // Month
        int minMouth = initCalendar.getPersianMonth() + 1;
        if (minMouth > nowCalendar.getPersianMonth() + 1) {
            minMouth = nowCalendar.getPersianMonth() + 1;
        }
        int maxMouth = maxCalendar.getPersianMonth() + 1;
        initPicker(mMonthPicker, minMouth, maxMouth,
                PersianCalendarConstants.getMouthsByRange(minMouth, maxMouth));
        mMonthPicker.setOnValueChangedListener(monthChangedListener);
        mMonthPicker.setWrapSelectorWheel(false);

        // Day
        updateDaysCountByMonth(initCalendar.getPersianMonth() + 1);
        mDayPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        setDividerColor(mDayPicker, Color.BLACK);

        // Hour
        initPicker(mHourPicker, HOUR_MIN_VALUE, HOUR_MAX_VALUE);

        // Minutes
        initPicker(mMinutesPicker, MINUTES_MIN_VALUE, MINUTES_MAX_VALUE);

        // If in year single choice, hide it
        if (initCalendar.getPersianYear() == maxCalendar.getPersianYear()) mYearContainer.setVisibility(View.GONE);

        setCurrentTimeToPickers();
    }


    private void onParseExtraData(Bundle bundle) {
        mInitDate = (Date) bundle.getSerializable(TimeDateDialogFragment.EXTRA_INIT_DATE);
        mMaxDate = (Date) bundle.getSerializable(TimeDateDialogFragment.EXTRA_MAX_DATE);
    }


    private void initPicker(NumberPicker picker, int minValue, int maxValue) {
        picker.setMinValue(minValue);
        picker.setMaxValue(maxValue);
        setNumberPickerTextColor(picker, Color.BLACK);
        setDividerColor(picker, Color.BLACK);
        picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }


    private void initPicker(NumberPicker picker, int minValue, int maxValue, String[] values) {
        picker.setMinValue(minValue);
        picker.setMaxValue(maxValue);
        picker.setDisplayedValues(values);
        setNumberPickerTextColor(picker, Color.BLACK);
        setDividerColor(picker, Color.BLACK);
        picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }


    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields)
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException | IllegalAccessException | IllegalArgumentException e){
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    private void setCurrentTimeToPickers() {
        mYearPicker.setValue(initCalendar.getPersianYear());
        mMonthPicker.setValue(initCalendar.getPersianMonth() + 1); // 0 - 12 need +1
        mDayPicker.setValue(initCalendar.getPersianDay());
        mHourPicker.setValue(mInitDate.getHours()); // TODO CHANGE
        mMinutesPicker.setValue(initCalendar.get(Calendar.MINUTE));
    }


    private void updateDaysCountByMonth(int month) {
        if (maxCalendar.getPersianMonth() + 1 == month) {
            mDayPicker.setMinValue(DAY_MIN_VALUE);
            mDayPicker.setMaxValue(nowCalendar.getPersianDay());
        } else {
            if (month < 7) {
                mDayPicker.setMinValue(nowCalendar.getPersianDay());
                mDayPicker.setMaxValue(DAY_MAX_VALUE);
            } else if (month > 6 && month < 12) {
                mDayPicker.setMinValue(nowCalendar.getPersianDay());
                mDayPicker.setMaxValue(DAY_LEAP_VALUE);
            } else {
                mDayPicker.setMinValue(nowCalendar.getPersianDay());
                if (PersianCalendarUtils.isPersianLeapYear(mYearPicker.getValue()))
                    mDayPicker.setMaxValue(DAY_LEAP_VALUE);
                else mDayPicker.setMaxValue(DAY_NOT_LEAP_VALUE);
            }
        }
    }


    private NumberPicker.OnValueChangeListener monthChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            updateDaysCountByMonth(newVal);
        }
    };

    public long getCurrentTime() {
        initCalendar.set(Calendar.MINUTE, mMinutesPicker.getValue());
        initCalendar.set(Calendar.HOUR_OF_DAY, mHourPicker.getValue());

        return PersianCalendarUtils.convertPersianToJulian(initCalendar,
                mYearPicker.getValue(),
                mMonthPicker.getValue() - 1,
                mDayPicker.getValue());
    }
}
