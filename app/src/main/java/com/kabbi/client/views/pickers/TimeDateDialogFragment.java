package com.kabbi.client.views.pickers;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;

import com.kabbi.client.R;

import java.util.Date;

/**
 * Created by gootax on 03.05.17.
 */

public class TimeDateDialogFragment extends DialogFragment {

    public static final String EXTRA_INIT_DATE = "dateInit";
    public static final String EXTRA_MAX_DATE = "dateMax";

    private static TimeSelectedListener sTimeSelectedListener;

    private Date mInitDate;
    private Date mMaxDate;


    public static TimeDateDialogFragment getInstance(Date dateInit,
                                                     Date dateMax,
                                                     TimeSelectedListener listener) {
        TimeDateDialogFragment mDialogFragment = new TimeDateDialogFragment();

        Bundle data = new Bundle();
        data.putSerializable(EXTRA_INIT_DATE, dateInit);
        data.putSerializable(EXTRA_MAX_DATE, dateMax);
        sTimeSelectedListener = listener;

        mDialogFragment.setArguments(data);
        return mDialogFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onParseExtraData(getArguments());
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog.getWindow() != null) dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_picker, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final PersianDateTimeFragment persianFragment = PersianDateTimeFragment.getInstance(mInitDate, mMaxDate);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_date_container, persianFragment)
                .commit();

        Button anyTime = (Button) view.findViewById(R.id.btn_choice_time);
        Button nowTime = (Button) view.findViewById(R.id.btn_now_time);


        anyTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sTimeSelectedListener.onTimeChangedToAny(persianFragment.getCurrentTime());
                dismiss();
            }
        });

        nowTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sTimeSelectedListener.onTimeChangedToNow();
                dismiss();
            }
        });
    }


    private void onParseExtraData(Bundle bundle) {
        mInitDate = (Date) bundle.getSerializable(EXTRA_INIT_DATE);
        mMaxDate = (Date) bundle.getSerializable(EXTRA_MAX_DATE);
    }


    public static class Builder {

        private TimeSelectedListener mListener;

        private Date mInitDate;
        private Date mMaxDate;


        public Builder setListener(TimeSelectedListener listener) {
            mListener = listener;
            return this;
        }

        public Builder setInitialDate(Date initTime) {
            mInitDate = initTime;
            return this;
        }

        public Builder setMaxDate(Date maxTime) {
            mMaxDate = maxTime;
            return this;
        }

        public TimeDateDialogFragment build() {
            return TimeDateDialogFragment.getInstance(
                    mInitDate, mMaxDate, mListener
            );
        }

    }


}
