package com.kabbi.client.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.models.Option;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gootax on 26.07.17.
 */

public class WishesAdapter extends RecyclerView.Adapter<WishesAdapter.WishVIewHolder> {


    private List<Option> mOptions;
    private String mCurrency;

    public WishesAdapter(List<Option> options, String currency) {
        mOptions = options;
        mCurrency = currency;
    }

    @Override
    public WishVIewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wishes, parent, false);
        return new WishVIewHolder(view);
    }

    @Override
    public void onBindViewHolder(WishVIewHolder holder, int position) {
        holder.cbWish.setChecked(mOptions.get(position).isChecked());
        holder.tvWithName.setText(mOptions.get(position).getOptionName());
        holder.tvWithCost.setText(mOptions.get(position).getPrice() + " " + mCurrency);
    }

    @Override
    public int getItemCount() {
        return mOptions.size();
    }

    class WishVIewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CheckBox cbWish;
        private TextView tvWithName;
        private TextView tvWithCost;

        WishVIewHolder(View itemView) {
            super(itemView);
            cbWish = (CheckBox) itemView.findViewById(R.id.cb_wishes);
            tvWithName = (TextView) itemView.findViewById(R.id.tv_wishes);
            tvWithCost = (TextView) itemView.findViewById(R.id.tv_wishes_cost);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Option option = mOptions.get(getAdapterPosition());

            if (option.isChecked()) cbWish.setChecked(false);
            else cbWish.setChecked(true);

            option.setChecked(cbWish.isChecked());
            option.save(); // TODO Check
        }
    }
}
