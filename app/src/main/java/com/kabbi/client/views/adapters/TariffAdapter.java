package com.kabbi.client.views.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.models.Tariff;

import java.util.List;


public class TariffAdapter extends RecyclerView.Adapter<TariffAdapter.TariffViewHolder> {

    private List<Tariff> mTariffs;
    private TariffClickListener mTariffListener;
    private Tariff mCurrentTariff;

    public TariffAdapter(List<Tariff> mTariffs, TariffClickListener mTariffListener, Tariff currentTariff) {
        this.mTariffs = mTariffs;
        this.mTariffListener = mTariffListener;
        this.mCurrentTariff = currentTariff;
    }


    @Override
    public TariffViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_footertariff, parent, false);
        TariffViewHolder viewHolder = new TariffViewHolder(view);
        viewHolder.bindAction(mTariffListener);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(TariffViewHolder holder, int position) {
        int listPosition = holder.getAdapterPosition();
        holder.bindData(mTariffs.get(listPosition));
    }


    @Override
    public int getItemCount() {
        return mTariffs.size();
    }


    class TariffViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLlTariffContent;
        private SimpleDraweeView mTariffImage;
        private TextView mTariffName;
        private TypedValue mTypedArray;
        private Tariff mTariff;


        TariffViewHolder(View itemView) {
            super(itemView);
            mLlTariffContent = (LinearLayout) itemView.findViewById(R.id.ll_tariff_cont);
            mTariffImage = (SimpleDraweeView) itemView.findViewById(R.id.iv_tariff_image);
            mTariffName = (TextView) itemView.findViewById(R.id.tv_tariff_name);

            mTypedArray = new TypedValue();
            itemView.getContext()
                    .getTheme()
                    .resolveAttribute(R.attr.accent_bg_tariff, mTypedArray, true);
        }


        private void bindData(Tariff tariff) {
            mTariff = tariff;
            Uri imgUri = Uri.parse(mTariff.getTariffIconMini());
            mTariffImage.setImageURI(imgUri);
            mTariffName.setText(mTariff.getTariffName());

            if (tariff.getTariffId().equals(mCurrentTariff.getTariffId())) {
                mLlTariffContent.setBackgroundColor(mTypedArray.data);
            } else {
                mLlTariffContent.setBackgroundColor(Color.TRANSPARENT);
            }
        }


        private void bindAction(final TariffClickListener tariffClickListener) {
            mLlTariffContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tariffClickListener != null)
                        tariffClickListener.onTariffClicked(mTariff);
                    mCurrentTariff = mTariff;
                    notifyDataSetChanged();
                }
            });
        }

    }

    public interface TariffClickListener {
        void onTariffClicked(Tariff tariff);
    }

}
