package com.kabbi.client.views.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.activities.OrderDetailActivity;
import com.kabbi.client.activities.PreOrderActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.utils.TimeHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.kabbi.client.utils.persian.PersianCalendarUtils.getPersianFormat;

public class LastOrdersRecyclerAdapter extends
        RecyclerView.Adapter<LastOrdersRecyclerAdapter.LastOrdersViewHolder>
        implements View.OnClickListener {

    private List<Order> orderList;
    private Context context;
    private String currency;
    private String type;
    private int size;

    public LastOrdersRecyclerAdapter(Context context, List<Order> orderList, String type, int size) {
        this.orderList = orderList;
        this.context = context;
        this.type = type;
        this.size = size;
        City city = City.getCity(Profile.getProfile().getCityId());
        if (city == null ||city.getCurrency().equals("RUB"))
            currency = this.context.getString(R.string.currency);
        else
            currency = city.getCurrency();

    }

    @Override
    public LastOrdersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        if (type.equals("pre"))
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_preorders, null);
        else
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lastorders, null);
        return new LastOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LastOrdersViewHolder customViewHolder, int position) {
        SimpleDraweeView draweeView = customViewHolder.draweeView;
        List<Address> addressList;
        Order order = orderList.get(position);
        addressList = RoutePoint.getRoute(order);
        customViewHolder.tvCost.setVisibility(View.GONE);

        if (type.equals("pre")) {
            customViewHolder.tvCost.setText(context.getString(R.string.activities_LastOrdersActivity_pre));
            customViewHolder.tvStatus.setText(formatTime(new Date().getTime(), orderList.get(position).getOrderTime()));
            customViewHolder.tvCost.setVisibility(View.VISIBLE);
            if (customViewHolder.tvPreOrderStatus != null) {
                customViewHolder.tvPreOrderStatus.setText(getOrderLabel(orderList.get(position).getStatus()));
                if (orderList.get(position).getStatus().equals("rejected")) {
                    customViewHolder.tvPreOrderStatus.setTextColor(context.getResources().getColor(R.color.textColorRed));
                } else {
                    customViewHolder.tvPreOrderStatus.setTextColor(context.getResources().getColor(R.color.textColorGreen));
                }
            }
            String orderDate = "";
            Profile profile = Profile.getProfile();
            switch (profile.getCalendar()) {
                case Profile.GREGORIAN_CALENDAR:
                    String time = orderList.get(position).getOrderTime();
                    orderDate = formatDate(TimeHelper.getFakeTimeFromString(time).getTime());
                    break;
                case Profile.PERSIAN_CALENDAR:
                    orderDate = getPersianFormat(order.getOrderTime());
                    break;
            }
            customViewHolder.tvDate.setText(orderDate);
        } else {
            if (orderList.get(position).getCost() != null && orderList.get(position).getCost().length() > 0) {
                String cost = "0";
                try {
                    JSONObject jsonCost = new JSONObject(orderList.get(position).getCost());
                    cost = jsonCost.getString("summary_cost");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                customViewHolder.tvCost.setVisibility(View.VISIBLE);
                customViewHolder.tvCost.setText(cost);
                customViewHolder.tvCurrency.setText(currency);
            }

            switch (orderList.get(position).getStatus()) {
                case "completed":
                    customViewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textColorGreen));
                    customViewHolder.tvStatus.setText(R.string.completed);
                    break;
                case "rejected":
                    customViewHolder.tvStatus.setText(R.string.rejected);
                    customViewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textColorRed));
                    break;
                default:
                    customViewHolder.tvStatus.setText(orderList.get(position).getStatusLabel());
                    customViewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textColorRed));
                    break;
            }
            customViewHolder.tvDate
                    .setText(formatDate(orderList.get(position).getCreateTime()));
        }


        if (orderList.get(position).getCar().length() > 0)
            customViewHolder.tvCar.setText(orderList.get(position).getCar());
        else
            customViewHolder.tvCar.setText(context.getString(R.string.activities_OrderDetailActivity_empty_driver));

        Uri uriFace = Uri.parse(orderList.get(position).getPhoto());
        customViewHolder.draweeViewFace.setImageURI(uriFace);

        String[] labels = {"A", "B", "C", "D", "E"};

        StringBuilder builder = new StringBuilder();
        for (int a = 0; a < addressList.size(); a++) {
            builder.append(String.format("&markers=color:black|label:%s|" + addressList.get(a).getLat()
                    + "," + addressList.get(a).getLon(), labels[a]));
        }

        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?" + builder +
                "&size=" + size + "x" + size/2 + "&language=" + Locale.getDefault().getLanguage()
                + "&maptype=roadmap" + builder.toString()
                + "&path=" + "color:0x4f4a9f%7Cweight:5%7Cenc:" + orderList.get(position).getRoute());

        ViewGroup.LayoutParams params = draweeView.getLayoutParams();
        params.height = size/2;
        draweeView.setLayoutParams(params);

        draweeView.setImageURI(uri);

        customViewHolder.customView.setTag(R.id.tag_order_id, orderList.get(position).getOrderId());
        customViewHolder.customView.setTag(R.id.tag_order_status, orderList.get(position).getStatus());
        customViewHolder.customView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    @Override
    public void onClick(View view) {
        if (type.equals("pre"))
            context.startActivity(new Intent(context, PreOrderActivity.class).putExtra("order_id", view.getTag(R.id.tag_order_id).toString()));
        else
            context.startActivity(new Intent(context, OrderDetailActivity.class).putExtra("order_id", view.getTag(R.id.tag_order_id).toString()).putExtra("type", "last").putExtra("status", view.getTag(R.id.tag_order_status).toString()));
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
        notifyDataSetChanged();
    }

    String getOrderLabel(String status) {
        switch (status) {
            case "pre_order":
                return context.getString(R.string.pre_order);
            case "new":
                return context.getString(R.string.text_new);
            case "rejected":
                return context.getString(R.string.rejected);
            case "completed":
                return context.getString(R.string.completed);
            default:
                return context.getString(R.string.text_run);
        }
    }

    class LastOrdersViewHolder extends RecyclerView.ViewHolder {

        private View customView;
        private TextView tvCost;
        private TextView tvCurrency;
        private TextView tvStatus;
        private TextView tvDate;
        private TextView tvCar;
        private TextView tvPreOrderStatus;
        private SimpleDraweeView draweeView;
        private SimpleDraweeView draweeViewFace;

        LastOrdersViewHolder(View view) {
            super(view);
            this.tvCost = (TextView) view.findViewById(R.id.tv_lastorders_cost);
            this.tvCurrency = (TextView) view.findViewById(R.id.tv_lastorders_currency);
            this.tvStatus = (TextView) view.findViewById(R.id.tv_lastorders_status);
            this.tvDate = (TextView) view.findViewById(R.id.tv_lastorders_date);
            this.tvCar = (TextView) view.findViewById(R.id.tv_lastorders_car);
            this.tvPreOrderStatus = (TextView) view.findViewById(R.id.tv_preorder_status);
            this.draweeView = (SimpleDraweeView) view.findViewById(R.id.sdv_map);
            this.draweeViewFace = (SimpleDraweeView) view.findViewById(R.id.sdv_face);
            if (!AppParams.USE_PHOTO) draweeViewFace.setVisibility(View.INVISIBLE);
            this.customView = view;
        }

    }

    @SuppressLint("SimpleDateFormat")
    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(long longDateNow, String strDatePlant) {
        Date datePlant = TimeHelper.getFakeTimeFromString(strDatePlant);

        String str = "%d " + context.getString(R.string.activities_PreOrderActivity_date_day_short) +
                ". %d " + context.getString(R.string.activities_PreOrderActivity_date_hour_short) +
                ". %d " + context.getString(R.string.fragments_FragmentAbstractMap_time_minute_short) + ".";


        if (datePlant != null) {
            long mSec = datePlant.getTime() - longDateNow;
            if (mSec > 0) {
                long days = TimeUnit.MILLISECONDS.toDays(mSec);
                long hours = TimeUnit.MILLISECONDS.toHours(mSec) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mSec));
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mSec) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mSec));
                return String.format(str, days, hours, minutes);
            } else {
                return String.format(str, 0, 0, 0);
            }
        } else {
            return String.format(str, 0, 0, 0);
        }
    }
}