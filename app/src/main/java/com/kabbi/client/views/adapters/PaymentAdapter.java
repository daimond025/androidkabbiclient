package com.kabbi.client.views.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.kabbi.client.R;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.Constants;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.DeleteCardListener;
import com.kabbi.client.network.requests.PostDeleteClientCard;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gootax on 06.09.16.
 */
public class PaymentAdapter extends BaseSwipeAdapter {

    public interface PaymentDataDidChanged {
        void onChangeData(String currentPaymentType, String currentCompany, String currentPan);
    }

    public static final String TAG = PaymentAdapter.class.getSimpleName();


    private static final int TYPE_PAYMENT_ADAPTER_PROFILE = 0;
    private static final int TYPE_PAYMENT_ADAPTER_ORDER = 1;

    private Profile profile;
    private Order order;
    private List<PaymentType> types;

    private String currentPaymentType;
    private String currentCompany;
    private String currentPan;

    private SpiceManager spiceManager;
    private Context context;
    private int adapterType = 0;

    private PaymentDataDidChanged mPaymentDataListener;


    public PaymentAdapter(List<PaymentType> types, Profile profile, SpiceManager spiceManager, Context context) {
        this.types = clearDisablePayments(types);
        this.profile = profile;
        this.spiceManager = spiceManager;
        this.context = context;
        currentPaymentType = profile.getPaymentType();
        currentCompany = profile.getCompany();
        currentPan = profile.getPan();
        adapterType = TYPE_PAYMENT_ADAPTER_PROFILE;
    }

    public PaymentAdapter(List<PaymentType> types, Order order, SpiceManager spiceManager, Context context) {
        this.types = clearDisablePayments(types);
        this.order = order;
        this.spiceManager = spiceManager;
        this.context = context;
        currentPaymentType = order.getPaymentType();
        currentCompany = order.getCompanyId();
        currentPan = order.getPan();
        adapterType = TYPE_PAYMENT_ADAPTER_ORDER;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.sl_payment_types;
    }


    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_payment, parent, false);
    }


    @Override
    public void fillValues(int position, View itemView) {
        PaymentViewHolder holder = new PaymentViewHolder(itemView, position);
        holder.bindData(itemView, position);
    }


    public void updateData(List<PaymentType> types) {
        this.types = clearDisablePayments(types);
        notifyDataSetChanged();
    }

    private List<PaymentType> clearDisablePayments(List<PaymentType> types) {
        List<PaymentType> payments = new ArrayList<>();
        for (PaymentType paymentType: types) {
            switch (paymentType.getPaymentType()) {
                case Constants.PAYMENT_CASH:
                    if (AppParams.PAYMENT_WITH_CASH) payments.add(paymentType);
                    break;
                case Constants.PAYMENT_CARD:
                    if (AppParams.PAYMENT_WITH_CARDS) payments.add(paymentType);
                    break;
                case Constants.PAYMENT_CORPORATION:
                    if (AppParams.PAYMENT_WITH_COMPANY) payments.add(paymentType);
                    break;
                case Constants.PAYMENT_PERSONAL:
                    if (AppParams.PAYMENT_WITH_PERSONAL) payments.add(paymentType);
                    break;
            }
        }
        return payments;
    }

    public void updateCurrentPayment(String paymentType) {
        currentPaymentType = paymentType;
    }


    private void deleteCard(int currentPosition) {
        final int position = currentPosition;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.MyCustomAlertDialogTheme)
                .setTitle(R.string.activities_PaymentActivity_tittle_remove_card)
                .setMessage(R.string.activities_PaymentActivity_message_remove_card)
                .setPositiveButton(context.getString(R.string.activities_PaymentActivity_dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                spiceManager.execute(new PostDeleteClientCard(profile.getAppId(),
                                                profile.getApiKey(), profile.getClientId(), profile.getPhone(),
                                                profile.getTenantId(), types.get(position).getPaymentId(), AppParams.TYPE_CLIENT),
                                        new DeleteCardListener(types.get(position).getPaymentId()));
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(context.getString(R.string.activities_PaymentActivity_dialog_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog mDeleteCardDialog = alertDialog.create();
        mDeleteCardDialog.show();
    }

    public void setPaymentDataListener(PaymentDataDidChanged paymentDataListener) {
        this.mPaymentDataListener = paymentDataListener;
    }


    public String getCurrentPaymentType() {
        return currentPaymentType;
    }


    public String getCurrentCompany() {
        return currentCompany;
    }


    public String getCurrentPan() {
        return currentPan;
    }


    @Override
    public int getCount() {
        return types.size();
    }


    @Override
    public Object getItem(int i) {
        return types.get(i);
    }


    @Override
    public long getItemId(int i) {
        return i;
    }


    private class PaymentViewHolder {

        private ImageView ivPaymentType;
        private TextView tvPaymentTitle;
        private TextView tvPaymentDescription;
        private TextView tvPaymentBalance;
        private ImageView tvPaymentChoice;
        private SwipeLayout slPaymentTypes;
        private RelativeLayout rlPayment;
        private Button ivDeleteCard;
        private ImageView bSwipeCard;
        private int mCurrentPosition;

        PaymentViewHolder(View itemView, int position) {
            mCurrentPosition = position;
            ivPaymentType = (ImageView) itemView.findViewById(R.id.iv_payment_type);
            tvPaymentTitle = (TextView) itemView.findViewById(R.id.tv_payment_title);
            tvPaymentDescription = (TextView) itemView.findViewById(R.id.tv_payment_description);
            tvPaymentBalance = (TextView) itemView.findViewById(R.id.tv_payment_balance);
            tvPaymentChoice = (ImageView) itemView.findViewById(R.id.iv_payment_choice);
            rlPayment = (RelativeLayout) itemView.findViewById(R.id.rl_payment);
            slPaymentTypes = (SwipeLayout) itemView.findViewById(R.id.sl_payment_types);
            ivDeleteCard = (Button) itemView.findViewById(R.id.btn_delete_card);
            bSwipeCard = (ImageView) itemView.findViewById(R.id.iv_payment_swipe_card);
            bSwipeCard = (ImageView) itemView.findViewById(R.id.iv_payment_swipe_card);

            rlPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (types.get(mCurrentPosition).getPaymentType()) {
                        case "CORP_BALANCE":
                            currentCompany = types.get(mCurrentPosition).getPaymentId();
                            break;
                        case "CARD":
                            currentPan = types.get(mCurrentPosition).getPaymentId();
                            break;
                    }
                    currentPaymentType = types.get(mCurrentPosition).getPaymentType();
                    notifyDataSetChanged();

                    if (mPaymentDataListener != null) {
                        mPaymentDataListener.onChangeData(currentPaymentType, currentCompany, currentPan);
                    }
                }
            });
            slPaymentTypes.setShowMode(SwipeLayout.ShowMode.LayDown);

            ivDeleteCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (types.get(mCurrentPosition).getPaymentType().equals("CARD")) {
                        deleteCard(mCurrentPosition);
                    }
                    else slPaymentTypes.close();
                }
            });
        }
        
        
        private void bindData(View itemView, int position) {
            slPaymentTypes = (SwipeLayout) itemView.findViewById(R.id.sl_payment_types);
            tvPaymentChoice.setVisibility(View.INVISIBLE);
            bSwipeCard.setVisibility(View.GONE);
            tvPaymentBalance.setVisibility(View.GONE);
            tvPaymentDescription.setVisibility(View.GONE);
            slPaymentTypes.setClickToClose(true);
            slPaymentTypes.setSwipeEnabled(false);

            slPaymentTypes.close();

            switch (types.get(position).getPaymentType()) {

                case "CARD":
                    if (!AppParams.PAYMENT_WITH_CARDS) slPaymentTypes.setVisibility(View.GONE);
                    ivPaymentType.setImageResource(R.drawable.card);
                    tvPaymentTitle.setText(types.get(position).getPaymentId());

                    if (adapterType == TYPE_PAYMENT_ADAPTER_PROFILE) {
                        slPaymentTypes.setSwipeEnabled(true);
                    } else {
                        slPaymentTypes.setSwipeEnabled(false);
                    }

                    bSwipeCard.setVisibility(View.VISIBLE);
                    break;
                case "PERSONAL_ACCOUNT":
                    if (!AppParams.PAYMENT_WITH_PERSONAL) slPaymentTypes.setVisibility(View.GONE);
                    ivPaymentType.setImageResource(R.drawable.parsonal);
                    tvPaymentTitle.setText(R.string.activities_PaymentActivity_type_account);
                    if (types.get(position).getBalance() != null) {
                        tvPaymentBalance.setText(types.get(position).getBalance());
                        tvPaymentBalance.setVisibility(View.VISIBLE);
                    }
                    break;
                case "CORP_BALANCE":
                    if (!AppParams.PAYMENT_WITH_COMPANY) slPaymentTypes.setVisibility(View.GONE);
                    ivPaymentType.setImageResource(R.drawable.man);
                    tvPaymentTitle.setText(R.string.activities_PaymentActivity_type_company);
                    if (types.get(position).getBalance() != null && AppParams.SHOW_COMPANY_BALANCE) {
                        tvPaymentBalance.setText(types.get(position).getBalance());
                        tvPaymentBalance.setVisibility(View.VISIBLE);
                    }
                    if (types.get(position).getPaymentDescription() != null) {
                        tvPaymentDescription.setText(types.get(position).getPaymentDescription());
                        tvPaymentDescription.setVisibility(View.VISIBLE);
                    }

                    break;
                case "CASH":
                    if (!AppParams.PAYMENT_WITH_CASH) slPaymentTypes.setVisibility(View.GONE);
                    ivPaymentType.setImageResource(R.drawable.wallet);
                    tvPaymentTitle.setText(R.string.activities_PaymentActivity_type_cash);
                    break;
            }

            if (currentPaymentType.equals(types.get(position).getPaymentType())) {
                switch (types.get(position).getPaymentType()) {
                    case "CORP_BALANCE":
                        if (currentCompany.equals(types.get(position).getPaymentId())) {
                            tvPaymentChoice.setVisibility(View.VISIBLE);
                        }
                        break;
                    case "CARD":
                        if (currentPan.equals(types.get(position).getPaymentId())) {
                            tvPaymentTitle.setText(types.get(position).getPaymentId());
                            tvPaymentChoice.setVisibility(View.VISIBLE);
                        }
                        break;
                    default:
                        tvPaymentChoice.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
        

    }
}
