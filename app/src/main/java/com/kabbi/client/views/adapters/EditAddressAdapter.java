package com.kabbi.client.views.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.models.Address;
import com.kabbi.client.utils.AddressList;

import java.util.ArrayList;
import java.util.List;

public class EditAddressAdapter extends BaseAdapter {


    public interface ActionDelegate {
        void onDeleteClicked(int position);
    }

    private Context context;
    private LayoutInflater inflater;
    private List<Address> addressList;
    private List<Integer> icons;
    private ActionDelegate mActionDelegate;
    
    public EditAddressAdapter(Context context, List<Address> list) {
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addressList = list;
        initIcons();
    }

    private void initIcons() {
        icons = new ArrayList<>();
        icons.add(R.drawable.a);
        icons.add(R.drawable.b);
        icons.add(R.drawable.c);
        icons.add(R.drawable.d);
        icons.add(R.drawable.e);
    }

    private static class AddressViewHolder {
        ImageView itemIcon;
        TextView itemStreet;
        TextView itemCity;
        ImageView itemRemove;
    }

    @Override
    public int getCount() {
        return ((AddressList) addressList).realSize();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_edit_header, parent, false);
            AddressViewHolder viewHolder = new AddressViewHolder();
            viewHolder.itemIcon = (ImageView) rowView.findViewById(R.id.iv_item_icon);
            viewHolder.itemStreet = (TextView) rowView.findViewById(R.id.tv_item_street);
            viewHolder.itemCity = (TextView) rowView.findViewById(R.id.tv_item_city);
            viewHolder.itemRemove = (ImageButton) rowView.findViewById(R.id.btn_item_remove);
            rowView.setTag(viewHolder);
        }
        AddressViewHolder holder = (AddressViewHolder) rowView.getTag();
        customizeViews(position, holder);
        return rowView;
    }

    private void customizeViews(final int position, AddressViewHolder holder) {
        holder.itemIcon.setImageResource(icons.get(position));
        Address currentAddress = addressList.get(position);
        setStreetText(currentAddress, holder.itemStreet);
        setStreetColor(currentAddress, holder.itemStreet, holder.itemCity);
        setCity(currentAddress, holder.itemCity, position);
        setActionButton(position, currentAddress, holder.itemRemove);
    }

    private void setStreetText(Address currentAddress, TextView itemStreet) {
        if (!currentAddress.isEmpty()) {
            itemStreet.setVisibility(View.VISIBLE);
            itemStreet.setText(currentAddress.getLabel());
        } else {
            itemStreet.setVisibility(View.GONE);
        }
    }

    private void setStreetColor(Address currentAddress, TextView itemStreet, TextView itemCity) {
        itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorBlack));
        itemCity.setTextColor(ContextCompat.getColor(context, R.color.textColorGrey));
    }

    private void setCity(Address currentAddress, TextView itemCity, int position) {
        boolean isFakeSecondPoint =
                (position == AddressList.SECOND_POINT) && currentAddress.isEmpty();
        String city = currentAddress.getCity();

        if (currentAddress.isEmpty() || city == null || city.isEmpty()) {
            itemCity.setVisibility(View.GONE);
        } else {
            itemCity.setText(city);
            itemCity.setVisibility(View.VISIBLE);
        }

        if (isFakeSecondPoint){
            itemCity.setVisibility(View.VISIBLE);
            itemCity.setText(R.string.fragments_MainHeaderFragment_empty_address);
        }
    }

    private void setActionButton(final int position, Address currentAddress, View itemRemove) {

        boolean isFakeSecondPoint =
                (position == AddressList.SECOND_POINT) && currentAddress.isEmpty();
        if (position == 0 || isFakeSecondPoint) {
            itemRemove.setVisibility(View.GONE);
        } else {
            itemRemove.setVisibility(View.VISIBLE);
            itemRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mActionDelegate != null) {
                        mActionDelegate.onDeleteClicked(position);
                    }
                }
            });
        }
    }

    public void setActionDelegate(ActionDelegate actionDelegate) {
        this.mActionDelegate = actionDelegate;
    }

    public void updateList(List<Address> list) {
        this.addressList = list;
        Log.d("Logos", "Logos updateList: " + addressList);
        notifyDataSetChanged();
    }

}