package com.kabbi.client.views;

import android.util.Log;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.kabbi.client.events.ui.OrderTimeEvent;
import com.kabbi.client.models.Profile;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Listener for SlideDateTimePicker
 */
public class DateTimeListener extends SlideDateTimeListener {

    @Override
    public void onDateTimeSet(Date date) {
        Long time = date.getTime() - Profile.getProfile().getInaccuracy();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);
        String dateTime = dateFormat.format(new Date(time));
        EventBus.getDefault().post(new OrderTimeEvent(dateTime));
    }

    @Override
    public void onDateTimeCancel()
    {
        EventBus.getDefault().post(new OrderTimeEvent(""));
    }



}
