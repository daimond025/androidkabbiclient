package com.kabbi.client.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

import com.kabbi.client.R;

public class ToastWrapper {

    private Toast mCustomToast;

    @SuppressLint("ShowToast")
    public ToastWrapper(Context context, @StringRes int text) {
        mCustomToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        mCustomToast.setGravity(Gravity.CENTER, 0, 0);
    }

    @SuppressLint("ShowToast")
    public ToastWrapper(Context context, CharSequence text) {
        mCustomToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        mCustomToast.setGravity(Gravity.CENTER, 0, 0);
    }

    @SuppressLint("ShowToast")
    public ToastWrapper(Context context, int requestCode, boolean isLong) {
        int mToastMessage;
        switch (requestCode) {
            case 1:
                mToastMessage = R.string.error_internal_error;
                break;
            case 2:
                mToastMessage = R.string.error_incorrectly_signature;
                break;
            case 3:
                mToastMessage = R.string.error_unknown_request;
                break;
            case 4:
                mToastMessage = R.string.error_bad_param;
                break;
            case 5:
                mToastMessage = R.string.error_missing_input_parameter;
                break;
            case 6:
                mToastMessage = R.string.error_empty_value;
                break;
            case 7:
                mToastMessage = R.string.error_empty_data_in_database;
                break;
            case 8:
                mToastMessage = R.string.error_empty_tenant_id;
                break;
            case 9:
                mToastMessage = R.string.error_black_list;
                break;
            case 10:
                mToastMessage = R.string.error_need_reauth;
                break;
            case 11:
                mToastMessage = R.string.error_bad_pay_type;
                break;
            case 12:
                mToastMessage = R.string.error_no_money;
                break;
            case 13:
                mToastMessage = R.string.error_bad_bonus;
                break;
            case 14:
                mToastMessage = R.string.error_payment_gate_error;
                break;
            case 15:
                mToastMessage = R.string.error_invalid_pan;
                break;
            case 432:
                mToastMessage = R.string.error_failure_request;
                break;
            default:
                mToastMessage = R.string.error_unknown_error;
        }

        if (isLong)  mCustomToast = Toast.makeText(context, mToastMessage, Toast.LENGTH_LONG);
           else  mCustomToast = Toast.makeText(context, mToastMessage, Toast.LENGTH_SHORT);

        mCustomToast.setGravity(Gravity.CENTER, 0, 0);
    }

    public void show() {
        mCustomToast.show();
    }

}
