package com.kabbi.client.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.kabbi.client.R;
import com.kabbi.client.utils.LocaleHelper;

import java.util.List;
import java.util.Locale;


public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>{

    private List<String> mLanguages;
    private Context mContext;
    private String mCurrentLang;


    public LanguageAdapter(Context context, List<String> languages, String currentLang) {
        mContext = context;
        mLanguages = languages;
        mCurrentLang =  currentLang;
    }


    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language, parent, false);
        return new LanguageViewHolder(view);
    }


    @Override
    public void onBindViewHolder(LanguageViewHolder holder, final int position) {
        String lang = mLanguages.get(position);
        String resourceName = "activities.OptionsActivity.lang_"  + lang;
        String stringRes = "string";

        holder.mLanguageButton.setChecked(false);

        int resId = getResourceId(resourceName, stringRes, mContext.getPackageName());
        if (resId == 0) holder.mLanguageButton.setVisibility(View.GONE);
        else holder.mLanguageButton.setText(resId);

        if (mCurrentLang.equals(lang)) holder.mLanguageButton.setChecked(true);

        holder.mLanguageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentLang = mLanguages.get(position);
                notifyDataSetChanged();
                LocaleHelper.setLocale(mContext, mCurrentLang);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLanguages.size();
    }


    public String getCurrentLang() {
        return mCurrentLang;
    }

    class LanguageViewHolder extends RecyclerView.ViewHolder {

        RadioButton mLanguageButton;

        LanguageViewHolder(View itemView) {
            super(itemView);
            mLanguageButton = (RadioButton) itemView.findViewById(R.id.rb_language);
        }

    }

    private int getResourceId(String pVariableName, String pResourceName, String pPackageName)
    {
        try {
            return mContext.getResources().getIdentifier(pVariableName, pResourceName, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
