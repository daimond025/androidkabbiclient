package com.kabbi.client.views.pager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kabbi.client.R;
import com.kabbi.client.fragments.AddressesFragment;
import com.kabbi.client.models.Address;

import java.util.ArrayList;

/**
 * Created by gootax on 11.09.17.
 */

public class AddressesPager extends FragmentPagerAdapter {

    private static final int ADDRESS_PAGE_COUNT = 2;

    public static final int ADDRESSES_RECENT = 0;
    public static final int ADDRESSES_NEAR = 1;
    public static final int ADDRESSES_TRANS = 2;

    private ArrayList<Address> mRecentAddresses;
    private ArrayList<Address> mNearAddresses;
    private ArrayList<Address> mTransAddresses;
    private Context mContext;

    public ArrayList<AddressesFragment> fragments = new ArrayList<>();

    public AddressesPager(FragmentManager fm,
                          Context context,
                          ArrayList<Address> nearAddresses,
                          ArrayList<Address> recentAddresses,
                          ArrayList<Address> transAddresses) {
        super(fm);

        mContext = context;
        mNearAddresses = nearAddresses;
        mRecentAddresses = recentAddresses;
        mTransAddresses = transAddresses;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case ADDRESSES_NEAR:
                AddressesFragment recentFragment = AddressesFragment.newInstance(position, mRecentAddresses);
                fragments.add(recentFragment);
                return recentFragment;
            case ADDRESSES_RECENT:
                AddressesFragment nearFragment = AddressesFragment.newInstance(position, mNearAddresses);
                fragments.add(nearFragment);
                return nearFragment;
            case ADDRESSES_TRANS:
                AddressesFragment transFragment = AddressesFragment.newInstance(position, mTransAddresses);
                fragments.add(transFragment);
                return transFragment;
            default:
                throw new IllegalArgumentException("ViewPager has size " + ADDRESS_PAGE_COUNT
                        + " but current item has position " + position);

        }
    }


    @Override
    public int getCount() {
        return ADDRESS_PAGE_COUNT;
    }


    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case ADDRESSES_NEAR:
                return mContext.getString(R.string.fragments_MainHeaderFragment_nearly);
            case ADDRESSES_RECENT:
                return mContext.getString(R.string.fragments_MainHeaderFragment_recent);
            case ADDRESSES_TRANS:
                return mContext.getString(R.string.fragments_MainHeaderFragment_airs);
            default:
                throw new IllegalArgumentException("ViewPager has size " + ADDRESS_PAGE_COUNT
                        + " but current item has position " + position);
        }
    }
}
