package com.kabbi.client.views.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kabbi.client.R;

import java.util.ArrayList;

public class AuthFragmentPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private Context context;
    private ArrayList<String> tabTitles = new ArrayList<>();
    private ArrayList<Fragment> mFragments = new ArrayList<>();


    public AuthFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fillTitles();
    }

    public void addFragment(Fragment fragment) {
        mFragments.add(fragment);
        notifyDataSetChanged();
    }

    private void fillTitles() {
        tabTitles.add(context.getString(R.string.activities_AuthActivity_title_signup));
        tabTitles.add(context.getString(R.string.activities_AuthActivity_title_signin));
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }

}
