package com.kabbi.client.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kabbi.client.R;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Profile;

public class CallDialogFragment extends DialogFragment {

    private View view;
    private RadioGroup radioGroup;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_call, null);
        radioGroup = (RadioGroup) view.findViewById(R.id.rg_call);
        final City city = City.getCity(Profile.getProfile().getCityId());

        if (city != null && city.getPhone().length() > 0) {
            RadioButton btnCityPhone = new RadioButton(getActivity());
            btnCityPhone.setText(getString(R.string.fragments_CallDialogFragment_disp_phone));
            btnCityPhone.setTag(city.getPhone());
            radioGroup.addView(btnCityPhone);
        }


        if (((OrderMenuFragment) getParentFragment()).getDriverPhone().length() > 1) {
            RadioButton btnCityPhone = new RadioButton(getActivity());
            btnCityPhone.setText(getString(R.string.fragments_CallDialogFragment_driver_phone));
            btnCityPhone.setTag(((OrderMenuFragment) getParentFragment()).getDriverPhone());
            radioGroup.addView(btnCityPhone);
        }


        try {
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new AlertDialog.Builder(getActivity(), R.style.MyCustomAlertDialogTheme)
                .setTitle(getString(R.string.fragments_CallDialogFragment_title))
                .setPositiveButton(getString(R.string.fragments_CallDialogFragment_call),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                try {
                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    if (((OrderMenuFragment) getParentFragment()).getDriverPhone().equals(city.getPhone()))
                                        callIntent.setData(Uri.parse("tel:" + radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString()));
                                    else
                                        callIntent.setData(Uri.parse("tel:+" + radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString()));
                                    startActivity(callIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.fragments_CallDialogFragment_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }

}
