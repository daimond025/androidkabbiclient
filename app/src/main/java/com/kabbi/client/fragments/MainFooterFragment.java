package com.kabbi.client.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.client.R;
import com.kabbi.client.activities.AuthActivity;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.PaymentActivity;
import com.kabbi.client.activities.TariffActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.app.Constants;
import com.kabbi.client.app.DeepParams;
import com.kabbi.client.events.api.client.CallCostEvent;
import com.kabbi.client.events.api.client.ConstingOrderEvent;
import com.kabbi.client.events.api.client.CreateOrderEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.api.client.SaveTariffsEvent;
import com.kabbi.client.events.ui.ChangeTariff;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Cost;
import com.kabbi.client.models.DeepOrder;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Tariff;
import com.kabbi.client.network.listeners.ClientHistoryAddressListener;
import com.kabbi.client.network.listeners.CompanyRequestListener;
import com.kabbi.client.network.listeners.ConstingOrderListener;
import com.kabbi.client.network.listeners.CostListener;
import com.kabbi.client.network.listeners.CreateOrderListener;
import com.kabbi.client.network.listeners.TariffsRequestListener;
import com.kabbi.client.network.requests.ConstingOrderRequest;
import com.kabbi.client.network.requests.CreateOrderRequest;
import com.kabbi.client.network.requests.GetClientHistoryAddressRequest;
import com.kabbi.client.network.requests.GetCostRequest;
import com.kabbi.client.network.requests.GetTariffsListRequest;
import com.kabbi.client.utils.AppPreferences;
import com.kabbi.client.utils.DetectCity;
import com.kabbi.client.utils.espresso.EspressoIdlingResource;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.kabbi.client.activities.PaymentActivity.SUPPORT_CITY;

public class MainFooterFragment extends BaseSpiceFragment {

    private String cityId = "";
    private String mPromoCode;
    private LinearLayout linearLayout;
    private Button btnCreateOrder;
    private ProgressBar pbCreateOrder;
    private Profile profile;
    private Tariff tariff;

    private LinearLayout paymentLayout;
    private ImageView paymentImg;
    private TextView paymentTitle;
    private TypedValue typedValue;
    private TextInputEditText mEtPromoCode;
    private HorizontalScrollView mHsvTariffs;


    private LinearLayout mLlCallCost;
    private LinearLayout mLlCostInfo;
    private LinearLayout mLlCostInfoFix;
    private TextView mTvCostInfoFix;
    private TextView mTvCostInfo;
    private TextView tvCallCost;
    private TextView tvCallCostCurrency;
    private TextView tvCallTime;
    private TextView tvCallTimeCurrency;
    private TextView tvCallDirection;
    private TextView tvCallDirectionCurrency;
    private boolean isUDSBonusActive;
    private boolean isTariffExist;
    private boolean isAuthorized;
    private boolean isAddressCountEnough;
    private boolean isAddressExist;
    private boolean isSolvency;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_footer, container, false);
        EventBus.getDefault().register(this);

        typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.accent_bg_tariff, typedValue, true);

        btnCreateOrder = (Button) view.findViewById(R.id.btn_createorder);
        pbCreateOrder = (ProgressBar) view.findViewById(R.id.pb_createorder);
        linearLayout = (LinearLayout) view.findViewById(R.id.ll_tariffs);
        mHsvTariffs = (HorizontalScrollView) view.findViewById(R.id.hsv_tariff_list);


        tvCallCost = (TextView) view.findViewById(R.id.tv_fragmentmain_callcost);
        tvCallCostCurrency = (TextView) view.findViewById(R.id.tv_fragmentmain_callcost_currency);
        tvCallTime = (TextView) view.findViewById(R.id.tv_fragmentmain_time);
        tvCallTimeCurrency = (TextView) view.findViewById(R.id.tv_fragmentmain_time_currency);
        tvCallDirection = (TextView) view.findViewById(R.id.tv_fragmentmain_direction);
        tvCallDirectionCurrency = (TextView) view.findViewById(R.id.tv_fragmentmain_direction_currency);
        mTvCostInfo = (TextView) view.findViewById(R.id.tv_fragmentmain_cost_info);
        mTvCostInfoFix = (TextView) view.findViewById(R.id.tv_fragmentmain_callcost_fix);
        mLlCallCost = (LinearLayout) view.findViewById(R.id.ll_call_cost);
        mLlCostInfo = (LinearLayout) view.findViewById(R.id.ll_cost_info);
        mLlCostInfoFix = (LinearLayout) view.findViewById(R.id.ll_call_cost_fix);

        setInitialCostText();

        paymentLayout = (LinearLayout) view.findViewById(R.id.payment_layout);
        paymentImg = (ImageView) view.findViewById(R.id.payment_img);
        paymentTitle = (TextView) view.findViewById(R.id.payment_title);
        initPaymentViews();
        btnCreateOrder.setEnabled(false);
        btnCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTariffExist = false;
                isAuthorized = false;
                isAddressCountEnough = true;
                isAddressExist = false;
                isSolvency = false;
                isUDSBonusActive = false;

                // Check tariff
                String tariffId = ((MainActivity) getActivity()).getOrder().getTariff();
                if (tariffId != null && !tariffId.isEmpty()) {
                    tariff = Tariff.getTariffById(tariffId);
                    isTariffExist = true;
                } else {
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_empty_tariff).show();
                }

                // Check auth
                if (Profile.getProfile().isAuthorized()) {
                    isAuthorized = true;
                } else {
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_empty_profile).show();

                    Intent startActivityIntent = new Intent(getActivity(), AuthActivity.class);
                    startActivityIntent
                            .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                    startActivity(startActivityIntent);
                    return;
                }

                // Check address count
                if (!AppParams.ORDER_WITH_ONE_ADDRESS) {
                    int addressCount = ((MainActivity) getActivity()).getAddressList().size();
                    if (addressCount < 2) {
                        isAddressCountEnough = false;
                        new ToastWrapper(getActivity(),
                                R.string.fragments_MainFooterFragment_not_enough_addresses).show();
                    }
                }

                // Check addresses
                List<Address> addressesList = ((MainActivity) getActivity()).getAddressList();
                DetectCity detectCity = new DetectCity(City.getCities());
                if (addressesList.size() > 0) {
                    Address address = addressesList.get(0);

                    if (address.getLabel().equals(getString(R.string.fragments_MainHeaderFragment_locating))
                    || address.getLabel().equals(getString(R.string.fragments_MainHeaderFragment_loading))) {
                        new ToastWrapper(getActivity(), R.string.fragments_MainFooterFragment_from_place_not_define).show();
                    }

                    try {
                        double lat = Double.parseDouble(address.getLat());
                        double lon = Double.parseDouble(address.getLon());
                        if (detectCity.getCity(lat, lon) == null) {
                            isAddressExist = false;
                            new ToastWrapper(getActivity(), "Адрес отправления вне зоны заказа").show();
                        } else {
                            isAddressExist = true;
                        }
                    } catch (Exception e) {
                        isAddressExist = false;
                        new ToastWrapper(getActivity(), "Адрес отправления вне зоны заказа").show();
                    }
                }

                // Check payment
                if (profile.getPaymentType().equals("CARD") && profile.getPan().equals("")){
                    isSolvency = false;
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_no_card_error).show();
                    Intent startActivityIntent = new Intent(getActivity(), PaymentActivity.class);
                    startActivity(startActivityIntent);
                } else {
                    isSolvency = true;
                }


                if (profile.getBonusId() == 2 && profile.getBonusUdsState() == Profile.BONUS_UDS_STATUS_ACTIVATE
                        && profile.getPaymentBonus() == Profile.BONUS_ACTIVE
                        && tariff != null && tariff.getBonus().equals("1")) {
                    activateUdsPromoCode();
                }
                else isUDSBonusActive = true;

                createOrder();
            }
        });

        return view;
    }

    public void initPaymentViews() {
        City city = City.getCity(cityId);
        if (city != null) updateTariffView(city.getTarifffs());

        profile = Profile.getProfile();
        String paymentType = profile.getPaymentType();
        Log.d("Logos", "initPaymentViews: " +  paymentType);
        switch (paymentType) {
            case "CASH":
                paymentImg.setImageResource(R.drawable.wallet);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_cash);
                break;
            case "PERSONAL_ACCOUNT":
                paymentImg.setImageResource(R.drawable.parsonal);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_account);
                break;
            case "CORP_BALANCE":
                paymentImg.setImageResource(R.drawable.man);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_corp);
                break;
            case "CARD":
                paymentImg.setImageResource(R.drawable.card);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_card);
                break;
        }
        paymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent paymentIntent = new Intent(getActivity(), PaymentActivity.class);
                paymentIntent.putExtra(SUPPORT_CITY, ((MainActivity) getActivity()).currentCity != null);
                startActivity(paymentIntent);
            }
        });
    }

    public void createOrder() {
        if (isTariffExist && isAuthorized && isAddressCountEnough && isAddressExist
                && isSolvency && isUDSBonusActive) {
            ((MainActivity) getActivity()).getOrder().setCreateTime(new Date().getTime() - profile.getInaccuracy());
            getSpiceManager().execute(new CreateOrderRequest(profile.getAppId(), profile.getApiKey(),  getActivity(),
                            String.valueOf(new Date().getTime() - profile.getInaccuracy()),
                            ((MainActivity) getActivity()).getOrder(), profile,
                            ((MainActivity) getActivity()).getAddressList(), mPromoCode),
                    new CreateOrderListener(getActivity()));
            btnCreateOrder.setVisibility(View.GONE);
            pbCreateOrder.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() != null) {

            AppPreferences appPreferences = new AppPreferences(getActivity());
            List<Tariff> tariffList = Tariff.getTariffs(City.getCity(polygonDetectEvent.getCity().getCityId()));

            boolean bTime = false;
            boolean bCityId = false;
            boolean bBdTariff = false;

            if (appPreferences.getText(Constants.AP_SAVE_TARIFFS).length() > 0 &&
                    ((Long.valueOf(appPreferences.getText(Constants.AP_SAVE_TARIFFS)) +
                            AppParams.TIME_UPDATE_TARIFFS) > new Date().getTime()))
                bTime = true;

            if (tariffList.size() > 0) bBdTariff = true;

            if (cityId.equals(polygonDetectEvent.getCity().getCityId())) bCityId = true;

            else cityId = polygonDetectEvent.getCity().getCityId();

            if (bTime && bBdTariff && bCityId) {
                btnCreateOrder.setEnabled(true);
                if (linearLayout.getChildCount() == 0) {
                    updateTariffView(tariffList);
                    startCostRequest();
                }
            } else {
                Profile profile = Profile.getProfile();
                getSpiceManager().execute(new GetTariffsListRequest(profile.getAppId(), profile.getApiKey(),  getActivity(),
                                String.valueOf((new Date()).getTime()),
                                polygonDetectEvent.getCity().getCityId(), profile.getPhone(), profile.getTenantId()),
                        new TariffsRequestListener(getActivity()));
                if (!profile.getCityId().isEmpty() && !profile.getPhone().isEmpty()) {
                    getSpiceManager().execute(new GetClientHistoryAddressRequest(profile.getAppId(),
                                    profile.getApiKey(), getContext(), profile.getTenantId(),
                                    AppParams.TYPE_CLIENT,  profile.getPhone(), profile.getCityId()),
                            new ClientHistoryAddressListener());
                }
            }

        } else {
            cityId = "";
            linearLayout.removeAllViews();
            btnCreateOrder.setEnabled(true);
        }
    }

    @Subscribe
    public void onMessage(CreateOrderEvent createOrderEvent) {
        btnCreateOrder.setVisibility(View.VISIBLE);
        pbCreateOrder.setVisibility(View.GONE);
    }

    public void setStateCreateOrder() {
        btnCreateOrder.setVisibility(View.VISIBLE);
        pbCreateOrder.setVisibility(View.GONE);
    }


    @Subscribe
    public void onMessage(final SaveTariffsEvent saveTariffsEvent) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                btnCreateOrder.setEnabled(true);
                if (saveTariffsEvent.getTariffList() != null) {
                    updateTariffView(saveTariffsEvent.getTariffList());
                }
            }
        });
        startCostRequest();
    }

    public void activateUdsPromoCode() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_promo_code, null);
        mEtPromoCode = (TextInputEditText) view.findViewById(R.id.et_detail_bonus);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.MyCustomAlertDialogTheme)
                .setTitle(getString(R.string.fragments_MainFooterFragment_activate_promocode))
                .setPositiveButton(getString(R.string.fragments_MainFooterFragment_dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    isUDSBonusActive = true;
                                    mPromoCode = mEtPromoCode.getText().toString();
                                    createOrder();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getString(R.string.fragments_MainFooterFragment_dialog_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        })
                .setView(view);
        AlertDialog alertDialog1 = alertDialog.create();
        alertDialog1.show();
    }



    public void updateTariffView(List<Tariff> tariffList)  {
        linearLayout.removeAllViews();
        if (tariffList == null) return;
        ArrayList<Tariff> validateTariffs = validateTariffs(tariffList);

        boolean isDeepOrder = false;
        boolean isSetted = false;

        DeepOrder deepOrder = null;
        if (((MainActivity) getActivity()).getDeepOrder() != null) {
            deepOrder = ((MainActivity) getActivity()).getDeepOrder();
            isDeepOrder = true;
        }

        for (int i = 0; i < validateTariffs.size(); i++) {
            final String currentId = validateTariffs.get(i).getTariffId();
            if (validateTariffs.get(i).getClientTypes().contains(profile.getClientType())) {
                final View viewTariff = LayoutInflater.from(getActivity()).inflate(R.layout.item_footertariff, null);
                viewTariff.findViewById(R.id.iv_tariff_info).setVisibility(View.GONE);
                if (!validateTariffs.get(i).getTariffIcon().isEmpty()) {
                    SimpleDraweeView tariffImage = (SimpleDraweeView)
                            viewTariff.findViewById(R.id.iv_tariff_image);
                    Uri imgUri = Uri.parse(validateTariffs.get(i).getTariffIconMini());
                    tariffImage.setImageURI(imgUri);
                }
                ((TextView) viewTariff.findViewById(R.id.tv_tariff_name)).setText(validateTariffs.get(i).getTariffName());
                viewTariff.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int count = linearLayout.getChildCount();
                        try {
                            if ((int) view.findViewById(R.id.ll_tariff_cont).getTag(R.id.tag_tariff) == 1) {
                                getActivity().startActivityForResult(new Intent(getActivity(),
                                                TariffActivity.class).putExtra("tariff_id",
                                        ((MainActivity) getActivity()).getOrder().getTariff()),
                                        MainActivity.REQUEST_CODE_TARIFF);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        for (int i = 0; i < count; i++) {
                            linearLayout.getChildAt(i).findViewById(R.id.iv_tariff_info).setVisibility(View.GONE);
                            linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(Color.TRANSPARENT);
                            linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 0);
                        }

                        view.findViewById(R.id.ll_tariff_cont).setBackgroundResource(0);
                        view.findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
                        view.findViewById(R.id.iv_tariff_info).setVisibility(View.VISIBLE);

                        view.findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
                        ((MainActivity) getActivity()).getOrder().setTariff(currentId);
                        startCostRequest();
                    }
                });
                viewTariff.setTag(R.id.tag_id, validateTariffs.get(i).getTariffId());
                linearLayout.addView(viewTariff);


                if (deepOrder != null && deepOrder.getTariffId().equals(currentId)) {
                    viewTariff.findViewById(R.id.ll_tariff_cont).setBackgroundResource(0);
                    viewTariff.findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
                    viewTariff.findViewById(R.id.iv_tariff_info).setVisibility(View.VISIBLE);
                    mHsvTariffs.post(new Runnable() {
                        public void run() {
                            mHsvTariffs.smoothScrollTo(viewTariff.getLeft(), 0);
                        }
                    });
                    ((MainActivity) getActivity()).getOrder().setTariff(deepOrder.getTariffId());
                    isSetted = true;
                }
            }
        }

        if (linearLayout.getChildCount() > 0 && validateTariffs.size() > 0 && (!isSetted || !isDeepOrder)) {
            linearLayout.getChildAt(0).findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
            linearLayout.getChildAt(0).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
            linearLayout.getChildAt(0).findViewById(R.id.iv_tariff_info).setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).getOrder().setTariff(validateTariffs.get(0).getTariffId());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) scrollToEndView();

        EspressoIdlingResource.increment();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void scrollToEndView() {
        Configuration config = getResources().getConfiguration();
        if (config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            new Handler().post(new Runnable() {
                public void run() {
                    mHsvTariffs.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            });
        }
    }


    private ArrayList<Tariff> validateTariffs(List<Tariff> tariffs) {
        ArrayList<Tariff> validateTariffs = new ArrayList<>();
        for (Tariff tariff : tariffs) {
            if (tariff.getClientTypes().contains(profile.getClientType())) {
                if (profile.getClientType().equals("base")
                        || profile.getClientType().equals("company") && tariff.getCompanies().contains(profile.getCompany())) {
                    validateTariffs.add(tariff);
                }
            }
        }
        return validateTariffs;
    }

    @Subscribe
    public void onMessage(ChangeTariff changeTariff) {
        ((MainActivity) getActivity()).getOrder().setTariff(changeTariff.getTariffId());
        int count = linearLayout.getChildCount();
        for (int i = 0; i < count; i++) {
            if (linearLayout.getChildAt(i).getTag(R.id.tag_id).toString().equals(changeTariff.getTariffId())) {
                linearLayout.getChildAt(i).findViewById(R.id.iv_tariff_info).setVisibility(View.VISIBLE);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
            } else {
                linearLayout.getChildAt(i).findViewById(R.id.iv_tariff_info).setVisibility(View.GONE);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(Color.TRANSPARENT);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 0);
            }
        }
    }

    @Subscribe
    public void onMessage(CallCostEvent callCostEvent) {
        String tariffId = ((MainActivity) getActivity()).getOrder().getTariff();
        if (AppParams.USE_MULTI_CALLCOST) {
            mTvCostInfo.setText(callCostEvent.getDis() + " "
                + getString(R.string.activities_OrderDetailActivity_distance) + ", "
                + callCostEvent.getTime() + " "
                + getString(R.string.fragments_FragmentAbstractMap_time_minute_long));
            return;
        }
        String currency;
        try {
            City city = City.getCity(Profile.getProfile().getCityId());

            if (city.getCurrency().equals("RUB"))
                currency = getString(R.string.currency);
            else
                currency = city.getCurrency();
        } catch (Exception e) {
            currency = getString(R.string.currency);
        }


        if (callCostEvent.getCost().length() > 0) {
            mLlCostInfo.setVisibility(View.GONE);
            if (callCostEvent.isFix()) {
                mLlCallCost.setVisibility(View.GONE);
                mLlCostInfoFix.setVisibility(View.VISIBLE);

                mTvCostInfoFix.setText(getString(R.string.activities_OrderDetailActivity_fix_cost) + " " + callCostEvent.getCost() + " " + currency
                    + "\n" + callCostEvent.getDis() + " " + getString(R.string.activities_OrderDetailActivity_distance) + " "
                 + callCostEvent.getTime() + " " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long));
            } else {
                mLlCallCost.setVisibility(View.VISIBLE);
                mLlCostInfoFix.setVisibility(View.GONE);
                tvCallCost.setText("~ " + callCostEvent.getCost() + " ");
                tvCallCostCurrency.setText(currency);
                tvCallTime.setText(callCostEvent.getDis() + " ");
                tvCallTimeCurrency.setText(getString(R.string.activities_OrderDetailActivity_distance));
                tvCallDirection.setText(callCostEvent.getTime() + " ");
                tvCallDirectionCurrency.setText(getString(R.string.fragments_FragmentAbstractMap_time_minute_long));
            }
        } else {
            mLlCallCost.setVisibility(View.GONE);
            mLlCostInfo.setVisibility(View.VISIBLE);
            mTvCostInfo.setText(R.string.fragments_MainFooterFragment_infocost_error);
        }
    }

    @Subscribe
    public void onMessage(ConstingOrderEvent event) {
        String currency;
        try {
            City city = City.getCity(Profile.getProfile().getCityId());

            if (city.getCurrency().equals("RUB"))
                currency = getString(R.string.currency);
            else
                currency = city.getCurrency();
        } catch (Exception e) {
            currency = getString(R.string.currency);
        }

        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            View tariffView = linearLayout.getChildAt(i);
            for (Cost cost: event.getCosts()) {
                if (cost.getTariffId().equals(tariffView.getTag(R.id.tag_id))) {
                    TextView tariffCost = tariffView.findViewById(R.id.tv_tariff_cost);
                    tariffCost.setVisibility(View.VISIBLE);
                    String costText = "";
                    if (!cost.isFix()) {
                        costText += "~";
                    }
                    costText += cost.getSummaryCost() + " " + currency;
                    tariffCost.setText(costText);
                }
            }
        }
    }

    private void setInitialCostText() {
        mLlCallCost.setVisibility(View.GONE);
        mLlCostInfo.setVisibility(View.VISIBLE);
        if (((MainActivity) getActivity()).getAddressList().size() < 2) {
            mTvCostInfo.setText(R.string.fragments_MainFooterFragment_infocost_second_point);
        } else {
            mTvCostInfo.setText(R.string.fragments_MainFooterFragment_infocost_default);
        }
    }

    public void startCostRequest() {
        if (AppParams.USE_MULTI_CALLCOST) {
            if (((MainActivity) getActivity()).getAddressList().size() <= 1) {
                ((MainFragment) getParentFragment()).hideCostWindow();
                for (int i = 0; i < linearLayout.getChildCount(); i++) {
                    View tariffView = linearLayout.getChildAt(i);
                    TextView tariffCost = tariffView.findViewById(R.id.tv_tariff_cost);
                    tariffCost.setVisibility(View.GONE);
                }
                return;
            }

            getSpiceManager().execute(new ConstingOrderRequest(profile.getAppId(), profile.getApiKey(),
                    getActivity(), String.valueOf(new Date().getTime()),
                    ((MainActivity) getActivity()).getOrder(), profile,
                    ((MainActivity) getActivity()).getAddressList()), new ConstingOrderListener());
        }
        mLlCallCost.setVisibility(View.GONE);
        mLlCostInfo.setVisibility(View.VISIBLE);
        if (((MainActivity) getActivity()).getAddressList().size() > 1) {
            mTvCostInfo.setText(R.string.fragments_MainFooterFragment_infocost_default);
            mTvCostInfoFix.setText("");
            Profile profile = Profile.getProfile();
            getSpiceManager().execute(new GetCostRequest(profile.getAppId(), profile.getApiKey(),  getActivity(),
                    String.valueOf(new Date().getTime()),
                    ((MainActivity) getActivity()).getOrder(), profile,
                    ((MainActivity) getActivity()).getAddressList(), mPromoCode), new CostListener(getActivity()));
        } else {
            mTvCostInfo.setText(R.string.fragments_MainFooterFragment_infocost_second_point);
        }

    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
