package com.kabbi.client.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.OrderCompletedActivity;
import com.kabbi.client.activities.OrderDetailActivity;
import com.kabbi.client.activities.PreOrderActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.ConstingOrderEvent;
import com.kabbi.client.events.ui.map.CarBusyEvent;
import com.kabbi.client.events.api.client.CreateOrderEvent;
import com.kabbi.client.events.ui.map.GetCurrentLocationEvent;
import com.kabbi.client.events.ui.map.MotionMapEvent;
import com.kabbi.client.events.api.client.OrderInfoEvent;
import com.kabbi.client.events.ui.map.PointsMapEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.api.client.RejectOrderEvent;
import com.kabbi.client.maps.FragmentAbstractMap;
import com.kabbi.client.maps.MapFactory;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Car;
import com.kabbi.client.models.City;
import com.kabbi.client.models.Option;
import com.kabbi.client.models.Order;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.RoutePoint;
import com.kabbi.client.network.listeners.CarsListener;
import com.kabbi.client.network.listeners.OrderInfoListener;
import com.kabbi.client.network.listeners.RejectResultOrderListener;
import com.kabbi.client.network.requests.GetCarsRequest;
import com.kabbi.client.network.requests.GetOrderInfoRequest;
import com.kabbi.client.network.requests.RejectOrderResultRequest;
import com.kabbi.client.utils.AnalyticsHelper;
import com.kabbi.client.utils.ColorHelper;
import com.kabbi.client.utils.HashMD5;
import com.kabbi.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.kabbi.client.app.Constants.PAYMENT_CARD;
import static com.kabbi.client.app.Constants.PAYMENT_CORPORATION;

public class MainFragment extends BaseSpiceFragment {

    private LinearLayout llDown;

    private boolean bMotionEventHide;
    private boolean upEvent, cameraChangeEvent;
    public boolean isActive = false;

    public String statusOrder;
    private int requestCount;

    public ProgressDialog cancelProgressDialog;
    private ImageButton ibCurrentPosition;
    private FloatingActionButton addItemFab;
    private TextView tvCostWindow;

    private Fragment footerFragment, headerFragment;
    private ClarifyMenuFragment clarifyItemsFragment;
    private OrderMenuFragment mOrderMenuFragment;

    private Handler getInfoHandler;
    private Runnable getInfoRunnable;
    private Handler rejectHandler;
    private Runnable rejectRunnable;

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    public boolean enableAddress;

    public Profile profile;
    public String driverPhone;
    public City city;

    private LinearLayout mMenuContainer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profile = Profile.getProfile();

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        bMotionEventHide = true;
        upEvent = true;
        cameraChangeEvent = true;
        enableAddress = true;
        driverPhone = "";

        statusOrder = "empty";

        city = City.getCity(profile.getCityId());

        cancelProgressDialog = new ProgressDialog(getActivity());

        ibCurrentPosition = (ImageButton) view.findViewById(R.id.ib_current_position);

        mMenuContainer = (LinearLayout) view.findViewById(R.id.ll_menu);
        tvCostWindow = (TextView) view.findViewById(R.id.cost_text_table);

        Fragment fragment = getChildFragmentManager().findFragmentByTag("main");
        if (fragment == null) {
            Fragment mapFragment = MapFactory.getFragmentMap(getActivity().getApplicationContext(), profile.getMap());
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mapContainer, mapFragment, "main")
                    .commit();
        }

        if (((MainActivity) getActivity()).getOrder().getStatus().equals("empty")) {

            ((MainActivity) getActivity()).setTitleText(getString(R.string.activities_MainActivity_title));

            footerFragment = new MainFooterFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_footer_fragment, footerFragment, "mainFooter")
                    .commit();

            headerFragment = new MainHeaderFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                    .commit();

            clarifyItemsFragment = new ClarifyMenuFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.ll_menu, clarifyItemsFragment, "mainClarify")
                    .commit();
            if (!AppParams.USE_WISH_PANEL) mMenuContainer.setVisibility(View.GONE);

        } else {
            ((MainActivity) getActivity()).activeOrder = true;

            view.findViewById(R.id.iv_center).setVisibility(View.GONE);

            List<Address> addressList = RoutePoint.getRoute(((MainActivity) getActivity()).getOrder());
            EventBus.getDefault().post(new PointsMapEvent(addressList,
                    ((MainActivity) getActivity()).activeOrder,
                    ((MainActivity) getActivity()).getOrder().getStatus(), ""));

            headerFragment = new TrackingOrderHeaderFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                    .commit();

            mOrderMenuFragment = new OrderMenuFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.ll_menu, mOrderMenuFragment, "mainOrderMenu")
                    .commit();

            getActivity().setTitle(((MainActivity) getActivity()).getOrder().getStatusLabel());
            ((MainActivity) getActivity()).setTitleText(((MainActivity) getActivity()).getOrder().getStatusLabel());
            startOrderInfo();

            getSpiceManager().execute(new GetOrderInfoRequest(profile.getAppId(), profile.getApiKey(),  getActivity(),
                            String.valueOf((new Date()).getTime()),
                            ((MainActivity) getActivity()).getOrder(),
                            profile.getTenantId()),
                    new OrderInfoListener());
        }

        llDown = (LinearLayout) view.findViewById(R.id.ll_down);
        LinearLayout llUp = (LinearLayout) view.findViewById(R.id.ll_up);

        llDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ibCurrentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).getGoogleApiClient().reconnect();
                enableAddress = true;
                EventBus.getDefault().post(new GetCurrentLocationEvent());
            }
        });

        llDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        customizeViews();
    }

    private void initViews(View view) {
        addItemFab = (FloatingActionButton)
                view.findViewById(R.id.frag_main_fab);
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.accent_bg, typedValue, true);
        if (ColorHelper.isColorDark(typedValue.data)) {
            addItemFab.setImageResource(R.drawable.ic_add_white);
        } else {
            addItemFab.setImageResource(R.drawable.ic_add_black);
        }
    }

    private void customizeViews() {
        addItemFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                MainHeaderFragment headerFragment = (MainHeaderFragment)
                        getChildFragmentManager().findFragmentById(R.id.fl_header_fragment);
                int currentPosition = 1;
                if (activity != null) currentPosition = activity.getAddressList().size();
                if (headerFragment != null) headerFragment.addItemList(currentPosition);
            }
        });
        hideFab();
    }

    public void callUpdateAddressList() {
        ((MainHeaderFragment) getChildFragmentManager()
                .findFragmentByTag("mainHeader")).updateAddressList();
    }

    public void callUpdatePayment() {
        MainFooterFragment footerFragment = (MainFooterFragment) getChildFragmentManager()
                .findFragmentByTag("mainFooter");
        if (footerFragment != null) footerFragment.initPaymentViews();
    }

    public void callUpdateOrderTime() {
        clarifyItemsFragment.callUpdateOrderTime();
    }

    public String[] callMapFragmentForCoords() {
        return ((FragmentAbstractMap) getChildFragmentManager()
                .findFragmentByTag("main")).getMarkerCoords();
    }

    @Subscribe
    public void onMessage(CreateOrderEvent createOrderEvent) {
        switch (createOrderEvent.getRequestCode()) {
            case 0:
                AnalyticsHelper.sendEvent(AnalyticsHelper.METRICA_EVENT_CREATE_ORDER);
                ((MainActivity) getActivity()).getOrder().setUuid(UUID.randomUUID().toString());
                if (createOrderEvent.getType().equals("new")) {
                    ((MainActivity) getActivity()).activeOrder = true;

                    Order order = ((MainActivity) getActivity()).getOrder();

                    order.setStatus("new");
                    order.setOrderId(createOrderEvent.getOrderId());

                    order.setOrderNumber(createOrderEvent.getOrderNumber());
                    if (profile.getPaymentType().equals(PAYMENT_CORPORATION)) order.setCompanyId(profile.getCompany());
                    if (profile.getPaymentType().equals(PAYMENT_CARD)) order.setPan(profile.getPan());
                    order.save();

                    RoutePoint.saveOrderAddressesAndRoute(order,
                            ((MainActivity) getActivity()).getAddressList());
                    getActivity().findViewById(R.id.iv_center).setVisibility(View.GONE);
                    EventBus.getDefault().post(new PointsMapEvent(((MainActivity) getActivity()).getAddressList(),
                            ((MainActivity) getActivity()).activeOrder,
                            order.getStatus(), ""));

                    List<Option> optionList = Option.getAllOptions();
                    for (Option option : optionList) {
                        option.setChecked(false);
                        option.save();
                    }

                    hideFab();

                    getChildFragmentManager()
                            .beginTransaction()
                            .remove(footerFragment)
                            .commit();

                    headerFragment = new TrackingOrderHeaderFragment();
                    getChildFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                            .commit();

                    mOrderMenuFragment = new OrderMenuFragment();
                    mMenuContainer.setVisibility(View.VISIBLE);
                    getChildFragmentManager()
                            .beginTransaction()
                            .replace(R.id.ll_menu, mOrderMenuFragment, "mainOrderMenu")
                            .commit();


                    ((MainActivity) getActivity()).setTitleText(getString(R.string.fragments_MainFragment_status_new));
                    startOrderInfo();
                } else {
                    ((MainActivity) getActivity()).getOrder().setStatus("pre_order");
                    ((MainActivity) getActivity()).getOrder().setOrderId(createOrderEvent.getOrderId());
                    ((MainActivity) getActivity()).getOrder().setOrderNumber(createOrderEvent.getOrderNumber());
                    ((MainActivity) getActivity()).getOrder().save();
                    RoutePoint.saveOrderAddressesAndRoute(((MainActivity) getActivity()).getOrder(),
                            ((MainActivity) getActivity()).getAddressList());
                    List<Option> optionList = Option.getAllOptions();
                    for (Option option : optionList) {
                        option.setChecked(false);
                        option.save();
                    }

                    startActivity(new Intent(getActivity(), PreOrderActivity.class)
                            .putExtra("order_id", createOrderEvent.getOrderId()));
                }
                break;
            default:
                new ToastWrapper(getContext(), createOrderEvent.getRequestCode(), false).show();
                MainFooterFragment footerFragment = (MainFooterFragment)
                        getChildFragmentManager().findFragmentByTag("mainFooter");
                if (footerFragment != null) {
                    footerFragment.setStateCreateOrder();
                }
        }
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {
        String title;

        switch (orderInfoEvent.getStatus()) {
            case "rejected":
                title = getString(R.string.rejected);
                break;
            case "completed":
                title = getString(R.string.completed);
                break;
            default:
                title = orderInfoEvent.getStatusLabel();
        }

        ((MainActivity) getActivity()).setTitleText(title);

        if (AppParams.USE_CALLS_DRIVER) driverPhone = orderInfoEvent.getDriverPhone(); // TODO MOVE TO FRAGMENT

        statusOrder = orderInfoEvent.getStatus();
        mOrderMenuFragment.updateMenuState();

        ((MainActivity) getActivity()).getOrder().setStatusLabel(orderInfoEvent.getStatusLabel());
        ((MainActivity) getActivity()).getOrder().setStatus(orderInfoEvent.getStatus());
        ((MainActivity) getActivity()).getOrder().setCar(orderInfoEvent.getCarDesc());
        ((MainActivity) getActivity()).getOrder().setDriver(orderInfoEvent.getDriverName());
        ((MainActivity) getActivity()).getOrder().setPhoto(orderInfoEvent.getPhoto());
        ((MainActivity) getActivity()).getOrder().setCost(orderInfoEvent.getCost());
        ((MainActivity) getActivity()).getOrder().setFixCost(orderInfoEvent.getFixCost());
        ((MainActivity) getActivity()).getOrder().setStatusId(orderInfoEvent.getStatusId());
        ((MainActivity) getActivity()).getOrder().setPaymentType(orderInfoEvent.getPaymentType());
        ((MainActivity) getActivity()).getOrder().setStatusDescription(orderInfoEvent.getStatusDescription());
        if (orderInfoEvent.getPan() != null) {
            ((MainActivity) getActivity()).getOrder().setPan(orderInfoEvent.getPan());
        }
        ((MainActivity) getActivity()).getOrder().save();

        switch (orderInfoEvent.getStatus()) {
            case "new":
                EventBus.getDefault().post(
                        new PointsMapEvent(RoutePoint.getRoute(((MainActivity) getActivity()).getOrder()),
                                ((MainActivity) getActivity()).activeOrder, orderInfoEvent.getStatus(), orderInfoEvent.getTime()));
                break;
            case "pre_order":
                break;
            case "completed":
                cancelProgressDialog.cancel();
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);
                if (rejectHandler != null)
                    rejectHandler.removeCallbacks(rejectRunnable);
                if (orderInfoEvent.getAddresses() != null) {
                    for (Address address : orderInfoEvent.getAddresses()) {
                        if (address.getLabel().isEmpty()) {
                            String gpsAddress =
                                    getString(R.string.fragments_MainHeaderFragment_gps_address);
                            address.setLabel(gpsAddress);
                            address.setStreet(gpsAddress);
                            address.setCity("");
                            address.setGps(true);
                            String tempHash = HashMD5.getHash(address.getCity() + address.getStreet() +
                                    address.getHouse() + address.getLabel() + address.getLat()
                                    + address.getLon());
                            address.setHash(tempHash);
                        }
                    }
                    RoutePoint.getDeletePoints(((MainActivity) getActivity()).getOrder());
                    RoutePoint.saveOrderAddressesAndRoute(((MainActivity) getActivity()).getOrder(), orderInfoEvent.getAddresses());
                }
                startActivity(new Intent(getActivity(), OrderCompletedActivity.class).putExtra("order_id",
                        ((MainActivity) getActivity()).getOrder().getOrderId()));
                getActivity().finish();
                break;
            case "rejected":
                AnalyticsHelper.sendEvent(AnalyticsHelper.METRICA_EVENT_REJECT_ORDER);
                cancelProgressDialog.cancel();
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);
                if (rejectHandler != null)
                    rejectHandler.removeCallbacks(rejectRunnable);
                ((MainActivity) getActivity()).getOrder().setStatusLabel(getString(R.string.rejected));
                ((MainActivity) getActivity()).getOrder().save();

                startActivity(new Intent(getActivity(), OrderDetailActivity.class).putExtra("order_id",
                        ((MainActivity) getActivity()).getOrder().getOrderId()).putExtra("type", "main").putExtra("status", "rejected"));
                getActivity().finish();
                break;
            default:
                EventBus.getDefault().post(new PointsMapEvent(RoutePoint.getRoute(((MainActivity) getActivity()).getOrder()),
                        ((MainActivity) getActivity()).activeOrder, orderInfoEvent.getStatus(), orderInfoEvent.getTime()));
                EventBus.getDefault().post(new CarBusyEvent(new Car(orderInfoEvent.getLat(), orderInfoEvent.getLon(), orderInfoEvent.getDegree(), null)));
        }
    }

    public void hideCostWindow() {
        tvCostWindow.setVisibility(View.GONE);
    }

    @Subscribe
    public void onMessage(RejectOrderEvent event) {
        switch (event.getType()) {
            case "begin":
                startRejectOrder();
                break;
            case "process":
                if (requestCount > 4)
                    EventBus.getDefault().post(new OrderInfoEvent("rejected", "rejected", "", "", "", "",
                            0, 0, (float) 0, 0, "", "0", "0", "", "", "", null, null));
                else requestCount++;
                break;
            case "complete":
                break;
            case "error":
                new ToastWrapper(getContext(), R.string.error_reject_order).show();
                if (rejectHandler != null)
                    rejectHandler.removeCallbacks(rejectRunnable);
                cancelProgressDialog.cancel();
                break;
            default:
                break;
        }
    }


    private void startOrderInfo() {
        if (getInfoHandler != null && getInfoRunnable != null) {
            getInfoHandler.removeCallbacks(getInfoRunnable);
        }
        getInfoHandler = new Handler();
        getInfoRunnable = new Runnable() {
            @Override
            public void run() {
                getSpiceManager().execute(new GetOrderInfoRequest(profile.getAppId(), profile.getApiKey(),  getActivity(),
                                String.valueOf((new Date()).getTime()),
                                ((MainActivity) getActivity()).getOrder(),
                                profile.getTenantId()),
                        new OrderInfoListener());
                getInfoHandler.postDelayed(getInfoRunnable, 5000);
            }
        };
        getInfoHandler.post(getInfoRunnable);
    }

    public void rejectOrder() {
        cancelProgressDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order_execute));
        cancelProgressDialog.setCancelable(false);
        cancelProgressDialog.show();
    }

    private void startRejectOrder() {
        if (rejectHandler != null && rejectRunnable != null) {
            rejectHandler.removeCallbacks(rejectRunnable);
        }
        rejectHandler = new Handler();
        rejectRunnable = new Runnable() {
            @Override
            public void run() {
                getSpiceManager().execute(
                        new RejectOrderResultRequest(
                                profile.getAppId(), profile.getApiKey(),
                                ((MainActivity) getActivity()).getOrder(),
                                Profile.getProfile().getTenantId()),
                        new RejectResultOrderListener());
                rejectHandler.postDelayed(rejectRunnable, 1000);
            }
        };
        rejectHandler.post(rejectRunnable);
    }

    @Override
    public void onDestroy() {
        if (addItemFab != null) addItemFab.setOnClickListener(null);
        if (getInfoHandler != null)
            getInfoHandler.removeCallbacks(getInfoRunnable);
        if (rejectHandler != null)
            rejectHandler.removeCallbacks(rejectRunnable);
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        isActive = true;
        if (profile.getCityId().length() > 0 &&
                ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0 &&
                !statusOrder.equals("car_assigned")) {
            getSpiceManager().execute(new GetCarsRequest(profile.getAppId(), profile.getApiKey(),
                            getActivity(), profile.getCityId(),
                            ((MainActivity) getActivity()).getAddressList().get(0).getLat(),
                            ((MainActivity) getActivity()).getAddressList().get(0).getLon(),
                            profile.getTenantId(), false, AppParams.CAR_RADIUS),
                    new CarsListener());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((MainActivity) getActivity()).activeOrder)
            ibCurrentPosition.setVisibility(View.GONE);
        else
            ibCurrentPosition.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        isActive = false;
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessage(MotionMapEvent motionMapEvent) {
        switch (motionMapEvent.getType()) {
            case MotionMapEvent.UP:
                upEvent = true;
                showContent();
                bMotionEventHide = true;
                break;
            case MotionMapEvent.DOWN:
                if (!((MainActivity) getActivity()).activeOrder) {
                    upEvent = false;
                    if (bMotionEventHide) hideContent();
                }
                break;
        }
    }

    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() == null && !((MainActivity) getActivity()).activeOrder) {
            hideContent();
        } else {
            city = City.getCity(profile.getCityId());
            if (!((MainActivity) getActivity()).activeOrder &&
                    profile.getCityId().length() > 0 &&
                    ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0 &&
                    !statusOrder.equals("car_assigned")) {

                getSpiceManager().execute(new GetCarsRequest(profile.getAppId(), profile.getApiKey(),
                                getActivity(), profile.getCityId(),
                                ((MainActivity) getActivity()).getAddressList().get(0).getLat(),
                                ((MainActivity) getActivity()).getAddressList().get(0).getLon(),
                                profile.getTenantId(), false, AppParams.CAR_RADIUS),
                        new CarsListener());
            }
        }
    }

    private void hideContent() {
        llDown.animate().translationY(llDown.getHeight());
    }

    public void showContent() {
        llDown.animate().translationY(0);
    }

    public void startCostRequest() {
        MainFooterFragment footerFragment = (MainFooterFragment)
                getChildFragmentManager().findFragmentByTag("mainFooter");
        if (footerFragment != null) footerFragment.startCostRequest();
    }

    public void showFab() {
        addItemFab.show();
    }

    public void hideFab() {
        addItemFab.hide();
    }

}