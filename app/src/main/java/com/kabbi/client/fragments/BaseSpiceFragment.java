package com.kabbi.client.fragments;

import android.support.v4.app.Fragment;

import com.octo.android.robospice.SpiceManager;
import com.kabbi.client.network.RetrofitSpiceService;

public class BaseSpiceFragment extends Fragment {


    public BaseSpiceFragment() {
        // Required empty public constructor
    }

    private SpiceManager spiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    public void onStart() {
        spiceManager.start(getContext());
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

}
