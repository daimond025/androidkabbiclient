package com.kabbi.client.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.activities.OrderCompletedActivity;
import com.kabbi.client.activities.OrderDetailActivity;
import com.kabbi.client.models.Profile;

/**
 * Created by gootax on 22.09.16.
 */
public class CompletedMenuFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complete_menu_fragment, container, false);

        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_bg, typedValue, true);
        TypedValue typedValueText = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        TextView mTVMenuReport = (TextView) view.findViewById(R.id.tv_menu_report);
        TextView mTVMenuAbout = (TextView) view.findViewById(R.id.tv_menu_about);
        LinearLayout mLlMenuReport = (LinearLayout) view.findViewById(R.id.ll_menu_report);
        LinearLayout mLlMenuAbout = (LinearLayout) view.findViewById(R.id.ll_menu_about);
        LinearLayout mLlMenuComplete = (LinearLayout) view.findViewById(R.id.ll_items_report);

        mTVMenuReport.setTextColor(typedValueText.data);
        mTVMenuAbout.setTextColor(typedValueText.data);
        mLlMenuReport.setBackgroundColor(typedValue.data);
        mLlMenuAbout.setBackgroundColor(typedValue.data);
        mLlMenuComplete.setBackgroundColor(typedValue.data);

        mLlMenuReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity().getApplicationContext(), OrderDetailActivity.class)
                        .putExtra("order_id", ((OrderCompletedActivity) getActivity()).order.getOrderId())
                        .putExtra("type", "complete")
                        .putExtra("status", ((OrderCompletedActivity) getActivity()).order.getStatus()));
            }
        });

        final Profile profile = Profile.getProfile();

        mLlMenuAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String sendText = getString(R.string.activities_OrderCompletedActivity_share, getString(R.string.app_name), profile.getAndroidUrl(), profile.getIosUrl());
                sendIntent.putExtra(Intent.EXTRA_TEXT, sendText);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.activities_OrderCompletedActivity_share_title)));
            }
        });
        return view;
    }
}
