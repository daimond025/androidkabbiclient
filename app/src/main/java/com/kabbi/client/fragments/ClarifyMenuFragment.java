package com.kabbi.client.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.kabbi.client.R;
import com.kabbi.client.activities.DetailAddressActivity;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.WishesActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.TimeHelper;
import com.kabbi.client.views.DateTimeListener;
import com.kabbi.client.views.TimeDateListener;
import com.kabbi.client.views.pickers.TimeDateDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.kabbi.client.models.Profile.GREGORIAN_CALENDAR;
import static com.kabbi.client.utils.persian.PersianCalendarUtils.getPersianFormat;

/**
 * Created by gootax on 22.09.16.
 */
public class ClarifyMenuFragment extends Fragment {

    private MainFragment mMainFragment;
    private TextView mLabelClock;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainFragment = ((MainFragment) getParentFragment());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.clarify_menu_fragment, container, false);
        mLabelClock = (TextView) view.findViewById(R.id.tv_menu_label_clock);

        LinearLayout menuClock = (LinearLayout) view.findViewById(R.id.ll_menu_clock);
        if (!AppParams.USE_ORDER_TIME) menuClock.setVisibility(View.GONE);
        LinearLayout menuWishes = (LinearLayout) view.findViewById(R.id.ll_menu_wishes);
        if (!AppParams.USE_WISHES) menuWishes.setVisibility(View.GONE);
        LinearLayout menuAddress = (LinearLayout) view.findViewById(R.id.ll_menu_address);
        if (!AppParams.USE_ADDRESS_DETAIL) menuAddress.setVisibility(View.GONE);

        menuClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMainFragment.isActive) {
                    showDateTimeDialog();
                }
            }
        });

        menuWishes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((MainActivity) getActivity()).getOrder().getTariff() != null && ((MainActivity) getActivity()).getOrder().getTariff().length() == 0) {
                    getActivity();
                } else {
                    getActivity().startActivityForResult(new Intent(getActivity(), WishesActivity.class)
                            .putExtra("tariff_id", ((MainActivity) getActivity()).getOrder().getTariff()), MainActivity.REQUEST_CODE_WISHES);
                }
            }
        });
        menuAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAddress = new Intent(getActivity(), DetailAddressActivity.class);
                Address firstAddress = ((MainActivity) getActivity()).getAddressList().get(0);
                if (firstAddress.isGps()) {
                    intentAddress.putExtra("street", firstAddress.getStreet());
                }
                intentAddress.putExtra("porch", firstAddress.getPorch());
                intentAddress.putExtra("apt", firstAddress.getApt());
                intentAddress.putExtra("comment", ((MainActivity) getActivity()).getOrder().getComment());
                getActivity().startActivityForResult(intentAddress, MainActivity.REQUEST_CODE_DETAIL);
            }
        });


        return view;
    }

    private void showDateTimeDialog() {
        Date timeToSet = null;
        Date timeMax;
        String savedTime = ((MainActivity) getActivity()).getOrder().getOrderTime();
        if (!savedTime.isEmpty()) {
            timeToSet = TimeHelper.getFakeTimeFromString(savedTime);
        }

        if (savedTime.isEmpty() || timeToSet == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE, 10);
            timeToSet = cal.getTime();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, 1);
        timeMax = cal.getTime();

        if (Profile.getProfile().getCalendar() == GREGORIAN_CALENDAR) {
            new SlideDateTimePicker.Builder(getChildFragmentManager())
                    .setListener(new DateTimeListener())
                    .setInitialDate(timeToSet)
                    .setIs24HourTime(true)
                    .setMaxDate(timeMax)
                    .setIndicatorColor(Color.parseColor("#" + Integer.toHexString(
                            ContextCompat.getColor(getContext(), R.color.colorPrimary))))
                    .build()
                    .show();
        } else {
            new TimeDateDialogFragment.Builder()
                    .setInitialDate(timeToSet)
                    .setMaxDate(timeMax)
                    .setListener(new TimeDateListener())
                    .build()
                    .show(getChildFragmentManager(), "PersianDate");
        }
    }


    public void callUpdateOrderTime() {
        SimpleDateFormat newFormatter = new SimpleDateFormat(
                "dd.MM HH:mm", Locale.ENGLISH);
        String tempTime = ((MainActivity) getActivity()).getOrder().getOrderTime();
        if (tempTime == null || tempTime.isEmpty()) {
            tempTime = getString(R.string.menu_footer_clock);
        } else {
            if (Profile.getProfile().getCalendar() == GREGORIAN_CALENDAR) {
                tempTime = newFormatter.format(TimeHelper.getFakeTimeFromString(tempTime));
            } else tempTime = getPersianFormat(tempTime);
        }
        mLabelClock.setText(tempTime);
    }

}
