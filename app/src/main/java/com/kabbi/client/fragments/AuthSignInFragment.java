package com.kabbi.client.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.activities.CountryActivity;
import com.kabbi.client.activities.EditProfileActivity;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.activities.ProfileActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.events.api.client.ProfileEvent;
import com.kabbi.client.events.api.client.RequestCompletedEvent;
import com.kabbi.client.models.City;
import com.kabbi.client.models.PaymentType;
import com.kabbi.client.models.Profile;
import com.kabbi.client.models.Referral;
import com.kabbi.client.network.listeners.AcceptPassRequestListener;
import com.kabbi.client.network.listeners.ClientBalancesListener;
import com.kabbi.client.network.listeners.ClientHistoryAddressListener;
import com.kabbi.client.network.listeners.ReferralSystemListListener;
import com.kabbi.client.network.listeners.TariffsRequestListener;
import com.kabbi.client.network.requests.GetClientBalances;
import com.kabbi.client.network.requests.GetClientHistoryAddressRequest;
import com.kabbi.client.network.requests.GetReferralSystemListRequest;
import com.kabbi.client.network.requests.GetTariffsListRequest;
import com.kabbi.client.network.requests.PostAcceptPassRequest;
import com.kabbi.client.utils.AnalyticsHelper;
import com.kabbi.client.utils.InputMask;
import com.kabbi.client.views.ToastWrapper;

import java.util.Date;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.kabbi.client.app.AppParams.USE_PROFILE_DATA;
import static com.kabbi.client.app.AppParams.USE_REFERRAL;
import static com.kabbi.client.app.AppParams.WITH_COUNTRY;
import static com.kabbi.client.network.listeners.AcceptPassRequestListener.REFERRAL_ERROR;

public class AuthSignInFragment extends BaseSpiceFragment implements TextView.OnEditorActionListener {

    public static final int FRAG_SIGN_IN_CODE = 103;

    private Profile profile;
    private String phoneFromDb;
    private String phoneMask;
    private String country;

    private InputMask inputMask;
    private boolean mFormatting;

    private TextInputEditText phoneEdt;
    private TextInputEditText codeEdt;
    private TextInputEditText referral;

    private Button countryBtn;
    private Button confirmBtn;

    private ProgressDialog waitResponsesDialog;

    private boolean isBalancesResponseCompleted;
    private boolean isReferralResponseCompleted;
    private boolean isTariffResponseCompleted;
    private boolean isClientHistoryResponseCompleted;

    public static AuthSignInFragment newInstance() {
        return new AuthSignInFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profile = Profile.getProfile();
        setProfileValues();
    }


    private void setProfileValues() {
        phoneFromDb = profile.getPhone();
        String phoneMaskFromDb = profile.getPhoneMask();
        if (!phoneMaskFromDb.isEmpty()) {
            phoneMask = phoneMaskFromDb;
        } else {
            phoneMask = getString(R.string.activities_AuthActivity_edit_phone_mask);
        }
        String countryFromDb = profile.getCountry();
        if (!countryFromDb.isEmpty()) {
            country = countryFromDb;
        } else {
            country = getString(R.string.activities_AuthActivity_edit_country);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth_sign_in, container, false);
        countryBtn = (Button) view.findViewById(R.id.signin_country);
        countryBtn.setTransformationMethod(null);
        countryBtn.setText(country);
        countryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(
                        new Intent(getActivity(), CountryActivity.class), FRAG_SIGN_IN_CODE);
            }
        });
        if (!WITH_COUNTRY) countryBtn.setVisibility(View.GONE);
        phoneEdt = (TextInputEditText) view.findViewById(R.id.signin_phone);
        setEditPhoneValues();
        setEditPhoneListeners();
        codeEdt = (TextInputEditText) view.findViewById(R.id.signin_code);
        codeEdt.setOnEditorActionListener(this);
        referral = (TextInputEditText) view.findViewById(R.id.referral_code);
        TextInputLayout mMReferralCodeLayout = (TextInputLayout) view.findViewById(R.id.referral_code_layout);
        if (Referral.getReferralByCityId(profile.getCityId()) == null) {
            mMReferralCodeLayout.setVisibility(View.GONE);
        }
        confirmBtn = (Button) view.findViewById(R.id.signin_confirm);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPassword();
            }
        });
        return view;
    }


    private void setEditPhoneValues() {
        inputMask = new InputMask(phoneMask, phoneFromDb);
        mFormatting = false;
        String newText = inputMask.getNewText();
        phoneEdt.setText(newText);
        phoneEdt.setSelection(inputMask.getSelection(newText));
    }


    private void setEditPhoneListeners() {
        phoneEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selection = inputMask.getSelection(phoneEdt.getText().toString().trim());
                if (selection == phoneMask.length()) {
                    codeEdt.requestFocus();
                } else {
                    phoneEdt.setSelection(selection);
                    mFormatting = false;
                }
            }
        });

        phoneEdt.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            String phoneStr;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newPhone = s.toString().trim().replaceAll("\\D+", "");
                if (newPhone.startsWith("49")) {
                    String mask = inputMask.getGermanMask(newPhone);
                    inputMask = new InputMask(mask, newPhone);
                    phoneMask = mask;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                phoneStr = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    String result = inputMask.afterTextChangedSetText(phoneStr, s);
                    phoneEdt.setText(result);
                    phoneEdt.setSelection(inputMask.afterTextChangedSetSelection(result));
                } else {
                    mFormatting = false;
                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe
    public void onMessage(ProfileEvent event) {
        confirmBtn.setEnabled(true);

        switch (event.getSuccess()) {
            case 0:
                waitResponsesDialog.cancel();
                resetResponseCompletedVariables();
                new ToastWrapper(getContext(),
                        R.string.activities_AuthActivity_signin_req_error).show();
                break;
            case 1:
                if (event.getReferral_success() == 1) new ToastWrapper(getContext(),
                        R.string.activities_ReferralActivity_referral_code_success).show();
                Profile newProfile = event.getProfile();

                if (USE_REFERRAL) {
                    getSpiceManager().execute(new GetReferralSystemListRequest(
                            profile.getAppId(),
                            profile.getApiKey(),
                            getContext(), profile.getTenantId(),
                            newProfile.getPhone()), new ReferralSystemListListener(getContext()));
                }

                if (profile.getCityId() != null || !profile.getCityId().isEmpty() || !profile.getCityId().equals("0")) {
                    profile.setCityId(City.getFirstCity().getCityId());
                    profile.save();
                }

                getSpiceManager().execute(new GetTariffsListRequest(profile.getAppId(), profile.getApiKey(),
                                getActivity(), String.valueOf((new Date()).getTime()),
                                profile.getCityId(), newProfile.getPhone(), profile.getTenantId()),
                        new TariffsRequestListener(getActivity()));

                getSpiceManager().execute(new GetClientBalances(profile.getAppId(),
                                profile.getApiKey(),
                                getContext(),
                                profile.getTenantId(),
                                profile.getCityId(), Profile.getProfile().getPhone()),
                        new ClientBalancesListener(getActivity()));

                getSpiceManager().execute(new GetClientHistoryAddressRequest(profile.getAppId(),
                                profile.getApiKey(), getContext(), profile.getTenantId(),
                                AppParams.TYPE_CLIENT, profile.getPhone(), profile.getCityId()),
                        new ClientHistoryAddressListener());

                newProfile.setAuthorized(true);
                newProfile.setPaymentBonus(Profile.BONUS_INACTIVE);
                newProfile.save();
                PaymentType.addPersonalPayment(profile.getPhone(), profile.getBalanceValue(), profile.getBalanceCurrency());

                AnalyticsHelper.sendEvent(AnalyticsHelper.METRICA_EVENT_REG);
                break;
            case REFERRAL_ERROR:
                waitResponsesDialog.cancel();
                resetResponseCompletedVariables();
                new ToastWrapper(getContext(),
                        R.string.activities_AuthActivity_signin_referral_error).show();
                break;
            default:
                resetResponseCompletedVariables();
                waitResponsesDialog.cancel();
                break;
        }
    }


    @Subscribe
    public void onMessage(RequestCompletedEvent event) {
        int code = event.getRequestCode();
        switch (code) {
            case RequestCompletedEvent.REFERRAL:
                isReferralResponseCompleted = true;
                break;
            case RequestCompletedEvent.TARIFF:
                isTariffResponseCompleted = true;
                break;
            case RequestCompletedEvent.BALANCES:
                isBalancesResponseCompleted = true;
                break;
            case RequestCompletedEvent.CLIENT_HISTORY:
                isClientHistoryResponseCompleted = true;
                break;
        }
        Intent backIntent = new Intent(getActivity(), MainActivity.class);
        backIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_AUTH);

        boolean isProfileWithoutData = TextUtils.isEmpty(profile.getName())
                || TextUtils.isEmpty(profile.getEmail())
                || TextUtils.isEmpty(profile.getSurname());

        if (USE_REFERRAL) {
            if (isBalancesResponseCompleted && isTariffResponseCompleted && isReferralResponseCompleted && isClientHistoryResponseCompleted) {
                waitResponsesDialog.cancel();
                if (USE_PROFILE_DATA && isProfileWithoutData) {
                    Intent profileIntent = new Intent(getActivity(), EditProfileActivity.class);
                    profileIntent.putExtra(EditProfileActivity.EXTRA_REQUIRED, true);
                    startActivity(profileIntent);
                    getActivity().finish();
                } else {
                    NavUtils.navigateUpTo(getActivity(), backIntent);
                }
            }
        } else {
            if (isBalancesResponseCompleted && isTariffResponseCompleted && isClientHistoryResponseCompleted) {
                waitResponsesDialog.cancel();
                if (USE_PROFILE_DATA && isProfileWithoutData) {
                    Intent profileIntent = new Intent(getActivity(), EditProfileActivity.class);
                    profileIntent.putExtra(EditProfileActivity.EXTRA_REQUIRED, true);
                    startActivity(profileIntent);
                    getActivity().finish();
                } else {
                    NavUtils.navigateUpTo(getActivity(), backIntent);
                }
            }
        }
    }


    private void resetResponseCompletedVariables() {
        isReferralResponseCompleted = false;
        isTariffResponseCompleted = false;
        isBalancesResponseCompleted = false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FRAG_SIGN_IN_CODE
                && resultCode == Activity.RESULT_CANCELED) {
            updateViews();
        }
    }


    public void updateViews() {
        setProfileValues();
        countryBtn.setText(country);
        setEditPhoneValues();
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            sendPassword();
            return true;
        }
        return false;
    }


    private void sendPassword() {
        waitResponsesDialog = new ProgressDialog(getActivity());
        waitResponsesDialog.setMessage(getString(R.string.activities_OptionsActivity_progress_title));
        waitResponsesDialog.setCancelable(false);
        waitResponsesDialog.show();

        boolean phoneIsValid = true;
        int countNum = phoneMask
                .replaceAll("_","0")
                .replaceAll("\\D", "")
                .length();
        if (phoneEdt.getText().toString().trim()
                .replaceAll("\\D+", "").length() != countNum) {
            phoneEdt.setError(getString(R.string.activities_AuthActivity_edit_phone_error));
            phoneIsValid = false;
        }
        if (phoneIsValid) {
            confirmBtn.setEnabled(false);
            profile.setPhoneMask(phoneMask);
            profile.setPhone(phoneEdt.getText().toString().trim().replaceAll("\\D+", ""));
            profile.save();

            getSpiceManager().execute(new PostAcceptPassRequest(profile.getAppId(), profile.getApiKey(),  getContext(),
                            phoneEdt.getText().toString().trim().replaceAll("\\D+", ""),
                            codeEdt.getText().toString().trim(),
                            Integer.valueOf(profile.getCityId()),
                            String.valueOf((new Date()).getTime()), profile.getTenantId(), referral.getText().toString()),
                    new AcceptPassRequestListener());
        }
    }

}