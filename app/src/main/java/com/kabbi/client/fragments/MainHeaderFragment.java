package com.kabbi.client.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kabbi.client.BuildConfig;
import com.kabbi.client.R;
import com.kabbi.client.activities.AutoCompleteActivity;
import com.kabbi.client.activities.DetailAddressActivity;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.app.DeepParams;
import com.kabbi.client.events.ui.map.CoordsForReverseEvent;
import com.kabbi.client.events.ui.ItemAddressActionEvent;
import com.kabbi.client.events.ui.map.MapChangedEvent;
import com.kabbi.client.events.ui.map.MotionMapEvent;
import com.kabbi.client.events.ui.map.PolygonDetectEvent;
import com.kabbi.client.events.ui.UpdatePointsEvent;
import com.kabbi.client.events.api.geo.ReverseEvent;
import com.kabbi.client.events.ui.map.PolygonEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.models.City;
import com.kabbi.client.models.DeepOrder;
import com.kabbi.client.models.Profile;
import com.kabbi.client.utils.AddressList;
import com.kabbi.client.utils.HashMD5;
import com.kabbi.client.views.adapters.AddressAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MainHeaderFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView listView;
    private AddressAdapter addressAdapter;
    private String latForReverse;
    private String lonForReverse;
    private List<Address> mAddressesList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_header, container, false);

        mAddressesList = ((MainActivity) getActivity()).getAddressList();
        listView = (ListView) view.findViewById(R.id.lv_header);
        addressAdapter = new AddressAdapter(getActivity(), mAddressesList);
        listView.setOnItemClickListener(this);
        listView.setAdapter(addressAdapter);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();

        if (addressList.size() <= 1 && position > 0) {
            Address address = addressList.get(0);
            if (address.getLabel().equals(getString(R.string.fragments_MainHeaderFragment_unknown_city))) return;
        }

        String[] coords = ((MainFragment) getParentFragment()).callMapFragmentForCoords();
        Intent autoCompleteIntent = new Intent(getActivity(), AutoCompleteActivity.class);
        autoCompleteIntent.putExtra(MainActivity.POINT_NUM_KEY, position);
        autoCompleteIntent.putExtra(AutoCompleteActivity.USER_ADDRESS_KEY, mAddressesList.get(position).getLabel());
        autoCompleteIntent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, coords);
        startActivity(autoCompleteIntent);
    }

    public void updateAddressList() {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
        addressAdapter.updateList(addressList, true);
        if (addressList.size() > 1 && addressList.size() < 5) {
            ((MainFragment) getParentFragment()).showFab();
            if (((MainActivity) getActivity()).getOrder().getTariff() == null
                    || ((MainActivity) getActivity()).getOrder().getTariff().isEmpty()) {
                EventBus.getDefault().post(new PolygonDetectEvent(City.getCity(Profile.getProfile().getCityId())));
            }
        } else {
            ((MainFragment) getParentFragment()).hideFab();
        }
    }

    @Subscribe
    public void onMessage(ReverseEvent event) {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
        if (((MainFragment) getParentFragment()).enableAddress) {
            if (addressList.size() <= 1) {
                if (addressList.get(0).getUseType().equals(Address.TYPE_AUTOCOMPLETE)) {
                    addressList.get(0).setUseType(Address.TYPE_ROUTE);
                    return;
                }

                Address address = event.getAddress();

                boolean isGpsAddress = event.isGpsAddress();
                address.setLat(latForReverse);
                address.setLon(lonForReverse);
                if (isGpsAddress) {
                    String gpsAddress =
                            getString(R.string.fragments_MainHeaderFragment_gps_address);
                    address.setLabel(gpsAddress);
                    address.setStreet(gpsAddress);
                    address.setCity("");
                    address.setGps(true);
                    String hash = HashMD5.getHash(address.getCity() + address.getStreet() +
                            address.getHouse() + address.getLabel() + address.getLat()
                            + address.getLon());
                    address.setHash(hash);
                }
                ((MainActivity) getActivity()).getAddressList().set(0, address);
                addressAdapter.updateList(addressList, true);
            }

            if (addressList.size() > 1 && addressList.size() < 5) {
                ((MainFragment) getParentFragment()).showFab();
            } else {
                ((MainFragment) getParentFragment()).hideFab();
            }
        }

        ((MainFragment) getParentFragment()).enableAddress = true;
    }

    public void addItemList(int position) {
        String[] coords = ((MainFragment) getParentFragment()).callMapFragmentForCoords();
        Intent autoCompleteIntent = new Intent(getActivity(), AutoCompleteActivity.class);
        autoCompleteIntent.putExtra(MainActivity.POINT_NUM_KEY, position);
        autoCompleteIntent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, coords);
        startActivity(autoCompleteIntent);
    }


    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() == null) {
            ((MainFragment) getParentFragment()).enableAddress = false;
            Address address = new Address();
            address.setLabel(getString(R.string.fragments_MainHeaderFragment_unknown_city));
            ((MainActivity) getActivity()).getAddressList().set(0, address);
            addressAdapter.updateList(((MainActivity) getActivity()).getAddressList(), false);
            ((MainFragment) getParentFragment()).hideFab();
            EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.DOWN));
        } else {
            EventBus.getDefault().post(new MotionMapEvent(MotionMapEvent.UP));
            ((MainFragment) getParentFragment()).enableAddress = true;
        }
    }

    @Subscribe
    public void onMessage(MapChangedEvent mapChangedEvent) {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();

        if (mAddressesList.size() == 1 && !addressList.get(0).getUseType().equals(Address.TYPE_AUTOCOMPLETE)) {
            Address address = mAddressesList.get(0);
            switch (mapChangedEvent.getActionType()) {
                case MapChangedEvent.TYPE_START:
                    address.setLabel(getString(R.string.fragments_MainHeaderFragment_locating));
                    address.setCity("");
                    address.setUseType(Address.TYPE_FAKE);
                    break;
                case MapChangedEvent.TYPE_LOADING:
                    address.setLabel(getString(R.string.fragments_MainHeaderFragment_loading));
                    address.setCity("");
                    address.setUseType(Address.TYPE_FAKE);
                    break;
            }
            ((MainActivity) getActivity()).getAddressList().set(0, address);
            addressAdapter.updateList(((MainActivity) getActivity()).getAddressList(), true);
        }
    }

    @Subscribe
    public void onMessage(ItemAddressActionEvent deleteItemListEvent) {
        switch (deleteItemListEvent.getAction()) {
            case ItemAddressActionEvent.ACTION_INFO:
                Intent intentAddress = new Intent(getActivity(), DetailAddressActivity.class);
                Address firstAddress = ((MainActivity) getActivity()).getAddressList().get(0);
                if (firstAddress.isGps()) {
                    intentAddress.putExtra("street", firstAddress.getStreet());
                }
                intentAddress.putExtra("porch", firstAddress.getPorch());
                intentAddress.putExtra("apt", firstAddress.getApt());
                intentAddress.putExtra("comment", ((MainActivity) getActivity()).getOrder().getComment());
                getActivity().startActivityForResult(intentAddress, MainActivity.REQUEST_CODE_DETAIL);
                break;
            case ItemAddressActionEvent.ACTION_DELETE:
                int position = deleteItemListEvent.getPosition();
                List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
                if (addressList.size() == 2 && position == AddressList.SECOND_POINT) {
                    // set second point to empty state
                    ((MainActivity) getActivity()).getAddressList()
                            .remove(position);
                    ((MainActivity) getActivity()).getAddressList()
                            .add(new Address());
                    ((MainActivity) getActivity()).getAddressList()
                            .get(AddressList.SECOND_POINT).setEmpty(true);
                    ((MainFragment) getParentFragment()).hideFab();
                } else {
                    // remove point
                    ((MainActivity) getActivity()).getAddressList()
                            .remove(position);
                    ((MainFragment) getParentFragment()).showFab();
                }
                addressAdapter = new AddressAdapter(getActivity(), addressList);
                listView.setAdapter(addressAdapter);
                EventBus.getDefault().post(new UpdatePointsEvent());
                break;
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessage(CoordsForReverseEvent event) {
        latForReverse = event.getLat();
        lonForReverse = event.getLon();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}