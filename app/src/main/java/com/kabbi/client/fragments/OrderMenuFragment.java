package com.kabbi.client.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.activities.EditOrderActivity;
import com.kabbi.client.activities.MainActivity;
import com.kabbi.client.app.AppParams;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.RejectOrderListener;
import com.kabbi.client.network.requests.RejectOrderRequest;

import java.util.Date;

import static com.kabbi.client.app.AppParams.IS_LET_REJECT_AFTER_ASSIGNED;
import static com.kabbi.client.app.AppParams.USE_CALLS_OFFICE;

/**
 * Created by gootax on 22.09.16.
 */
public class OrderMenuFragment extends BaseSpiceFragment {

    private LinearLayout mMenuCall;
    private LinearLayout mMenuReject;
    private LinearLayout mMenuEdit;
    private ImageView mIVMenuCall;
    private ImageView mIVMenuReject;
    private MainFragment mMainFragment;

    public static final String EXTRA_EDIT_ORDER_ID = "EXTRA_EDIT_ORDER_ID";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainFragment = ((MainFragment) getParentFragment());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_menu_fragment, container, false);

        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_bg, typedValue, true);
        TypedValue typedValueText = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);
        mMenuCall = (LinearLayout) view.findViewById(R.id.ll_menu_call);

        if (!AppParams.USE_CALLS) {
            mMenuCall.setVisibility(View.GONE);
        }

        mMenuReject = (LinearLayout) view.findViewById(R.id.ll_menu_reject);
        mMenuEdit = (LinearLayout) view.findViewById(R.id.ll_menu_edit);

        TextView mTVMenuCall = (TextView) view.findViewById(R.id.tv_menu_call);
        TextView mTVMenuReject = (TextView) view.findViewById(R.id.tv_menu_reject);
        mIVMenuCall = (ImageView) view.findViewById(R.id.iv_menu_call);
        mIVMenuReject = (ImageView) view.findViewById(R.id.iv_menu_reject);
        LinearLayout mLlMenuOrder = (LinearLayout) view.findViewById(R.id.ll_items_order);

        mLlMenuOrder.setBackgroundColor(typedValue.data);
        mMenuCall.setBackgroundColor(typedValue.data);
        mMenuReject.setBackgroundColor(typedValue.data);
        mTVMenuReject.setTextColor(typedValueText.data);
        mTVMenuCall.setTextColor(typedValueText.data);

        if ((mMainFragment.city != null && USE_CALLS_OFFICE && mMainFragment.city.getPhone().length() > 0) || mMainFragment.driverPhone.length() > 1) {
            mMenuCall.setEnabled(true);
            mIVMenuCall.setImageResource(R.drawable.phone);
        } else {
            mMenuCall.setEnabled(false);
            mIVMenuCall.setImageResource(R.drawable.phone_disabled);
        }

        if ((mMainFragment.statusOrder.equals("new") || mMainFragment.statusOrder.equals("car_assigned")) || IS_LET_REJECT_AFTER_ASSIGNED) {
            mMenuReject.setEnabled(true);
            mIVMenuReject.setImageResource(R.drawable.cencel);
        } else {
            mMenuReject.setEnabled(false);
            mIVMenuReject.setImageResource(R.drawable.cencel_disabled);
        }

        if (TextUtils.isEmpty(mMainFragment.statusOrder)) {
            mMenuEdit.setClickable(false);
        }

        mMenuCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((mMainFragment.city != null && mMainFragment.city.getPhone().length() > 0)
                        || mMainFragment.driverPhone.length() > 1) {
                    DialogFragment dialogFragment = new CallDialogFragment();
                    dialogFragment.show(getChildFragmentManager(), "dialogFragmentCall");
                }
            }
        });

        mMenuReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded()) tryRejectOrder();
            }
        });

        if (AppParams.USE_EDIT_ORDER) {
            mMenuEdit.setVisibility(View.VISIBLE);

            mMenuEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), EditOrderActivity.class);
                    intent.putExtra(EXTRA_EDIT_ORDER_ID, ((MainActivity) mMainFragment.getActivity()).getOrder().getOrderId());
                    startActivity(intent);
                }
            });
        } else {
            mMenuEdit.setVisibility(View.GONE);
        }

        return view;
    }


    public void tryRejectOrder() {
        if ((mMainFragment.statusOrder.equals("new") || mMainFragment.statusOrder.equals("car_assigned")) || IS_LET_REJECT_AFTER_ASSIGNED) {
            try {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyCustomAlertDialogTheme);
                alertDialog.setTitle(getString(R.string.fragments_MainFragment_delete_order1));
                alertDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order2));
                alertDialog.setPositiveButton(getString(R.string.fragments_MainFragment_yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mMainFragment.rejectOrder();
                                Profile profile = Profile.getProfile();
                                getSpiceManager().execute(new RejectOrderRequest(profile.getAppId(), profile.getApiKey(),
                                                getActivity(),
                                                String.valueOf((new Date()).getTime()),
                                                ((MainActivity) getActivity()).getOrder(),
                                                mMainFragment.profile.getTenantId()),
                                        new RejectOrderListener());
                            }
                        });
                alertDialog.setNegativeButton(getString(R.string.fragments_MainFragment_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public String getDriverPhone() {
        return mMainFragment.driverPhone;
    }


    public void updateMenuState() {
        mMenuEdit.setEnabled(true);
        if ((mMainFragment.city != null && AppParams.USE_CALLS_OFFICE && mMainFragment.city.getPhone().length() > 0) || mMainFragment.driverPhone.length() > 1) {
            mMenuCall.setEnabled(true);
            mIVMenuCall.setImageResource(R.drawable.phone);
        } else {
            mMenuCall.setEnabled(false);
            mIVMenuCall.setImageResource(R.drawable.phone_disabled);
        }

        if ((mMainFragment.statusOrder.equals("new") || mMainFragment.statusOrder.equals("car_assigned")) || IS_LET_REJECT_AFTER_ASSIGNED) {
            mMenuReject.setEnabled(true);
            mIVMenuReject.setImageResource(R.drawable.cencel);
        } else {
            mMenuReject.setEnabled(false);
            mIVMenuReject.setImageResource(R.drawable.cencel_disabled);
        }
    }

}
