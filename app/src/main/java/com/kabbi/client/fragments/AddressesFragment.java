package com.kabbi.client.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.events.ui.AddressSelectedEvent;
import com.kabbi.client.models.Address;
import com.kabbi.client.views.adapters.AutoCompleteAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


/**
 * Created by gootax on 11.09.17.
 */

public class AddressesFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String TAB_POSITION = "TAB_POSITION";
    public static final String TAB_ADDRESSES = "TAB_ADDRESSES";

    private ListView mLvAddresses;
    private ArrayList<Address> mAddresses;
    private AutoCompleteAdapter mAddressAdapter;
    private TextView mTvAddresesEmpty;

    public static AddressesFragment newInstance(int position, ArrayList<Address> addresses) {
        Bundle bundle = new Bundle();
        bundle.putInt(TAB_POSITION, position);
        bundle.putParcelableArrayList(TAB_ADDRESSES,  addresses);

        AddressesFragment addressesFragment = new AddressesFragment();
        addressesFragment.setArguments(bundle);

        return addressesFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAddresses = getArguments().getParcelableArrayList(TAB_ADDRESSES);
        Log.d("Logos", "onAttach: " + mAddresses.size());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_adresses, container, false);
    }


    @Override
    public void onViewCreated(View content, @Nullable Bundle savedInstanceState) {
        Log.d("Logos", "onViewCreated");
        initView(content);
        prepareView();
    }


    private void initView(View content) {
        mLvAddresses = (ListView) content.findViewById(R.id.lv_addresses);
        mTvAddresesEmpty = (TextView) content.findViewById(R.id.tv_addresses_empty);
    }

    private void prepareView() {
        mLvAddresses.setOnItemClickListener(this);
        mAddressAdapter = new AutoCompleteAdapter(getContext(), mAddresses);
        mLvAddresses.setAdapter(mAddressAdapter);

        if (mAddresses.size() > 0) {
            mTvAddresesEmpty.setVisibility(View.GONE);
            mLvAddresses.setVisibility(View.VISIBLE);
        } else  {
            mTvAddresesEmpty.setVisibility(View.VISIBLE);
            mLvAddresses.setVisibility(View.GONE);
        }
    }

    public void updateView(ArrayList<Address> addresses) {
        mAddresses = addresses;
        mAddressAdapter.updateData(mAddresses);

        if (mAddresses.size() > 0) {
            mTvAddresesEmpty.setVisibility(View.GONE);
            mLvAddresses.setVisibility(View.VISIBLE);
        } else  {
            mTvAddresesEmpty.setVisibility(View.VISIBLE);
            mLvAddresses.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EventBus.getDefault().post(new AddressSelectedEvent(mAddresses.get(position)));
    }
}
