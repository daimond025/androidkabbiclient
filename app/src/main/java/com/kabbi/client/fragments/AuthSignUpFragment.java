package com.kabbi.client.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.kabbi.client.R;
import com.kabbi.client.activities.AuthActivity;
import com.kabbi.client.activities.CountryActivity;
import com.kabbi.client.activities.OfferActivity;
import com.kabbi.client.events.api.client.SignUpEvent;
import com.kabbi.client.models.Profile;
import com.kabbi.client.network.listeners.SendPassRequestListener;
import com.kabbi.client.network.requests.PostSendPassRequest;
import com.kabbi.client.utils.InputMask;
import com.kabbi.client.views.ToastWrapper;

import java.util.Date;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.kabbi.client.app.AppParams.WITH_COUNTRY;

public class AuthSignUpFragment extends BaseSpiceFragment implements TextView.OnEditorActionListener {

    public static final int FRAG_SIGN_UP_CODE = 889;

    private Profile profile;
    private String phoneFromDb;
    private String phoneMask;
    private String country;

    private InputMask inputMask;
    private boolean mFormatting;

    private TextInputEditText phoneEdt;
    private Button countryBtn;
    private Button confirmBtn;

    public static AuthSignUpFragment newInstance() {
        return new AuthSignUpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profile = Profile.getProfile();
        setProfileValues();
    }

    private void setProfileValues() {
        phoneFromDb = profile.getPhone();
        String phoneMaskFromDb = profile.getPhoneMask();
        if (!phoneMaskFromDb.isEmpty()) {
            phoneMask = phoneMaskFromDb;
        } else {
            phoneMask = getString(R.string.activities_AuthActivity_edit_phone_mask);
        }
        String countryFromDb = profile.getCountry();
        if (!countryFromDb.isEmpty()) {
            country = countryFromDb;
        } else {
            country = getString(R.string.activities_AuthActivity_edit_country);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth_sign_up, container, false);
        countryBtn = (Button) view.findViewById(R.id.signup_country);
        countryBtn.setTransformationMethod(null);
        countryBtn.setText(country);
        countryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(
                        new Intent(getActivity(), CountryActivity.class), FRAG_SIGN_UP_CODE);
            }
        });
        if (!WITH_COUNTRY) countryBtn.setVisibility(View.GONE);
        phoneEdt = (TextInputEditText) view.findViewById(R.id.signup_phone);
        phoneEdt.setOnEditorActionListener(this);
        setEditPhoneValues();
        setEditPhoneListeners();
        confirmBtn = (Button) view.findViewById(R.id.signup_confirm);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPhone();
            }
        });

        TextView offer = (TextView) view.findViewById(R.id.tv_offer);
        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OfferActivity.class));
            }
        });

        return view;
    }

    private void setEditPhoneValues() {
        inputMask = new InputMask(phoneMask, phoneFromDb);
        mFormatting = false;
        String newText = inputMask.getNewText();
        phoneEdt.setText(newText);
        phoneEdt.setSelection(inputMask.getSelection(newText));
    }

    private void setEditPhoneListeners() {
        phoneEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selection = inputMask.getSelection(phoneEdt.getText().toString().trim());
                if (selection != phoneMask.length()) {
                    phoneEdt.setSelection(selection);
                    mFormatting = false;
                }
                phoneEdt.setSelection(inputMask.getSelection(phoneEdt.getText().toString().trim()));
                mFormatting = false;
            }
        });
        phoneEdt.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            String phoneStr;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newPhone = s.toString().trim().replaceAll("\\D+", "");
                if (newPhone.startsWith("49")) {
                    String mask = inputMask.getGermanMask(newPhone);
                    inputMask = new InputMask(mask, newPhone);
                    phoneMask = mask;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                phoneStr = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    String result = inputMask.afterTextChangedSetText(phoneStr, s);
                    phoneEdt.setText(result);
                    phoneEdt.setSelection(inputMask.afterTextChangedSetSelection(result));
                } else {
                    mFormatting = false;
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessage(SignUpEvent event) {
        confirmBtn.setEnabled(true);
        if (event.getSuccess() == 1) {
            AuthActivity activity = (AuthActivity) getActivity();
            if (activity != null) {
                activity.updateSignInFragViews();
                activity.setFragment(AuthActivity.FRAGMENT_SIGN_IN);
            }
        } else if (event.getSuccess() == 0) {
            new ToastWrapper(getContext(),
                    R.string.activities_AuthActivity_signup_req_error).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FRAG_SIGN_UP_CODE
                && resultCode == Activity.RESULT_CANCELED) {
            updateViews();
        }
    }

    public void updateViews() {
        setProfileValues();
        countryBtn.setText(country);
        setEditPhoneValues();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            sendPhone();
            return true;
        }
        return false;
    }

    private void sendPhone() {
        boolean phoneIsValid = true;
        int countNum = phoneMask
                .replaceAll("_","0")
                .replaceAll("\\D", "")
                .length();
        if (phoneEdt.getText().toString().trim()
                .replaceAll("\\D+", "").length() != countNum) {
            phoneEdt.setError(getString(R.string.activities_AuthActivity_edit_phone_error));
            phoneIsValid = false;
        }
        if (phoneIsValid) {
            confirmBtn.setEnabled(false);
            profile.setPhoneMask(phoneMask);
            profile.setPhone(phoneEdt.getText().toString().trim().replaceAll("\\D+", ""));
            profile.save();
            getSpiceManager().execute(new PostSendPassRequest(profile.getAppId(), profile.getApiKey(),  getContext(),
                            phoneEdt.getText().toString().trim().replaceAll("\\D+", ""),
                            String.valueOf((new Date()).getTime()), profile.getTenantId()),
                    new SendPassRequestListener());
        }
    }

}
