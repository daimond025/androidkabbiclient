package com.kabbi.client.app;


/**
 * Created by gootax on 19.10.16.
 */

public class Constants {

    // AP - Application Preferences
    public static final String AP_SAVE_PAYMENTS = "save_balances";
    public static final String AP_SAVE_TARIFFS = "save_tariff";

    public static final int REQUEST_FAILURE_CODE = 432;

    public static final String PAYMENT_CASH = "CASH";
    public static final String PAYMENT_CARD = "CARD";
    public static final String PAYMENT_CORPORATION = "CORP_BALANCE";
    public static final String PAYMENT_PERSONAL = "PERSONAL_ACCOUNT";

}