package com.kabbi.client.app;


import android.app.Application;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.common.android.HandlerUtil;
import com.facebook.stetho.inspector.domstorage.SharedPreferencesHelper;
import com.kabbi.client.models.Address;
import com.kabbi.client.utils.AnalyticsHelper;
import com.kabbi.client.utils.LocaleHelper;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

import static com.kabbi.client.app.AppParams.DEFAULT_LANG;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (AppParams.IS_DEBUG) Stetho.initializeWithDefaults(this);
        Fabric.with(this, new Crashlytics());
        ActiveAndroid.initialize(this);
        Fresco.initialize(this);
        AnalyticsHelper.init(this);
        clearCache();
    }


    private void clearCache() {
        File file = new File(getCacheDir(), "Rotate.jpg");
        if (file.exists())
            file.delete();
    }

}
