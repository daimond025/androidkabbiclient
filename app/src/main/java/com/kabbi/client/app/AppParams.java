package com.kabbi.client.app;

import com.kabbi.client.R;

public class AppParams {


    /** COMMON SETTINGS **/
    // If demo client can change Tenant ID and Color scheme
    public static final boolean IS_DEMO = false;
    // For Steho
    public static final boolean IS_DEBUG = false;
    // URL for requests
    // public static final String URL = "https://ca2.kabbi.com:8089"; // "http://ca1.artem.taxi.lcl" // "https://ca.uatgootax.ru"
    public static final String URL = "https://ca2.kabbi.com:8089";
    // URL for geo requests
    public static final String GEO_URL = "https://geo.kabbi.com/v1";
    // Unique key for tenant
    public static final String TENANT_ID = "2538"; // EXAMPLE: "68";
    // Unique key for service tenant use to signature
    public static final String API_KEY = "wporimv8y43v3#$V#3tl3kmgvlhermlc3DDFWEfwe"; // EXAMPLE: 13131313 | wporimv8y43v3#$V#3tl3kmgvlhermlc3DDFWEfwe
    // Unique key for tenant application
    public static final String APP_ID = "1630"; // EXAMPLE: 1630 | 1275
    // For server response
    public static final String CLIENT_VERSION = "1.21.0"; // EXAMPLE: 1.12.0
    //
    public static final String TYPE_CLIENT = "android";


    /** METRICS SETTINGS **/
    // Need or not using metrics by Yandex
    public static final boolean WITH_METRICS = true;
    // Yandex AppMetrics Key
    public static final String METRICS_KEY = "28dcd8b5-c639-4827-b361-5f63af327e78";


    /** LANGUAGE SETTINGS **/
    // Can user change country
    public static final boolean WITH_COUNTRY = true;
    // Languages for choose
    public static final String[] LANGUAGES = {"en", "de"};
    // Edit when adding a new language
    public static final String[] SUPPORT_LANG = {"ar", "en", "ru", "az", "fa", "tk", "uz", "de", "it", "me", "sr", "tg", "av", "ky", "hy"};
    // Default application language
    public static final String DEFAULT_LANG = "de";


    /** PUSH SETTINGS **/
    // Notification parameter
    public static final int NOTIFICATION_ID = 42;
    // Google cloud messaging info
    public static final String PUSH_SENDER_ID = "996050118777"; // EXAMPLE: 407168236755


    /** THEME SETTINGS **/
    public static final int DEFAULT_THEME = IS_DEMO ? R.style.AppTheme1 : R.style.AppThemeDef;
    public static final int[] THEMES = {R.style.AppTheme1, R.style.AppTheme2, R.style.AppTheme3,
            R.style.AppTheme4, R.style.AppTheme5, R.style.AppTheme6};


    /** CALENDAR SETTINGS **/
    /* Default calendar for order time
    0 - Gregorian | 1 - Persian */
    public static final boolean USE_CALENDAR_EXTENSION = false;
    public static final int DEFAULT_CALENDAR = 0;
    public static final int[] CALENDARS = {0, 1};


    /** MAPS SETTINGS **/
    // 0 - OSM | 1 - Google | 2 - Google Hybrid | 3 - Google Hybrid
    public static final int DEFAULT_MAP = 1;
    // 1 - Google | 2 - Google Hybrid | 3 - Yandex | 4 - 2GIS
    public static final int[] MAPS = {1, 2};
    public static final int[] ALL_MAPS = {1, 2, 3, 4};


    /** SEARCH SETTINGS **/
    public static final boolean WITH_SHOW_CAR = true; // in B, C, D, E points
    // Search on the whole map
    public static final boolean USE_SEARCH = false; // in B, C, D, E points
    // Radius of the find address inside autocomplete
    public static final String AUTOCOMPLETE_RADIUS = "40";
    // Radius of the zone with cars
    public static final String CAR_RADIUS = "3000";
    public static final String RADIUS_PUBLIC_TANSPORT = "3000";
    // Radius of the find address through reverse
    public static final String REVERSE_RADIUS = "0.1";
    // When there is no polygon
    public static final int ORDERS_RADIUS = 40;


    /** PHOTO SETTINGS **/
    // Use driver or car photo
    public static final boolean USE_PHOTO = true;
    // What photo is using
    public static final String CAR_PHOTO = "car_photo";
    public static final String DRIVER_PHOTO =  "driver_photo";
    public static final String PHOTO_TYPE = DRIVER_PHOTO;


    /** CALLS SETTINGS **/
    // Can user call the driver or dispatcher
    public static final boolean USE_CALLS = true;
    // Can user call the dispatcher
    public static final boolean USE_CALLS_OFFICE = true;
    // Can user call the dispatcher
    public static final boolean USE_CALLS_DRIVER = true;


    /** OTHER SETTINGS **/
    // Use full splash image
    public static final boolean FILL_SPLASH = false;
    // Can user create order with only 1 address
    public static final boolean ORDER_WITH_ONE_ADDRESS = true;
    // Only one address in list
    public static final boolean WITH_MAX_ONE_ADDRESS = false;
    // Need using for if application using referral system
    public static final boolean USE_REFERRAL = true;
    // Review not sending if rating lower 5 and not descriptions
    public static final boolean USE_REVIEW_BLOCK = true;
    // Tax on Order(if > 0 then it showing)
    public static final double TAX = 0;
    // Order states
    public static final String STATUS_ID_COMPLETE_PAID = "37";
    public static final String STATUS_ID_COMPLETE_NOPAID = "38";
    // Tariff Update Time
    public static final long TIME_UPDATE_TARIFFS = 21600000;
    // Allow edit order
    public static final boolean USE_EDIT_ORDER = true;
    public static final boolean SHOWN_CLIENT_ID = false;
    public static final boolean USE_REVIEW_DETAIL = true;
    public static final boolean USE_REVIEW_FOR_REJECTED = false;
    public static final boolean USE_PUBLIC_TRANSPORT = false;
    public static final boolean USE_PROFILE_DATA = false;

    public static final String WEB_APPLICATION_URL = "None";
    public static final String WEB_APPLICATION_TITLE = "None";
    public static final boolean USE_WEB_APPLICATION = false;
    public static final boolean USE_MULTI_CALLCOST = true;
    public static final boolean IS_LET_REJECT_AFTER_ASSIGNED = true;


    /** WISHES PANEL SETTINGS **/
    public static final boolean USE_ORDER_TIME = true;
    public static final boolean USE_WISHES = true;
    public static final boolean USE_ADDRESS_DETAIL = true;
    public static final boolean USE_WISH_PANEL = USE_ORDER_TIME || USE_WISHES || USE_ADDRESS_DETAIL;


    /** ADDRESS DETAIL SETTINGS **/
    // Can user define flat
    public static final boolean WITH_FLAT = false;
    // Can user define flat
    public static final boolean WITH_STREET = true;
    // Can user define flat
    public static final boolean WITH_PORCH = false;
    // Can user define flat
    public static final boolean WITH_COMMENT = true;


    /** PAYMENTS SETTINGS **/
    // Can user pay by bank card
    public static final boolean PAYMENT_WITH_CARDS = true;
    // Can user pay by bank card
    public static final boolean PAYMENT_WITH_PERSONAL = false;
    // Can show corporation balance
    public static final boolean SHOW_COMPANY_BALANCE = true;
    // Can user pay by corporation
    public static final boolean PAYMENT_WITH_COMPANY = true;
    // Can user pay by cash
    public static final boolean PAYMENT_WITH_CASH = true;
    // Can user pay by bonus
    public static final boolean PAYMENT_WITH_BONUS = true;

}
