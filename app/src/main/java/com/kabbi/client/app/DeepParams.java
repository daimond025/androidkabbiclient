package com.kabbi.client.app;

import com.kabbi.client.BuildConfig;

public class DeepParams {

    public static final String DEEP_SCHEME = BuildConfig.APPLICATION_ID;

    public static final String EXTRA_DEEP_ORDER = "EXTRA_DEEP_ORDER";

    public static final String DEEP_LAT_FROM = "lat";
    public static final String DEEP_LON_FROM = "lon";
    public static final String DEEP_CITY_FROM = "city";
    public static final String DEEP_STREET_FROM = "street";
    public static final String DEEP_HOUSE_FROM = "house";

    public static final String DEEP_LAT_TO = "dlat";
    public static final String DEEP_LON_TO = "dlon";
    public static final String DEEP_CITY_TO = "dcity";
    public static final String DEEP_STREET_TO = "dstreet";
    public static final String DEEP_HOUSE_TO = "dhouse";
    public static final String DEEP_TARIFF_ID = "tariff_id";

}
