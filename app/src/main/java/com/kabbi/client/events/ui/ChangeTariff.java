package com.kabbi.client.events.ui;


public class ChangeTariff {

    private String tariffId;

    public ChangeTariff(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffId() {
        return tariffId;
    }
}
