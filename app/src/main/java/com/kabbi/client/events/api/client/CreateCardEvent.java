package com.kabbi.client.events.api.client;


public class CreateCardEvent {

    private String webPageString;
    private String orderId;
    private boolean hasError;

    public CreateCardEvent(String webPageString, String orderId, boolean hasError) {
        this.webPageString = webPageString;
        this.orderId = orderId;
        this.hasError = hasError;
    }

    public String getWebPageString() {
        return webPageString;
    }

    public String getOrderId() {
        return orderId;
    }

    public boolean hasError() {
        return hasError;
    }

}
