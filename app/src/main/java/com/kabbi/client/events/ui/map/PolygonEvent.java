package com.kabbi.client.events.ui.map;

/**
 * Created by gootax on 18.01.17.
 */

public class PolygonEvent {
    private String lat;
    private String lon;


    public PolygonEvent(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
