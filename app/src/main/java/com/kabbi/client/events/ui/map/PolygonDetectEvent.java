package com.kabbi.client.events.ui.map;


import com.kabbi.client.models.City;

public class PolygonDetectEvent {

    private City city;

    public PolygonDetectEvent(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }
}
