package com.kabbi.client.events.api.client;

public class CallCostEvent {

    private String cost;
    private String dis;
    private String time;
    private boolean isFix;

    public CallCostEvent(String cost, String dis, String time, boolean isFix) {
        this.cost = cost;
        this.dis = dis;
        this.time = time;
        this.isFix = isFix;
    }

    public boolean isFix() {
        return isFix;
    }

    public String getCost() {
        return cost;
    }

    public String getDis() {
        return dis;
    }

    public String getTime() {
        return time;
    }
}
