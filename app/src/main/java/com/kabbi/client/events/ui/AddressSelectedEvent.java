package com.kabbi.client.events.ui;

import com.kabbi.client.models.Address;

/**
 * Created by gootax on 11.09.17.
 */

public class AddressSelectedEvent  {

    private Address mAddress;

    public AddressSelectedEvent(Address address) {
        mAddress = address;
    }

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address address) {
        mAddress = address;
    }
}
