package com.kabbi.client.events.api.geo;

import com.kabbi.client.models.Address;

import java.util.ArrayList;

public class GeoEvent {

    private ArrayList<Address> addresses;

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

}
