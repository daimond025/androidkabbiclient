package com.kabbi.client.events.api.client;


import android.support.annotation.StringRes;

public class SaveCitiesEvent {

    private boolean saveCities;
    private int infoText;

    public SaveCitiesEvent(boolean saveCities, @StringRes int infoText) {
        this.saveCities = saveCities;
        this.infoText = infoText;
    }

    public boolean isSaveCities() {
        return saveCities;
    }

    public int getInfoText() {
        return infoText;
    }

}
