package com.kabbi.client.events.api.client;


import java.util.ArrayList;

public class ClientCardsEvent {

    private ArrayList<String> clientCards;

    public ClientCardsEvent(ArrayList<String> clientCards) {
        this.clientCards = clientCards;
    }

    public ArrayList<String> getClientCards() {
        return clientCards;
    }

}
