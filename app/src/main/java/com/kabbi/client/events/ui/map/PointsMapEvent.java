package com.kabbi.client.events.ui.map;

import com.kabbi.client.models.Address;

import java.util.List;

public class PointsMapEvent {

    private List<Address> addressList;
    private boolean activeOrder;
    private String statusOrder;
    private String time;

    public PointsMapEvent(List<Address> addressList, boolean activeOrder, String statusOrder, String time) {
        this.addressList = addressList;
        this.activeOrder = activeOrder;
        this.statusOrder = statusOrder;
        this.time = time;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public boolean isActiveOrder() {
        return activeOrder;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public String getTime() {
        return time;
    }
}
