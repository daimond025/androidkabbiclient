package com.kabbi.client.events.api.client;

/**
 * Created by gootax on 17.10.16.
 */

public class RejectOrderEvent {

    private String type;

    public RejectOrderEvent(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
