package com.kabbi.client.events.api.client;


public class CreateOrderEvent {

    private String orderId;
    private String orderNumber;
    private String type;
    private int requestCode;

    public CreateOrderEvent(int requestCode) {
        this.requestCode = requestCode;
    }

    public CreateOrderEvent(String orderId, String orderNumber, int requestCode, String type) {
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.requestCode = requestCode;
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getType() {
        return type;
    }

    public int getRequestCode() {
        return requestCode;
    }
}
