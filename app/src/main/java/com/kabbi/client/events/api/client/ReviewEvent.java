package com.kabbi.client.events.api.client;


public class ReviewEvent {

    private String status;

    public ReviewEvent(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
