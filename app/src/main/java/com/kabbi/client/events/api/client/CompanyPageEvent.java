package com.kabbi.client.events.api.client;


public class CompanyPageEvent {

    private String webPageString;

    public CompanyPageEvent(String webPageString) {
        this.webPageString = webPageString;
    }

    public String getWebPageString() {
        return webPageString;
    }

}
