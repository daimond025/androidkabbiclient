package com.kabbi.client.events.ui.map;

import com.kabbi.client.models.Car;

public class CarBusyEvent {

    private Car car;

    public CarBusyEvent(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }
}
