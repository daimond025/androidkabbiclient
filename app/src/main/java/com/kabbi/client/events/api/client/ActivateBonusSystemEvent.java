package com.kabbi.client.events.api.client;

/**
 * Created by gootax on 08.09.16.
 */
public class ActivateBonusSystemEvent {

    public int activateId;

    public ActivateBonusSystemEvent(int activateId) {
        this.activateId = activateId;
    }

    public int getActivateId() {
        return activateId;
    }
}
