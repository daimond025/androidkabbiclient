package com.kabbi.client.events.ui.map;

/**
 * Created by gootax on 21.11.16.
 */

public class MotionMapEvent {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int CHANGE = 2;

    private int type;

    public MotionMapEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
