package com.kabbi.client.events.ui;


public class ItemAddressActionEvent {

    public static final int ACTION_DELETE = 0;
    public static final int ACTION_INFO = 1;

    private int position;
    private int action;

    public ItemAddressActionEvent(int action, int position) {
        this.position = position;
        this.action = action;
    }

    public int getAction() {
        return action;
    }

    public int getPosition() {
        return position;
    }

}
