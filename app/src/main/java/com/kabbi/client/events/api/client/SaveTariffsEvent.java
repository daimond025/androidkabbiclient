package com.kabbi.client.events.api.client;


import com.kabbi.client.models.Option;
import com.kabbi.client.models.Tariff;

import java.util.List;

public class SaveTariffsEvent {

    private List<Tariff> tariffList;

    public SaveTariffsEvent(List<Tariff> tariffList) {
        this.tariffList = tariffList;

    }

    public List<Tariff> getTariffList() {
        return tariffList;
    }

}
