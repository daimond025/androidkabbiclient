package com.kabbi.client.events.api.client;


public class DeleteCardEvent {

    private String message;
    private String paymentID;

    public DeleteCardEvent(String message) {
        this.message = message;
    }

    public DeleteCardEvent(String message, String paymentID) {
        this.message = message;
        this.paymentID = paymentID;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}