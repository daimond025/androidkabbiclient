package com.kabbi.client.events.api.client;

/**
 * Created by andrew on 24/10/2017.
 */

public class EditOrderEvent {

    private String updateId;

    public EditOrderEvent(String updateId) {
        this.updateId = updateId;
    }

    public String getUpdateId() {
        return updateId;
    }

}
