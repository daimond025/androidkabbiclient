package com.kabbi.client.events.api.client;

import android.util.Log;

import com.kabbi.client.models.Order;

import java.util.ArrayList;

/**
 * Created by gootax on 07.12.16.
 */

public class OrdersInfoEvent {

    private ArrayList<Order> list;

    public OrdersInfoEvent(ArrayList<Order> list) {
        this.list = list;
    }

    public ArrayList<Order> getList() {
        return list;
    }

    public void setList(ArrayList<Order> list) {
        this.list = list;
    }
}
