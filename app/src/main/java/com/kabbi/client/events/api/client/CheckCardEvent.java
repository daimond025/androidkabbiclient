package com.kabbi.client.events.api.client;

public class CheckCardEvent {

    private int resultCode;

    public CheckCardEvent(int resultCode) {
        this.resultCode = resultCode;
    }

    public int getResultCode() {
        return resultCode;
    }

}
