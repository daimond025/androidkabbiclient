package com.kabbi.client.events.ui.map;

/**
 * Created by gootax on 11.09.17.
 */

public class MapChangedEvent {

    public static final int TYPE_START = 1;
    public static final int TYPE_LOADING = 2;


    private int actionType = 0;

    public MapChangedEvent(int actionType) {
        this.actionType = actionType;
    }

    public int getActionType() {
        return actionType;
    }
}
