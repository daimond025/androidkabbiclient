package com.kabbi.client.events.api.client;

/**
 * Created by gootax on 14.04.17.
 */

public class ActivateReferralCodeEvent {

    private int isActivated;

    public ActivateReferralCodeEvent(int isActivated) {
        this.isActivated = isActivated;
    }

    public boolean isActivated() {
        return isActivated > 0;
    }
}
