package com.kabbi.client.events.api.client;

import android.content.Intent;

/**
 * Created by gootax on 16.08.16.
 */
public class UpdateStateEvent {

    private Intent intent;

    public UpdateStateEvent(Intent intent) {
        this.intent = intent;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
