package com.kabbi.client.events.api.client;

/**
 * Created by gootax on 14.04.17.
 */

public class ActivateReferralEvent {

    private int result;
    private String code;

    public ActivateReferralEvent(int result, String code) {
        this.result = result;
        this.code = code;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
