package com.kabbi.client.events.api.client;

import com.kabbi.client.models.Profile;

public class ProfileEvent {

    private Profile profile;
    private int success;
    private int referral_success;

    public ProfileEvent(Profile profile, int success) {
        this.profile = profile;
        this.success = success;
    }

    public ProfileEvent(Profile profile, int success, int referral_success) {
        this.profile = profile;
        this.success = success;
        this.referral_success = referral_success;
    }

    public int getReferral_success() {
        return referral_success;
    }

    public void setReferral_success(int referral_success) {
        this.referral_success = referral_success;
    }

    public Profile getProfile() {
        return profile;
    }

    public int getSuccess() {
        return success;
    }

}
