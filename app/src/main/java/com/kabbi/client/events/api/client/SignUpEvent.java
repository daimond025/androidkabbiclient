package com.kabbi.client.events.api.client;

public class SignUpEvent {

    private int success;

    public SignUpEvent(int success) {
        this.success = success;
    }

    public int getSuccess() {
        return success;
    }

}