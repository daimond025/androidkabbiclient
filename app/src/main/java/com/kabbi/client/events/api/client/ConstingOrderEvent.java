package com.kabbi.client.events.api.client;

import com.kabbi.client.models.Cost;

import java.util.List;

public class ConstingOrderEvent {

    private List<Cost> mCosts;
    private long cost;
    private long time;

    public ConstingOrderEvent(List<Cost> costs, long cost, long time) {
        mCosts = costs;
        this.cost = cost;
        this.time = time;
    }

    public long getCost() {
        return cost;
    }

    public long getTime() {
        return time;
    }

    public List<Cost> getCosts() {
        return mCosts;
    }

}
