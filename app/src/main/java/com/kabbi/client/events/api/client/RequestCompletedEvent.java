package com.kabbi.client.events.api.client;

/**
 * Created by koflox on 14.08.17.
 */

public class RequestCompletedEvent
{
    public static final int BALANCES = 1;
    public static final int TARIFF = 2;
    public static final int REFERRAL = 3;
    public static final int CLIENT_HISTORY = 4;

    private int requestCode;

    public RequestCompletedEvent(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }
}
