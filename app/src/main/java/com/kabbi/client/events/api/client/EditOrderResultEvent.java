package com.kabbi.client.events.api.client;

/**
 * Created by andrew on 24/10/2017.
 */

public class EditOrderResultEvent {

    private int updateResult;

    public EditOrderResultEvent(int updateResult) {
        this.updateResult = updateResult;
    }

    public int getUpdateResult() {
        return updateResult;
    }

}
