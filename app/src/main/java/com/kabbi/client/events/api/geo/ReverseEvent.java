package com.kabbi.client.events.api.geo;

import com.kabbi.client.models.Address;

public class ReverseEvent {

    private Address address;
    private boolean isGpsAddress;

    public ReverseEvent(Address address, boolean isGpsAddress) {
        this.address = address;
        this.isGpsAddress = isGpsAddress;
    }

    public Address getAddress() {
        return address;
    }

    public boolean isGpsAddress() {
        return isGpsAddress;
    }

}
